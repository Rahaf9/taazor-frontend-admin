export const users = [
  {
    "BasicInfo": {
      "name": "John Doe",
      "email": "john@example.com",
      "image": "/images/the light green 2/People search-amico.png",
      "nationality": "Syrian",
      "gender": "Male",
      "location": "Damascus Syria",
      "birth": "1/4/2001",
      "phone": "0934212063",
      "status":"Single"
    },
    "id": 1,
    "PersonalInfo": {
      "topic": "Software Engineering",
      "militaryStatus": "مؤجل",
      "driveLicense": "true",
      "experince": "5 year of experince",
      "education": "Undergratudted"
    },
    "JobRequirements": {
      "jobEnvironment": "online",
      "jobTime": "full time",
      "cityOfWork": "Damascus",
      "working": "true"
    }

  },
  {
    "BasicInfo": {
      "name": "Jane Smith",
      "email": "jane@example.com",
      "image": "/images/the light green 2/People search-amico.png",
      "nationality": "Syrian",
      "gender": "Male",
      "location": "Damascus Syria",
      "birth": "1/4/2001",
      "phone": "0934212063",
      "status":"Single"
    },
    "id": 2,
    "PersonalInfo": {
      "topic": "Software Engineering",
      "militaryStatus": "مؤجل",
      "driveLicense": "true",
      "experince": "5 year of experince",
      "education": "Undergratudted"
    },
    "JobRequirements": {
      "jobEnvironment": "online",
      "jobTime": "full time",
      "cityOfWork": "Damascus",
      "working": "true"
    }

  },
  {
    "BasicInfo": {
      "name": "Jane Smith",
      "email": "jane@example.com",
      "image": "/images/the light green 2/People search-amico.png",
      "nationality": "Syrian",
      "gender": "Male",
      "location": "Damascus Syria",
      "birth": "1/4/2001",
      "phone": "0934212063",
      "status":"Single"
    },
    "id": 3,
    "PersonalInfo": {
      "topic": "Software Engineering",
      "militaryStatus": "مؤجل",
      "driveLicense": "true",
      "experince": "5 year of experince",
      "education": "Undergratudted"
    },
    "JobRequirements": {
      "jobEnvironment": "online",
      "jobTime": "full time",
      "cityOfWork": "Damascus",
      "working": "true"
    }


  },
  {
    "BasicInfo": {
      "name": "Jane Smith",
      "email": "jane@example.com",
      "image": "/images/the light green 2/People search-amico.png",
      "nationality": "Syrian",
      "gender": "Male",
      "location": "Damascus Syria",
      "birth": "1/4/2001",
      "phone": "0934212063",
      "status":"Single"
    },
    "id": 4,
    "PersonalInfo": {
      "topic": "Software Engineering",
      "militaryStatus": "مؤجل",
      "driveLicense": "true",
      "experince": "5 year of experince",
      "education": "Undergratudted"
    },
    "JobRequirements": {
      "jobEnvironment": "online",
      "jobTime": "full time",
      "cityOfWork": "Damascus",
      "working": "true"
    }


  },
  {
    "BasicInfo": {
      "name": "Jane Smith",
      "email": "jane@example.com",
      "image": "/images/the light green 2/People search-amico.png",
      "nationality": "Syrian",
      "gender": "Male",
      "location": "Damascus Syria",
      "birth": "1/4/2001",
      "phone": "0934212063",
      "status":"Single"
    },
    "id": 5,
    "PersonalInfo": {
      "topic": "Software Engineering",
      "militaryStatus": "مؤجل",
      "driveLicense": "true",
      "experince": "5 year of experince",
      "education": "Undergratudted"
    },
    "JobRequirements": {
      "jobEnvironment": "online",
      "jobTime": "full time",
      "cityOfWork": "Damascus",
      "working": "true"
    }


  },
  {
    "BasicInfo": {
      "name": "Jane Smith",
      "email": "jane@example.com",
      "image": "/images/the light green 2/People search-amico.png",
      "nationality": "Syrian",
      "gender": "Male",
      "location": "Damascus Syria",
      "birth": "1/4/2001",
      "phone": "0934212063",
      "status":"Single"
    },
    "id": 6,
    "PersonalInfo": {
      "topic": "Software Engineering",
      "militaryStatus": "مؤجل",
      "driveLicense": "true",
      "experince": "5 year of experince",
      "education": "Undergratudted"
    },
    "JobRequirements": {
      "jobEnvironment": "online",
      "jobTime": "full time",
      "cityOfWork": "Damascus",
      "working": "true"
    }


  },

  // Add more user objects as needed
];    


export const jobDetails = [
  {
    "name": "Soft Tech",
    "Show": "true",
    "topic": "Information Technology",
    "jobTitle": "Software Engineering",
    "salary": "500$",
    "applicantsNum": "200 applicants",
    "educationLevel": "Undergraduated",
    "requires": "Notice i need employee has 29 years old and more",
    "isUserImage": "true",
    "languages": ["Arabic","English","French"],
    "driveLicense": "true",
    "militaryStatus": "مؤجل",
    "endDate": "6/7/2024",
    "experince": "2 years",
    "jobEnvironment": "Online",
    "jobTime": "Full Time",
    "gender": "Male",
    "location": "Damascus",
    "quaificationReq": "i need someone has 9 class",
    "aboutCompany": "this is a very motivated company and high skilled experince"
  },
];

export const courseDetails = [
  {
    "name": "Soft Tech",
    "topic": "Information Technology",
    "cost": "500$",
    "applicantsNum": "200 applicants",
    "requires": "Notice i need employee has 29 years old and more",
    "duration":"12 days",
    "startDate": "6/7/2024",
    "endDate": "6/7/2024",
    "days": "2 days",
    "jobEnvironment": "Online",
    "gender": "Male",
    "location": "Damascus",
    "aboutCompany": "this is a very motivated company and high skilled experince",
    "courseName": "Course 1",
  },
];

// const iconMap = {
//     "name": <UserOutlined />,
//     "jobTitle": <IdcardOutlined />,
//     "topic": <FormOutlined />,
//     "salary": <DollarOutlined />,
//     "applicantsNum": <PhoneOutlined />,
//     "educationLevel": <BookOutlined />,
//     "requires": <FileTextOutlined />,
//     "languages": <GlobalOutlined />,
//     "driveLicense": <CarOutlined />,
//     "militaryStatus": <SafetyCertificateOutlined />,
//     "endDate": <CalendarOutlined />,
//     "experince": <ProfileOutlined />,
//     "jobEnvironment": <EnvironmentOutlined />,
//     "jobTime": <ClockCircleOutlined />,
//     "gender": <ManOutlined />,
//     "location": <EnvironmentOutlined />,
//     "quaificationReq": <InfoCircleOutlined />,
//     "aboutCompany": <InfoCircleOutlined />,
// };

export const consultants = [
  {
    "id":"1",
   "image":"/images/the light green 2/People search-amico.png" ,
   "name":"Nasser Dahdal",
   "topic":"Software Engineering"
  },
  {
    "id":"1",
    "image":"/images/the light green 2/People search-amico.png" ,
    "name":"Nasser Dahdal",
    "topic":"Software Engineering"
   },
   {
    "id":"1",
    "image":"/images/the light green 2/People search-amico.png" ,
    "name":"Nasser Dahdal",
    "topic":"Software Engineering"
   },
   {
    "id":"1",
    "image":"/images/the light green 2/People search-amico.png" ,
    "name":"Nasser Dahdal",
    "topic":"Software Engineering"
   },
   {
    "id":"1",
    "image":"/images/the light green 2/People search-amico.png" ,
    "name":"Nasser Dahdal",
    "topic":"Software Engineering"
   },
   {
    "id":"1",
    "image":"/images/the light green 2/People search-amico.png" ,
    "name":"Nasser Dahdal",
    "topic":"Software Engineering"
   },
   {
   "id":"1",
    "image":"/images/the light green 2/People search-amico.png" ,
    "name":"Nasser Dahdal",
    "topic":"Software Engineering"
   },
   {
   "id":"1",
    "image":"/images/the light green 2/People search-amico.png" ,
    "name":"Nasser Dahdal",
    "topic":"Software Engineering"
   }
];


export const messages =  [
  {
  "id": 1,
  "created_at": "2024-04-21T10:33:43.000000Z",
  "updated_at": "2024-04-21T10:33:43.000000Z",
  "deleted_at": null,
  "advisor_id": 1,
  "company_id": 1,
  "status": "waiting",
  "message": "test conculution from company to advisor",
  "topic": "Software",
  "type": "company"
  },
{
  "id": 1,
  "created_at": "2024-04-21T10:33:43.000000Z",
  "updated_at": "2024-04-21T10:33:43.000000Z",
  "deleted_at": null,
  "advisor_id": 1,
  "company_id": 1,
  "status": "waiting",
  "message": "test conculution from advisor to company",
  "topic": "Software",
  "type": "advisor"
  },
  {
    "id": 1,
    "created_at": "2024-04-21T10:33:43.000000Z",
    "updated_at": "2024-04-21T10:33:43.000000Z",
    "deleted_at": null,
    "advisor_id": 1,
    "company_id": 1,
    "status": "waiting",
    "message": "test conculution from company to advisor",
    "topic": "Software",
    "type": "company"
    },
  {
    "id": 1,
    "created_at": "2024-04-21T10:33:43.000000Z",
    "updated_at": "2024-04-21T10:33:43.000000Z",
    "deleted_at": null,
    "advisor_id": 1,
    "company_id": 1,
    "status": "waiting",
    "message": "test conculution from advisor to company",
    "topic": "Software",
    "type": "advisor"
    },
    {
      "id": 1,
      "created_at": "2024-04-21T10:33:43.000000Z",
      "updated_at": "2024-04-21T10:33:43.000000Z",
      "deleted_at": null,
      "advisor_id": 1,
      "company_id": 1,
      "status": "waiting",
      "message": "test conculution from company to advisor",
      "topic": "Software",
      "type": "company"
      },
    {
      "id": 1,
      "created_at": "2024-04-21T10:33:43.000000Z",
      "updated_at": "2024-04-21T10:33:43.000000Z",
      "deleted_at": null,
      "advisor_id": 1,
      "company_id": 1,
      "status": "waiting",
      "message": "test conculution from advisor to company",
      "topic": "Software",
      "type": "advisor"
      },
      {
        "id": 1,
        "created_at": "2024-04-21T10:33:43.000000Z",
        "updated_at": "2024-04-21T10:33:43.000000Z",
        "deleted_at": null,
        "advisor_id": 1,
        "company_id": 1,
        "status": "waiting",
        "message": "test conculution from company to advisor",
        "topic": "Software",
        "type": "company"
        },
      {
        "id": 1,
        "created_at": "2024-04-21T10:33:43.000000Z",
        "updated_at": "2024-04-21T10:33:43.000000Z",
        "deleted_at": null,
        "advisor_id": 1,
        "company_id": 1,
        "status": "waiting",
        "message": "test conculution from advisor to company",
        "topic": "Software",
        "type": "advisor"
        }
  ]

  export const profileData = {
    "success": true,
    "data": {
        "id": 13,
        "created_at": "2024-05-11T17:36:01.000000Z",
        "updated_at": "2024-05-11T17:37:17.000000Z",
        "deleted_at": null,
        "status": "acceptable",
        "name": "Tech Solutions",
        "phone": "+963123456789",
        "topic": "softwareEngineering",
        "location_map": "{\"latitude\": \"33.5138\", \"longitude\": \"36.2765\"}",
        "location": "Damascus, Syria",
        "fax": null,
        "documents": ["Docs1","Docs2","Docs3","Docs4","Docs5",],
        "type": ["IT", "Software Development"],
        "logo": "/images/my needs for project/taazor 1.png",
        "otp_code": null,
        "email": "ffdssdfsdfdwedfs@gmail.com",
        "is_complete": 1,
        "email_verification": null,
        "is_verified": 1,
        "password": "$2y$12$JPgpWnVd0hqO00R04Ijoh.oBnN78HXnzFsyelSVu74C0zoSocbh3y",
        "email_verified_at": "2024-05-11 17:36:24",
        "about_us": "A leading provider of IT and software engineering services in Damascus, specializing in innovative solutions for businesses of all sizes."
    },
    "message": "company"
}

export const arabicTopics = [
  { value: 'administration/operations/management', label: 'إدارة / عمليات' },
  { value: 'data entry/archiving', label: 'إدخال بيانات / الأرشفة' },
  { value: 'strategy/consulting', label: 'تخطيط / مستشار' },
  { value: 'research and development/statistics/analyst', label: 'البحث والتطوير / إحصائيات / المحلل' },
  { value: 'it/software development', label: 'IT / تطوير البرمجيات' },
  { value: 'banking/insurance', label: 'الخدمات المصرفية / تأمين' },
  { value: 'house keeping/office boys/porters', label: 'التدبير المنزلي / ساعي المكتب / بواب' },
  { value: 'translation/writing/editorial', label: 'ترجمة / كتابة / تحرير' },
  { value: 'marketing/pr/advertising', label: 'تسويق / PR / دعاية' },
  { value: 'graphic design/animation/art', label: 'التصميم الجرافيكي / الرسوم المتحركة / الفن' },
  { value: 'education/teaching/training', label: 'تعليم / تدريس / تدريب' },
  { value: 'social media/journalism/publishing', label: 'وسائل التواصل الأجتماعي / الصحافة / نشر' },
  { value: 'quality', label: 'الجودة' },
  { value: 'safety/guard services', label: 'أمان / خدمات الحراسة' },
  { value: 'customer service/support', label: 'خدمة الزبائن / الدعم' },
  { value: 'manufacturing/production', label: 'تصنيع / إنتاج' },
  { value: 'sport/nutrition/physiotherapy', label: 'رياضة / تغذية / العلاج الطبيعي' },
  { value: 'farming and agriculture', label: 'الزراعة' },
  { value: 'drivers/delivery', label: 'سائق / توصيل طلبات' },
  { value: 'secretarial/receptionist', label: 'سكرتارية / موظف الإستقبال' },
  { value: 'tourism/travel/hotels', label: 'السياحة / السفر / الفنادق' },
  { value: 'pharmaceutical', label: 'الادوية' },
  { value: 'medical/healthcare/nursing', label: 'الطب / الرعاية الصحية / التمريض' },
  { value: 'dentists/prosthodontics', label: 'طب الأسنان / التركيبات' },
  { value: 'technician/workers', label: 'فني / عامل' },
  { value: 'legal/contracts', label: 'قانون / عقد' },
  { value: 'chemistry/laboratories', label: 'كيمياء / مختبرات' },
  { value: 'logistics/warehouse/supply chain', label: 'الخدمات اللوجستية / مستودع / الموردين' },
  { value: 'sales/retail/distribution', label: 'مبيعات / بيع بالتجزئة / توزيع' },
  { value: 'accounting/finance', label: 'محاسبة / تمويل' },
  { value: 'project/program management', label: 'مشروع / إدارة البرنامج' },
  { value: 'purchasing/procurement', label: 'شراء / تحصيل' },
  { value: 'restaurant/catering/cuisine', label: 'مطعم / تقديم الطعام / مطبخ' },
  { value: 'human resources', label: 'الموارد البشرية' },
  { value: 'fashion and beauty', label: 'الموضة والجمال' },
  { value: 'film and photography/sound/music', label: 'السينما والتصوير الفوتوغرافي / صوت / موسيقى' },
  { value: 'engineering - construction/civil/architecture', label: 'الهيئة الهندسية / مدني / هندسة معمارية' },
  { value: 'interior design/decoration', label: 'تصميم داخلي / زخرفة' },
  { value: 'engineering - other', label: 'هندسة - اخرى' },
  { value: 'engineering - telecom/technology', label: 'هندسة - اتصالات / اتصالات' },
  { value: 'engineering - mechanical/electrical/medical', label: 'الهندسة الميكانيكية / الكهرباء' },
  { value: 'engineering - oil & gas/energy', label: 'الهندسة - النفط والغاز / الطاقة' },
  { value: 'c-level executive/gm/director', label: 'C-Level تنفيذي / GM / مخرج' },
  { value: 'psychological support/community services', label: 'دعم نفسي / خدمات المجتمع' },
  { value: 'other', label: 'اخرى' }
];

export const englishTopics = [
  { value: 'administration/operations/management', label: 'Administration / Operations / Management' },
  { value: 'data entry/archiving', label: 'Data Entry / Archiving' },
  { value: 'strategy/consulting', label: 'Strategy / Consulting' },
  { value: 'research and development/statistics/analyst', label: 'Research and Development / Statistics / Analyst' },
  { value: 'it/software development', label: 'IT / Software Development' },
  { value: 'banking/insurance', label: 'Banking / Insurance' },
  { value: 'house keeping/office boys/porters', label: 'House Keeping / Office Boys / Porters' },
  { value: 'translation/writing/editorial', label: 'Translation / Writing / Editorial' },
  { value: 'marketing/pr/advertising', label: 'Marketing / PR / Advertising' },
  { value: 'graphic design/animation/art', label: 'Graphic Design / Animation / Art' },
  { value: 'education/teaching/training', label: 'Education / Teaching / Training' },
  { value: 'social media/journalism/publishing', label: 'Social Media / Journalism / Publishing' },
  { value: 'quality', label: 'Quality' },
  { value: 'safety/guard services', label: 'Safety / Guard Services' },
  { value: 'customer service/support', label: 'Customer Service / Support' },
  { value: 'manufacturing/production', label: 'Manufacturing / Production' },
  { value: 'sport/nutrition/physiotherapy', label: 'Sport / Nutrition / Physiotherapy' },
  { value: 'farming and agriculture', label: 'Farming and Agriculture' },
  { value: 'drivers/delivery', label: 'Drivers / Delivery' },
  { value: 'secretarial/receptionist', label: 'Secretarial / Receptionist' },
  { value: 'tourism/travel/hotels', label: 'Tourism / Travel / Hotels' },
  { value: 'pharmaceutical', label: 'Pharmaceutical' },
  { value: 'medical/healthcare/nursing', label: 'Medical / Healthcare / Nursing' },
  { value: 'dentists/prosthodontics', label: 'Dentists / Prosthodontics' },
  { value: 'technician/workers', label: 'Technician / Workers' },
  { value: 'legal/contracts', label: 'Legal / Contracts' },
  { value: 'chemistry/laboratories', label: 'Chemistry / Laboratories' },
  { value: 'logistics/warehouse/supply chain', label: 'Logistics / Warehouse / Supply Chain' },
  { value: 'sales/retail/distribution', label: 'Sales / Retail / Distribution' },
  { value: 'accounting/finance', label: 'Accounting / Finance' },
  { value: 'project/program management', label: 'Project / Program Management' },
  { value: 'purchasing/procurement', label: 'Purchasing / Procurement' },
  { value: 'restaurant/catering/cuisine', label: 'Restaurant / Catering / Cuisine' },
  { value: 'human resources', label: 'Human Resources' },
  { value: 'fashion and beauty', label: 'Fashion and Beauty' },
  { value: 'film and photography/sound/music', label: 'Film and Photography / Sound / Music' },
  { value: 'engineering - construction/civil/architecture', label: 'Engineering - Construction / Civil / Architecture' },
  { value: 'interior design/decoration', label: 'Interior Design / Decoration' },
  { value: 'engineering - other', label: 'Engineering - Other' },
  { value: 'engineering - telecom/technology', label: 'Engineering - Telecom / Technology' },
  { value: 'engineering - mechanical/electrical/medical', label: 'Engineering - Mechanical / Electrical / Medical' },
  { value: 'engineering - oil & gas/energy', label: 'Engineering - Oil & Gas / Energy' },
  { value: 'c-level executive/gm/director', label: 'C-Level Executive / GM / Director' },
  { value: 'psychological support/community services', label: 'Psychological Support / Community Services' },
  { value: 'other', label: 'Other' }
];

export const databaseNoti = [
        {
            "id": "5ef31ac3-7521-4737-ad33-d0fa49b80994",
            "type": "App\\Notifications\\ServiceNotification",
            "notifiable_type": "App\\Models\\Company",
            "notifiable_id": 2,
            "data": {
                "messages": {
                    "ar": {
                        "title": "مهتم جديد بالكورس!",
                        "description": "لديكم مستخدم جديد أظهر اهتمامًا بأحد الكورسات. يمكنكم مراجعة تفاصيل الاهتمام والتواصل مع المستخدم لمزيد من المعلومات"
                    },
                    "en": {
                        "title": "New Interest in Your Course!",
                        "description": "A new user has shown interest in one of your courses. You can review the interest details and contact the user for more information."
                    }
                },
                "url": "http://86.38.218.161:8080/api/company/applications/course/2"
            },
            "seen": 0,
            "read_at": null,
            "created_at": "2024-08-11T22:20:16.000000Z",
            "updated_at": "2024-08-11T22:20:16.000000Z"
        },
        {
            "id": "d57b4f34-49d9-4fb3-9152-3adb302b6b8e",
            "type": "App\\Notifications\\ServiceNotification",
            "notifiable_type": "App\\Models\\Company",
            "notifiable_id": 2,
            "data": {
                "messages": {
                    "ar": {
                        "title": "مهتم جديد بالكورس!",
                        "description": "لديكم مستخدم جديد أظهر اهتمامًا بأحد الكورسات. يمكنكم مراجعة تفاصيل الاهتمام والتواصل مع المستخدم لمزيد من المعلومات"
                    },
                    "en": {
                        "title": "New Interest in Your Course!",
                        "description": "A new user has shown interest in one of your courses. You can review the interest details and contact the user for more information."
                    }
                },
                "url": "http://86.38.218.161:8080/api/company/applications/course/2"
            },
            "seen": 0,
            "read_at": null,
            "created_at": "2024-08-11T21:45:49.000000Z",
            "updated_at": "2024-08-11T21:45:49.000000Z"
        },
        {
          "id": "d57b4f34-49d9-4fb3-9152-3adb302b6b8e",
          "type": "App\\Notifications\\ServiceNotification",
          "notifiable_type": "App\\Models\\Company",
          "notifiable_id": 2,
          "data": {
              "messages": {
                  "ar": {
                      "title": "مهتم جديد بالكورس!",
                      "description": "لديكم مستخدم جديد أظهر اهتمامًا بأحد الكورسات. يمكنكم مراجعة تفاصيل الاهتمام والتواصل مع المستخدم لمزيد من المعلومات"
                  },
                  "en": {
                      "title": "New Interest in Your Course!",
                      "description": "A new user has shown interest in one of your courses. You can review the interest details and contact the user for more information."
                  }
              },
              "url": "http://86.38.218.161:8080/api/company/applications/course/2"
          },
          "seen": 0,
          "read_at": null,
          "created_at": "2024-08-11T21:45:49.000000Z",
          "updated_at": "2024-08-11T21:45:49.000000Z"
      },{
        "id": "d57b4f34-49d9-4fb3-9152-3adb302b6b8e",
        "type": "App\\Notifications\\ServiceNotification",
        "notifiable_type": "App\\Models\\Company",
        "notifiable_id": 2,
        "data": {
            "messages": {
                "ar": {
                    "title": "مهتم جديد بالكورس!",
                    "description": "لديكم مستخدم جديد أظهر اهتمامًا بأحد الكورسات. يمكنكم مراجعة تفاصيل الاهتمام والتواصل مع المستخدم لمزيد من المعلومات"
                },
                "en": {
                    "title": "New Interest in Your Course!",
                    "description": "A new user has shown interest in one of your courses. You can review the interest details and contact the user for more information."
                }
            },
            "url": "http://86.38.218.161:8080/api/company/applications/course/2"
        },
        "seen": 0,
        "read_at": null,
        "created_at": "2024-08-11T21:45:49.000000Z",
        "updated_at": "2024-08-11T21:45:49.000000Z"
    }
    ]


// if ("serviceWorker" in navigator) {
//   const serviceWorker = await navigator.serviceWorker
//     .register("/firebase-messaging-sw.js");
//     serviceWorker.showNotification("hello world")
//     alert("service worker registered")
    
// }

