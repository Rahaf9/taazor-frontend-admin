import React, { useEffect } from 'react';
import SideBar from './General/Reusable Componenets/SideBar/SideBar';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { ROUTES } from './General/Routes/Route';
import Login from './compnents/Login/Login';
import UserDetails from './compnents/UserDetails/UserDetails';
import BlockedUserDetails from './compnents/BlockedUserDetails/BlockedUserDetails'
import CurrentCourseDetails from './compnents/CurrentCourseDetails/CurrentCourseDetails';
import CurrentJobDetails from './compnents/CurrentJobDetails/CurrentJobDetails';
import ExpiredJobDetails from './compnents/ExpiredJobDetails/ExpiredJobDetails';
import ExpiredCourseDetails from './compnents/ExpiredCourseDetails/ExpiredCourseDetails';
import CourseUsersComponent from './compnents/CourseUsers/CourseUsers';
import CourseUserDetails from './compnents/CourseUserDetails/CourseUserDetails';
import JobUsersComponent from './compnents/JobUsers/JobUsers';
import JobUserDetails from './compnents/JobUserDetails/JobUserDetails';
import CurrentJobAcceptableUsersComponent from './compnents/CurrentJobAcceptableUsers/CurrentJobAcceptableUsers';
import CurrentJobAcceptableUserDetails from './compnents/CurrentJobAcceptableUserDetails/CurrentJobAcceptableUserDetails';
import ExpiredJobAcceptableUsers from './compnents/ExpiredJobAcceptableUsers/ExpiredJobAcceptableUsers';
import ExpiredJobAcceptableUserDetails from './compnents/ExpiredJobAcceptableUserDetails/ExpiredJobAcceptableUserDetails';
import ExpiredJobUsersComponent from './compnents/ExpiredJobUsers/ExpiredJobUsers';
import ExpiredJobUserDetails from './compnents/ExpiredJobUserDetails/ExpiredJobUserDetails';
import ExpiredCourseUsersComponent from './compnents/ExpiredCourseUsers/ExpiredCourseUsers';
import ExpiredCourseUserDetails from './compnents/ExpiredCourseUserDetails/ExpiredCourseUserDetails';
import AddCoordinator from './compnents/AddCoordinator/AddCoordinator';
import AddAdvisor from './compnents/AddAdvisor/AddAdvisor';
import UpdateCoordinator from './compnents/UpdateCoordinator/UpdateCoordinator';
import UpdateAdvisor from './compnents/UpdateAdvisor/UpdateAdvisor';
import WaitingProfile from './compnents/WaitingProfile/WaitingProfile';
import AcceptedCompaniesProfile from './compnents/AcceptedCompaniesProfile/AcceptedCompaniesProfile';
import RejectCompaniesProfile from './compnents/RejectedCompaniesProfile/RejectedCompaniesProfile';
import BlockedComaniesProfile from './compnents/BlockedComaniesProfile/BlockedComaniesProfile';
import CompanyConsultantDetails from './compnents/CompanyConsultantDetails/CompanyConsultantDetails';
import UserConsultantDetails from './compnents/UserConsultantDetails/UserConsultantDetails';
import AdvisorConsultionsReceived from './compnents/AdvisorConsultionsReceived/AdvisorConsultionsReceived';
import AdvisorCompanyConsultantDetails from './compnents/AdvisorCompanyConsultantDetails/AdvisorCompanyConsultantDetails';
import AdvisorUserConsultantDetails from './compnents/AdvisorUserConsultantDetails/AdvisorUserConsultantDetails';
import UserCv from './compnents/UserCv/UserCv';
import WaitingComponent from './compnents/WaitingComponent/WaitingComponent';
import WaitingJobDetails from './compnents/WaitingJobDetails/WaitingJobDetails';
import JobsNotConvertedUsers from './compnents/JobsNotConvertedUsers/JobsNotConvertedUsers';
//import useNotifications from './General/CustomHooks/useNotification';
import ScrollToTop from './General/ScrollToTop/ScrollToTop';
// import { onMessage } from 'firebase/messaging';
// import { messaging } from './firebase/firebaseConfig';


const App = () => {

  // useNotifications();

  //here to listen to the forground messaging
  // here we must take the data retreived by the backend and redirect the user to a new page  
  // onMessage(messaging, (payload) => {
  //   // get the notification if you are liggoned
  //   console.log(`the noti is recieved locally and it is ${JSON.stringify(payload.notification)}`)

  //   const notificationOptions = {
  //     body: payload.notification.body,
  //   };
    
  //   if (localStorage.getItem('token') != null && localStorage.getItem('token') != '') {
  //     console.log('the company token is',localStorage.getItem('token'));
  //     const notification = new Notification(payload.notification.title, notificationOptions);

  //     //and after displaying the notification here we will get the url of going to the new page when click on 
  //     // the notification
  //     notification.onclick = (event) => {
  //       event.preventDefault(); // Prevent the browser from focusing the Notification's tab
  //       console.log('Notification clicked');
  //     };
  //   }
    
  // });


  return (
    <Router>
      {/* this will be displayed when we change any route or when we navigate in other words */}
      <ScrollToTop />
      <Routes>
        <Route exact path="/" element={<Login />} />
        {/* <Route  path={`${ROUTES.Login.path}`} element={<Login />}/> */}

        <Route path={`${ROUTES.CurrentJobDetails.path}/:index`} element={<CurrentJobDetails />} />
        <Route path={`${ROUTES.WaitingJobDetails.path}/:index`} element={<WaitingJobDetails />} />
        <Route path={`${ROUTES.CurrentCourseDetails.path}/:index`} element={<CurrentCourseDetails />} />
        <Route path={`${ROUTES.ExpiredJobDetails.path}/:index`} element={<ExpiredJobDetails />} />
        <Route path={`${ROUTES.ExpiredCourseDetails.path}/:index`} element={<ExpiredCourseDetails />} />
        <Route path={`${ROUTES.UserDetails.path}/:index`} element={<UserDetails />} />
        <Route path={`${ROUTES.BlockedUserDetails.path}/:index`} element={<BlockedUserDetails />} />
        <Route path={`${ROUTES.JobsNotConvertedUsers.path}/:index`} element={<JobsNotConvertedUsers />} />

        <Route path={`${ROUTES.CompanyConsulations.path}/:index`} element={<CompanyConsultantDetails />} />
        <Route path={`${ROUTES.UserConsulations.path}/:index`} element={<UserConsultantDetails />} />

        <Route path={`${ROUTES.SideBar.path}${ROUTES.CurrentJob.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.JobsNotConverted.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.WaitingJobs.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.WaitingCompanies.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.AcceptedCompanies.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.RejectedCompanies.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.BlockedCompanies.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.CurrentCourse.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.ExpiredCourse.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.ExpiredJob.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.Advisors.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.Coordanitors.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.Statistics.path}`} element={<SideBar />} />

        <Route path={`${ROUTES.SideBar.path}${ROUTES.Notification.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.CompanyConsulations.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.UserConsulations.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.BlockedUsers.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.BlockedUsers.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.Users.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.Logout.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.SideBar.path}${ROUTES.LanguageSwitcher.path}`} element={<SideBar />} />
        <Route path={`${ROUTES.CourseUsers.path}/:index`} element={<CourseUsersComponent />} />
        <Route path={`${ROUTES.JobUsers.path}/:index`} element={<JobUsersComponent />} />
        <Route path={`${ROUTES.ExpiredJobUsers.path}/:index`} element={<ExpiredJobUsersComponent />} />
        <Route path={`${ROUTES.ExpiredJobUserDetails.path}/:index`} element={<ExpiredJobUserDetails />} />
        <Route path={`${ROUTES.CurrentJobAcceptableUsers.path}/:index`} element={<CurrentJobAcceptableUsersComponent />} />
        <Route path={`${ROUTES.CurrentJobAcceptableUserDetails.path}/:index`} element={<CurrentJobAcceptableUserDetails />} />
        <Route path={`${ROUTES.ExpiredJobAcceptableUsers.path}/:index`} element={<ExpiredJobAcceptableUsers />} />
        <Route path={`${ROUTES.ExpiredJobAcceptableUserDetails.path}/:index`} element={<ExpiredJobAcceptableUserDetails />} />
        <Route path={`${ROUTES.JobUserDetails.path}/:index`} element={<JobUserDetails />} />
        <Route path={`${ROUTES.CourseUserDetails.path}/:index`} element={<CourseUserDetails />} />
        <Route path={`${ROUTES.ExpiredCourseUsers.path}/:index`} element={<ExpiredCourseUsersComponent />} />
        <Route path={`${ROUTES.ExpiredCourseUserDetails.path}/:index`} element={<ExpiredCourseUserDetails />} />
        {/* <Route  path={`${ROUTES.AdvisorDetails.path}`} element={<AdvisorsDetails />}/> */}
        <Route path={`${ROUTES.AddCorrdinator.path}`} element={<AddCoordinator />} />
        <Route path={`${ROUTES.UpdateCorrdinator.path}/:index`} element={<UpdateCoordinator />} />
        <Route path={`${ROUTES.AddAdvisor.path}`} element={<AddAdvisor />} />
        <Route path={`${ROUTES.UpdateAdvisor.path}/:index`} element={<UpdateAdvisor />} />
        <Route path={`${ROUTES.WaitingCompanies.path}/:index`} element={<WaitingProfile />} />
        <Route path={`${ROUTES.AcceptedCompaniesProfile.path}/:index`} element={<AcceptedCompaniesProfile />} />
        <Route path={`${ROUTES.RejectedCompaniesProfile.path}/:index`} element={<RejectCompaniesProfile />} />
        <Route path={`${ROUTES.BlockedCompaniesProfile.path}/:index`} element={<BlockedComaniesProfile />} />
        <Route path={`${ROUTES.AdvisorConsultionsReceived.path}/:index`} element={<AdvisorConsultionsReceived />} />
        <Route path={`${ROUTES.AdvisorCompanyConsultantDetails.path}/:index`} element={<AdvisorCompanyConsultantDetails />} />
        <Route path={`${ROUTES.AdvisorUserConsultantDetails.path}/:index`} element={<AdvisorUserConsultantDetails />} />
        <Route path={`${ROUTES.UserCv.path}/:index`} element={<UserCv />} />
        <Route path="*" element={<WaitingComponent title={'OOps.. Not Found'} />} />
      </Routes>

    </Router>
  );

}
export default App;
