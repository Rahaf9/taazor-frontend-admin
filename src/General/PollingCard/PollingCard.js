import React from "react";
import './PollingCard.css';
import { horizAroundAlignCenter, horizAroundAlignLeftColumn, horizFlex, horizVerFlex, horizVerFlexColumn } from "../Styles/general styles";
import { DeleteOutlined } from '@ant-design/icons';
import { BlackColor, RedColor, WhiteColor, lightGreenSecOpacity } from "../Styles/colors";

const PollingCard = () => {

    const elements = [1, 2, 3, 4];
    const rates = ['75%', '10%', '10%', '5%']

    return (
        <div className="PollingCardReusable p-4">
            <div className={`${horizAroundAlignCenter} mb-2`}>
                <div style={{ fontWeight: 'bold' }}>
                    This is the first question
                </div>
                <div>
                    <DeleteOutlined style={{ color: RedColor, fontWeight: 'bold' }} />
                </div>
            </div>
            {
                elements.map(() => {
                    return (
                        <div className={`w-100 p-1 my-2 ${horizVerFlex}`} style={{ backgroundColor: lightGreenSecOpacity, borderRadius: '10px' }}>
                            hello
                        </div>
                    )
                })
            }
            <div className={`h-100 ${horizAroundAlignLeftColumn}`}>
                {
                    rates.map((rate, index) => {
                        // Check if the current index is even to start a new pair
                        if (index % 2 === 0) {
                            return (
                                <div key={index} style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}>
                                    <div style={{
                                        width: '50px', backgroundColor: WhiteColor, height: '10px',
                                        borderRadius: '10px',
                                    }}></div> <div style={{fontWeight:'bold'}}>{rate}</div>
                                    <div style={{
                                        width: '50px', backgroundColor: WhiteColor, height: '10px',
                                        borderRadius: '10px',
                                    }}></div> <div style={{fontWeight:'bold'}}>{rates[index + 1]}</div>
                                </div>
                            );
                        }
                    })
                }
            </div>
        </div>
    )

}

export default PollingCard;