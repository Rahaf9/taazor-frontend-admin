import React from 'react';
import { Modal, Carousel, Button } from 'antd';
import './QuestionsAnswersModal.css';  // Import the CSS file
import { OrangeColor } from '../Styles/colors';

const QuestionDetailsModal = ({ visible, onClose, data }) => {
  return (
    <Modal
      title="Questions and Answers"
      visible={visible}
      onOk={onClose}
      onCancel={onClose}
      footer={[
        <Button key="back" onClick={onClose}>
          Cancel
        </Button>,
        <Button key="submit" type="primary" onClick={onClose} style={{background:OrangeColor}}>
          OK
        </Button>,
      ]}
    >
      <div className="carousel-note">
        Swipe left or right to navigate through the questions
      </div>
      <Carousel dotPosition="bottom">
        {data.map((question) => (
          <div key={question.id} className="carousel-item">
            <h3>{question.question}</h3>
            <ul>
              {question.options.map((option) => (
                <li key={option.id}>{option.option_text}</li>
              ))}
            </ul>
          </div>
        ))}
      </Carousel>
    </Modal>
  );
};

export default QuestionDetailsModal;
