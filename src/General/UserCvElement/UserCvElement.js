import React from "react";
import './UserCvElement.css'

const UserCvElement = ({title,body}) => {

    return(
        <>
            {
                body && <div className="UserCvElementContainer">
                    <div className="UserCvElementTitle">
                        {title}
                    </div>
                    <div className="w-75">
                        {body}
                    </div>
                </div>
            }
        </>
    )

}

export default UserCvElement;