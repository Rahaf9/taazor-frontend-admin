import React, { useState } from 'react';
import { DatePicker } from 'antd';
import { WhiteColor, lightGreenSecOpacity } from '../Styles/colors';
import {
    CalendarOutlined
} from '@ant-design/icons';
import './DatePickerInterface.css';

const DatePickerInterface = ({ onChange, placeholder }) => {
    const [selectedDate, setSelectedDate] = useState(null);

    const handleDateChange = (date) => {
        setSelectedDate(date);
        if (onChange) {
            onChange(date.format('YY-MM-DD')); // Pass the selected date to the parent component
        }
    };

    return (
        <div>
            <DatePicker
                onChange={handleDateChange}
                placeholder={placeholder}
                style={{ backgroundColor: `${lightGreenSecOpacity}`,margin:'5px' }}
                suffixIcon={<CalendarOutlined style={{ color: 'white' }} />}
                className="custom-date-picker" 
            />

            {/* <p>Selected Date: {selectedDate ? selectedDate.format('YYYY-MM-DD') : 'None'}</p> */}
        </div>
    );
};

export default DatePickerInterface;
