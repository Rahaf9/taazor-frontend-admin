import React, { useEffect, useState } from 'react';
import './IconsCardReusable.css';
import { horizBetween, horizBetweenAlignCenter, horizBetweenAlignCenterColumn, horizVerFlex } from '../Styles/general styles';
import { CardReuseTitleStyle } from '../Styles/texts';
import { lightGreenPrimary } from '../Styles/colors';
import { useTranslation } from 'react-i18next';
import { useSpring, animated } from '@react-spring/web';
import ConfirmationModal from '../Reusable Componenets/ConfirmationModal/ConfirmationModal';

const IconsCardReusable = ({ title, data, onDetailsIconClick, onShowIconClick, myIndex, onAcceptIconClick, onAcceptCompany, onRejectCompany, onUnBlockedCompany, onBlockedCompany }) => {
    const { t } = useTranslation();
    const [isAcceptModalVisible, setIsAcceptModalVisible] = useState(false);
    const [isRejectModalVisible, setIsRejectModalVisible] = useState(false);
    const [isUnBlockModalVisible, setIsUnBlockModalVisible] = useState(false);
    const [isBlockModalVisible, setIsBlockModalVisible] = useState(false);
    const [currentIndex, setCurrentIndex] = useState(null);

    const handleDetailsIconClick = (index) => onDetailsIconClick(index);
    const handleShowIconClick = (index) => onShowIconClick(index);
    const handleAcceptIconClick = (index) => onAcceptIconClick(index);
    const handleAcceptSetTrueModal = (index) => { setCurrentIndex(index); setIsAcceptModalVisible(true); };
    const handleRejectSetTrueModal = (index) => { setCurrentIndex(index); setIsRejectModalVisible(true); };
    const handleUnBlockSetTrueModal = (index) => { setCurrentIndex(index); setIsUnBlockModalVisible(true); };
    const handleBlockSetTrueModal = (index) => { setCurrentIndex(index); setIsBlockModalVisible(true); };
    const handleAcceptCancel = () => setIsAcceptModalVisible(false);
    const handleRejectCancel = () => setIsRejectModalVisible(false);
    const handleUnBlockCancel = () => setIsUnBlockModalVisible(false);
    const handleBlockCancel = () => setIsBlockModalVisible(false);

    useEffect(() => { console.log(`the current index is ${currentIndex}`) }, [currentIndex]);

    // Animations
    const cardAnimation = useSpring({
        from: { opacity: 0, transform: 'translateY(-20px)' },
        to: { opacity: 1, transform: 'translateY(0)' },
        config: { duration: 500 }
    });

    return (
        <animated.div style={cardAnimation} className='IconsCardReusable'>
            <div className={`h-25 uppercard p-4 ${horizVerFlex}`} style={CardReuseTitleStyle}>
                {title}
            </div>

            <div className={`h-75 bottomcard p-4 ${horizBetweenAlignCenter}`}>
                {data.map((item, index) => (
                    <div className={`w-100 h-100 ${horizBetweenAlignCenterColumn}`} key={index}>
                        <div className={`w-50 h-50 m-2 ${horizVerFlex}`} style={{ cursor: 'pointer' }} onClick={() => {
                            if (item.description === t('details')) {
                                handleDetailsIconClick(myIndex);
                            } else if (item.description === t('show')) {
                                handleShowIconClick(myIndex);
                            } else if (item.description === t('doneUser')) {
                                handleAcceptIconClick(myIndex);
                            } else if (item.description === t('accept')) {
                                handleAcceptSetTrueModal(myIndex);
                            } else if (item.description === t('reject')) {
                                handleRejectSetTrueModal(myIndex);
                            } else if (item.description === t('unBlock')) {
                                handleUnBlockSetTrueModal(myIndex);
                            } else if (item.description === t('block')) {
                                handleBlockSetTrueModal(myIndex);
                            }
                        }}>
                            {item.icon()}
                        </div>
                        <span className='w-100 h-50 text-center m-2' style={{ color: lightGreenPrimary }}>{item.description}</span>
                    </div>
                ))}
            </div>
            <ConfirmationModal
                visible={isAcceptModalVisible}
                onOk={() => { onAcceptCompany(currentIndex); handleAcceptCancel(); }}
                onCancel={handleAcceptCancel}
                modalContent={t('Are you sure you want to perform this action?')}
            />
            <ConfirmationModal
                visible={isRejectModalVisible}
                onOk={() => { onRejectCompany(currentIndex); handleRejectCancel(); }}
                onCancel={handleRejectCancel}
                modalContent={t('Are you sure you want to perform this action?')}
            />
            <ConfirmationModal
                visible={isUnBlockModalVisible}
                onOk={() => { onUnBlockedCompany(currentIndex); handleUnBlockCancel(); }}
                onCancel={handleUnBlockCancel}
                modalContent={t('Are you sure you want to perform this action?')}
            />
            <ConfirmationModal
                visible={isBlockModalVisible}
                onOk={() => { onBlockedCompany(currentIndex); handleBlockCancel(); }}
                onCancel={handleBlockCancel}
                modalContent={t('Are you sure you want to perform this action?')}
            />
        </animated.div>
    );
};

export default IconsCardReusable;
