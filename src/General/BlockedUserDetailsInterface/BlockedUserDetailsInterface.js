import React, { useEffect, useState } from "react";
import { horizAroundAlignCenterColumn, horizBetween, horizBetweenAlignCenter, horizStartAlignCenter, horizVerFlex, horizVerFlexColumn } from "../Styles/general styles";
import { WhiteColor, lightGreenPrimary, lightGreenSecOpacity } from "../Styles/colors";
import { UserDetailsDesceStyle, UserCardButtonStyle, UserDetailsTitleStyle } from "../Styles/texts";
import { MailOutlined, FlagOutlined, ManOutlined, EnvironmentOutlined, GiftOutlined, PhoneOutlined } from '@ant-design/icons';
import IconTextEntry from "../IconTextEntry/IconTextEntry";
import StatusEntry from "../StatusEntry/StatusEntry";
import { useParams,useNavigate } from 'react-router-dom';
import { Button } from 'antd';

const BlockedUserDetailsInterface = ({Selectors, data,myIndex,routePath1 }) => {
    const navigate  = useNavigate();
    const {users} = Selectors;

    // here you must add the action you want to get the user details
    const { index } = useParams();
    const [details,setDetails] = useState({});

    useEffect(()=>{
        console.log(`this is the index of the user job we want to get its details ${index}
        and user ${users}`)
        // here we will get the details of this user
        setDetails(users[index]);
        // now instead of making the index from an existing array you must to call the data from the api 
        // if the array is empty or get the data from the reducer
        console.log(`${JSON.stringify(details)}`)
    },[]);

    // useEffect(()=>{
    //     console.log(`details changed ${JSON.stringify(details)}`)
    // },[details]);

    if (!details) {
        return null; // If details is null, return null to render nothing
    }
   

        
        
        const handleDetailsIconClick = (index) => {
            console.log("Details Icon in blocked user details clicked at index:", index);
            // Do whatever you need with the index
            navigate(`${routePath1}/${index}`)
        };  
  
    return (
        <div style={{ width: '100vw', height: '100vh' }} className={`${horizBetween} mt-5`}>
            <div className={`UserDetailsLeftSide w-50 h-100 ${horizVerFlexColumn}`}>
                <div className="h-75 w-50 p-4" style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                    <div style={UserDetailsTitleStyle}>
                        Basic User information
                    </div>
                    <div className={`h-25 mt-3 ${horizVerFlex}`} style={{
                        background: lightGreenSecOpacity,
                        borderRadius: '20PX'
                    }}>
                        <div className={`w-50 h-75 ${horizVerFlex}`}>
                            <img style={{ height: '100%', width: '70%' }} src="/images/the light green 2/People search-amico.png" />
                        </div>
                        <div className={`w-50 h-100 ${horizAroundAlignCenterColumn} py-3`} style={UserDetailsDesceStyle}>
                            <div>
                                {details.BasicInfo?.name ?? ''} {/* Safely access nested property */}
                            </div>
                            <div>
                            {details.BasicInfo?.status ?? ''}
                            </div>
                        </div>
                    </div>
                    {/* this is for the icons and the texts info */}
                    <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '64%' }}>
                        <IconTextEntry icon={<MailOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details.BasicInfo?.email ?? ''} />
                        <IconTextEntry icon={<FlagOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details.BasicInfo?.nationality ?? ''} />
                        <IconTextEntry icon={<ManOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details.BasicInfo?.gender ?? ''} />
                        <IconTextEntry icon={<EnvironmentOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details.BasicInfo?.location ?? ''} />
                        <IconTextEntry icon={<GiftOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details.BasicInfo?.birth ?? ''}/>
                        <IconTextEntry icon={<PhoneOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details.BasicInfo?.phone ?? ''} />
                    </div>
                </div>
                <div className={`h-25 p-2 ${horizBetweenAlignCenter}`}>
                <Button style={UserCardButtonStyle} onClick={() => handleDetailsIconClick(myIndex)}> UnBlock</Button>
            </div>
            </div>
            <div className={`UserDetailsRightSide w-50 h-100 ${horizVerFlexColumn}`}>
                <div className={`h-50 w-50 p-4`} style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                    <div style={UserDetailsTitleStyle}>
                        Personal information
                    </div>
                    <div className={`w-100 d-flex mt-4 ${horizAroundAlignCenterColumn}`} style={{ height: '64%' }}>
                        <StatusEntry title={'Military Status'} text={details.PersonalInfo?.militaryStatus ?? ''} />
                        <StatusEntry title={'Drive License'} text={details.PersonalInfo?.driveLicense ?? ''} />
                        <StatusEntry title={'Topic'} text={details.PersonalInfo?.topic ?? ''} />
                        <StatusEntry title={'Experience'} text={details.PersonalInfo?.experince ?? ''} />
                        <StatusEntry title={'Education'} text={details.PersonalInfo?.education ?? ''} />
                    </div>
                </div>
                <div className={`h-50 w-50 p-4 mt-3`} style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                    <div style={UserDetailsTitleStyle}>
                        Job Requirements
                    </div>
                    <div className={`w-100 d-flex mt-4 ${horizAroundAlignCenterColumn}`} style={{ height: '64%' }}>
                        <StatusEntry title={'Job Environment'} text={details.JobRequirements?.email ?? ''} />
                        <StatusEntry title={'Job Time'} text={details.JobRequirements?.jobTime ?? ''}/>
                        <StatusEntry title={'City of Work'} text={details.JobRequirements?.cityOfWork ?? ''} />
                        <StatusEntry title={'Working?'} text={details.JobRequirements?.working ?? ''}/>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default BlockedUserDetailsInterface;
