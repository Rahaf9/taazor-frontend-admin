// Sample dataset of country codes with flags icons
export const countriesData = [
    { code: '+1', flag: '🇺🇸' }, // USA
    { code: '+44', flag: '🇬🇧' }, // UK
    { code: '+86', flag: '🇨🇳' }, // China
    { code: '+91', flag: '🇮🇳' }, // India
    { code: '+81', flag: '🇯🇵' }, // Japan
    { code: '+82', flag: '🇰🇷' }, // South Korea
    { code: '+66', flag: '🇹🇭' }, // Thailand
    { code: '+84', flag: '🇻🇳' }, // Vietnam
    { code: '+62', flag: '🇮🇩' }, // Indonesia
    { code: '+63', flag: '🇵🇭' }, // Philippines
    // Add more countries as needed
  ];

 export const jobTitlesData = [
    "Software Engineer",
    "Data Scientist",
    "Product Manager",
    "UI/UX Designer",
    "Full Stack Developer",
    "Frontend Developer",
    "Backend Developer",
    "DevOps Engineer",
    "Machine Learning Engineer",
    "QA Engineer",
    "Business Analyst",
    "Project Manager",
    "Technical Support Engineer",
    "Network Engineer",
    // Add more job titles as needed
  ];
  