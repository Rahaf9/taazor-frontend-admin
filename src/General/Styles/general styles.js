import { lightGreenPrimary } from "./colors";

export const onBoardingStyle = {
    borderRadius: "17% 83% 57% 43% / 51% 1% 99% 49%",
    backgroundColor:`${lightGreenPrimary}`,
    position:"absolute",
    top:"-20%",
    right:"-20%",
    rotate:"40deg"
};


 // local styles
 export const verticalFlexibleColumnEnd = "d-flex flex-column justify-content-center align-items-end"
 export const horizVerFlex = "d-flex justify-content-center align-items-center"
 export const horizLeftVerFlex = "d-flex justify-content-left align-items-center"
 export const horizVerFlexColumn = "d-flex flex-column justify-content-center align-items-center"
 export const horizFlex = "d-flex justify-content-center"
 export const horizBetween = "d-flex justify-content-around align-items-end"
 export const horizFlexEnd = "d-flex justify-content-end align-items-start"
 export const horizStartAlignEndColumn = "d-flex flex-column justify-content-start align-items-end"
 export const horizBetweenAlignCenterColumn = "d-flex flex-column justify-content-between align-items-center"
 export const horizAroundAlignCenterColumn = "d-flex flex-column justify-content-around align-items-center"
 export const horizAroundAlignCenter = "d-flex justify-content-around align-items-center"
 export const horizCenterAlignCenterColumn = "d-flex flex-column justify-content-center align-items-center"
 export const horizAroundAlignLeftColumn = "d-flex flex-column justify-content-around align-items-left"
 export const horizBetweenAlignCenter = "d-flex justify-content-around align-items-center"

 export const horizStartAlignCenter = "d-flex justify-content-left align-items-center";
 export const horizEndAlignCenter = "d-flex justify-content-end align-items-center";