import { BlackColor, OrangeColor, RedColor, WhiteColor, lightGreenPrimary, lightGreenSecOpacity } from "./colors";


export const onBoardingTitleStyle = {
    fontSize: '70px',
    fontWeight: '500',
    textAlign:'left',
    color: `${lightGreenPrimary}`
};

export const onBoardingDescStyle = {
    fontSize: '20px',
    fontWeight: '400',
    textAlign:'left',
    color: `${BlackColor}`
};

export const LogRegTitleStyle = {
    fontSize: '40px',
    fontWeight: '650',
    textAlign:'center',
    color: `${BlackColor}`
};

export const jobQuesTitleStyle = {
    fontSize: '30px',
    fontWeight: '650',
    textAlign:'center',
    color: `${BlackColor}`
};

export const onErrorText = {
    fontSize: '11px',
    fontWeight: '500',
    textAlign:'left',
    color: `${RedColor}`
};

export const CompleteInfoTitleStyle = {
    fontSize: '35px',
    fontWeight: '650',
    textAlign:'center',
    color: `${BlackColor}`
};

export const CompleteInfoDescStyle = {
    fontSize: '13px',
    fontWeight: '700',
    textAlign:'left',
    color: `${BlackColor}`
};

export const CompleteInfoDescWhiteStyle = {
    fontSize: '16px',
    fontWeight: '600',
    textAlign:'left',
    color: `${WhiteColor}`
};

export const CardReuseTitleStyle = {
    fontSize: '14.5px',
    fontWeight: '600',
    textAlign:'center',
    color: `${WhiteColor}`
};

export const UserCardButtonStyle = {
    fontSize: '15px',
    fontWeight: '600',
    textAlign:'center',
    color: `${WhiteColor}`,
    backgroundColor:`${OrangeColor}`
};

export const UserDetailsTitleStyle = {
    fontSize: '18px',
    fontWeight: '500',
    textAlign:'left',
    color: `${WhiteColor}`,
};

export const UserDetailsDesceStyle = {
    fontSize: '15px',
    fontWeight: '500',
    textAlign:'left',
    color: `${WhiteColor}`,
};
    
export const UserDetailsNormalStyle = {
    fontSize: '13px',
    fontWeight: '400',
    textAlign:'left',
    color: `${WhiteColor}`,
};

export const CurrentCourseDetailsNormalStyle = {
    fontSize: '14px',
    fontWeight: '400',
    textAlign:'left',
    display:'flex',
    alignItems:'center',
    color: `${WhiteColor}`,
};

export const jobDetailsTitleStyle = {
    fontSize: '18px',
    fontWeight: '500',
    textAlign:'left',
    color: `${WhiteColor}`,
    backgroundColor:`${lightGreenSecOpacity}`,
    borderRadius:'10px'
};

export const addCourseStyle = {
    fontSize: '18px',
    fontWeight: '500',
    textAlign:'left',
    color: `${WhiteColor}`,
    borderRadius:'10px'
};

export const addCourseDescStyle = (lang) => ({
    fontSize: '.9rem',
    fontWeight: '500',
    textAlign: lang === 'ar' ? 'left' : 'right',
    color: `${WhiteColor}`,
    borderRadius:'10px'
});

export const addJobDesc2Style = {
    fontSize: '.6rem',
    fontWeight: '500',
    textAlign:'left',
    color: `${WhiteColor}`,
    borderRadius:'10px'
};

export const profileMainStyle = {
    fontSize: '1.2rem',
    fontWeight: 'bold',
    textAlign:'left',
    color: `${BlackColor}`,
    width:'60%',
};

export const profileMain2Style = {
    fontSize: '1.2rem',
    fontWeight: 'bold',
    textAlign:'left',
    color: `${WhiteColor}`,
    width:'60%',
    backgroundColor: lightGreenSecOpacity
};

export const profileSecStyle = {
    fontSize: '.9rem',
    fontWeight: '500',
    textAlign:'left',
    color: `${WhiteColor}`,
    width: '100%'
};

export const profileSec2Style = {
    fontSize: '.9rem',
    fontWeight: '500',
    textAlign:'left',
    color: `${BlackColor}`,
    width: '100%'
};


export const profileRedStyle = {
    fontSize: '1.2rem',
    fontWeight: 'bold',
    textAlign:'left',
    color: `${RedColor}`,
    width: '100%'
};

export const WaitingTitleStyle = {
    fontSize: '3rem',
    fontWeight: 'bold',
    textAlign:'center',
    color: `${lightGreenPrimary}`
};

export const addCoordinatorTitleStyle = {
    fontSize: '25px',
    fontWeight: '650',
    textAlign:'center',
    color: `${BlackColor}`
};
