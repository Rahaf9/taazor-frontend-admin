import React from 'react';
import { Row, Col } from 'antd';
import { WhiteColor, lightGreenSecondary } from '../Styles/colors';

const HorizontalScroll = ({ items }) => {
  return (
    <div style={{ width: '100%', overflowX: 'scroll',scrollbarWidth: 'none', msOverflowStyle: 'none',height:'25%' }}>
      <Row style={{ flexWrap: 'nowrap' }}>
        {items.map((item, index) => (
          <Col span={8} key={index}>
            <div style={{ backgroundColor: `${lightGreenSecondary}`, margin: '5px',padding:'3px',textAlign:'center',width:'95%',
                 color:`${WhiteColor}`,fontWeight:'bold',borderRadius:'8px' }}>
              {item}
            </div>
          </Col>
        ))}
      </Row>
    </div>
  );
}

export default HorizontalScroll;
