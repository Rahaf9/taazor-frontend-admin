import React from 'react';
import { useSpring, animated } from '@react-spring/web';
import './UserCard.css';
import { horizBetweenAlignCenter, horizBetweenAlignCenterColumn, horizVerFlex } from '../Styles/general styles';
import { CardReuseTitleStyle, UserCardButtonStyle } from '../Styles/texts';
import { Button } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const UserCard = ({ data, myIndex, routePath, additionalButtons, openModal }) => {
  const navigate = useNavigate();
  const { t } = useTranslation();

  const springProps = useSpring({
    from: { opacity: 0, transform: 'scale(0.5)' },
    to: { opacity: 1, transform: 'scale(1)' },
    config: { tension: 200, friction: 20 } // Optional spring configuration
  });

  const handleDetailsIconClick = (index) => {
    navigate(`${routePath}/${index}`);
  };

  const topicString = Array.isArray(data.topic) ? data.topic.join(' | ') : data.topic ? data.topic : Array.isArray(data.role_name) ? data.role_name.join(" | ") : data.role_name;

  return (
    <div className='p-1 w-100 h-100'>
      <animated.div className='UserCard mt-2' style={springProps} >
        <animated.div className={`h-100 uppercard ${horizVerFlex}`} style={{borderRadius:'10%'}}>
          {/* Image Container */}
          <div className="image-container">
            <img 
              className="fixed-size-img-2" style={{borderRadius:'10%'}}
              src={data.image ?? '/images/the light green 2/People search-rafiki.png'} 
              alt={data.name} 
            />
          </div>
        </animated.div>

        <animated.div className={`h-100 bottomcard p-2 ${horizBetweenAlignCenterColumn}`} style={{ maxWidth: '100%' }} >
          <animated.div className={`w-100 h-100 m-2 ${horizBetweenAlignCenterColumn}`}>
            <div className={`w-100 h-100 text-center m-2 `} style={CardReuseTitleStyle}>{data.name}</div>
            <div className={`w-100 h-100 text-center m-3 `} style={CardReuseTitleStyle}>{t(topicString)}</div>
          </animated.div>
        </animated.div>

        <animated.div className={`h-100 p-2 ${horizBetweenAlignCenter}`}>
          {routePath && <Button style={UserCardButtonStyle} onClick={() => handleDetailsIconClick(myIndex)}>Details</Button>}
          {additionalButtons &&
            additionalButtons.map((button, index) => (
              <div key={index} onClick={() => openModal(index, myIndex)}>
                {button}
              </div>
            ))}
        </animated.div>
      </animated.div>
    </div>
  );
};

export default UserCard;
