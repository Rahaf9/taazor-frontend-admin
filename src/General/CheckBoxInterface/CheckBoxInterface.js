// CustomCheckbox.js
import React from 'react';
import { Checkbox } from 'antd';
import './CheckBoxInterface.css'


const CheckBoxInterface = ({ id, checked, disabled, label, onChange }) => {
    return (
       <Checkbox
         checked={checked}
         disabled={disabled}
         onChange={() => onChange(id, !checked)}
         style={{color:'white',fontSize:'.7rem'}}
       >
         {label}
       </Checkbox>
    );
   };
   
export default CheckBoxInterface;