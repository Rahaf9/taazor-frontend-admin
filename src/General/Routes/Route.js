import AddAdvisor from "../../compnents/AddAdvisor/AddAdvisor";

export const ROUTES = {
    
    CompaniesReferAdvisor : {
        key : 'CompaniesReferAdvisor',
        path : '/CompaniesReferAdvisor'
    },
    Login : {
        key : 'login',
        path : '/login'
    },
    WaitingCompanies : {
        key : 'waitingCompanies',
        path : '/waitingCompanies'
    },
    WaitingJobs : {
        key : 'waitingJobs',
        path : '/waitingJobs'
    },
    WaitingJobDetails : {
        key : 'waitingJobDetails',
        path : '/waitingJobDetails'
    },
    AcceptedCompanies : {
        key : 'AcceptedCompanies',
        path : '/AcceptedCompanies'
    },
    RejectedCompanies : {
        key : 'RejectedCompamies',
        path : '/RejectedCompamies'
    },
    BlockedCompanies : {
        key : 'BlockedCompamies',
        path : '/BlockedCompamies'
    },

    SideBar : {
        key : 'side bar',
        path : '/sidebar'
    },
    CurrentJob : {
        key : 'current jobs',
        path : '/currentjobs'
    },
    JobsNotConverted : {
        key : 'JobsNotConverted',
        path : '/JobsNotConverted'
    },
    JobsNotConvertedUsers : {
        key : 'JobsNotConvertedUsers',
        path : '/JobsNotConvertedUsers'
    },
    ExpiredJob : {
        key : 'expired jobs',
        path : '/ExpiredJob'
    },
    CurrentCourse : {
        key : 'current courses',
        path : '/currentcourses'
    },
    ExpiredCourse : {
        key : 'Expired courses',
        path : '/Expiredcourses'
    },
   
    Notification : {
        key : 'notifications',
        path : '/notifications'
    },
    Users : {
        key : 'Users',
        path : '/Users'
    },
    UserDetails : {
        key : 'UserDetails',
        path : `/UserDetails`
    },
    BlockedUsers : {
        key : 'BlockedUsers',
        path : '/BlockedUsers'
    },
    BlockedUserDetails : {
        key : 'BlockedUserDetails',
        path : `/BlockedUserDetails`
    },
    AdvisorDetails : {
        key : 'AdvisorDetails',
        path : `/AdvisorDetails`
    },
    CurrentJobDetails : {
        key : 'CurrentJobDetails',
        path : `/CurrentJobDetails`
    },
    ExpiredJobDetails : {
        key : 'ExpiredJobDetails',
        path : `/ExpiredJobDetails`
    },
    ExpiredJobUsers : {
        key : 'ExpiredJobUsers',
        path : '/ExpiredJobUsers'
    },
    ExpiredJobUserDetails : {
        key : 'ExpiredJobUserDetails',
        path : '/ExpiredJobUserDetails'
    },
    CurrentCourseDetails : {
        key : 'CurrentCourseDetails',
        path : `/CurrentCourseDetails`
    },
    ExpiredCourseDetails : {
        key : 'ExpiredCourseDetails',
        path : `/ExpiredCourseDetails`
    },
    ExpiredCourseUsers : {
        key : 'ExpiredCourseUsers',
        path : '/ExpiredCourseUsers'
    },
    ExpiredCourseUserDetails : {
        key : 'ExpiredCourseUserDetails',
        path : '/ExpiredCourseUserDetails'
    },
    Advisors : {
        key : 'Advisors',
        path : `/Advisors`
    },
    UserConsulations : {
        key : 'UserConsulations',
        path : `/UserConsulations`
    },
    CompanyConsulations : {
        key : 'CompanyConsulations',
        path : `/CompanyConsulations`
    },
    Coordanitors : {
        key : 'Coordanitors',
        path : `/Coordanitors`
    },
    Statistics : {
        key : 'Statistics',
        path : `/Statistics`
    },
    JobUsers : {
        key : 'JobUsers',
        path : `/JobUsers`
    },
    CurrentJobAcceptableUsers : {
        key : 'CurrentJobAcceptableUsers',
        path : `/CurrentJobAcceptableUsers`
    },
    ExpiredJobAcceptableUsers : {
        key : 'ExpiredJobAcceptableUsers',
        path : '/ExpiredJobAcceptableUsers'
    },
    CurrentJobAcceptableUserDetails : {
        key : 'CurrentJobAcceptableUserDetails',
        path : `/CurrentJobAcceptableUserDetails`
    },
    ExpiredJobAcceptableUserDetails : {
        key : 'ExpiredJobAcceptableUserDetails',
        path : `/ExpiredJobAcceptableUserDetails`
    },
    JobUserDetails : {
        key : 'JobUserDetails',
        path : `/JobUserDetails`
    },
    CourseUsers : {
        key : 'CourseUsers',
        path : `/CourseUsers`
    },
    CourseUserDetails : {
        key : 'CourseUserDetails',
        path : `/CourseUserDetails`
    },
    AddCorrdinator : {
        key : 'AddCorrdinator',
        path : `/AddCorrdinator`
    },
    UpdateCorrdinator : {
        key : 'UpdateCorrdinator',
        path : `/UpdateCorrdinator`
    },
    AddAdvisor:{
        key : 'AddAdvisor',
        path : `/AddAdvisor`
    },
    UpdateAdvisor : {
        key : 'UpdateAdvisor',
        path : `/UpdateAdvisor`
    },
    AcceptedCompaniesProfile : {
        key : 'AcceptedCompaniesProfile',
        path : `/AcceptedCompaniesProfile`
    },
    RejectedCompaniesProfile : {
        key : 'RejectedCompaniesProfile',
        path : `/RejectedCompaniesProfile`
    },
    BlockedCompaniesProfile : {
        key : 'BlockedCompaniesProfile',
        path : `/BlockedCompaniesProfile`
    },
    AdvisorConsultionsReceived : {
        key : 'AdvisorConsultionsReceived',
        path : `/AdvisorConsultionsReceived`
    },
    AdvisorCompanyConsultantDetails : {
        key : 'AdvisorCompanyConsultantDetails',
        path : `/AdvisorCompanyConsultantDetails`
    },
    AdvisorUserConsultantDetails : {
        key : 'AdvisorUserConsultantDetails',
        path : `/AdvisorUserConsultantDetails`
    },
    UserCv:{
        key: 'UserCv',
        path: '/UserCv'
    },
    Logout:{
        key: 'Logout',
        path: '/Logout'
    },
    LanguageSwitcher:{
        key: 'LanguageSwitcher',
        path: '/LanguageSwitcher'
    },  
      
}