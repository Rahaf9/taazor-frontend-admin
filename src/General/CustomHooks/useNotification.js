import { useEffect } from 'react';
import Pusher from 'pusher-js';
import { notification } from 'antd';

const useNotifications = () => {
  useEffect(() => {
    // Initialize Pusher with your credentials
    const pusher = new Pusher('cbc84ce846f9c910a20a', {
      cluster: 'ap2',
      encrypted: true,
      authEndpoint: 'http://86.38.218.161:8080/broadcasting/auth',
      auth: {
        headers: {
          'Authorization': `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vODYuMzguMjE4LjE2MTo4MDgwL2FwaS9tYW5hZ2VyL2xvZ2luIiwiaWF0IjoxNzIwMjIwMTY2LCJleHAiOjE3MjA4MjQ5NjYsIm5iZiI6MTcyMDIyMDE2NiwianRpIjoiQUM4RFZSNjFtWlRWVUJ0QSIsInN1YiI6IjEiLCJwcnYiOiI2ZTY4OTYyMjU0ZDZkZjQwMWQwM2IzMGEzOWUxNjIzNjRlMTY0NmNkIn0.WY6lkezqzSqewyXZ7L75EkmSlrMHUiIGXqUDmx1BztY` // If using JWT or other authorization methods
        }
      }
    });

    // Enable Pusher logging - don't include this in production
    Pusher.logToConsole = true;

    // Subscribe to the appropriate private channel
    const channel = pusher.subscribe('App.Models.Manager.1');

    // Bind to the subscription error event
    // channel.bind('pusher:subscription_error', function(status) {
    //   console.error(`Subscription error: ${JSON.stringify(status)}`);
    //   notification.error({
    //     message: 'Subscription Error',
    //     description: `Failed to subscribe to the channel. Status: ${JSON.stringify(status)}`,
    //   });
    // });

    // Bind to the event
    channel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function (data) {
      console.log(`A new notification received and the data is ${JSON.stringify(data)}`);
      // Display the notification
      notification.success({
        message: 'New Notification',
        description: data.body,
        icon: <i className={data.icon} />,
        onClick: () => {
          window.open(data.url, '_blank');
        },
      });
    });

    // Handle connection errors
    // pusher.connection.bind('error', function(err) {
    //   console.error('Pusher connection error:', err);
    // });

    // // Log detailed info about the auth endpoint response
    // pusher.connection.bind('state_change', (states) => {
    //   console.log("State changed", states.previous, " -> ", states.current);
    // });

    // pusher.connection.bind('connected', () => {
    //   console.log("Connected with socket ID:", pusher.connection.socket_id);
    // });

    // pusher.connection.bind('error', (error) => {
    //   if (error.data.code === 4004) {
    //     console.log("Over limit!");
    //   } else {
    //     console.log("Error:", error);
    //   }
    // });

    // Cleanup on component unmount
    return () => {
      channel.unbind_all();
      channel.unsubscribe();
    };
  }, []);
};

export default useNotifications;
