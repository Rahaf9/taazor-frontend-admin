// UserCardInterface.js
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import UserCard from '../UserCard/UserCard';
import { Col, Row } from 'antd';
import { useTranslation } from 'react-i18next';
import Spinner from '../Reusable Componenets/Spinner/Spinner';
import Pagination from '../Reusable Componenets/Pagination/Pagination';
import { coordinatorsAction } from '../../actions/CoordinatorsAction';
import { horizVerFlex } from '../Styles/general styles';

const UserInterface = ({ userCards, fetchUserCards, routePath, index, isLoading, additionalButtons, openModal }) => {

    const dispatch = useDispatch();

    useEffect(() => {
        // Trigger the action to fetch user cards when the component mounts
        // this index represent the job or course index
        dispatch(fetchUserCards(index, 1));
        // console.log(`the userCards at first its value is ${JSON.stringify(userCards)}`)
    }, []);

    // useEffect(() => {
    //     console.log(`the userCards at change its value is ${JSON.stringify(userCards)}`)
    // }, [userCards]);



    const { t } = useTranslation();

    return (
        <div className='w-100 h-100'>
            {
                isLoading ? <Spinner /> :
                    <div style={{ height: '100%' }}>
                        <Row className='d-flex justify-content-around ' style={{ width: '100%' }} gutter={16}>
                            {userCards && userCards.data?.length > 0 ?
                                userCards.data.map((card, mapIndex) => (
                                    <Col style={{ margin: '10px' }} className='mb-5'
                                        key={mapIndex} xs={24} sm={24} md={18} lg={11} xl={10}>
                                        <UserCard data={card} myIndex={card.id} routePath={routePath}
                                            additionalButtons={additionalButtons} openModal={openModal} />
                                        {
                                            (card.priority_application === 0 ||  card.priority_application) && <div className={`${horizVerFlex} mt-3`}>
                                                {`the priority is ${card.priority_application}`}
                                            </div>
                                        }
                                    </Col>
                                )) : <div className='flexible text-danger fw-bold mt-5'>
                                    {t('no_items_found')}
                                </div>
                            }
                        </Row>
                        <div className={`${horizVerFlex} m-5`}>
                            <Pagination onPageChange={(page) => dispatch(fetchUserCards(index, page))} currentPage={userCards.current_page} totalPages={userCards.last_page ?? '0'} />
                        </div>
                    </div>
            }
        </div>
    );
};

export default UserInterface;
