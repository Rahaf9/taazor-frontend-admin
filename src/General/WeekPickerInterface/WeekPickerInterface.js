import React from 'react';
import { Select } from 'antd';
import './WeekPickerInterface.css'

const { Option } = Select;

const DayOfWeekPicker = ({ onDaySelect, options }) => {
 const handleDayChange = (value) => {
    onDaySelect(value);
 };

 return (
    <div className='w-100'>
      <Select defaultValue="" style={{ width: '100%' }} onChange={handleDayChange}>
        <Option value="">Select Number Of Days</Option>
        {options.map((option, index) => (
          <Option key={index} value={option.value}>{option.label}</Option>
        ))}
      </Select>
    </div>
 );
};

export default DayOfWeekPicker;
