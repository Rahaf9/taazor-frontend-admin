import React from "react";
import { CheckCircleOutlined  } from '@ant-design/icons';
import { WhiteColor } from "../Styles/colors";
import { UserDetailsNormalStyle } from "../Styles/texts";
import { horizFlex } from "../Styles/general styles";

const StatusEntry = ({ title,text }) => {
  return (
    <div className={`flex-1 w-100 py-1`} style={{ display: 'flex', justifyContent: 'space-between' }}>
      <div className={`w-50`} style={{ display: 'flex', alignItems: 'center',color:WhiteColor,fontWeight:'500',fontSize:'13px' }}>
        {title}
      </div>
      <div className={`w-75`} style={UserDetailsNormalStyle}>
        <CheckCircleOutlined  style={{fontSize:'13px',marginRight:'10px'}}/>
        {text}
      </div>
    </div>
  );
};

export default StatusEntry;
