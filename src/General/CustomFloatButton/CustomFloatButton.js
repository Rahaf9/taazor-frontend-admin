import React, { useState } from 'react';
import { FloatButton } from 'antd';
import { PlusCircleFilled } from '@ant-design/icons';
import { lightGreenPrimary } from '../Styles/colors';

const CustomFloatButton = ({floatButtons}) => {
  const [open, setOpen] = useState(true);
  const onChange = (checked) => {
    setOpen(checked);
  };


  return (
    <>
      <FloatButton.Group
        open={open}
        onClick={() => setOpen(!open)}
        trigger="click"
        style={{
          right: 24,
        }}
        icon={<PlusCircleFilled style={{color:lightGreenPrimary}}/>}
      >
        {floatButtons}
      </FloatButton.Group>
    </>
  );
};

export default CustomFloatButton;
