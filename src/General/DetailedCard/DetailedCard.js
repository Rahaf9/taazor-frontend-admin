import React from 'react';
import { EditOutlined } from '@ant-design/icons';
import { Avatar, Card } from 'antd';
import { OrangeColor, lightGreenPrimary } from '../Styles/colors';
import { horizVerFlex } from '../Styles/general styles';
import { useSpring, animated } from '@react-spring/web';

const { Meta } = Card;

const DetailedCard = ({ action, description, title, image, onDetailsClick, request_date, titleIcon, descriptionIcon }) => {
  // Define animation for the card
  const cardAnimation = useSpring({
    from: { opacity: 0, transform: 'translateY(-20px)' },
    to: { opacity: 1, transform: 'translateY(0)' },
    config: { duration: 500 }
  });

  return (
    <animated.div style={cardAnimation}>
      <Card
        style={{
          width: '100%',
          backgroundColor: lightGreenPrimary,
          border: `2px solid ${OrangeColor}`
        }}
        actions={[
          <EditOutlined key="edit" onClick={onDetailsClick} />,
          action
        ]}
      >
        <Meta className={`${horizVerFlex}`}
          avatar={
            <div style={{ width: '80px', height: '100%' }}>
              {request_date ? request_date : <Avatar src={image} style={{ width: '100%', height: '100%' }} />}
            </div>
          }
          title={
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {titleIcon && <span style={{ marginRight: '8px' }}>{titleIcon}</span>}
              {title}
            </div>
          }
          description={
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {descriptionIcon && <span style={{ marginRight: '8px' }}>{descriptionIcon}</span>}
              {description}
            </div>
          }
        />
      </Card>
    </animated.div>
  );
}

export default DetailedCard;
