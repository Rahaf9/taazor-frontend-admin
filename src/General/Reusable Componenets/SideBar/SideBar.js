import { useState } from "react";
import Layout, { Content } from "antd/es/layout/layout";
import Sider from "antd/es/layout/Sider";
import { useLocation } from "react-router-dom";
import Logo from "../Logo/Logo";
import MenuList from "../MenuList/MenuList";
import { ROUTES } from "../../Routes/Route";
import CurrentJob from "../../../compnents/CurrentJob/CurrentJob";
import CurrentCourse from "../../../compnents/CurrentCourse/CurrentCourse";
import WaitingCompanies from "../../../compnents/WaitingCompanies/WaitingCompanies";
import Notifications from "../../../compnents/Notifications/Notifications";
import Users from "../../../compnents/Users/Users";
import { lightGreenPrimary } from "../../Styles/colors";
import YourConsultants from "../../../compnents/CompanyConsultants/CompanyConsultants";
import AcceptedCompanies from "../../../compnents/AcceptedCompanies/AcceptedCompanies";
import RejectedCompanies from "../../../compnents/RejectedCompanies/RejectedCompanies";
import BlockedCompanies from "../../../compnents/BlockedCompanies/BlockedCompanies";
import BlockedUsers from "../../../compnents/BlockedUser/BlockedUser";
import Advisors from "../../../compnents/Advisors/Advisors";
import Coordanitors from "../../../compnents/Coordanitors/Coordanitors";
import Statistics from "../../../compnents/Statistics/Statistics";
import { useTranslation } from "react-i18next";
import ExpiredJob from "../../../compnents/ExpiredJob/ExpiredJob";
import ExpiredCourse from "../../../compnents/ExpiredCourse/ExpiredCourse";
import CompanyConsultants from "../../../compnents/CompanyConsultants/CompanyConsultants";
import UserConsultants from "../../../compnents/UserConsultants/UserConsultants";
import LogoutComponent from "../../../compnents/LogoutComponent/LogoutComponent";
import LanguageSwitcher from "../../../compnents/LanguageSwitcher/LanguageSwitcher";
import WaitingJobs from "../../../compnents/WaitingJobs/WaitingJobs";
import JobsNotConverted from "../../../compnents/JobsNotConverted/JobsNotConverted";
function SideBar() {

  const [darkTheme ] = useState(false);
  const [collapsed , setCollapsed] = useState(false);
  const [broken , setBroken] = useState(false);
  const location = useLocation();
  const { t,i18n } = useTranslation();
   
  const getPage = ()=>{
    
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.CurrentJob.path}`) return <CurrentJob/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.JobsNotConverted.path}`) return <JobsNotConverted/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.CurrentCourse.path}`) return <CurrentCourse/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.ExpiredCourse.path}`) return <ExpiredCourse/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.ExpiredJob.path}`) return <ExpiredJob/>;
    if(location.pathname === `${ROUTES.SideBar.path}${ROUTES.WaitingJobs.path}`) return <WaitingJobs/>;
 
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Notification.path}`) return <Notifications/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Statistics.path}`) return <Statistics/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.CompanyConsulations.path}`) return <CompanyConsultants/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.UserConsulations.path}`) return <UserConsultants/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.WaitingCompanies.path}`) return <WaitingCompanies/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.AcceptedCompanies.path}`) return <AcceptedCompanies/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.RejectedCompanies.path}`) return <RejectedCompanies/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.BlockedCompanies.path}`) return <BlockedCompanies/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Users.path}`) return <Users/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.BlockedUsers.path}`) return <BlockedUsers/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Advisors.path}`) return <Advisors/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Coordanitors.path}`) return <Coordanitors/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.UserConsulations.path}`) return <UserConsultants/>;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Logout.path}`) return <LogoutComponent />;
    if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.LanguageSwitcher.path}`) return <LanguageSwitcher />;
    
  }     

  return(
    <Layout dir={i18n.dir()}>
            <Sider
                breakpoint='sm'
                collapsed={!broken ? collapsed : true}
                collapsible
                trigger={null}
                style={{ background: `${lightGreenPrimary}`,minHeight:'100vh' }}
                theme={darkTheme ? 'dark' : 'light'}
                className='text-light'
                onBreakpoint={(broken) => {
                    if (broken) {
                        console.log('broken called');
                        setBroken(true);
                    } else {
                        console.log('not broken called');
                        setBroken(false);
                    }
                }}
                collapsedWidth={80} // Set the collapsed width
                width={200} // Set the expanded width
            >
                <Logo
                    collapse={collapsed}
                    collapseFunc={!broken ? () => setCollapsed(!collapsed) : () => setCollapsed(false)}
                />
                <MenuList darkTheme={darkTheme} />
            </Sider>
            <Content style={{minHeight:'100vh'}}>
                {getPage()}
            </Content>
        </Layout>
  );

}

export default SideBar;