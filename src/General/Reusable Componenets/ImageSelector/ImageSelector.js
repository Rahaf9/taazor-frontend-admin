// ImageSelector.js
import React, { useState } from 'react';
import { Modal, Upload, Button } from 'antd';
import { UploadOutlined, PlusOutlined } from '@ant-design/icons';
import './ImageSelector.css'; // Assuming you have a CSS file for styling

const ImageSelector = ({ initialImage, onImageChange, icon }) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleChange = (value) => {
        if (!value ||!value.file ||!value.file.originFileObj) {
            console.error('No file selected.');
            return;
        }

        const file = value.file.originFileObj;
        if (file.type && file.type.startsWith('image/')) {
            onImageChange(URL.createObjectURL(file));
            setIsModalVisible(false);
        } else {
            console.error('Please upload an image file.');
        }
    };

    return (
        <div className="imageSelectorContainer mt-3">
            <div className="imageDisplayWrapper">
                <img
                    src={initialImage}
                    alt="Current Image"
                    style={{ width: '100%', height: '100%' }}
                />
                {/* <Button
                    type="primary"
                    shape="circle"
                    // disabled={true}
                    icon={icon}
                    onClick={() => setIsModalVisible(true)}
                /> */}
            </div>
            <Modal
                title="Select Image"
                visible={isModalVisible}
                onCancel={() => setIsModalVisible(false)}
                footer={null}
            >
                <Upload
                    onChange={handleChange}
                    accept="image/*"
                    showUploadList={false}
                >
                    <Button icon={<UploadOutlined />}>Upload Image</Button>
                </Upload>
            </Modal>
        </div>
    );
};

export default ImageSelector;
