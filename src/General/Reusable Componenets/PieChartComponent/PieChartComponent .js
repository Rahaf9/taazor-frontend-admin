import React from 'react';
import { PieChart, Pie, Cell, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const colors = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#A28FD0', '#82CA9D', '#FF69B4'];

const PieChartComponent = ({ data, title }) => {
    return (
        <div className="peopleHired">
            <div className="text-percentage">
                {title}
            </div>
            <div className="percentage-circle" style={{ height: '400px', width: '300px' }}>
                <ResponsiveContainer>
                    <PieChart>
                        <Pie
                            data={data}
                            cx="50%"
                            cy="50%"
                            outerRadius={80}
                            fill="#82CA9D"
                            dataKey="value"
                            label={({ name, value }) => `${name}: ${value}`}
                        >
                            {
                                data.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
                                ))
                            }
                        </Pie>
                        <Tooltip />
                        <Legend verticalAlign="bottom" height={100} />
                    </PieChart>
                </ResponsiveContainer>
            </div>
        </div>
    );
}

export default PieChartComponent;
