import { FireOutlined,MenuOutlined  } from '@ant-design/icons';
import './Logo.css';

const Logo = ({collapseFunc,collapse}) => {

    const style = {
        fontSize : '1.2rem'
    }

    return(
        <div className="logo">
            <div className="logo-icon back-linear">
                {
                    !collapse ? <img src='/images/my needs for project/taazor 1.png' size={'37px'} className="hide-sm" style={
                        {
                            height:'50px',
                            width:'50px'
                        }
                    }/> : null
                }
                <MenuOutlined onClick={collapseFunc} className="pointer" style={style}/>
            </div>
        </div>
    );
    
}

export default Logo;