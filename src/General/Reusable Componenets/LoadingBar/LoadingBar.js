import React from 'react';
import { Spin } from 'antd'; // Using Ant Design's Spin component for the LoadingBar
import './LoadingBar.css'; // Import the CSS file

const LoadingBar = (color) => (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '10px' }}>
        <Spin size="large" className="custom-loadingbar" style={{color:color}} /> {/* Display loading bar */}
    </div>
);

export default LoadingBar;
