import { Menu } from "antd";
import MenuItem from "antd/es/menu/MenuItem";
import SubMenu from "antd/es/menu/SubMenu";
import './MenuList.css';
import {
    BookOutlined, ClockCircleOutlined, CloseCircleOutlined, GlobalOutlined,
    CheckSquareOutlined, StopOutlined, TeamOutlined, LogoutOutlined, ExceptionOutlined, ControlOutlined, BellOutlined, SolutionOutlined, BarChartOutlined
} from '@ant-design/icons';
import { ROUTES } from "../../Routes/Route";
import { Link, useLocation } from "react-router-dom";
import { RedColor, WhiteColor, lightGreenPrimary } from "../../Styles/colors";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";

const MenuList = ({ darkTheme }) => {

    const location = useLocation();
    const { t } = useTranslation();
    const style = {
        fontSize: '1.2rem',
        color: `${WhiteColor}`
    }
    const noStyle = { color: 'inherit', textDecoration: 'none', color: `${WhiteColor}`, fontSize: '.8rem' };
    const flexCenter = { display: 'flex', justifyContent: 'center', alignItems: 'center' };
    // const getRole=(role)=> {
    //     return localStorage.getItem('roleNmae')=== 'admin'? 
    // }

    const roleName = localStorage.getItem('roleName');

    const getKeyFromPath = (path) => {
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.CurrentJob.path}`) return "current_jobs"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.ExpiredJob.path}`) return "expired_jobs"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.CurrentCourse.path}`) return "current_courses"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.ExpiredCourse.path}`) return "expired_courses"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Notification.path}`) return "notification"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Statistics.path}`) return "statistics"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.CompanyConsulations.path}`) return "company_consulations"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.WaitingCompanies.path}`) return "waitingCompanies"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.AcceptedCompanies.path}`) return "acceptedCompanies"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.RejectedCompanies.path}`) return "rejectedCompanies"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.BlockedCompanies.path}`) return "blockedCompanies"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Users.path}`) return "users"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.BlockedUsers.path}`) return "blockedUsers"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Advisors.path}`) return "advisors"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Coordanitors.path}`) return "coordanitors"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.UserConsulations.path}`) return "user_consulations"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.Logout.path}`) return "logout"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.LanguageSwitcher.path}`) return "language"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.WaitingJobs.path}`) return "waiting_jobs"
        if (location.pathname === `${ROUTES.SideBar.path}${ROUTES.JobsNotConverted.path}`) return "jobs_not_converted"
        return '';
    };

    const selectedKey = getKeyFromPath(location.pathname);

    return (
        <div>
            {
                <Menu selectedKeys={[selectedKey]} style={{ background: `${lightGreenPrimary}`, height: '80%' }} theme={darkTheme ? 'dark' : 'light'} mode="inline" className="menu">
                    {roleName.includes('admin') &&
                        <>
                            <MenuItem key={'waitingCompanies'} icon={<ClockCircleOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.WaitingCompanies.path}`} style={noStyle} >
                                    {t('Waiting Companies')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'rejectedCompanies'} icon={<CloseCircleOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.RejectedCompanies.path}`} style={noStyle} >
                                    {t('Rejected Companies')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'acceptedCompanies'} icon={<CheckSquareOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.AcceptedCompanies.path}`} style={noStyle} >
                                    {t('Accepted Companies')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'blockedCompanies'} icon={<StopOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.BlockedCompanies.path}`} style={noStyle} >
                                    {t('Blocked Companies')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'users'} icon={<TeamOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.Users.path}`} style={noStyle} >
                                    {t('Users')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'blockedUsers'} icon={<StopOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.BlockedUsers.path}`} style={noStyle} >
                                    {t('Blocked Users')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'coordanitors'} icon={<SolutionOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.Coordanitors.path}`} style={noStyle} >
                                    {t('Coordinators')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'statistics'} icon={<BarChartOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.Statistics.path}`} style={noStyle} >
                                    {t('Statistics')}
                                </Link>
                            </MenuItem>

                            <MenuItem key={'expired_jobs'} icon={<ControlOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.ExpiredJob.path}`} style={noStyle} >
                                    {t('Expired Job')}
                                </Link>
                            </MenuItem>
                            <MenuItem
                                key={'advisors'} icon={<ExceptionOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.Advisors.path}`} style={noStyle} >
                                    {t('Advisors')}
                                </Link>
                            </MenuItem>

                            <MenuItem key={'expired_courses'} icon={<ControlOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.ExpiredCourse.path}`} style={noStyle} >
                                    {t('Expired Courses')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'waiting_jobs'} icon={<ControlOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.WaitingJobs.path}`} style={noStyle} >
                                    {t('Waiting Jobs')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'notification'} icon={<BellOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.Notification.path}`} style={noStyle} >
                                    {t('Notifications')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'user_consulations'} icon={<ExceptionOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.UserConsulations.path}`} style={noStyle} >
                                    {t('User Consulats')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'company_consulations'} icon={<ExceptionOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.CompanyConsulations.path}`} style={noStyle} >
                                    {t('Company Consulats')}
                                </Link>
                            </MenuItem>
                            <MenuItem key={'current_jobs'} icon={<ControlOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.CurrentJob.path}`} style={noStyle} >
                                    {t('Job Opportunities')}
                                </Link>
                            </MenuItem>
                            {/* <MenuItem key={'waiting_jobs'} icon={<ControlOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.WaitingJobs.path}`} style={noStyle} >
                                    {t('Waiting Opportunities')}
                                </Link>
                            </MenuItem> */}
                            <MenuItem key={'current_courses'} icon={<BookOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.CurrentCourse.path}`} style={noStyle}>
                                    {t('Course Opportunities')}
                                </Link>
                            </MenuItem>
                        </>
                    }


                    {
                        (roleName.includes('user_consultation_coordinator')) && <>
                            <MenuItem key={'user_consulations'} icon={<ExceptionOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.UserConsulations.path}`} style={noStyle} >
                                    {t('User Consulats')}
                                </Link>
                            </MenuItem>
                        </>
                    }
                    {
                        (roleName.includes('company_consultation_coordinator')) && <>

                            <MenuItem key={'company_consulations'} icon={<ExceptionOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.CompanyConsulations.path}`} style={noStyle} >
                                    {t('Company Consulats')}
                                </Link>
                            </MenuItem>
                        </>
                    }
                    {
                        (roleName.includes('job_posting_requests_coordinator')) && <>

                            <MenuItem key={'waiting_jobs'} icon={<ControlOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.WaitingJobs.path}`} style={noStyle} >
                                    {t('Waiting Jobs')}
                                </Link>
                            </MenuItem>
                        </>
                    }
                    {
                        (roleName.includes('job_requests_coordinator')) && <>

                            <MenuItem key={'jobs_not_converted'} icon={<BookOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.JobsNotConverted.path}`} style={noStyle}>
                                    {t('JobNotConverted')}
                                </Link>
                            </MenuItem>
                        </>
                    }
                    <MenuItem key={'notification'} icon={<BellOutlined style={style} />}>
                                <Link to={`${ROUTES.SideBar.path}${ROUTES.Notification.path}`} style={noStyle} >
                                    {t('Notifications')}
                                </Link>
                            </MenuItem>
                    <MenuItem key={'logout'} icon={<LogoutOutlined style={style} />}>
                        <Link to={`${ROUTES.SideBar.path}${ROUTES.Logout.path}`} style={noStyle}>
                            {t('Logout')}
                        </Link>
                    </MenuItem>
                    <MenuItem key={'language'} icon={<GlobalOutlined style={style} />}>
                        <Link to={`${ROUTES.SideBar.path}${ROUTES.LanguageSwitcher.path}`} style={noStyle}>
                            {t('Languages')}
                        </Link>
                    </MenuItem>


                </Menu>
            }
        </div>
    );

}

export default MenuList;