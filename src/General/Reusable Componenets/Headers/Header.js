import React from 'react';
import { Layout } from "antd";
import { useTranslation } from 'react-i18next';
import { WhiteColor, lightGreenPrimary } from "../../Styles/colors";
import { horizVerFlex } from "../../Styles/general styles";
import { useSpring, animated } from '@react-spring/web';

const { Header } = Layout;

function CustomHeader({ title, icon }) {
    const { t, i18n } = useTranslation();

    // Define animation for the header
    const headerAnimation = useSpring({
        from: { opacity: 0, transform: 'translateY(-20px)' },
        to: { opacity: 1, transform: 'translateY(0)' },
        config: { duration: 500 }
    });

    return (
        <animated.div style={headerAnimation}>
            <Header 
                className={`w-100 ${horizVerFlex}`} 
                style={{ 
                    background: lightGreenPrimary, 
                    color: WhiteColor, 
                    direction: i18n.dir() === 'rtl' ? 'rtl' : 'ltr',
                    textAlign: i18n.dir() === 'rtl' ? 'right' : 'left',
                }}
            >
                <div className={`col-10 h-100`}>
                    {title}
                </div>
                <div className={`col-2 h-100 ${horizVerFlex}`}>
                    {icon}
                </div>
            </Header>
        </animated.div>
    );
}

export default CustomHeader;
