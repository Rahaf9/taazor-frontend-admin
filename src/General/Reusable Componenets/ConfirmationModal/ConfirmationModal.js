// CustomModal.js
import React from 'react';
import { Button, Modal } from 'antd';
import { OrangeColor, WhiteColor } from '../../Styles/colors';
import { useTranslation } from 'react-i18next';

const ConfirmationModal = ({ visible, onOk, onCancel, modalContent }) => {
  const { t, i18n } = useTranslation()
  return (
    <Modal
      title="Confirmation"
      visible={visible}
      footer={[
        <Button key="cancel" onClick={onCancel} style={{ background: OrangeColor, color: WhiteColor }}>
          {t('no')}
        </Button>,
        <Button key="ok" type="primary" onClick={onOk} style={{ background: OrangeColor, color: WhiteColor }}>
          {t('ok')}
        </Button>,
      ]}
    >
      {modalContent}
    </Modal>
  )
};

export default ConfirmationModal;
