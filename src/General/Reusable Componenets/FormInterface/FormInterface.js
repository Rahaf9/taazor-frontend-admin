import React, { useEffect, useState } from 'react';
import { useSpring, animated } from '@react-spring/web';
import { Form, Input, Button, notification } from 'antd';
import { OrangeColor, lightGreenPrimary, lightGreenSecOpacity } from '../../Styles/colors';
import { horizVerFlex, horizVerFlexColumn, horizBetweenAlignCenterColumn } from '../../Styles/general styles';
import './FormInterface.css'
import { LogRegTitleStyle, onErrorText } from '../../Styles/texts';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import LoadingBar from '../LoadingBar/LoadingBar';
import registerServiceWorker from '../../registerServiceWorker ';
import requestPermission from '../../../firebase/requestPermission';

const FormInterface = ({ title, placeholders, buttonName, placeholderIcons, photo, additionalItems, onSubmit, Selectors, keys, RefreshPage, routePage }) => {
    const { errorSelector, loadingSelector, doneSelector } = Selectors;
    const [formData, setFormData] = useState(Array(placeholders.length).fill(''));
    const [mappedFormData, setMappedFormData] = useState({});
    const registerErrors = useSelector(errorSelector);
    const isLoading = useSelector(loadingSelector);
    const done = useSelector(doneSelector);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const location = useLocation();

    const handleChange = (index, value) => {
        const newData = [...formData];
        newData[index] = value;
        setFormData(newData);
        let key = keys[index];

        setMappedFormData(prevData => ({
            ...prevData,
            [key]: value
        }));

        console.log(mappedFormData);
    };

    useEffect(() => {
        console.log(`the errors inside register ${JSON.stringify(registerErrors.message)}`)
        registerErrors && registerErrors.message && notification.error({
            message: 'Error',
            description: registerErrors.message,
        });
        dispatch(RefreshPage());
    }, [registerErrors.message]);

    useEffect(() => {
        if (done) {
            //here we must check that this is the login
            // if (location.pathname === '/') {
            //     registerServiceWorker()
            //         .then(() => {
            //             return requestPermission();
            //         })
            //         .catch((error) => {
            //             console.error("Service worker registration or permission request failed:", error);
            //         });
            // }
            navigate(routePage);

        }
        dispatch(RefreshPage());
    }, [done]);

    const handleSubmit = (values) => {
        console.log('Form data values:', values);
        onSubmit(mappedFormData);
    };

    const formAnimation = useSpring({
        from: { opacity: 0, transform: 'translateY(20px)' },
        to: { opacity: 1, transform: 'translateY(0)' },
        config: { duration: 500 }
    });

    const imageAnimation = useSpring({
        from: { opacity: 0, transform: 'translateX(-20px)' },
        to: { opacity: 1, transform: 'translateX(0)' },
        config: { duration: 500, delay: 300 }
    });

    return (
        <div>
            <div className='FormContainerStyle'>
                <animated.div style={imageAnimation} className='leftFormSection m-5'>
                    <div style={LogRegTitleStyle}>{title}</div>
                    <div className={`h-75 w-100 ${horizVerFlex} my-5`}>
                        <img className='w-75 h-100' src={photo} alt="form visual" />
                    </div>
                </animated.div>
                <div className='rightFormSection'>
                    <div className='d-flex flex-column align-items-center my-4 px-lg-0 px-xl-3' style={{ height: '100vh', width: '80%' }}>
                        <img src='/images/my needs for project/taazor 1.png' alt="logo" />
                        <animated.div style={formAnimation} className={`w-100 ${horizBetweenAlignCenterColumn}`}>
                            <Form layout="vertical" onFinish={handleSubmit} className={`w-100 m-5 py-3 ${horizBetweenAlignCenterColumn}`}>
                                <div className='w-75'>
                                    {placeholders.map((placeholder, index) => (
                                        <div key={index} className={`w-100 ${horizVerFlex}`}>
                                            <Form.Item className='w-100 d-block'>
                                                <Input style={{ background: `${lightGreenSecOpacity}`, color: 'white' }}
                                                    value={formData[index]}
                                                    onChange={(e) => handleChange(index, e.target.value)}
                                                    placeholder={placeholder}
                                                    suffix={placeholderIcons[index]}
                                                />
                                                {
                                                    registerErrors && JSON.stringify(Object.keys(registerErrors)[0]) === JSON.stringify(keys[index]) &&
                                                    <span style={onErrorText}>{registerErrors[Object.keys(registerErrors)[0]]}</span>
                                                }
                                            </Form.Item>
                                        </div>
                                    ))}
                                    <Form.Item style={{ display: 'flex', justifyContent: 'center' }}>
                                        <Button type="primary" htmlType="submit" style={{ background: OrangeColor }}>
                                            {isLoading ? <LoadingBar /> : buttonName}
                                        </Button>
                                    </Form.Item>
                                </div>
                                <div className='w-75'>
                                    {additionalItems && additionalItems.map((item, index) => (
                                        <div className='px-2 w-100 text-white' key={index}>
                                            {item.text}
                                            {item.buttonText && (
                                                <Button style={{ color: OrangeColor, fontWeight: 'bold' }} type="link" onClick={item.onClick}>{item.buttonText}</Button>
                                            )}
                                        </div>
                                    ))}
                                </div>
                            </Form>
                        </animated.div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FormInterface;
