import React from 'react';
import { Progress } from "antd";
import { StarFilled } from '@ant-design/icons';
import { OrangeColor } from '../../Styles/colors';

const ProgressBarComponent = ({ data, title, valueKey = 'percent', labelKey = 'name',identifier="percent" }) => {
    return (
        <div className="peopleHired">
            <div className="text-percentage">
                {title}
            </div>
            <div className="percentage-column">
                {data.map((item, index) => (
                    <div key={index} style={{ marginBottom: '16px' }}>
                        <Progress
                            type="line"
                            // that mean if it is rate that every single star is represent an 20%
                            percent={identifier === 'percent' ? parseFloat(item[valueKey]) : parseFloat(item[valueKey]) * 20}
                            strokeColor="#82CA9D"
                            style={{ width: '100%' }}
                            showInfo={false}
                        />
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>{item[labelKey]}</div> <div>{item[valueKey]} {identifier === 'percent' ? '%' : 
                                <StarFilled style={{color:OrangeColor}}/>}</div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default ProgressBarComponent;
    