import React, { useEffect } from "react";
import { MailOutlined, FlagOutlined, ManOutlined, EnvironmentOutlined, GiftOutlined, PhoneOutlined } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary, lightGreenSecOpacity } from "../../Styles/colors";
import { UserDetailsDesceStyle, UserDetailsTitleStyle, jobDetailsTitleStyle } from "../../Styles/texts";
import IconTextEntry from "../../IconTextEntry/IconTextEntry";
import StatusEntry from "../../StatusEntry/StatusEntry";
import { horizAroundAlignCenterColumn, horizBetween, horizBetweenAlignCenterColumn, horizCenterAlignCenterColumn, horizStartAlignCenter, horizVerFlex, horizVerFlexColumn } from "../../Styles/general styles";
import HorizontalScroll from "../../HorizontalScroll/HorizontalScroll";

const OpportunityInterface = ({ companyName, iconTextEntries, statusEntries }) => {

    if (!companyName) {
        return null;
    }

    return (
        <div style={{ width: '100vw', height: '76vh' }} className={`${horizBetween} mt-5`}>
            <div className={`UserDetailsLeftSide w-50 h-100 ${horizVerFlexColumn}`}>
                <div className="h-100 w-50 px-4 pt-1 pb-3" style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                    <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '100%' }}>
                        <div style={jobDetailsTitleStyle} className="d-flex w-100 p-2">
                            {companyName}
                        </div>
                        {/* Map over iconTextEntries array to render IconTextEntry components */}
                        {/* // Take only the first 7 elements from iconTextEntries */}
                        {iconTextEntries.slice(0, 7).map((entry, index) => (
                            <IconTextEntry key={index} icon={entry.icon} text={entry.text} />
                        ))}
                    </div>
                </div>
            </div>
            <div className={`UserDetailsRightSide w-50 h-100 ${horizVerFlexColumn}`}>
                <div className={`w-50 p-4`} style={{ background: lightGreenPrimary, borderRadius: '20px', height: '40%' }}>
                    { statusEntries[0] && statusEntries[0].title == 'Languages' &&
                        <div style={UserDetailsTitleStyle} className="mb-2">
                            {statusEntries[0].title}
                        </div>
                    }
                    {
                        statusEntries[0] && statusEntries[0].title == 'Languages' ? <HorizontalScroll items={statusEntries[0].text} /> : ''
                    }
                    <div className={`w-100 d-flex mt-2 ${horizCenterAlignCenterColumn}`} style={{ height: '64%' }}>
                        {/* Map over statusEntries array to render StatusEntry components and take from elements 2 to 3*/}
                        {statusEntries.slice(1, 3).map((entry, index) => (
                            <StatusEntry key={index} title={entry.title} text={entry.text} />
                        ))}
                    </div>
                </div>
                <div className={`w-50 p-4 mt-3`} style={{ background: lightGreenPrimary, borderRadius: '20px', height: '60%' }}>
                    <div className={`w-100 d-flex mt-2 ${horizCenterAlignCenterColumn}`} style={{ height: '64%' }}>
                        {/* Map over statusEntries array to render StatusEntry components from the fourth elemnt to the end*/}
                        {statusEntries.slice(3).map((entry, index) => (
                            <StatusEntry key={index} title={entry.title} text={entry.text} />
                        ))}
                    </div>
                    <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '36%' }}>
                        {iconTextEntries.slice(7).map((entry, index) => (
                            <IconTextEntry key={index} icon={entry.icon} text={entry.text} />
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default OpportunityInterface;
