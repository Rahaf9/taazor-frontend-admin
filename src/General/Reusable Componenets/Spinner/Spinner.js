import React from 'react';
import { Spin } from 'antd'; // Using Ant Design's Spin component for the spinner
import './Spinner.css'; // Import the CSS file

const Spinner = () => (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <Spin size="large" className="custom-spinner" /> {/* Apply custom class */}
    </div>
);

export default Spinner;
