import { Button, Modal, Select, notification } from "antd";
import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import Spinner from "../Spinner/Spinner";
import LoadingBar from "../LoadingBar/LoadingBar";
import { OrangeColor, WhiteColor } from "../../Styles/colors";
import { useNavigate } from "react-router-dom";

const AdvisorSelectingModal = ({ 
    visible, 
    onCancel, 
    onOk, 
    advisors, 
    isLoading, 
    errors, 
    isReferLoading, 
    done, 
    refreshErrors,
    topic
}) => {

    const { t } = useTranslation();
    const { Option } = Select;
    const [selectedAdvisorId, setSelectedAdvisorId] = useState(null);
    const navigate = useNavigate();

    useEffect(() => {
        if (errors) {
            console.log(`the errors inside company refer modal ${JSON.stringify(errors.message)}`);
            errors && errors.message && notification.error({
                message: t('Error'),
                description: errors.message,
            });
            refreshErrors();
        }
    }, [errors.message]);

    useEffect(() => {
        if (done) {
            notification.success({
                message: t('referSuccess'),
                description: t('referSuccess'),
            });
            refreshErrors();
            navigate(-1)
        }
    }, [done]);

    return (
        <Modal 
            visible={visible} 
            title={t('select_advisor')} 
            footer={[
                <Button key="cancel" onClick={onCancel} style={{ background: OrangeColor, color: WhiteColor }}>
                    {t('no')}
                </Button>,
                <Button 
                    key="ok" 
                    type="primary" 
                    onClick={() => onOk(selectedAdvisorId)} 
                    style={{ background: OrangeColor, color: WhiteColor }}
                >
                    {isReferLoading ? <LoadingBar /> : t('ok')}
                </Button>
            ]}
        >
            {isLoading ? <Spinner /> : (
                <Select
                    placeholder={t('select_advisor')}
                    onChange={(value) => setSelectedAdvisorId(value)}
                    style={{ width: 250 }}
                >
                    {Array.isArray(advisors) && advisors.map((advisor) => (
                        topic === advisor.topics &&  <Option key={advisor.id} value={advisor.id}>
                            {advisor.name} | {advisor.topics}
                        </Option>
                    ))}
                </Select>
            )}
        </Modal>
    );
}

export default AdvisorSelectingModal;
