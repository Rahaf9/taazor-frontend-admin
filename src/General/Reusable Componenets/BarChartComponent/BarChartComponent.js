import React from 'react';
import { BarChart, CartesianGrid, XAxis, YAxis, Bar, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const BarChartComponent = ({ data, title }) => {
    return (
        <div className="peopleHired">
            <div className="text-percentage">
                {title}
            </div>
            <div className="percentage-column">
                <ResponsiveContainer width="100%" height={300}>
                    <BarChart data={data} layout="vertical">
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis type="number" dataKey="percent" />
                        <YAxis type="category" dataKey="name" /> 
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="percent" fill="#82CA9D" />
                    </BarChart>
                </ResponsiveContainer>
            </div>
        </div>
    );
}

export default BarChartComponent;
