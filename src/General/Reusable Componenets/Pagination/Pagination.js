import React from 'react';
import { Pagination as AntdPagination } from 'antd';
import './Pagination.css';

const Pagination = ({ currentPage, totalPages, onPageChange }) => {
  const handlePageChange = (page) => {
    console.log(`the page is ${page} and the current page is ${currentPage}`)
    if (page !== currentPage) {
      console.log(`must beclicked`)
      onPageChange(page);
    }
  };

  return (
    <AntdPagination
      current={currentPage}
      total={totalPages * 10} // `antd` requires total items, assuming 10 items per page
      pageSize={10} // Adjust based on your API
      showSizeChanger={false} // Hide the size changer
      onChange={handlePageChange}
    />
  );
};

export default Pagination;
















// import React from 'react';

// const Pagination = ({ currentPage, totalPages, onPageChange }) => {
//   const handlePrevious = () => {
//     if (currentPage > 1) {
//       onPageChange(currentPage - 1);
//     }
//   };

//   const handleNext = () => {
//     if (currentPage < totalPages) {
//       onPageChange(currentPage + 1);
//     }
//   };

//   const handlePageClick = (page) => {
//     if (page !== currentPage) {
//       onPageChange(page);
//     }
//   };

//   const renderPageNumbers = () => {
//     let pages = [];
//     for (let i = 1; i <= totalPages; i++) {
//       pages.push(
//         <button
//           key={i}
//           onClick={() => handlePageClick(i)}
//           className={i === currentPage ? 'active' : ''}
//         >
//           {i}
//         </button>
//       );
//     }
//     return pages;
//   };

//   return (
//     <div className="pagination">
//       <button onClick={handlePrevious} disabled={currentPage === 1}>
//         &laquo; Previous
//       </button>
//       {renderPageNumbers()}
//       <button onClick={handleNext} disabled={currentPage === totalPages}>
//         Next &raquo;
//       </button>
//     </div>
//   );
// };

// export default Pagination;
