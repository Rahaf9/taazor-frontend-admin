import React from "react";
import { MailFilled } from '@ant-design/icons';
import { WhiteColor } from "../Styles/colors";
import { CurrentCourseDetailsNormalStyle } from "../Styles/texts";

const IconTextEntry = ({ icon, text }) => {
  return (
    <div className={`w-100`} style={{ display: 'flex', justifyContent: 'space-between',height:'60px',alignContent:'center' }}>
      <div className={`w-25 h-100`} style={{ display: 'flex', alignItems: 'center' }}>
        {icon}
      </div>
      <div className={`w-75 h-100`} style={CurrentCourseDetailsNormalStyle}>
        {text}
      </div>
    </div>
  );
};

export default IconTextEntry;
