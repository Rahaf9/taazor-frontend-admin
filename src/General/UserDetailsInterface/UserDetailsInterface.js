import React, { useEffect, useState } from "react";
import { horizAroundAlignCenterColumn, horizVerFlex, horizVerFlexColumn } from "../Styles/general styles";
import { WhiteColor, lightGreenPrimary, lightGreenSecOpacity } from "../Styles/colors";
import { UserDetailsDesceStyle, UserDetailsNormalStyle, UserDetailsTitleStyle } from "../Styles/texts";
import { MailOutlined, FlagOutlined, ManOutlined, EnvironmentOutlined, GiftOutlined, PhoneOutlined } from '@ant-design/icons';
import IconTextEntry from "../IconTextEntry/IconTextEntry";
import StatusEntry from "../StatusEntry/StatusEntry";
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import './UserDetailsInterface.css'
import Spinner from "../Reusable Componenets/Spinner/Spinner";
import WaitingComponent from "../../compnents/WaitingComponent/WaitingComponent";

const UserDetailsInterface = ({ detailsAction, selectorAction, additionalContent }) => {

    const disptach = useDispatch();
    const details = useSelector(selectorAction.detailsSelector)
    const isLoading = useSelector(selectorAction.loadingSelector);
    const done = useSelector(selectorAction.doneSelector);
    const { t } = useTranslation();

    // here you must add the action you want to get the user details
    const { index } = useParams();

    useEffect(() => {
        console.log(`this is the index of the user job we want to get its details ${index}`)
        disptach(detailsAction(index))
        console.log(`${JSON.stringify(details)}`)
    }, []);

    // if (!details) {
    //     return null; // If details is null, return null to render nothing
    // }

    // Function to safely parse JSON
    // const safeParseJSON = (jsonString) => {
    //     try {
    //         console.log(`the topic is ${details.topic}`)
    //         return JSON.parse(jsonString);
    //     } catch (error) {
    //         console.error("Error parsing JSON:", error);
    //         return [];
    //     }
    // };

    // // Parse the topic field
    // const topics = safeParseJSON(details?.topic) ?? [];

    return (
        <div className="w-100 h-100">
            {
                isLoading ? <Spinner /> : !done ? <WaitingComponent title={'errorOccuered'} /> :
                    <div className={`horizBetweenJobUsersDetails mt-5`}>
                        <div className={`UserDetailsLeftSide ${horizVerFlexColumn}`}>
                            <div className="p-4" style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                                <div style={UserDetailsTitleStyle}>
                                    {t('basicUserInformation')}
                                </div>
                                <div className={`h-25 mt-3 ${horizVerFlex}`} style={{
                                    background: lightGreenSecOpacity,
                                    borderRadius: '20PX'
                                }}>
                                    <div className={`w-50 h-75 ${horizVerFlex}`}>
                                        <img className="fixed-size-img" src={details?.image ? details?.image : "/images/the light green 2/People search-amico.png"} alt="User" />
                                    </div>

                                    <div className={`w-50 h-100 ${horizAroundAlignCenterColumn} py-3`} style={UserDetailsDesceStyle}>
                                        <div>
                                            {`${details?.first_name} ${details?.last_name}` ?? ''} {/* Safely access nested property */}
                                        </div>
                                        {/* details.BasicInfo?.status */}
                                        <div>
                                            {details?.marital_status ?? ''}
                                        </div>
                                    </div>
                                </div>
                                {/* this is for the icons and the texts info */}
                                <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '64%' }}>
                                    <IconTextEntry icon={<MailOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details?.email ?? ''} />
                                    <IconTextEntry icon={<FlagOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details?.nationality ?? ''} />
                                    <IconTextEntry icon={<ManOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details?.gender ?? ''} />
                                    <IconTextEntry icon={<EnvironmentOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details?.address ?? ''} />
                                    <IconTextEntry icon={<GiftOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details?.birthday ?? ''} />
                                    <IconTextEntry icon={<PhoneOutlined style={{ color: WhiteColor, fontSize: '17px' }} />} text={details?.phone ?? ''} />
                                </div>
                            </div>
                            <div className="h-25 w-50 bg-light">
                                {additionalContent(details?.is_applied)}
                            </div>
                        </div>
                        <div className={`UserDetailsRightSide ${horizVerFlexColumn}`}>
                            <div className={`h-50 p-4`} style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                                <div style={UserDetailsTitleStyle}>
                                    {t('personalInformation')}
                                </div>
                                {/* details.PersonalInfo?.militaryStatus */}
                                <div className={`w-100 d-flex mt-4 ${horizAroundAlignCenterColumn}`} style={{ height: '64%' }}>
                                    <StatusEntry title={t('military_status')} text={details?.military_status ?? ''} />
                                    <StatusEntry title={t('drive_license')} text={details?.driving_license == '0' ? 'Not Exist' : 'Exist'} />
                                    <StatusEntry title={t('topic')} text={details?.topic?.join(' | ') ?? ''} />
                                    <StatusEntry title={t('experience')} text={`${details?.experience_years} years` ?? ''} />
                                    <StatusEntry title={t('education')} text={details?.education ?? ''} />
                                </div>
                            </div>
                            <div className={`h-50 p-4 mt-3`} style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                                <div style={UserDetailsTitleStyle}>
                                    {t('jobRequirements')}
                                </div>
                                {/* details.JobRequirements?.email */}
                                {details?.job_envirnoment ?
                                    <div className={`w-100 d-flex mt-4 ${horizAroundAlignCenterColumn}`} style={{ height: '64%' }}>
                                        <StatusEntry title={t('job_environment')} text={details?.job_envirnoment ?? ''} />
                                        <StatusEntry title={t('job_time')} text={details?.job_time ?? ''} />
                                        <StatusEntry title={t('cityOfWork')} text={details?.cityOfWork ?? ''} />
                                        <StatusEntry title={t('working?')} text={details?.working ?? ''} />
                                    </div> : <div className={`w-100 h-100 ${horizVerFlex} p-4`}
                                        style={{
                                            color: WhiteColor
                                        }}>No Required {t('jobRequirements')}</div>}
                            </div>
                        </div>
                    </div>
            }
        </div>
    )

}

export default UserDetailsInterface;
