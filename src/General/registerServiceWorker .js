import requestPermission from "../firebase/requestPermission";

export const registerServiceWorker = async () => {
    if ('serviceWorker' in navigator && 'PushManager' in window) {
      try {
        const registration = await navigator.serviceWorker.register('/firebase-messaging-sw3.js');
        console.log('Service Worker registered with scope:', registration.scope);
  
        // Only call requestPermission after the service worker is ready
        const readyRegistration = await navigator.serviceWorker.ready;
        console.log(`Service worker is ready: ${readyRegistration}`);
        return readyRegistration;
      } catch (error) {
        console.error('Service Worker registration failed:', error);
        throw error; // Re-throw error to handle it where you call registerServiceWorker
      }
    } else {
      console.error("This browser doesn't support service workers or push notifications.");
      throw new Error("Service Worker or Push Manager not supported");
    }
  };

export default registerServiceWorker;