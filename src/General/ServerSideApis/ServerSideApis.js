export const domainServer = 'http://86.38.218.161:8080/api/manager';
// export const domainServer = 'https://tazzur.life/api/manager'
export const registerApi = '/register';
export const loginApi = '/login';
export const emailStepApi = '/forget_password';
export const newPasswordApi = '/confirm_reset_password';
export const verifyCode = '/verify_code';
export const completeInfoApi = '/complete_register';
export const consulutions = '/consulutions';
export const addConsulution = '/consulutions';
export const consulutionDetails = '/consulutions';
export const jobDetailsApi = "/jobs";
export const courseDetailsApi = "/courses";
export const jobsApi = '/jobs'
export const coursesApi = '/courses';
export const addCourseApi = '/courses';
export const addJobApi = '/jobs';
export const addNewPollApi = '/addNewPollApi'
export const getStatisticssApi = '/getStatistics'


export const userCourseApi = '/course_applications';
export const userJobApi = '/job_applications';
export const userCourseDetailsApi = '/course_application_details';
export const userJobDetailsApi = '/job_application_details';

export const companiesApi = "/companies";
export const changeCompanyStateApi = '/set_status';
export const seekers = "/seekers";
export const seeker = "/seeker";
export const cv = "/cv";
export const advisorsApi = "/advisors";
export const getAdvisors = "/get_advisors"
export const blockUnblockSeeker = "/block_unblock_seeker"
export const coordinators = "/coordinators"
export const notifications = "/notifications"
export const readAll = '/read-all'
export const deleteNoti = '/delete'
export const deleteAllNoti = '/delete-all'

export const companyConsulutions = '/CompanyConsulting';
export const userConsulting = '/UserConsulting';
export const convertApi = "/convert"
export const consultationsApi = "/consultations";
export const downloadDocs = "/download_docs"
export const userCvApi = '/userCvApi'
export const acceptRefuseJob = '/accept_refuse_job'
export const applications = '/applications'
export const priority = '/priority'
export const convert = '/convert'

