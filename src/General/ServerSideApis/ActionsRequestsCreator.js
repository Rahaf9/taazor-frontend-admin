// specificActions.js
import { getRequestAction, requestAction } from '../../actions/AllAdminActions';
import { acceptRefuse, acceptRefuseJob, advisorsApi, applications, blockUnblockSeeker, changeCompanyStateApi, companiesApi, convert, coordinators, course, coursesApi, detailsApplication, domainServer, jobsApi, priority } from './ServerSideApis';


// Action creator for unblocking a user
export const unblockUserAction = (userId) => {
    const url = `${domainServer}${blockUnblockSeeker}/${userId}`;
    const method = 'POST';
    const data = {
        'status': 'active'
    }
    return requestAction(url, method, data);
};

export const blcokUserAction = (userId) => {
    const url = `${domainServer}${blockUnblockSeeker}/${userId}`;
    const method = 'POST';
    const data = {
        'status': 'block'
    }
    return requestAction(url, method, data);
};

export const deleteAdvisorAction = (userId) => {
    const url = `${domainServer}${advisorsApi}/${userId}`;
    const method = 'DELETE';
    return requestAction(url, method);
};

export const deleteCoordinatorAction = (userId) => {
    const url = `${domainServer}${coordinators}/${userId}`;
    const method = 'DELETE';
    return requestAction(url, method);
};

export const acceptWaitingCompanyAction = (userId) => {
    const url = `${domainServer}${companiesApi}${changeCompanyStateApi}/${userId}`;
    const method = 'POST';
    const data = {
        'status':'acceptable'
    }
    return requestAction(url, method,data);
};

export const rejectWaitingCompanyAction = (userId) => {
    const url = `${domainServer}${companiesApi}${changeCompanyStateApi}/${userId}`;
    const method = 'POST';
    const data = {
        'status':'rejected'
    }
    return requestAction(url, method, data);
};

export const unBlockBlockedCompanyAction = (userId) => {
    const url = `${domainServer}${companiesApi}${changeCompanyStateApi}/${userId}`;
    const method = 'POST';
    const data = {
        'status':'acceptable'
    }
    return requestAction(url, method, data);
};


export const blockAcceptedCompanyAction = (userId) => {
    const url = `${domainServer}${companiesApi}${changeCompanyStateApi}/${userId}`;
    const method = 'POST';
    const data = {
        'status':'banned'
    }
    return requestAction(url, method, data);
};

export const getAdvisorDetailsAction = (userId) => {
    const url = `${domainServer}${advisorsApi}/${userId}`;
    const method = 'GET';
    return getRequestAction(url, method);
};

export const updateAdvisorAction = (userId,data) => {
    const url = `${domainServer}${advisorsApi}/${userId}`;
    const method = 'PUT';
    return requestAction(url, method, data);
};

export const getCoordinatorDetailsAction = (userId) => {
    const url = `${domainServer}${coordinators}/${userId}`;
    const method = 'GET';
    return getRequestAction(url, method);
};

export const updateCoordinatorsAction = (userId,data) => {
    const url = `${domainServer}${coordinators}/${userId}`;
    const method = 'PUT';
    return requestAction(url, method, data);
};

export const deleteCurrentCourseAction = (courseId) => {
    const url = `${domainServer}${coursesApi}/${courseId}`;
    const method = 'DELETE';
    return requestAction(url, method);
};

export const deleteCurrentJobAction = (jobId) => {
    const url = `${domainServer}${jobsApi}/${jobId}`;
    const method = 'DELETE';
    return requestAction(url, method);
};

export const acceptJobAction = (jobId) => {
    const url = `${domainServer}${acceptRefuseJob}/${jobId}`;
    const method = 'POST';
    const data = {
        'status':'accepted'
    }
    return requestAction(url, method, data);
};

export const rejectJobAction = (jobId) => {
    const url = `${domainServer}${acceptRefuseJob}/${jobId}`;
    const method = 'POST';
    const data = {
        'status':'rejected'
    }
    return requestAction(url, method, data);
};

export const deleteCompanyAccountAction = (companyID) => {
    const url = `${domainServer}${companiesApi}/${companyID}`;
    const method = 'DELETE';
    return requestAction(url, method);
};

export const convertJobUserAction = (userID,priorityValue) => {
    console.log(`the index of user is ${userID} and the priority value is ${priorityValue}`)
    const url = `${domainServer}${applications}${priority}/${userID}/${priorityValue}`;
    const method = 'POST';
    return requestAction(url, method);
};

export const convertAllJobUserAction = (jobId) => {
    console.log(`the index of job is ${jobId}`)
    const url = `${domainServer}${applications}${convert}/${jobId}`;
    const method = 'POST';
    return requestAction(url, method);
};
