// specificActions.js
import { companyProfileRequestAction } from '../../actions/CompaniesProfileAction';
import { companiesApi,domainServer } from './ServerSideApis';


// Action creator for unblocking a user
export const CompanyProfileAction = (userId) => {
    const url = `${domainServer}${companiesApi}/${userId}`;
    const method = 'GET';
    return companyProfileRequestAction(url, method);
};

