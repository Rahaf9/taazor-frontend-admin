import React from 'react';
import './UserCard.css'
import { horizBetweenAlignCenter, horizBetweenAlignCenterColumn, horizVerFlex } from '../Styles/general styles';
import { WhiteColor, lightGreenPrimary } from '../Styles/colors';
import { CardReuseTitleStyle, UserCardButtonStyle, UserCardButtonTextStyle } from '../Styles/texts';
import { Button } from 'antd';
import { ROUTES } from '../Routes/Route';
import { useNavigate } from 'react-router-dom';

// Reusable Card Component
const BlockedUserCard = ({ data,myIndex,routePath1 }) => {

    const navigate  = useNavigate();
    
    const handleDetailsIconClick = (index) => {
        console.log("Details Icon in Blocked user details clicked at index:", index);
        // Do whatever you need with the index
        navigate(`${routePath1}/${index}`)
    };

    return (
        <div className='UserCard'>
            <div className={`h-50 uppercard ${horizVerFlex}`}>
                {<img style={{ height: '90%', width: '28%' }} src={`${data.BasicInfo.image}`}></img>}
            </div>

            <div className={`h-25 bottomcard p-2 ${horizBetweenAlignCenter}`}>
                <div className={`w-100 h-100 m-2 ${horizVerFlex}`}>
                    <span className={`w-50 h-100 text-center m-2 ${horizVerFlex}`} style={CardReuseTitleStyle}>{data.BasicInfo.name}</span>
                    <span className={`w-50 h-100 text-center m-2 ${horizVerFlex}`} style={CardReuseTitleStyle}>{data.PersonalInfo.topic}</span>
                </div>
            </div>
            {/* you must to handle the buttons appearing here and to pass to this
            component a new function that handle the click event */}
            <div className={`h-25 p-2 ${horizBetweenAlignCenter}`}>
                <Button style={UserCardButtonStyle} onClick={() => handleDetailsIconClick(myIndex)}>Details</Button>
            </div>
        </div>
    );
};

export default BlockedUserCard;