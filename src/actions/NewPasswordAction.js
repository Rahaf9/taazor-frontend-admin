// actionTypes.js
import axios from "axios";
import { domainServer, newPasswordApi, loginApi } from "../General/ServerSideApis/ServerSideApis";
import { localStorageEmailValue } from "../General/Services Functions/Local Storage Functions";

export const SET_NEW_PASSWORD_FAILURE = 'SET_NEW_PASSWORD_FAILURE';
export const SET_NEW_PASSWORD_SUCCESS = 'SET_NEW_PASSWORD_SUCCESS';
export const SET_NEW_PASSWORD_REFRESH = 'SET_NEW_PASSWORD_REFRESH';
export const SET_NEW_PASSWORD_LOADING = 'SET_NEW_PASSWORD_LOADING';

// actionCreators.js

export const newPasswordFailure = (errors) => ({
  type: SET_NEW_PASSWORD_FAILURE,
  payload: errors,
});
  

export const newPasswordSuccess = (data) => ({
  type: SET_NEW_PASSWORD_SUCCESS,
  payload: data,
});

export const newPasswordRefreshPage = () => ({
  type: SET_NEW_PASSWORD_REFRESH
});

export const newPasswordLoading = (state) => ({
  type: SET_NEW_PASSWORD_LOADING,
  payload:state
});

export const newPasswordAction = (formData) => {
  
  return async (dispatch) => {

    dispatch(newPasswordLoading(true));
    
    try {
      console.log(`the email recived in newPassword action is ${localStorage.getItem('email')}`)
      const response = await axios.post(`${domainServer}${newPasswordApi}`, {
        'password':formData.password,
        'password_confirmation':formData.confirmPassword,
        'email':localStorage.getItem('email')
      });

      if (response.status === 200) {
        dispatch(newPasswordSuccess(response.data.data));
        console.log(`the status code is ${response.status}`)
        localStorage.removeItem('code')
        localStorage.removeItem('email')
      } else {
        dispatch(newPasswordFailure(response.data.message));
      }
    } catch (error) {

       if (error.response.status === 422) {
        console.error(`${JSON.stringify(error.response.data.errors.email)}`);
        dispatch(newPasswordFailure({'message':error.response.data.errors.email})); 
      } else{
        console.error('Error occurred during new password step:', error.response.data);
        dispatch(newPasswordFailure(error.response.data));
      }

    }

    dispatch(newPasswordLoading(false));
  }
};
