import axios from 'axios';
import { addConsulution, advisorsApi, consultationsApi, consulutionDetails, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';
import { TimePicker } from 'antd';

export const SET_ADIVSOR_CONSULTANT_DETAILS_FAILURE = 'SET_ADIVSOR_CONSULTANT_DETAILS_FAILURE';
export const SET_ADIVSOR_CONSULTANT_DETAILS_SUCCESS = 'SET_ADIVSOR_CONSULTANT_DETAILS_SUCCESS';
export const SET_ADIVSOR_CONSULTANT_DETAILS_REFRESH = 'SET_ADIVSOR_CONSULTANT_DETAILS_REFRESH';
export const SET_ADIVSOR_CONSULTANT_DETAILS_LOADING = 'SET_ADIVSOR_CONSULTANT_DETAILS_LOADING';

export const advisorConsultantDetailsFailure = (errors) => ({
    type: SET_ADIVSOR_CONSULTANT_DETAILS_FAILURE,
    payload: errors,
});

export const advisorConsultantDetailsSuccess = (data) => ({
    type: SET_ADIVSOR_CONSULTANT_DETAILS_SUCCESS,
    payload: data,
});

export const advisorConsultantDetailsRefreshPage = () => ({
    type: SET_ADIVSOR_CONSULTANT_DETAILS_REFRESH
});

export const advisorConsultantDetailsLoading = (state) => ({
    type: SET_ADIVSOR_CONSULTANT_DETAILS_LOADING,
    payload: state
});

export const advisorConsultantDetailsAction = (advisor_id,userId,type) => {

    console.log(`the advisor_id in detailed consultion action is ${advisor_id}`)

    return async (dispatch) => {

        dispatch(advisorConsultantDetailsLoading(true));

        try {
            const response = await axios.get(`${domainServer}${advisorsApi}${consultationsApi}/${userId}/${advisor_id}?type=${type}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`the advisor company consultant details on advisor_id ${advisor_id} is these data ${
                    JSON.stringify(response.data.data)
                }`)
                dispatch(advisorConsultantDetailsSuccess(response.data.data));
            } else {
                dispatch(advisorConsultantDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed consultants ${JSON.stringify(error.response.data)}`)
                dispatch(advisorConsultantDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed consultion:', error.response.data);
                dispatch(advisorConsultantDetailsFailure(error.response.data));
            }

        }

        // dispatch(advisorConsultantDetailsLoading(false));
    }
};
