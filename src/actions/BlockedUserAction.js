import axios from "axios";
import { users } from "../FakeData";
import { domainServer, seekers } from "../General/ServerSideApis/ServerSideApis";

export const BOLCKED_USERS_SUCCESS = "BOLCKED_USERS_SUCCESS";
export const BOLCKED_USERS_FAILURE = "BOLCKED_USERS_FAILURE";
export const BOLCKED_USERS_LOADING = "BOLCKED_USERS_LOADING";
export const BOLCKED_USERS_REFRESH = "BOLCKED_USERS_REFRESH";

export const blockedUsersSuccess = (users) => ({
    type: BOLCKED_USERS_SUCCESS,
    payload: users
});
export const blockedUsersFailure = (errors) => ({
    type: BOLCKED_USERS_FAILURE,
    payload: errors,
  });

  export const blockedUsersLoading = (state) => ({
    type: BOLCKED_USERS_LOADING,
    payload: state
  });
  

  export const blockedUsersAction = (index,page) => {
  
    return async (dispatch) => {
  
      dispatch(blockedUsersLoading(true));
      console.log(`the token of me in blockedUsers  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${seekers}?status=banned&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the success of blocked users is ${JSON.stringify(response.data.data)}`)
            dispatch(blockedUsersSuccess(response.data.data));
        } else {
          dispatch(blockedUsersFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your blockedUsers ${JSON.stringify(error.response.data)}`)
          dispatch(blockedUsersFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your blockedUsers:', error.response.data);
          dispatch(blockedUsersFailure(error.response.data));
        }
  
      }
  
      dispatch(blockedUsersLoading(false));
    }
  };
  