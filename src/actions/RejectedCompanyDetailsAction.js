import axios from 'axios';
import { addConsulution, advisorDetails, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const REJECTED_COMPANIES_DETAILS_FAILURE = 'REJECTED_COMPANIES_DETAILS_FAILURE';
export const REJECTED_COMPANIES_DETAILS_SUCCESS = 'REJECTED_COMPANIES_DETAILS_SUCCESS';
export const REJECTED_COMPANIES_DETAILS_REFRESH = 'REJECTED_COMPANIES_DETAILS_REFRESH';
export const REJECTED_COMPANIES_DETAILS_LOADING = 'REJECTED_COMPANIES_DETAILS_LOADING';

export const rejectedCompaiesDetailsFailure = (errors) => ({
    type: REJECTED_COMPANIES_DETAILS_FAILURE,
    payload: errors,
});

export const rejectedCompaiesDetailsSuccess = (data) => ({
    type: REJECTED_COMPANIES_DETAILS_SUCCESS,
    payload: data,
});

export const rejectedCompaiesDetailsRefreshPage = () => ({
    type: REJECTED_COMPANIES_DETAILS_REFRESH
});

export const rejectedCompaiesDetailsLoading = (state) => ({
    type: REJECTED_COMPANIES_DETAILS_LOADING,
    payload: state
});

export const rejectedCompaiesDetailsAction = (index) => {

    console.log(`the index in detailed advisor action is ${index}`)

    return async (dispatch) => {

        dispatch(rejectedCompaiesDetailsLoading(true));

        try {
            const response = await axios.post(`${domainServer}${consulutionDetails}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                dispatch(rejectedCompaiesDetailsSuccess(response.data.data));
            } else {
                dispatch(rejectedCompaiesDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed advisor ${JSON.stringify(error.response.data)}`)
                dispatch(rejectedCompaiesDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed advisor:', error.response.data);
                dispatch(rejectedCompaiesDetailsFailure(error.response.data));
            }

        }

        dispatch(rejectedCompaiesDetailsLoading(false));
    }
};
