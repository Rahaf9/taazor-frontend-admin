import axios from 'axios';
import { domainServer, completeInfoApi } from '../General/ServerSideApis/ServerSideApis';

export const SET_COMPLETE_INFO_FAILURE = 'SET_COMPLETE_INFO_FAILURE';
export const SET_COMPLETE_INFO_SUCCESS = 'SET_COMPLETE_INFO_SUCCESS';
export const SET_COMPLETE_INFO_REFRESH = 'SET_COMPLETE_INFO_REFRESH';
export const SET_COMPLETE_INFO_LOADING = 'SET_COMPLETE_INFO_LOADING';

export const completeinfoFailure = (errors) => ({
  type: SET_COMPLETE_INFO_FAILURE,
  payload: errors,
});

export const completeinfoSuccess = (data) => ({
  type: SET_COMPLETE_INFO_SUCCESS,
  payload: data,
});

export const completeinfoRefreshPage = () => ({
  type: SET_COMPLETE_INFO_REFRESH
});

export const completeinfoLoading = (state) => ({
  type: SET_COMPLETE_INFO_LOADING,
  payload:state
});

export const completeinfoAction = (formData) => {
  
  return async (dispatch) => {

    console.log(`the formData in complete info is ${JSON.stringify(formData)}`)
    dispatch(completeinfoLoading(true));
    
    try {
      const response = await axios.post(`${domainServer}${completeInfoApi}`, formData,{
        headers: {
          'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
      });

      if (response.status === 200) {
        console.log(`here in complete info we got 200 code`)
        dispatch(completeinfoSuccess(response.data.data));
      } else {
        console.log(`here in complete info we got 2** code and its data is ${JSON.stringify(response.data)}`)
        dispatch(completeinfoFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 409) {
        console.error(`errors completeinfo ${error.response.data.message}`);
        dispatch(completeinfoFailure(error.response.data));
      } else if (error.response.status === 400) {
        console.error(`errors completeinfo ${error.response.data.message}`);
        dispatch(completeinfoFailure(error.response.data)); // the form of the error.response.data is "message": "adkasfhksfh"
      }
      else if (error.response.status === 422) {
        console.error(`errors completeinfo ${JSON.stringify(error.response.data.errors)}`);
        dispatch(completeinfoFailure(error.response.data.errors)); //the form of the error.response.data is  {"password": [ "The password field confirmation does not match." ]}
      } else {
        console.error('Error occurred during complete registration:', error.response.data);
        dispatch(completeinfoFailure(error.response.data));
      }

    }

    dispatch(completeinfoLoading(false));
  }
};
