import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, coursesApi } from "../General/ServerSideApis/ServerSideApis";

export const CURRENT_COURSES_SUCCESS = "CURRENT_COURSES_SUCCESS";
export const CURRENT_COURSES_FAILURE = "CURRENT_COURSES_FAILURE";
export const CURRENT_COURSES_LOADING = "CURRENT_COURSES_LOADING";
export const CURRENT_COURSES_REFRESH = "CURRENT_COURSES_REFRESH";

export const CurrentCourseSuccess = (details) => ({
    type: CURRENT_COURSES_SUCCESS,
    payload: details
});

export const CurrentCourseFailure = (errors) => ({
    type: CURRENT_COURSES_FAILURE,
    payload: errors,
  });


export const CurrentCourseRefreshPage = () => ({
    type: CURRENT_COURSES_REFRESH
  });
  
  export const CurrentCourseLoading = (state) => ({
    type: CURRENT_COURSES_LOADING,
    payload: state
  });
  
  export const CurrentCourseAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(CurrentCourseLoading(true));
      console.log(`the token of me in current Courses  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${coursesApi}?status=current&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`this is the current courses data ${JSON.stringify(response.data.data)}`)
            dispatch(CurrentCourseSuccess(response.data.data));
        } else {
          dispatch(CurrentCourseFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your current Courses ${JSON.stringify(error.response.data)}`)
          dispatch(CurrentCourseFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your current Courses:', error.response.data);
          dispatch(CurrentCourseFailure(error.response.data));
        }
      }
  
      dispatch(CurrentCourseLoading(false));
    }
  };
  