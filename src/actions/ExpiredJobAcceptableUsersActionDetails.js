import axios from "axios";
import { users } from "../FakeData";
import { domainServer, userJobDetailsApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_SUCCESS = "EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_SUCCESS";
export const EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_FAILURE = "EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_FAILURE";
export const EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_LOADING = "EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_LOADING";
export const EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_REFRESH = "EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_REFRESH";

export const ExpiredJobAcceptableUserDetailsSuccess = (users) => ({
    type: EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_SUCCESS,
    payload: users
});

export const ExpiredJobAcceptableUserDetailsFailure = (errors) => ({
    type: EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_FAILURE,
    payload: errors,
});


export const ExpiredJobAcceptableUserDetailsRefreshPage = () => ({
    type: EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_REFRESH
});

export const ExpiredJobAcceptableUserDetailsLoading = (state) => ({
    type: EXPIRED_JOB_ACCEPTABLE_USER_DETAILS_LOADING,
    payload: state
});


export const ExpiredJobAcceptableUserDetailsAction = (index) => {

    return async (dispatch) => {

        console.log(`this is the index we want to get its user details ${JSON.stringify(index)}`)

        dispatch(ExpiredJobAcceptableUserDetailsLoading(true));

        try {
            // replace the ?status=Done with the correct api from the backend 
            const response = await axios.get(`${domainServer}${userJobDetailsApi}/${index}?status=accepted`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`this is the course user details data ${JSON.stringify(response.data.data)}`)
                dispatch(ExpiredJobAcceptableUserDetailsSuccess(response.data.data));
            } else {
                dispatch(ExpiredJobAcceptableUserDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in your course users ${JSON.stringify(error.response.data)}`)
                dispatch(ExpiredJobAcceptableUserDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during your course users:', error.response.data);
                dispatch(ExpiredJobAcceptableUserDetailsFailure(error.response.data));
            }

        }

        dispatch(ExpiredJobAcceptableUserDetailsLoading(false));

        // dispatch(CurrentUserDetailsSuccess(users));

    }

}