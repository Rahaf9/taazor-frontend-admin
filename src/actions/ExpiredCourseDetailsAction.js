import { jobDetails } from "../FakeData";
import axios from 'axios';
import { courseDetailsApi, domainServer, jobsApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_COURSE_DETAILS_SUCCESS = "EXPIRED_COURSE_DETAILS_SUCCESS";
export const EXPIRED_COURSE_DETAILS_FAILURE = "EXPIRED_COURSE_DETAILS_FAILURE";
export const EXPIRED_COURSE_DETAILS_LOADING = "EXPIRED_COURSE_DETAILS_LOADING";
export const EXPIRED_COURSE_DETAILS_REFRESH = "EXPIRED_COURSE_DETAILS_REFRESH";

export const ExpiredCourseDetailsSuccess = (details) => ({
    type: EXPIRED_COURSE_DETAILS_SUCCESS,
    payload: details
});

export const ExpiredCourseDetailsFailure = (errors) => ({
    type: EXPIRED_COURSE_DETAILS_FAILURE,
    payload: errors,
  });


export const ExpiredCourseDetailsRefreshPage = () => ({
    type: EXPIRED_COURSE_DETAILS_REFRESH
  });
  
  export const ExpiredCourseDetailsLoading = (state) => ({
    type: EXPIRED_COURSE_DETAILS_LOADING,
    payload: state
  });
  
  export const ExpiredCourseDetailsAction = (index) => {
  
    return async (dispatch) => {
  
      dispatch(ExpiredCourseDetailsLoading(true));
      console.log(`the token of me in expired course details is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${courseDetailsApi}/${index}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the Expired course details is ${JSON.stringify(response.data.data)}`)
            dispatch(ExpiredCourseDetailsSuccess(response.data.data));
        } else {
          dispatch(ExpiredCourseDetailsFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your Expired course details ${JSON.stringify(error.response.data)}`)
          dispatch(ExpiredCourseDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your consultions:', error.response.data);
          dispatch(ExpiredCourseDetailsFailure(error.response.data));
        }
  
      }
  
      dispatch(ExpiredCourseDetailsLoading(false));
    }
  };
  