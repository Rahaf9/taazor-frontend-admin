// requestActions.js
import axios from 'axios';

export const COMPANIES_PROFILE_ACTION_SUCCESS = 'COMPANIES_PROFILE_ACTION_SUCCESS';
export const COMPANIES_PROFILE_ACTION_FAILURE = 'COMPANIES_PROFILE_ACTION_FAILURE';
export const COMPANIES_PROFILE_ACTION_LOADING = 'COMPANIES_PROFILE_ACTION_LOADING';
export const COMPANIES_PROFILE_ACTION_REFRESH_PAGE = 'COMPANIES_PROFILE_ACTION_REFRESH_PAGE';

export const companiesProfileActionFailure = (errors) => ({
    type: COMPANIES_PROFILE_ACTION_FAILURE,
    payload: errors,
});

export const companiesProfileActionSuccess = (data) => ({
    type: COMPANIES_PROFILE_ACTION_SUCCESS,
    payload: data,
});

export const companiesProfileActionRefreshPage = () => ({
    type: COMPANIES_PROFILE_ACTION_REFRESH_PAGE
});

export const companiesProfileActionLoading = (state) => ({
    type: COMPANIES_PROFILE_ACTION_LOADING,
    payload: state
});

const fixedHeaders = {
    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
};

export const companyProfileRequestAction = (url, method, data = null) => async (dispatch) => {

    dispatch(companiesProfileActionLoading(true));

    try {
        const response = await axios({ url, method, data, headers: fixedHeaders });

        if (response.status === 200) {
            console.log(`the data is susccess ${JSON.stringify(response.data.data)}`)
            dispatch(companiesProfileActionSuccess(response.data.data));
        } else {
            dispatch(companiesProfileActionFailure(response.data.message));
        }
    } catch (error) {
        if (error.response.status === 401) {
            console.log(`i got 401 in all admin actions ${JSON.stringify(error.response.data)}`)
            dispatch(companiesProfileActionFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
          } else if (error.response.status === 422) {
            console.error(`errors all admin actions ${JSON.stringify(error.response.data.errors)}`);
            dispatch(companiesProfileActionFailure({'message':error.response.data.message})); //the form of the error.response.data is  {"password": [ "The password field confirmation does not match." ]}
          } else {
            console.error('Error occurred during waiting company ptofile:', error.response.data);
            dispatch(companiesProfileActionFailure(error.response.data));
          }
    }

    dispatch(companiesProfileActionLoading(false));
};