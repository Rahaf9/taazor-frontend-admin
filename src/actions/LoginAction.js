// actionTypes.js
import axios from "axios";
import { domainServer, loginApi } from "../General/ServerSideApis/ServerSideApis";

export const SET_LOGIN_FAILURE = 'SET_LOGIN_FAILURE';
export const SET_LOGIN_SUCCESS = 'SET_LOGIN_SUCCESS';
export const SET_LOGIN_REFRESH = 'SET_LOGIN_REFRESH';
export const SET_LOGIN_LOADING = 'SET_LOGIN_LOADING';

// actionCreators.js

export const loginFailure = (errors) => ({
  type: SET_LOGIN_FAILURE,
  payload: errors,
});


export const loginSuccess = (data) => ({
  type: SET_LOGIN_SUCCESS,
  payload: data,
});

export const loginRefreshPage = () => ({
  type: SET_LOGIN_REFRESH
});

export const loginLoading = (state) => ({
  type: SET_LOGIN_LOADING,
  payload: state
});

export const loginAction = (formData) => {

  return async (dispatch) => {

    dispatch(loginLoading(true));

    formData["fcm_token"] = localStorage.getItem('firebaseToken');
    console.log(`the data login before send is ${JSON.stringify(formData)}`)
    
    try {
      const response = await axios.post(`${domainServer}${loginApi}`, formData);

      if (response.status === 200) {
        dispatch(loginSuccess(response.data.data));
        localStorage.setItem('roleName', response.data.data.role_name);
        console.log(`the role name of this user loggined is ${JSON.stringify(response.data.data.role_name)}`)
        localStorage.setItem('token', response.data.data.token);
        localStorage.setItem('managerId', response.data.data.id);
        console.log(`please this is the data stored in the local storage ${localStorage.getItem(`data`)}`)
        console.log(`the status code is ${response.status}`)
      } else if (response.status === 202) {
        dispatch(loginSuccess(response.data.message));
        console.log(`the status code is ${response.status}`)
      } else {
        dispatch(loginFailure(response.data.message));
      }
    } catch (error) {

      console.log(`the error status is ${error} `)
      if (error.response.status === 409) {
        console.error(`${error.response.data.message}`);
        dispatch(loginFailure(error.response.data));
      } else if (error.response.status === 400) {
        console.error(`${error.response.data.message}`);
        dispatch(loginFailure(error.response.data)); // the form of the error.response.data is "message": "adkasfhksfh"
      }
      else if (error.response.status === 404) {
        console.error(`${error.response.data.errors}`);
        dispatch(loginFailure(error.response.data));
      } else if (error.response.status === 401) {
        console.error(`${error.response.data.errors}`);
        dispatch(loginFailure(error.response.data));
      } else if (error.response.status === 422) {
        console.error(`${error.response.data.errors}`);
        dispatch(loginFailure(error.response.data));
      } else {
        console.error('Error occurred during loging:', error.response.data);
        dispatch(loginFailure(error.response.data));
      }

    }

    dispatch(loginLoading(false));

  }
};
