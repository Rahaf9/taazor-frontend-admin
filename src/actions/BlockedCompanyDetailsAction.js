import axios from 'axios';
import { addConsulution, advisorDetails, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const BLOCKED_COMPANIES_DETAILS_FAILURE = 'BLOCKED_COMPANIES_DETAILS_FAILURE';
export const BLOCKED_COMPANIES_DETAILS_SUCCESS = 'BLOCKED_COMPANIES_DETAILS_SUCCESS';
export const BLOCKED_COMPANIES_DETAILS_REFRESH = 'BLOCKED_COMPANIES_REFRESH';
export const BLOCKED_COMPANIES_DETAILS_LOADING = 'BLOCKED_COMPANIES_LOADING';

export const blockedCompaiesDetailsFailure = (errors) => ({
    type: BLOCKED_COMPANIES_DETAILS_FAILURE,
    payload: errors,
});

export const blockedCompaiesDetailsSuccess = (data) => ({
    type: BLOCKED_COMPANIES_DETAILS_SUCCESS,
    payload: data,
});

export const blockedCompaiesDetailsRefreshPage = () => ({
    type: BLOCKED_COMPANIES_DETAILS_REFRESH
});

export const blockedCompaiesDetailsLoading = (state) => ({
    type: BLOCKED_COMPANIES_DETAILS_LOADING,
    payload: state
});

export const blockedCompaiesDetailsAction = (index) => {

    console.log(`the index in detailed advisor action is ${index}`)

    return async (dispatch) => {

        dispatch(blockedCompaiesDetailsLoading(true));

        try {
            const response = await axios.post(`${domainServer}${consulutionDetails}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                dispatch(blockedCompaiesDetailsSuccess(response.data.data));
            } else {
                dispatch(blockedCompaiesDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed advisor ${JSON.stringify(error.response.data)}`)
                dispatch(blockedCompaiesDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed advisor:', error.response.data);
                dispatch(blockedCompaiesDetailsFailure(error.response.data));
            }

        }

        dispatch(blockedCompaiesDetailsLoading(false));
    }
};
