// actionTypes.js

import { domainServer, verifyCode } from "../General/ServerSideApis/ServerSideApis";
import axios from 'axios';
import { localStorageEmailValue } from "../General/Services Functions/Local Storage Functions";


export const SET_OTP_FAILURE= 'SET_OTP_FAILURE';
export const SET_OTP_SUCCESS = 'SET_OTP_SUCCESS';
export const SET_OTP_REFRESH = 'SET_OTP_REFRESH';
export const SET_OTP_LOADING = 'SET_OTP_LOADING';

// actionCreators.js

export const otpFailure = (errors) => ({
  type: SET_OTP_FAILURE,
  payload: errors,
});
    
export const otpSuccess = (data) => ({
  type: SET_OTP_SUCCESS,
  payload: data,
});

export const otpRefreshPage = () => ({
  type: SET_OTP_REFRESH
});

export const otpLoading = (state) => ({
  type: SET_OTP_LOADING,
  payload:state
});

export const otpAction = (formData) => {
  
  return async (dispatch) => {

    dispatch(otpLoading(true));
    console.log(`this is the form data in otp action ${formData}`)
    
    try {
      const response = await axios.post(`${domainServer}${verifyCode}`, {
        'email':localStorage.getItem('email'),
        'code':formData //formData here is only otp so there is no object map like {keys:values}
      });

      if (response.status === 200) {
        dispatch(otpSuccess(response.data.data));
        localStorage.setItem('token',response.data.data)
      } else {
        dispatch(otpFailure(response.data.message));
      }
    } catch (error) {

     if (error.response.status === 400) {
        console.error(`errors otp ${error.response.data.message}`);
        dispatch(otpFailure(error.response.data)); // the form of the error.response.data is "message": "adkasfhksfh"
      } else if (error.response.status === 422) {
        console.error(`errors 422 otp ${error.response.data.message}`);
        dispatch(otpFailure(error.response.data)); // the form of the error.response.data is "message": "adkasfhksfh"
      } else if (error.response.status === 208) {
        console.error(`errors 208 otp ${error.response.data.message}`);
        dispatch(otpFailure({'message':error.response.data.message})); // the form of the error.response.data is "message": "adkasfhksfh"
      }
      else {
        console.error('Error occurred during otp verifying:', error.response.data);
        dispatch(otpFailure(error.response.data));
      }

    }

    dispatch(otpLoading(false));
  }
};