import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, companiesApi, downloadDocs } from "../General/ServerSideApis/ServerSideApis";
import { saveAs } from 'file-saver'; // Install this package using `npm install file-saver`


export const DOWNLOAD_COMPANIE_FILES_SUCCESS = "DOWNLOAD_COMPANIE_FILES_SUCCESS";
export const DOWNLOAD_COMPANIE_FILES_FAILURE = "DOWNLOAD_COMPANIE_FILES_FAILURE";
export const DOWNLOAD_COMPANIE_FILES_LOADING = "DOWNLOAD_COMPANIE_FILES_LOADING";
export const DOWNLOAD_COMPANIE_FILES_REFRESH = "DOWNLOAD_COMPANIE_FILES_REFRESH";

export const downloadCompanieFilesSuccess = (details) => ({
  type: DOWNLOAD_COMPANIE_FILES_SUCCESS,
  payload: details
});

export const downloadCompanieFilesFailure = (errors) => ({
  type: DOWNLOAD_COMPANIE_FILES_FAILURE,
  payload: errors,
});


export const downloadCompanieFilesRefreshPage = () => ({
  type: DOWNLOAD_COMPANIE_FILES_REFRESH
});

export const downloadCompanieFilesLoading = (state) => ({
  type: DOWNLOAD_COMPANIE_FILES_LOADING,
  payload: state
});

export const downloadCompanieFilesAction = (index) => {

  return async (dispatch) => {

    dispatch(downloadCompanieFilesLoading(true));
    console.log(`the token of me in download companiFilees  is ${JSON.stringify(localStorage.getItem('token'))}`)

    try {
      const response = await axios.get(`${domainServer}${companiesApi}${downloadDocs}/${index}`,
        {
          headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          },
          responseType: 'blob' // Ensure the response is treated as a blob
        });

      if (response.status === 200) {
        console.log(`the data download is success and the file response is ${JSON.stringify(response.data)}`)
       // Create a blob from the response data
       const blob = new Blob([response.data], { type: response.data.type });
       const fileName = response.headers['content-disposition']
         ? response.headers['content-disposition'].split('filename=')[1].split(';')[0]
         : 'downloadedFile'; // Default filename if not provided

       // Use the saveAs function from the file-saver package to save the file
       saveAs(blob, fileName);
        dispatch(downloadCompanieFilesSuccess(response.data.data));
      } else {
        dispatch(downloadCompanieFilesFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 401) {
        console.log(`i got 401 in download companiFilees ${JSON.stringify(error.response.data)}`)
        dispatch(downloadCompanieFilesFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else {
        console.error('Error occurred during your download companiFilees:', error.response.data);
        dispatch(downloadCompanieFilesFailure(error.response.data));
      }

    }

    dispatch(downloadCompanieFilesLoading(false));
  }
};
