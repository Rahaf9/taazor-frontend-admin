import axios from 'axios';
import { consulutionDetails, domainServer, seekers } from '../General/ServerSideApis/ServerSideApis';

export const USER_DETAILS_FAILURE = 'USER_DETAILS_FAILURE';
export const USER_DETAILS_SUCCESS = 'USER_DETAILS_SUCCESS';
export const USER_DETAILS_REFRESH = 'USER_DETAILS_REFRESH';
export const USER_DETAILS_LOADING = 'USER_DETAILS_LOADING';

export const userDetailsFailure = (errors) => ({
    type: USER_DETAILS_FAILURE,
    payload: errors,
});

export const userDetailsSuccess = (data) => ({
    type: USER_DETAILS_SUCCESS,
    payload: data,
});

export const userDetailsRefreshPage = () => ({
    type: USER_DETAILS_REFRESH
});

export const userDetailsLoading = (state) => ({
    type: USER_DETAILS_LOADING,
    payload: state
});

export const userDetailsAction = (index) => {

    console.log(`the index in detailed advisor action is ${index}`)

    return async (dispatch) => {

        dispatch(userDetailsLoading(true));

        try {
            const response = await axios.get(`${domainServer}${seekers}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`the users details is ${JSON.stringify(response.data.data)}`)
                dispatch(userDetailsSuccess(response.data.data));
            } else {
                dispatch(userDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed seeker details ${JSON.stringify(error.response.data)}`)
                dispatch(userDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed seeker:', error.response.data);
                dispatch(userDetailsFailure(error.response.data));
            }

        }

        dispatch(userDetailsLoading(false));
    }
};
