import { jobDetails } from "../FakeData";
import axios from 'axios';
import { domainServer, jobDetailsApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_JOB_DETAILS_SUCCESS = "EXPIRED_JOB_DETAILS_SUCCESS";
export const EXPIRED_JOB_DETAILS_FAILURE = "EXPIRED_JOB_DETAILS_FAILURE";
export const EXPIRED_JOB_DETAILS_LOADING = "EXPIRED_JOB_DETAILS_LOADING";
export const EXPIRED_JOB_DETAILS_REFRESH = "EXPIRED_JOB_DETAILS_REFRESH";

export const ExpiredJobDetailsSuccess = (details) => ({
    type: EXPIRED_JOB_DETAILS_SUCCESS,
    payload: details
});

export const ExpiredJobDetailsFailure = (errors) => ({
    type: EXPIRED_JOB_DETAILS_FAILURE,
    payload: errors,
  });


export const ExpiredJobDetailsRefreshPage = () => ({
    type: EXPIRED_JOB_DETAILS_REFRESH
  });
  
  export const ExpiredJobDetailsLoading = (state) => ({
    type: EXPIRED_JOB_DETAILS_LOADING,
    payload: state
  });
  
  export const ExpiredJobDetailsAction = (index) => {
  
    return async (dispatch) => {
  
      dispatch(ExpiredJobDetailsLoading(true));
      console.log(`the token of me in expired jobs details is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${jobDetailsApi}/${index}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the expired job details  is ${JSON.stringify(response.data.data)}`)
            dispatch(ExpiredJobDetailsSuccess(response.data.data));
        } else {
          dispatch(ExpiredJobDetailsFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your consultants ${JSON.stringify(error.response.data)}`)
          dispatch(ExpiredJobDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your consultions:', error.response.data);
          dispatch(ExpiredJobDetailsFailure(error.response.data));
        }
  
      }
  
      dispatch(ExpiredJobDetailsLoading(false));
    }
  };
  