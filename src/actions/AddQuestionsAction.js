export const ADD_QUESTIONS = "ADD_QUESTIONS"

export const addQuestions = (questions) => ({
    type: ADD_QUESTIONS,
    payload: questions,
  });