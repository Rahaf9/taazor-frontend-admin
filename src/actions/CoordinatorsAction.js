import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, coursesApi, coordinators } from "../General/ServerSideApis/ServerSideApis";

export const COORDINATORS_SUCCESS = "COORDINATORS_SUCCESS";
export const COORDINATORS_LOADING = "COORDINATORS_LOADING";
export const COORDINATORS_REFRESH = "COORDINATORS_REFRESH";
export const COORDINATORS_FAILURE = "COORDINATORS_FAILURE";

export const coordinatorsSuccess = (details) => ({
    type: COORDINATORS_SUCCESS,
    payload: details
});

export const coordinatorsFailure = (errors) => ({
    type: COORDINATORS_FAILURE,
    payload: errors,
  });


export const coordinatorsRefreshPage = () => ({
    type: COORDINATORS_REFRESH
  });
  
  export const coordinatorsLoading = (state) => ({
    type: COORDINATORS_LOADING,
    payload: state
  });
  
  export const coordinatorsAction = (index,page) => {
  
    return async (dispatch) => {
  
      dispatch(coordinatorsLoading(true));
      console.log(`the token of me in coordinators  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${coordinators}?page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the success of coordinators is ${JSON.stringify(response.data.data)}`)
            dispatch(coordinatorsSuccess(response.data.data));
        } else {
          dispatch(coordinatorsFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your advisors ${JSON.stringify(error.response.data)}`)
          dispatch(coordinatorsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your advisors:', error.response.data);
          dispatch(coordinatorsFailure(error.response.data));
        }
  
      }
  
      dispatch(coordinatorsLoading(false));
    }
  };
  