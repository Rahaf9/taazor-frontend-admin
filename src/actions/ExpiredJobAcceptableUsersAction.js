import axios from "axios";
import { domainServer, userJobApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_JOB_ACCEPTABLE_USERS_SUCCESS = "EXPIRED_JOB_ACCEPTABLE_USERS_SUCCESS";
export const EXPIRED_JOB_ACCEPTABLE_USERS_FAILURE = "EXPIRED_JOB_ACCEPTABLE_USERS_FAILURE";
export const EXPIRED_JOB_ACCEPTABLE_USERS_LOADING = "EXPIRED_JOB_ACCEPTABLE_USERS_LOADING";
export const EXPIRED_JOB_ACCEPTABLE_USER_REFRESH = "EXPIRED_JOB_ACCEPTABLE_USER_REFRESH";


export const expiredJobAcceptableUsersSuccess = (users) => ({
    type: EXPIRED_JOB_ACCEPTABLE_USERS_SUCCESS,
    payload: users
});

export const expiredJobAcceptableUsersFailure = (errors) => ({
    type: EXPIRED_JOB_ACCEPTABLE_USERS_FAILURE,
    payload: errors,
  });


export const expiredJobAcceptableUsersRefreshPage = () => ({
    type: EXPIRED_JOB_ACCEPTABLE_USER_REFRESH
  });
  
  export const expiredJobAcceptableUsersLoading = (state) => ({
    type: EXPIRED_JOB_ACCEPTABLE_USERS_LOADING,
    payload: state
  });

export const expiredJobAcceptableUsersUsersAction = (index,page) => {


    return async (dispatch) => {

        dispatch(expiredJobAcceptableUsersLoading(true));

        try {
          // here must replace the ?status=DoneUsers with the right url from the backend
          const response = await axios.get(`${domainServer}${userJobApi}/${index}?status=accepted&page=${page}`, {
              headers: {
                  'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                  'Authorization': `Bearer ${localStorage.getItem('token')}`,
              }
          });

          if (response.status === 200) {
              console.log(`this is the acceptable Course users data ${JSON.stringify(response.data.data)}`)
              dispatch(expiredJobAcceptableUsersSuccess(response.data.data));
          } else {
              dispatch(expiredJobAcceptableUsersFailure(response.data.message));
          }
      } catch (error) {

          if (error.response.status === 401) {
              console.log(`i got 401 in your acceptable Course users ${JSON.stringify(error.response.data)}`)
              dispatch(expiredJobAcceptableUsersFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
          } else {
              console.error('Error occurred during your acceptable Course users:', error.response.data);
              dispatch(expiredJobAcceptableUsersFailure(error.response.data));
          }

      }

        dispatch(expiredJobAcceptableUsersLoading(false));

    }

}