import axios from 'axios';
import { domainServer, coursesApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_COURSES_SUCCESS = "EXPIRED_COURSES_SUCCESS";
export const EXPIRED_COURSES_FAILURE = "EXPIRED_COURSES_FAILURE";
export const EXPIRED_COURSES_LOADING = "EXPIRED_COURSES_LOADING";
export const EXPIRED_COURSES_REFRESH = "EXPIRED_COURSES_REFRESH";

export const expiredCoursesSuccess = (details) => ({
    type: EXPIRED_COURSES_SUCCESS,
    payload: details
});

export const expiredCoursesFailure = (errors) => ({
    type: EXPIRED_COURSES_FAILURE,
    payload: errors,
  });


export const expiredCoursesRefreshPage = () => ({
    type: EXPIRED_COURSES_REFRESH
  });
  
  export const expiredCoursesLoading = (state) => ({
    type: EXPIRED_COURSES_LOADING,
    payload: state
  });
  
  export const expiredCoursesAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(expiredCoursesLoading(true));
      console.log(`the token of me in Expired Coursess  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${coursesApi}?status=finite&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the expired courses is ${JSON.stringify(response.data.data)}`)
            dispatch(expiredCoursesSuccess(response.data.data));
        } else {
          dispatch(expiredCoursesFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your Expired Coursess ${JSON.stringify(error.response.data)}`)
          dispatch(expiredCoursesFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your Expired Coursess:', error.response.data);
          dispatch(expiredCoursesFailure(error.response.data));
        }
  
      }
  
      dispatch(expiredCoursesLoading(false));
    }
  };
  