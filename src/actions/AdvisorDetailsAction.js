import axios from 'axios';
import { addConsulution, advisorDetails, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const ADVISOR_DETAILS_FAILURE = 'ADVISOR_DETAILS_FAILURE';
export const ADVISOR_DETAILS_SUCCESS = 'ADVISOR_DETAILS_SUCCESS';
export const ADVISOR_DETAILS_REFRESH = 'ADVISOR_DETAILS_REFRESH';
export const ADVISOR_DETAILS_LOADING = 'ADVISOR_DETAILS_LOADING';

export const advisorDetailsFailure = (errors) => ({
    type: ADVISOR_DETAILS_FAILURE,
    payload: errors,
});

export const advisorDetailsSuccess = (data) => ({
    type: ADVISOR_DETAILS_SUCCESS,
    payload: data,
});

export const advisorDetailsRefreshPage = () => ({
    type: ADVISOR_DETAILS_REFRESH
});

export const advisorDetailsLoading = (state) => ({
    type: ADVISOR_DETAILS_LOADING,
    payload: state
});

export const advisorDetailsAction = (index) => {

    console.log(`the index in detailed advisor action is ${index}`)

    return async (dispatch) => {

        dispatch(advisorDetailsLoading(true));

        try {
            const response = await axios.post(`${domainServer}${consulutionDetails}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                dispatch(advisorDetailsSuccess(response.data.data));
            } else {
                dispatch(advisorDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed advisor ${JSON.stringify(error.response.data)}`)
                dispatch(advisorDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed advisor:', error.response.data);
                dispatch(advisorDetailsFailure(error.response.data));
            }

        }

        dispatch(advisorDetailsLoading(false));
    }
};
