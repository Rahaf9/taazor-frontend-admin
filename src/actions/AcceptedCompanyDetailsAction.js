import axios from 'axios';
import { addConsulution, advisorDetails, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const ACCEPTED_COMPANIES_DETAILS_FAILURE = 'ACCEPTED_COMPANIES_DETAILS_FAILURE';
export const ACCEPTED_COMPANIES_DETAILS_SUCCESS = 'ACCEPTED_COMPANIES_DETAILS_SUCCESS';
export const ACCEPTED_COMPANIES_DETAILS_REFRESH = 'ACCEPTED_COMPANIES_REFRESH';
export const ACCEPTED_COMPANIES_DETAILS_LOADING = 'ACCEPTED_COMPANIES_LOADING';

export const acceptedCompaiesDetailsFailure = (errors) => ({
    type: ACCEPTED_COMPANIES_DETAILS_FAILURE,
    payload: errors,
});

export const acceptedCompaiesDetailsSuccess = (data) => ({
    type: ACCEPTED_COMPANIES_DETAILS_SUCCESS,
    payload: data,
});

export const acceptedCompaiesDetailsRefreshPage = () => ({
    type: ACCEPTED_COMPANIES_DETAILS_REFRESH
});

export const acceptedCompaiesDetailsLoading = (state) => ({
    type: ACCEPTED_COMPANIES_DETAILS_LOADING,
    payload: state
});

export const acceptedCompaiesDetailsAction = (index) => {

    console.log(`the index in detailed advisor action is ${index}`)

    return async (dispatch) => {

        dispatch(acceptedCompaiesDetailsLoading(true));

        try {
            const response = await axios.post(`${domainServer}${consulutionDetails}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                dispatch(acceptedCompaiesDetailsSuccess(response.data.data));
            } else {
                dispatch(acceptedCompaiesDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed advisor ${JSON.stringify(error.response.data)}`)
                dispatch(acceptedCompaiesDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed advisor:', error.response.data);
                dispatch(acceptedCompaiesDetailsFailure(error.response.data));
            }

        }

        dispatch(acceptedCompaiesDetailsLoading(false));
    }
};
