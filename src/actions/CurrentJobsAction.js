import axios from 'axios';
import { domainServer, jobsApi } from "../General/ServerSideApis/ServerSideApis";

export const CURRENT_JOB_SUCCESS = "CURRENT_JOB_SUCCESS";
export const CURRENT_JOB_FAILURE = "CURRENT_JOB_FAILURE";
export const CURRENT_JOB_LOADING = "CURRENT_JOB_LOADING";
export const CURRENT_JOB_REFRESH = "CURRENT_JOB_REFRESH";

export const CurrentJobSuccess = (details) => ({
    type: CURRENT_JOB_SUCCESS,
    payload: details
});

export const CurrentJobFailure = (errors) => ({
    type: CURRENT_JOB_FAILURE,
    payload: errors,
  });


export const CurrentJobRefreshPage = () => ({
    type: CURRENT_JOB_REFRESH
  });
  
  export const CurrentJobLoading = (state) => ({
    type: CURRENT_JOB_LOADING,
    payload: state
  });
  
  export const CurrentJobAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(CurrentJobLoading(true));
      console.log(`the token of me in current jobs  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${jobsApi}?status=current&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`this is the current jobs data ${JSON.stringify(response.data.data)}`)
            dispatch(CurrentJobSuccess(response.data.data));
        } else {
          dispatch(CurrentJobFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your current jobs ${JSON.stringify(error.response.data)}`)
          dispatch(CurrentJobFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your current jobs:', error.response.data);
          dispatch(CurrentJobFailure(error.response.data));
        }
  
      }
  
      dispatch(CurrentJobLoading(false));
    }
  };

  export const WaitingJobAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(CurrentJobLoading(true));
      console.log(`the token of me in waiting jobs  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${jobsApi}?status=waiting&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`this is the current jobs data ${JSON.stringify(response.data.data)}`)
            dispatch(CurrentJobSuccess(response.data.data));
        } else {
          dispatch(CurrentJobFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your current jobs ${JSON.stringify(error.response.data)}`)
          dispatch(CurrentJobFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your current jobs:', error.response.data);
          dispatch(CurrentJobFailure(error.response.data));
        }
  
      }
  
      dispatch(CurrentJobLoading(false));
    }
  };
  

  export const JobNotConvertedAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(CurrentJobLoading(true));
      console.log(`the token of me in current jobs  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${jobsApi}?is_converted=false&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`this is the not converted jobs data ${JSON.stringify(response.data.data)}`)
            dispatch(CurrentJobSuccess(response.data.data));
        } else {
          dispatch(CurrentJobFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your not converted jobs ${JSON.stringify(error.response.data)}`)
          dispatch(CurrentJobFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your current jobs:', error.response.data);
          dispatch(CurrentJobFailure(error.response.data));
        }
  
      }
  
      dispatch(CurrentJobLoading(false));
    }
  };