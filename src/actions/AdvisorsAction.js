import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, coursesApi, advisorsApi, getAdvisors } from "../General/ServerSideApis/ServerSideApis";

export const ADVISORS_SUCCESS = "ADVISORS_SUCCESS";
export const ADVISORS_LOADING = "ADVISORS_LOADING";
export const ADVISORS_REFRESH = "ADVISORS_REFRESH";
export const ADVISORS_FAILURE = "ADVISORS_FAILURE";

export const advisorsSuccess = (details) => ({
    type: ADVISORS_SUCCESS,
    payload: details
});

export const advisorsFailure = (errors) => ({
    type: ADVISORS_FAILURE,
    payload: errors,
  });


export const advisorsRefreshPage = () => ({
    type: ADVISORS_REFRESH
  });
  
  export const advisorsLoading = (state) => ({
    type: ADVISORS_LOADING,
    payload: state
  });
  
  export const advisorsAction = (index,page) => {
  
    return async (dispatch) => {
  
      dispatch(advisorsLoading(true));
      console.log(`  ${JSON.stringify(localStorage.getItem('token'))} 
      and the page is ${page}`)
  
      try {
        const response = await axios.get(`${domainServer}${advisorsApi}?page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the data inside the adivsors is ${JSON.stringify(response.data.data)}`)
            dispatch(advisorsSuccess(response.data.data));
        } else {
          dispatch(advisorsFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your advisors ${JSON.stringify(error.response.data)}`)
          dispatch(advisorsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your advisors:', error.response.data);
          dispatch(advisorsFailure(error.response.data));
        }
  
      }
  
      dispatch(advisorsLoading(false));
    }
  };

  export const allAdvisorsAction = () => {
  
    return async (dispatch) => {
  
      dispatch(advisorsLoading(true));
  
      try {
        const response = await axios.get(`${domainServer}${getAdvisors}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the data inside the adivsors is ${JSON.stringify(response.data.data)}`)
            dispatch(advisorsSuccess(response.data.data.advisors));
        } else {
          dispatch(advisorsFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your advisors ${JSON.stringify(error.response.data)}`)
          dispatch(advisorsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your advisors:', error.response.data);
          dispatch(advisorsFailure(error.response.data));
        }
  
      }
  
      dispatch(advisorsLoading(false));
    }
  };
  