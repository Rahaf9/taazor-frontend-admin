import axios from "axios";
import { users } from "../FakeData";
import { coursesApi, domainServer, userCourseApi, userCourseDetailsApi } from "../General/ServerSideApis/ServerSideApis";

export const COURSE_USER_DETAILS_SUCCESS = "COURSE_USER_DETAILS_SUCCESS";
export const COURSE_USER_DETAILS_FAILURE = "COURSE_USER_DETAILS_FAILURE";
export const COURSE_USER_DETAILS_LOADING = "COURSE_USER_DETAILS_LOADING";
export const COURSE_USER_DETAILS_REFRESH = "COURSE_USER_DETAILS_REFRESH";

export const courseUserDetailsSuccess = (users) => ({
    type: COURSE_USER_DETAILS_SUCCESS,
    payload: users
});

export const courseUserDetailsFailure = (errors) => ({
    type: COURSE_USER_DETAILS_FAILURE,
    payload: errors,
});


export const courseUserDetailsRefreshPage = () => ({
    type: COURSE_USER_DETAILS_REFRESH
});

export const courseUserDetailsLoading = (state) => ({
    type: COURSE_USER_DETAILS_LOADING,
    payload: state
});


export const courseUserDetailsAction = (index) => {

    return async (dispatch) => {

        console.log(`this is the index we want to get its user details ${JSON.stringify(index)}`)

        dispatch(courseUserDetailsLoading(true));

        try {
            const response = await axios.get(`${domainServer}${userCourseDetailsApi}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`this is the course user details data ${JSON.stringify(response.data.data)}`)
                dispatch(courseUserDetailsSuccess(response.data.data));
            } else {
                dispatch(courseUserDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in your course users ${JSON.stringify(error.response.data)}`)
                dispatch(courseUserDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during your course users:', error.response.data);
                dispatch(courseUserDetailsFailure(error.response.data));
            }

        }

        dispatch(courseUserDetailsLoading(false));

        // dispatch(courseUserDetailsSuccess(users));

    }

}