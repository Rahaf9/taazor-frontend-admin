// actionTypes.js

import { addNewPollApi, domainServer } from "../General/ServerSideApis/ServerSideApis";
import axios from 'axios';

export const SET_ADD_NEW_POLL_FAILURE = 'SET_ADD_NEW_POLL_FAILURE';
export const SET_ADD_NEW_POLL_SUCCESS = 'SET_ADD_NEW_POLL_SUCCESS';
export const SET_ADD_NEW_POLL_REFRESH = 'SET_ADD_NEW_POLL_REFRESH';
export const SET_ADD_NEW_POLL_LOADING = 'SET_ADD_NEW_POLL_LOADING';

// actionCreators.js

export const addNewPollFailure = (errors) => ({
  type: SET_ADD_NEW_POLL_FAILURE,
  payload: errors,
});

export const addNewPollSuccess = (data) => ({
  type: SET_ADD_NEW_POLL_SUCCESS,
  payload: data,
});

export const addNewPollRefreshPage = () => ({
  type: SET_ADD_NEW_POLL_REFRESH
});

export const addNewPollLoading = (state) => ({
  type: SET_ADD_NEW_POLL_LOADING,
  payload: state
});

export const addNewPollAction = (questions) => {

  return async (dispatch) => {

    dispatch(addNewPollLoading(true));
    console.log(`this is questions ${JSON.stringify(questions)}`)

    try {
      const response = await axios.post(`${domainServer}${addNewPollApi}`, {
        "forms": {
          "is_required": true,
          "questions": questions
        }
      }, {
        headers: {
          'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
      });

      if (response.status === 200) {
        dispatch(addNewPollSuccess(response.data.data));
      } else {
        dispatch(addNewPollFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 422) {
        console.error(`errors in add new poll ${error.response.data.message}`);
        dispatch(addNewPollFailure(error.response.data));
      } else if (error.response.status === 401) {
        console.log(`i got 401 in add new poll ${JSON.stringify(error.response.data)}`)
        dispatch(addNewPollFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else {
        console.error('Error occurred during add new poll:', error.response.data);
        dispatch(addNewPollFailure(error.response.data));
      }

    }

    dispatch(addNewPollLoading(false));
  }
};
