import axios from "axios";
import { users } from "../FakeData";
import { domainServer, userCourseApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_COURSE_USERS_SUCCESS = "EXPIRED_COURSE_USERS_SUCCESS";
export const EXPIRED_COURSE_USERS_FAILURE = "EXPIRED_COURSE_USERS_FAILURE";
export const EXPIRED_COURSE_USERS_LOADING = "EXPIRED_COURSE_USERS_LOADING";
export const EXPIRED_COURSE_USER_REFRESH = "EXPIRED_COURSE_USER_REFRESH";


export const expiredCourseUsersSuccess = (users) => ({
    type: EXPIRED_COURSE_USERS_SUCCESS,
    payload: users
});

export const expiredCourseUserFailure = (errors) => ({
    type: EXPIRED_COURSE_USERS_FAILURE,
    payload: errors,
  });


export const expiredCourseUserRefreshPage = () => ({
    type: EXPIRED_COURSE_USER_REFRESH
  });
  
  export const expiredCourseUserLoading = (state) => ({
    type: EXPIRED_COURSE_USERS_LOADING,
    payload: state
  });

export const expiredCourseUsersAction = (index,page) => {


    return async (dispatch) => {

        dispatch(expiredCourseUserLoading(true));

        try {
          // here must replace the userexpired courseApi with the courseUserApi when it be able to access to
          const response = await axios.get(`${domainServer}${userCourseApi}/${index}?status=Expired&?page=${page}`, {
              headers: {
                  'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                  'Authorization': `Bearer ${localStorage.getItem('token')}`,
              }
          });

          if (response.status === 200) {
              console.log(`this is the expired course users data ${JSON.stringify(response.data.data)}`)
              dispatch(expiredCourseUsersSuccess(response.data.data));
          } else {
              dispatch(expiredCourseUserFailure(response.data.message));
          }
      } catch (error) {

          if (error.response.status === 401) {
              console.log(`i got 401 in your expired course users ${JSON.stringify(error.response.data)}`)
              dispatch(expiredCourseUserFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
          } else {
              console.error('Error occurred during your expired course users:', error.response.data);
              dispatch(expiredCourseUserFailure(error.response.data));
          }

      }

        dispatch(expiredCourseUserLoading(false));

    }

}