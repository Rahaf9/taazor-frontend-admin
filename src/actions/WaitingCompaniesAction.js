import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, coursesApi, companiesApi } from "../General/ServerSideApis/ServerSideApis";

export const WAITING_COMPANIES_SUCCESS = "WAITING_COMPANIES_SUCCESS";
export const WAITING_COMPANIES_FAILURE = "WAITING_COMPANIES_FAILURE";
export const WAITING_COMPANIES_LOADING = "WAITING_COMPANIES_LOADING";
export const WAITING_COMPANIES_REFRESH = "WAITING_COMPANIES_REFRESH";

export const waitingCompaniesSuccess = (details) => ({
    type: WAITING_COMPANIES_SUCCESS,
    payload: details
});

export const waitingCompaniesFailure = (errors) => ({
    type: WAITING_COMPANIES_FAILURE,
    payload: errors,
  });


export const waitingCompaniesRefreshPage = () => ({
    type: WAITING_COMPANIES_REFRESH
  });
  
  export const waitingCompaniesLoading = (state) => ({
    type: WAITING_COMPANIES_LOADING,
    payload: state
  });
  
  export const waitingCompaniesAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(waitingCompaniesLoading(true));
      console.log(`the token of me in waiting companies  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${companiesApi}?status=waiting&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the data waiting is ${JSON.stringify(response.data.data)}`)
            dispatch(waitingCompaniesSuccess(response.data.data));
        } else {
          dispatch(waitingCompaniesFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in waiting companies ${JSON.stringify(error.response.data)}`)
          dispatch(waitingCompaniesFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your waiting companies:', error.response.data);
          dispatch(waitingCompaniesFailure(error.response.data));
        }
  
      }
  
      dispatch(waitingCompaniesLoading(false));
    }
  };
  