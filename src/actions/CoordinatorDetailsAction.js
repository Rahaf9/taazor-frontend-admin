import axios from 'axios';
import { addConsulution, advisorDetails, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const COORDIANTORS_DETAILS_FAILURE = 'COORDIANTORS_DETAILS_FAILURE';
export const COORDIANTORS_DETAILS_SUCCESS = 'COORDIANTORS_DETAILS_SUCCESS';
export const COORDIANTORS_DETAILS_REFRESH = 'COORDIANTORS_DETAILS_REFRESH';
export const COORDIANTORS_DETAILS_LOADING = 'COORDIANTORS_DETAILS_LOADING';

export const coordinatorsDetailsFailure = (errors) => ({
    type: COORDIANTORS_DETAILS_FAILURE,
    payload: errors,
});

export const coordinatorsDetailsSuccess = (data) => ({
    type: COORDIANTORS_DETAILS_SUCCESS,
    payload: data,
});

export const coordinatorsDetailsRefreshPage = () => ({
    type: COORDIANTORS_DETAILS_REFRESH
});

export const coordinatorsDetailsLoading = (state) => ({
    type: COORDIANTORS_DETAILS_LOADING,
    payload: state
});

export const coordinatorsDetailsAction = (index) => {

    console.log(`the index in detailed advisor action is ${index}`)

    return async (dispatch) => {

        dispatch(coordinatorsDetailsLoading(true));

        try {
            const response = await axios.post(`${domainServer}${consulutionDetails}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                dispatch(coordinatorsDetailsSuccess(response.data.data));
            } else {
                dispatch(coordinatorsDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed advisor ${JSON.stringify(error.response.data)}`)
                dispatch(coordinatorsDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed advisor:', error.response.data);
                dispatch(coordinatorsDetailsFailure(error.response.data));
            }

        }

        dispatch(coordinatorsDetailsLoading(false));
    }
};
