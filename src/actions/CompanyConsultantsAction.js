import axios from 'axios';
import { companyConsulutions, domainServer } from '../General/ServerSideApis/ServerSideApis';

export const SET_COMPANY_CONSULTANTS_FAILURE = 'SET_COMPANY_CONSULTANTS_FAILURE';
export const SET_COMPANY_CONSULTANTS_SUCCESS = 'SET_COMPANY_CONSULTANTS_SUCCESS';
export const SET_COMPANY_CONSULTANTS_REFRESH = 'SET_COMPANY_CONSULTANTS_REFRESH';
export const SET_COMPANY_CONSULTANTS_LOADING = 'SET_COMPANY_CONSULTANTS_LOADING';

export const companyConsultantsFailure = (errors) => ({
  type: SET_COMPANY_CONSULTANTS_FAILURE,
  payload: errors,
});

export const companyConsultantsSuccess = (data) => ({
  type: SET_COMPANY_CONSULTANTS_SUCCESS,
  payload: data,
});

export const companyConsultantsRefreshPage = () => ({
  type: SET_COMPANY_CONSULTANTS_REFRESH
});

export const companyConsultantsLoading = (state) => ({
  type: SET_COMPANY_CONSULTANTS_LOADING,
  payload: state
});

export const companyConsultantsAction = (page) => {

  return async (dispatch) => {

    dispatch(companyConsultantsLoading(true));
    console.log(`the token of me in company consultions is ${JSON.stringify(localStorage.getItem('token'))}`)

    try {
      const response = await axios.get(`${domainServer}${companyConsulutions}?page=${page}`,
        {
          headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
        });

      if (response.status === 200) {
        console.log(`the company consls success is ${JSON.stringify(response.data.data)}`)
        dispatch(companyConsultantsSuccess(response.data.data.consultations));
      } else {
        dispatch(companyConsultantsFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 401) {
        console.log(`i got 401 in company consultants ${JSON.stringify(error.response.data)}`)
        dispatch(companyConsultantsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else {
        console.error('Error occurred during company consultions:', error.response.data);
        dispatch(companyConsultantsFailure(error.response.data));
      }

    }

    dispatch(companyConsultantsLoading(false));
  }
};
