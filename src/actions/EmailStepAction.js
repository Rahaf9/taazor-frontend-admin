// actionTypes.js
import axios from "axios";
import { domainServer, emailStepApi, loginApi } from "../General/ServerSideApis/ServerSideApis";

export const SET_EMAIL_STEP_FAILURE = 'SET_EMAIL_STEP_FAILURE';
export const SET_EMAIL_STEP_SUCCESS = 'SET_EMAIL_STEP_SUCCESS';
export const SET_EMAIL_STEP_REFRESH = 'SET_EMAIL_STEP_REFRESH';
export const SET_EMAIL_STEP_LOADING = 'SET_EMAIL_STEP_LOADING';

// actionCreators.js

export const emailStepFailure = (errors) => ({
  type: SET_EMAIL_STEP_FAILURE,
  payload: errors,
});
  

export const emailStepSuccess = (data) => ({
  type: SET_EMAIL_STEP_SUCCESS,
  payload: data,
});

export const emailStepRefreshPage = () => ({
  type: SET_EMAIL_STEP_REFRESH
});

export const emailStepLoading = (state) => ({
  type: SET_EMAIL_STEP_LOADING,
  payload:state
});

export const emailStepAction = (formData) => {
  
  return async (dispatch) => {

    dispatch(emailStepLoading(true));
    
    try {
      const response = await axios.post(`${domainServer}${emailStepApi}`, formData);

      if (response.status === 200) {
        dispatch(emailStepSuccess(response.data.data));
        console.log(`the status code is ${response.status}`)
        localStorage.setItem('email',formData.email)
        console.log(`the email recived in newPassword action is ${localStorage.getItem('email')}`)
      } else {
        dispatch(emailStepFailure(response.data.message));
        localStorage.setItem({'code':`${response.data.data.code}`})
      }
    } catch (error) {

       if (error.response.status === 404) {
        console.error(`${error.response.data.message}`);
        dispatch(emailStepFailure(error.response.data)); 
      } else{
        console.error('Error occurred during forget password email step:', error.response.data);
        dispatch(emailStepFailure(error.response.data));
      }

    }

    dispatch(emailStepLoading(false));

  }
};
