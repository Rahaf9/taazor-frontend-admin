import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, coursesApi, companiesApi } from "../General/ServerSideApis/ServerSideApis";

export const REJECTED_COMPANIES_SUCCESS = " REJECTED_COMPANIES_SUCCESS";
export const  REJECTED_COMPANIES_FAILURE = " REJECTED_COMPANIES_FAILURE";
export const  REJECTED_COMPANIES_LOADING = " REJECTED_COMPANIES_LOADING";
export const  REJECTED_COMPANIES_REFRESH = " REJECTED_COMPANIES_REFRESH";

export const rejectedCompaniesSuccess = (details) => ({
    type: REJECTED_COMPANIES_SUCCESS,
    payload: details
});

export const  rejectedCompaniesFailure = (errors) => ({
    type: REJECTED_COMPANIES_FAILURE,
    payload: errors,
  });


export const  rejectedCompaniesRefreshPage = () => ({
    type: REJECTED_COMPANIES_REFRESH
  });
  
  export const  rejectedCompaniesLoading = (state) => ({
    type: REJECTED_COMPANIES_LOADING,
    payload: state
  });
  
  export const  rejectedCompaniesAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch( rejectedCompaniesLoading(true));
      console.log(`the token of me in blocked companies  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${companiesApi}?status=rejected&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the rejected companies ${JSON.stringify(response.data.data)}`)
            dispatch( rejectedCompaniesSuccess(response.data.data));
        } else {
          dispatch( rejectedCompaniesFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in ablocked companies ${JSON.stringify(error.response.data)}`)
          dispatch( rejectedCompaniesFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your Blocked companies:', error.response.data);
          dispatch( rejectedCompaniesFailure(error.response.data));
        }
  
      }
  
      dispatch( rejectedCompaniesLoading(false));
    }
  };
  