import axios from 'axios';
import { domainServer, userConsulting } from '../General/ServerSideApis/ServerSideApis';

export const SET_USER_CONSULTANT_DETAILS_FAILURE = 'SET_USER_CONSULTANT_DETAILS_FAILURE';
export const SET_USER_CONSULTANT_DETAILS_SUCCESS = 'SET_USER_CONSULTANT_DETAILS_SUCCESS';
export const SET_USER_CONSULTANT_DETAILS_REFRESH = 'SET_USER_CONSULTANT_DETAILS_REFRESH';
export const SET_USER_CONSULTANT_DETAILS_LOADING = 'SET_USER_CONSULTANT_DETAILS_LOADING';

export const userConsultantDetailsFailure = (errors) => ({
    type: SET_USER_CONSULTANT_DETAILS_FAILURE,
    payload: errors,
});

export const userConsultantDetailsSuccess = (data) => ({
    type: SET_USER_CONSULTANT_DETAILS_SUCCESS,
    payload: data,
});

export const userConsultantDetailsRefreshPage = () => ({
    type: SET_USER_CONSULTANT_DETAILS_REFRESH
});

export const userConsultantDetailsLoading = (state) => ({
    type: SET_USER_CONSULTANT_DETAILS_LOADING,
    payload: state
});

export const userConsultantDetailsAction = (index) => {

    console.log(`the index in detailed consultion action is ${index}`)

    return async (dispatch) => {

        dispatch(userConsultantDetailsLoading(true));

        try {
            const response = await axios.get(`${domainServer}${userConsulting}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                dispatch(userConsultantDetailsSuccess(response.data.data));
            } else {
                dispatch(userConsultantDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed Consultants ${JSON.stringify(error.response.data)}`)
                dispatch(userConsultantDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed consultion:', error.response.data);
                dispatch(userConsultantDetailsFailure(error.response.data));
            }

        }

        dispatch(userConsultantDetailsLoading(false));
    }
};
