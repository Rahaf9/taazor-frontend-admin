// requestActions.js
import axios from 'axios';

export const ALL_ADMIN_ACTION_SUCCESS = 'ALL_ADMIN_ACTION_SUCCESS';
export const ALL_ADMIN_ACTION_FAILURE = 'ALL_ADMIN_ACTION_FAILURE';
export const ALL_ADMIN_ACTION_LOADING = 'ALL_ADMIN_ACTION_LOADING';
export const ALL_ADMIN_ACTION_REFRESH_PAGE = 'ALL_ADMIN_ACTION_REFRESH_PAGE';

export const allAdminActionFailure = (errors) => ({
    type: ALL_ADMIN_ACTION_FAILURE,
    payload: errors,
});

export const allAdminActionSuccess = (data) => ({
    type: ALL_ADMIN_ACTION_SUCCESS,
    payload: data,
});

export const allAdminActionRefreshPage = () => ({
    type: ALL_ADMIN_ACTION_REFRESH_PAGE
});

export const allAdminActionLoading = (state) => ({
    type: ALL_ADMIN_ACTION_LOADING,
    payload: state
});

const fixedHeaders = {
    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
};

export const requestAction = (url, method, data = null) => async (dispatch) => {

    dispatch(allAdminActionLoading(true));

    try {
        const response = await axios({ url, method, data, headers: fixedHeaders });

        if (response.status === 200) {
            console.log(`the data is susccess ${JSON.stringify(response.data.data)}`)
            dispatch(allAdminActionSuccess(response.data.data));
        } else {
            dispatch(allAdminActionFailure(response.data.message));
        }
    } catch (error) {
        if (error.response.status === 401) {
            console.log(`i got 401 in all admin actions ${JSON.stringify(error.response.data)}`)
            dispatch(allAdminActionFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
          } else if (error.response.status === 422) {
            console.error(`errors all admin actions ${JSON.stringify(error.response.data.errors)}`);
            dispatch(allAdminActionFailure({'message':error.response.data.message})); //the form of the error.response.data is  {"password": [ "The password field confirmation does not match." ]}
          } else if (error.response.status === 421) {
            console.error(`errors all admin actions ${JSON.stringify(error.response.data.errors)}`);
            dispatch(allAdminActionFailure({'message':error.response.data.message})); //the form of the error.response.data is  {"password": [ "The password field confirmation does not match." ]}
          } else if (error.response.status === 404) {
            console.error(`errors all admin actions ${JSON.stringify(error.response.data.errors)}`);
            dispatch(allAdminActionFailure({'message':error.response.data.message})); //the form of the error.response.data is  {"password": [ "The password field confirmation does not match." ]}
          } else {
            console.error('Error occurred during complete registration:', error.response.data);
            dispatch(allAdminActionFailure(error.response.data));
          }
    }

    dispatch(allAdminActionLoading(false));
};

export const ALL_STORE_ADMIN_ACTION_SUCCESS = 'ALL_STORE_ADMIN_ACTION_SUCCESS';
export const ALL_STORE_ADMIN_ACTION_FAILURE = 'ALL_STORE_ADMIN_ACTION_FAILURE';
export const ALL_STORE_ADMIN_ACTION_LOADING = 'ALL_STORE_ADMIN_ACTION_LOADING';
export const ALL_STORE_ADMIN_ACTION_REFRESH_PAGE = 'ALL_STORE_ADMIN_ACTION_REFRESH_PAGE';

export const allStoreAdminActionFailure = (errors) => ({
    type: ALL_STORE_ADMIN_ACTION_FAILURE,
    payload: errors,
});

export const allStoreAdminActionSuccess = (data) => ({
    type: ALL_STORE_ADMIN_ACTION_SUCCESS,
    payload: data,
});

export const allStoreAdminActionRefreshPage = () => ({
    type: ALL_STORE_ADMIN_ACTION_REFRESH_PAGE
});

export const allStoreAdminActionLoading = (state) => ({
    type: ALL_STORE_ADMIN_ACTION_LOADING,
    payload: state
});

export const getRequestAction = (url, method, data = null) => async (dispatch) => {

    dispatch(allStoreAdminActionLoading(true));

    try {
        const response = await axios({ url, method, data, headers: fixedHeaders });

        if (response.status === 200) {
            console.log(`the data is susccess ${JSON.stringify(response.data.data)}`)
            dispatch(allStoreAdminActionSuccess(response.data.data));
        } else {
            dispatch(allStoreAdminActionFailure(response.data.message));
        }
    } catch (error) {
        let errorMessage = error.message;
        if (error.response) {
            errorMessage = error.response.data.message;
        }
        dispatch(allStoreAdminActionFailure(errorMessage));
    }

    dispatch(allStoreAdminActionLoading(false));
};
