import axios from "axios";
import { domainServer, userCourseApi } from "../General/ServerSideApis/ServerSideApis";

export const COURSE_USERS_SUCCESS = "COURSE_USERS_SUCCESS";
export const COURSE_USERS_FAILURE = "COURSE_USERS_FAILURE";
export const COURSE_USERS_LOADING = "COURSE_USERS_LOADING";
export const COURSE_USER_REFRESH = "COURSE_USER_REFRESH";

export const courseUsersSuccess = (users) => ({
    type: COURSE_USERS_SUCCESS,
    payload: users
});

export const courseUserFailure = (errors) => ({
    type: COURSE_USERS_FAILURE,
    payload: errors,
});


export const courseUserRefreshPage = () => ({
    type: COURSE_USER_REFRESH
});

export const courseUserLoading = (state) => ({
    type: COURSE_USERS_LOADING,
    payload: state
});


export const courseUsersAction = (index,page) => {

    return async (dispatch) => {

        dispatch(courseUserLoading(true));

        try {
            const response = await axios.get(`${domainServer}${userCourseApi}/${index}?page=${page}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`this is the course users data ${JSON.stringify(response.data.data)}`)
                dispatch(courseUsersSuccess(response.data.data));
            } else {
                dispatch(courseUserFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in your course users ${JSON.stringify(error.response.data)}`)
                dispatch(courseUserFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during your course users:', error.response.data);
                dispatch(courseUserFailure(error.response.data));
            }

        }

        dispatch(courseUserLoading(false));

        // dispatch(courseUsersSuccess(users));

    }

}