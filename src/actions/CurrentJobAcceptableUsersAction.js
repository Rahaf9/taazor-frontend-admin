import axios from "axios";
import { domainServer, userJobApi } from "../General/ServerSideApis/ServerSideApis";

export const CURRENT_JOB_ACCEPTABLE_USERS_SUCCESS = "CURRENT_JOB_ACCEPTABLE_USERS_SUCCESS";
export const CURRENT_JOB_ACCEPTABLE_USERS_FAILURE = "CURRENT_JOB_ACCEPTABLE_USERS_FAILURE";
export const CURRENT_JOB_ACCEPTABLE_USERS_LOADING = "CURRENT_JOB_ACCEPTABLE_USERS_LOADING";
export const CURRENT_JOB_ACCEPTABLE_USER_REFRESH = "CURRENT_JOB_ACCEPTABLE_USER_REFRESH";


export const currentJobAcceptableUsersSuccess = (users) => ({
    type: CURRENT_JOB_ACCEPTABLE_USERS_SUCCESS,
    payload: users
});

export const currentJobAcceptableUsersFailure = (errors) => ({
    type: CURRENT_JOB_ACCEPTABLE_USERS_FAILURE,
    payload: errors,
  });


export const currentJobAcceptableUsersRefreshPage = () => ({
    type: CURRENT_JOB_ACCEPTABLE_USER_REFRESH
  });
  
  export const currentJobAcceptableUsersLoading = (state) => ({
    type: CURRENT_JOB_ACCEPTABLE_USERS_LOADING,
    payload: state
  });

export const currentJobAcceptableUsersUsersAction = (index,page) => {


    return async (dispatch) => {

        dispatch(currentJobAcceptableUsersLoading(true));

        try {
          // here must replace the ?status=DoneUsers with the right url from the backend
          const response = await axios.get(`${domainServer}${userJobApi}/${index}?status=accepted&page=${page}`, {
              headers: {
                  'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                  'Authorization': `Bearer ${localStorage.getItem('token')}`,
              }
          });

          if (response.status === 200) {
              console.log(`this is the acceptable job users data ${JSON.stringify(response.data.data)}`)
              dispatch(currentJobAcceptableUsersSuccess(response.data.data));
          } else {
              dispatch(currentJobAcceptableUsersFailure(response.data.message));
          }
      } catch (error) {

          if (error.response.status === 401) {
              console.log(`i got 401 in your acceptable job users ${JSON.stringify(error.response.data)}`)
              dispatch(currentJobAcceptableUsersFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
          } else {
              console.error('Error occurred during your acceptable job users:', error.response.data);
              dispatch(currentJobAcceptableUsersFailure(error.response.data));
          }

      }

        dispatch(currentJobAcceptableUsersLoading(false));

    }

}