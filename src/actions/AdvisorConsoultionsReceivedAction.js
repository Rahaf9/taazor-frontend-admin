import axios from 'axios';
import { advisorsApi, domainServer } from '../General/ServerSideApis/ServerSideApis';

export const ADVISOR_CONSOULTIONS_RECEIVED_FAILURE = 'ADVISOR_CONSOULTIONS_RECEIVED_FAILURE';
export const ADVISOR_CONSOULTIONS_RECEIVED_SUCCESS = 'ADVISOR_CONSOULTIONS_RECEIVED_SUCCESS';
export const ADVISOR_CONSOULTIONS_RECEIVED_REFRESH = 'ADVISOR_CONSOULTIONS_RECEIVED_REFRESH';
export const ADVISOR_CONSOULTIONS_RECEIVED_LOADING = 'ADVISOR_CONSOULTIONS_RECEIVED_LOADING';

export const advisorConsolutionsReceivedFailure = (errors) => ({
    type: ADVISOR_CONSOULTIONS_RECEIVED_FAILURE,
    payload: errors,
});

export const advisorConsolutionsReceivedSuccess = (data) => ({
    type: ADVISOR_CONSOULTIONS_RECEIVED_SUCCESS,
    payload: data,
});

export const advisorConsolutionsReceivedRefreshPage = () => ({
    type: ADVISOR_CONSOULTIONS_RECEIVED_REFRESH
});

export const advisorConsolutionsReceivedLoading = (state) => ({
    type: ADVISOR_CONSOULTIONS_RECEIVED_LOADING,
    payload: state
});

export const advisorCompanyConsolutionsReceivedAction = (index) => {

    console.log(`the index in advisors company consultions received action is ${index}`)

    return async (dispatch) => {

        dispatch(advisorConsolutionsReceivedLoading(true));

        try {
            const response = await axios.get(`${domainServer}${advisorsApi}/${index}?type=companies`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`the values of advisors company cons is ${JSON.stringify(response.data.data)}`)
                dispatch(advisorConsolutionsReceivedSuccess(response.data.data));
            } else {
                dispatch(advisorConsolutionsReceivedFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in advisors consultions received ${JSON.stringify(error.response.data)}`)
                dispatch(advisorConsolutionsReceivedFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during advisors consultions received:', error.response.data);
                dispatch(advisorConsolutionsReceivedFailure(error.response.data));
            }

        }

        dispatch(advisorConsolutionsReceivedLoading(false));
    }
};


export const advisorUserConsolutionsReceivedAction = (index) => {

    console.log(`the index in advisors user consultions received action is ${index}`)

    return async (dispatch) => {

        dispatch(advisorConsolutionsReceivedLoading(true));

        try {
            const response = await axios.get(`${domainServer}${advisorsApi}/${index}?type=users`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`the values of advisors user cons is ${JSON.stringify(response.data.data)}`)
                dispatch(advisorConsolutionsReceivedSuccess(response.data.data));
            } else {
                dispatch(advisorConsolutionsReceivedFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in advisors consultions received ${JSON.stringify(error.response.data)}`)
                dispatch(advisorConsolutionsReceivedFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during advisors consultions received:', error.response.data);
                dispatch(advisorConsolutionsReceivedFailure(error.response.data));
            }

        }

        dispatch(advisorConsolutionsReceivedLoading(false));
    }
};
