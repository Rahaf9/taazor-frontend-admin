import axios from "axios";
import { users } from "../FakeData";
import { domainServer, userJobDetailsApi } from "../General/ServerSideApis/ServerSideApis";

export const CURRENT_JOB_ACCEPTABLE_USER_DETAILS_SUCCESS = "CURRENT_JOB_ACCEPTABLE_USER_DETAILS_SUCCESS";
export const CURRENT_JOB_ACCEPTABLE_USER_DETAILS_FAILURE = "CURRENT_JOB_ACCEPTABLE_USER_DETAILS_FAILURE";
export const CURRENT_JOB_ACCEPTABLE_USER_DETAILS_LOADING = "CURRENT_JOB_ACCEPTABLE_USER_DETAILS_LOADING";
export const CURRENT_JOB_ACCEPTABLE_USER_DETAILS_REFRESH = "CURRENT_JOB_ACCEPTABLE_USER_DETAILS_REFRESH";

export const CurrentJobAcceptableUserDetailsSuccess = (users) => ({
    type: CURRENT_JOB_ACCEPTABLE_USER_DETAILS_SUCCESS,
    payload: users
});

export const CurrentJobAcceptableUserDetailsFailure = (errors) => ({
    type: CURRENT_JOB_ACCEPTABLE_USER_DETAILS_FAILURE,
    payload: errors,
});


export const CurrentJobAcceptableUserDetailsRefreshPage = () => ({
    type: CURRENT_JOB_ACCEPTABLE_USER_DETAILS_REFRESH
});

export const CurrentJobAcceptableUserDetailsLoading = (state) => ({
    type: CURRENT_JOB_ACCEPTABLE_USER_DETAILS_LOADING,
    payload: state
});


export const CurrentJobAcceptableUserDetailsAction = (index) => {

    return async (dispatch) => {

        console.log(`this is the index we want to get its user details ${JSON.stringify(index)}`)

        dispatch(CurrentJobAcceptableUserDetailsLoading(true));

        try {
            // replace the ?status=Done with the correct api from the backend 
            const response = await axios.get(`${domainServer}${userJobDetailsApi}/${index}?status=accepted`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`this is the acceptable job user details data ${JSON.stringify(response.data.data)}`)
                dispatch(CurrentJobAcceptableUserDetailsSuccess(response.data.data));
            } else {
                dispatch(CurrentJobAcceptableUserDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in your acceptable job users ${JSON.stringify(error.response.data)}`)
                dispatch(CurrentJobAcceptableUserDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during your acceptable job users:', error.response.data);
                dispatch(CurrentJobAcceptableUserDetailsFailure(error.response.data));
            }

        }

        dispatch(CurrentJobAcceptableUserDetailsLoading(false));

        // dispatch(CurrentUserDetailsSuccess(users));

    }

}