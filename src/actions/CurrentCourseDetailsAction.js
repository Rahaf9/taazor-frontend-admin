import { jobDetails } from "../FakeData";
import axios from 'axios';
import { courseDetailsApi, domainServer, jobsApi } from "../General/ServerSideApis/ServerSideApis";

export const CURRENT_COURSE_DETAILS_SUCCESS = "CURRENT_COURSE_DETAILS_SUCCESS";
export const CURRENT_COURSE_DETAILS_FAILURE = "CURRENT_COURSE_DETAILS_FAILURE";
export const CURRENT_COURSE_DETAILS_LOADING = "CURRENT_COURSE_DETAILS_LOADING";
export const CURRENT_COURSE_DETAILS_REFRESH = "CURRENT_COURSE_DETAILS_REFRESH";

export const CurrentCourseDetailsSuccess = (details) => ({
    type: CURRENT_COURSE_DETAILS_SUCCESS,
    payload: details
});

export const CurrentCourseDetailsFailure = (errors) => ({
    type: CURRENT_COURSE_DETAILS_FAILURE,
    payload: errors,
  });


export const CurrentCourseDetailsRefreshPage = () => ({
    type: CURRENT_COURSE_DETAILS_REFRESH
  });
  
  export const CurrentCourseDetailsLoading = (state) => ({
    type: CURRENT_COURSE_DETAILS_LOADING,
    payload: state
  });
  
  export const CurrentCourseDetailsAction = (index) => {
  
    return async (dispatch) => {
  
      dispatch(CurrentCourseDetailsLoading(true));
      console.log(`the token of me in course details is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${courseDetailsApi}/${index}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
            dispatch(CurrentCourseDetailsSuccess(response.data.data));
        } else {
          dispatch(CurrentCourseDetailsFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your current course details ${JSON.stringify(error.response.data)}`)
          dispatch(CurrentCourseDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your consultions:', error.response.data);
          dispatch(CurrentCourseDetailsFailure(error.response.data));
        }
  
      }
  
      dispatch(CurrentCourseDetailsLoading(false));
    }
  };
  