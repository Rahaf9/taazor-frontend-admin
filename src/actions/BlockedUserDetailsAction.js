import axios from 'axios';
import { addConsulution, advisorDetails, domainServer, registerApi, seekers } from '../General/ServerSideApis/ServerSideApis';

export const BLOCKED_USER_DETAILS_FAILURE = 'BLOCKED_USER_DETAILS_FAILURE';
export const BLOCKED_USER_DETAILS_SUCCESS = 'BLOCKED_USER_DETAILS_SUCCESS';
export const BLOCKED_USER_DETAILS_REFRESH = 'BLOCKED_USER_DETAILS_REFRESH';
export const BLOCKED_USER_DETAILS_LOADING = 'BLOCKED_USER_DETAILS_LOADING';

export const blockedUserDetailsFailure = (errors) => ({
    type: BLOCKED_USER_DETAILS_FAILURE,
    payload: errors,
});

export const blockedUserDetailsSuccess = (data) => ({
    type: BLOCKED_USER_DETAILS_SUCCESS,
    payload: data,
});

export const blockedUserDetailsRefreshPage = () => ({
    type: BLOCKED_USER_DETAILS_REFRESH
});

export const blockedUserDetailsLoading = (state) => ({
    type: BLOCKED_USER_DETAILS_LOADING,
    payload: state
});

export const blockedUserDetailsAction = (index) => {

    console.log(`the index in detailed advisor action is ${index}`)

    return async (dispatch) => {

        dispatch(blockedUserDetailsLoading(true));

        try {
            const response = await axios.get(`${domainServer}${seekers}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });
            console.log(`the data in blocked users is ${JSON.stringify(response.data.data)}`)
            if (response.status === 200) {
                console.log(`the success in blocked users is ${JSON.stringify(response.data.data)}`)
                dispatch(blockedUserDetailsSuccess(response.data.data));
            } else {
                dispatch(blockedUserDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed blocked users ${JSON.stringify(error.response.data)}`)
                dispatch(blockedUserDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed blocked users:', error.response.data);
                dispatch(blockedUserDetailsFailure(error.response.data));
            }

        }

        dispatch(blockedUserDetailsLoading(false));
    }
};
