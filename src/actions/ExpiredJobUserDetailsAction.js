import axios from "axios";
import { users } from "../FakeData";
import { coursesApi, domainServer, userCourseApi, userCourseDetailsApi, userJobDetailsApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_JOB_USER_DETAILS_SUCCESS = "EXPIRED_JOB_USER_DETAILS_SUCCESS";
export const EXPIRED_JOB_USER_DETAILS_FAILURE = "EXPIRED_JOB_USER_DETAILS_FAILURE";
export const EXPIRED_JOB_USER_DETAILS_LOADING = "EXPIRED_JOB_USER_DETAILS_LOADING";
export const EXPIRED_JOB_USER_DETAILS_REFRESH = "EXPIRED_JOB_USER_DETAILS_REFRESH";

export const expiredJobUserDetailsSuccess = (users) => ({
    type: EXPIRED_JOB_USER_DETAILS_SUCCESS,
    payload: users
});

export const expiredJobUserDetailsFailure = (errors) => ({
    type: EXPIRED_JOB_USER_DETAILS_FAILURE,
    payload: errors,
});


export const expiredJobUserDetailsRefreshPage = () => ({
    type: EXPIRED_JOB_USER_DETAILS_REFRESH
});

export const expiredJobUserDetailsLoading = (state) => ({
    type: EXPIRED_JOB_USER_DETAILS_LOADING,
    payload: state
});


export const expiredJobUserDetailsAction = (index) => {

    return async (dispatch) => {

        console.log(`this is the index we want to get its user details ${JSON.stringify(index)}`)

        dispatch(expiredJobUserDetailsLoading(true));

        try {
            const response = await axios.get(`${domainServer}${userJobDetailsApi}/${index}?status=Expired`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`this is the job user details data ${JSON.stringify(response.data.data)}`)
                dispatch(expiredJobUserDetailsSuccess(response.data.data));
            } else {
                dispatch(expiredJobUserDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in your course users ${JSON.stringify(error.response.data)}`)
                dispatch(expiredJobUserDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during your course users:', error.response.data);
                dispatch(expiredJobUserDetailsFailure(error.response.data));
            }

        }

        dispatch(expiredJobUserDetailsLoading(false));

        // dispatch(expiredJobUserDetailsSuccess(users));

    }

}