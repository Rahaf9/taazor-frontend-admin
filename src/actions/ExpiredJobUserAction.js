import axios from "axios";
import { users } from "../FakeData";
import { domainServer, userJobApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_JOB_USERS_SUCCESS = "EXPIRED_JOB_USERS_SUCCESS";
export const EXPIRED_JOB_USERS_FAILURE = "EXPIRED_JOB_USERS_FAILURE";
export const EXPIRED_JOB_USERS_LOADING = "EXPIRED_JOB_USERS_LOADING";
export const EXPIRED_JOB_USER_REFRESH = "EXPIRED_JOB_USER_REFRESH";


export const expiredJobUsersSuccess = (users) => ({
    type: EXPIRED_JOB_USERS_SUCCESS,
    payload: users
});

export const expiredJobUserFailure = (errors) => ({
    type: EXPIRED_JOB_USERS_FAILURE,
    payload: errors,
  });


export const expiredJobUserRefreshPage = () => ({
    type: EXPIRED_JOB_USER_REFRESH
  });
  
  export const expiredJobUserLoading = (state) => ({
    type: EXPIRED_JOB_USERS_LOADING,
    payload: state
  });

export const expiredJobUsersAction = (index,page) => {


    return async (dispatch) => {

        dispatch(expiredJobUserLoading(true));

        try {
          // here must replace the userexpired jobApi with the jobUserApi when it be able to access to
          const response = await axios.get(`${domainServer}${userJobApi}/${index}?status=Expired&page=${page}`, {
              headers: {
                  'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                  'Authorization': `Bearer ${localStorage.getItem('token')}`,
              }
          });

          if (response.status === 200) {
              console.log(`this is the expired job users data ${JSON.stringify(response.data.data)}`)
              dispatch(expiredJobUsersSuccess(response.data.data));
          } else {
              dispatch(expiredJobUserFailure(response.data.message));
          }
      } catch (error) {

          if (error.response.status === 401) {
              console.log(`i got 401 in your expired job users ${JSON.stringify(error.response.data)}`)
              dispatch(expiredJobUserFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
          } else {
              console.error('Error occurred during your expired job users:', error.response.data);
              dispatch(expiredJobUserFailure(error.response.data));
          }

      }

        dispatch(expiredJobUserLoading(false));

    }

}