// actionTypes.js

import { addCourseApi, domainServer } from "../General/ServerSideApis/ServerSideApis";
import axios from 'axios';

export const SET_ADD_COURSE_FAILURE = 'SET_ADD_COURSE_FAILURE';
export const SET_ADD_COURSE_SUCCESS = 'SET_ADD_COURSE_SUCCESS';
export const SET_ADD_COURSE_REFRESH = 'SET_ADD_COURSE_REFRESH';
export const SET_ADD_COURSE_LOADING = 'SET_ADD_COURSE_LOADING';

// actionCreators.js

export const addCourseFailure = (errors) => ({
  type: SET_ADD_COURSE_FAILURE,
  payload: errors,
});

export const addCourseSuccess = (data) => ({
  type: SET_ADD_COURSE_SUCCESS,
  payload: data,
});

export const addCourseRefreshPage = () => ({
  type: SET_ADD_COURSE_REFRESH
});

export const addCourseLoading = (state) => ({
  type: SET_ADD_COURSE_LOADING,
  payload:state
});

export const addCourseAction = (formData) => {
  
  return async (dispatch) => {

    dispatch(addCourseLoading(true));
    console.log(`this is the data here in add course ${formData}`)
    
    try {
      const response = await axios.post(`${domainServer}${addCourseApi}`, {
        'duration': formData.mappedFormData.duration,
        'number_trainees': formData.mappedFormData.applicants,
        'reqs': formData.mappedFormData.reqs,
        'price': formData.mappedFormData.cost,
        'topic': formData.mappedFormData.topic,
        'name': formData.mappedFormData.name,
        'location': formData.mappedFormData.location,
        'start_date': formData.componentDate.startDate,
        'end_date': formData.componentDate.endDate,
        'days': formData.componentDate.day,
        'type': formData.envCheckedStatus.online ? 'Online' : formData.envCheckedStatus.offline ? 'Offline' :  'all',
        'gender': formData.genderCheckedStatus.male ? 'Male' : 'Female',
      },{
        headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
      });

      if (response.status === 200) {
        dispatch(addCourseSuccess(response.data.data));
      } else {
        dispatch(addCourseFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 422) {
        console.error(`errors in add Courses ${error.response.data.message}`);
        dispatch(addCourseFailure(error.response.data));
      } else if (error.response.status === 401) {
        console.log(`i got 401 in add Courses ${JSON.stringify(error.response.data)}`)
        dispatch(addCourseFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else{
        console.error('Error occurred during add consultion:', error.response.data);
        dispatch(addCourseFailure(error.response.data));
      }

    }

    dispatch(addCourseLoading(false));
  }
};
