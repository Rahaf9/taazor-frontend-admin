import axios from 'axios';
import { domainServer, companyConsulutions, convertApi } from '../General/ServerSideApis/ServerSideApis';

export const SET_COMPANY_REFER_FAILURE = 'SET_COMPANY_REFER_FAILURE';
export const SET_COMPANY_REFER_SUCCESS = 'SET_COMPANY_REFER_SUCCESS';
export const SET_COMPANY_REFER_REFRESH = 'SET_COMPANY_REFER_REFRESH';
export const SET_COMPANY_REFER_LOADING = 'SET_COMPANY_REFER_LOADING';

export const companyReferFailure = (errors) => ({
  type: SET_COMPANY_REFER_FAILURE,
  payload: errors,
});

export const companyReferSuccess = (data) => ({
  type: SET_COMPANY_REFER_SUCCESS,
  payload: data,
});

export const companyReferRefreshPage = () => ({
  type: SET_COMPANY_REFER_REFRESH
});

export const companyReferLoading = (state) => ({
  type: SET_COMPANY_REFER_LOADING,
  payload:state
});

export const companyReferAction = (advisorId,consultionId) => {
  
  return async (dispatch) => {

    console.log(`the formData in company refer info is ${JSON.stringify(advisorId)} and ${JSON.stringify(consultionId)}`)
    dispatch(companyReferLoading(true));
    
    try {
      const response = await axios.post(`${domainServer}${companyConsulutions}${convertApi}/${consultionId}`, {
        'advisor_id':advisorId
      },{
        headers: {
          'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
      });

      if (response.status === 200) {
        console.log(`here in company refer we got 200 code`)
        dispatch(companyReferSuccess(response.data.data));
      } else {
        console.log(`here in company refer we got 2** code and its data is ${JSON.stringify(response.data)}`)
        dispatch(companyReferFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 409) {
        console.error(`errors companyRefer ${error.response.data.message}`);
        dispatch(companyReferFailure(error.response.data));
      } else if (error.response.status === 400) {
        console.error(`errors companyRefer ${error.response.data.message}`);
        dispatch(companyReferFailure(error.response.data)); // the form of the error.response.data is "message": "adkasfhksfh"
      }
      else if (error.response.status === 422) {
        console.error(`errors companyRefer ${JSON.stringify(error.response.data.errors)}`);
        dispatch(companyReferFailure(error.response.data.errors)); //the form of the error.response.data is  {"password": [ "The password field confirmation does not match." ]}
      } else {
        console.error('Error occurred during company refer:', error.response.data);
        dispatch(companyReferFailure(error.response.data));
      }

    }

    dispatch(companyReferLoading(false));
  }
};
