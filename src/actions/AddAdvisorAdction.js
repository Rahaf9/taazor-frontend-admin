import axios from 'axios';
import { domainServer,advisorsApi } from '../General/ServerSideApis/ServerSideApis';

export const SET_ADD_ADVISOR_FAILURE = 'SET_ADD_ADVISOR_FAILURE';
export const SET_ADD_ADVISOR_SUCCESS = 'SET_ADD_ADVISOR_SUCCESS';
export const SET_ADD_ADVISOR_REFRESH = 'SET_ADD_ADVISOR_REFRESH';
export const SET_ADD_ADVISOR_LOADING = 'SET_ADD_ADVISOR_LOADING';

export const addAdvisorFailure = (errors) => ({
  type: SET_ADD_ADVISOR_FAILURE,
  payload: errors,
});

export const addAdvisorSuccess = (data) => ({
  type: SET_ADD_ADVISOR_SUCCESS,
  payload: data,
});

export const addAdvisorRefreshPage = () => ({
  type: SET_ADD_ADVISOR_REFRESH
});

export const addAdvisorLoading = (state) => ({
  type: SET_ADD_ADVISOR_LOADING,
  payload:state
});

export const addAdvisorAction = (formData) => {
  
  return async (dispatch) => {

    dispatch(addAdvisorLoading(true));
    
    try {
      const response = await axios.post(`${domainServer}${advisorsApi}`, formData ,{
        headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
      });

      if (response.status === 200) {
        console.log(`the Advisor added successfully`)
        dispatch(addAdvisorSuccess(response.data.data));
      } else {
        dispatch(addAdvisorFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 422) {
        console.error(`errors in add Advisors ${error.response.data.message}`);
        dispatch(addAdvisorFailure(error.response.data));
      } else if (error.response.status === 401) {
        console.log(`i got 401 in add Advisors ${JSON.stringify(error.response.data)}`)
        dispatch(addAdvisorFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else{
        console.error('Error occurred during add Advisors:', error.response.data);
        dispatch(addAdvisorFailure(error.response.data));
      }

    }

    dispatch(addAdvisorLoading(false));
  }
};
