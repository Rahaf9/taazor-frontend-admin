import { jobDetails } from "../FakeData";
import axios from 'axios';
import { domainServer, jobDetailsApi, jobsApi } from "../General/ServerSideApis/ServerSideApis";

export const CURRENT_JOB_DETAILS_SUCCESS = "CURRENT_JOB_DETAILS_SUCCESS";
export const CURRENT_JOB_DETAILS_FAILURE = "CURRENT_JOB_DETAILS_FAILURE";
export const CURRENT_JOB_DETAILS_LOADING = "CURRENT_JOB_DETAILS_LOADING";
export const CURRENT_JOB_DETAILS_REFRESH = "CURRENT_JOB_DETAILS_REFRESH";

export const CurrentJobDetailsSuccess = (details) => ({
    type: CURRENT_JOB_DETAILS_SUCCESS,
    payload: details
});

export const CurrentJobDetailsFailure = (errors) => ({
    type: CURRENT_JOB_DETAILS_FAILURE,
    payload: errors,
  });


export const CurrentJobDetailsRefreshPage = () => ({
    type: CURRENT_JOB_DETAILS_REFRESH
  });
  
  export const CurrentJobDetailsLoading = (state) => ({
    type: CURRENT_JOB_DETAILS_LOADING,
    payload: state
  });
  
  export const CurrentJobDetailsAction = (index) => {
  
    return async (dispatch) => {
  
      dispatch(CurrentJobDetailsLoading(true));
      console.log(`the token of me in jobs details is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${jobDetailsApi}/${index}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
            dispatch(CurrentJobDetailsSuccess(response.data.data));
        } else {
          dispatch(CurrentJobDetailsFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your consultants ${JSON.stringify(error.response.data)}`)
          dispatch(CurrentJobDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your consultions:', error.response.data);
          dispatch(CurrentJobDetailsFailure(error.response.data));
        }
  
      }
  
      dispatch(CurrentJobDetailsLoading(false));
    }
  };
  