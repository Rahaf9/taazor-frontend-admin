import axios from 'axios';
import { addConsulution, advisorDetails, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const WAITING_COMPANIES_DETAILS_FAILURE = 'WAITING_COMPANIES_DETAILS_FAILURE';
export const WAITING_COMPANIES_DETAILS_SUCCESS = 'WAITING_COMPANIES_DETAILS_SUCCESS';
export const WAITING_COMPANIES_DETAILS_REFRESH = 'WAITING_COMPANIES_DETAILS_REFRESH';
export const WAITING_COMPANIES_DETAILS_LOADING = 'WAITING_COMPANIES_DETAILS_LOADING';

export const waitingCompaiesDetailsFailure = (errors) => ({
    type: WAITING_COMPANIES_DETAILS_FAILURE,
    payload: errors,
});

export const waitingCompaiesDetailsSuccess = (data) => ({
    type: WAITING_COMPANIES_DETAILS_SUCCESS,
    payload: data,
});

export const waitingCompaiesDetailsRefreshPage = () => ({
    type: WAITING_COMPANIES_DETAILS_REFRESH
});

export const waitingCompaiesDetailsLoading = (state) => ({
    type: WAITING_COMPANIES_DETAILS_LOADING,
    payload: state
});

export const waitingCompaiesDetailsAction = (index) => {

    console.log(`the index in detailed advisor action is ${index}`)

    return async (dispatch) => {

        dispatch(waitingCompaiesDetailsLoading(true));

        try {
            const response = await axios.post(`${domainServer}${consulutionDetails}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                dispatch(waitingCompaiesDetailsSuccess(response.data.data));
            } else {
                dispatch(waitingCompaiesDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed advisor ${JSON.stringify(error.response.data)}`)
                dispatch(waitingCompaiesDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed advisor:', error.response.data);
                dispatch(waitingCompaiesDetailsFailure(error.response.data));
            }

        }

        dispatch(waitingCompaiesDetailsLoading(false));
    }
};
