import axios from 'axios';
import { companyConsulutions, consulutionDetails, domainServer } from '../General/ServerSideApis/ServerSideApis';

export const SET_COMPANY_CONSULTANT_DETAILS_FAILURE = 'SET_COMPANY_CONSULTANT_DETAILS_FAILURE';
export const SET_COMPANY_CONSULTANT_DETAILS_SUCCESS = 'SET_COMPANY_CONSULTANT_DETAILS_SUCCESS';
export const SET_COMPANY_CONSULTANT_DETAILS_REFRESH = 'SET_COMPANY_CONSULTANT_DETAILS_REFRESH';
export const SET_COMPANY_CONSULTANT_DETAILS_LOADING = 'SET_COMPANY_CONSULTANT_DETAILS_LOADING';

export const companyConsultantDetailsFailure = (errors) => ({
    type: SET_COMPANY_CONSULTANT_DETAILS_FAILURE,
    payload: errors,
});

export const companyConsultantDetailsSuccess = (data) => ({
    type: SET_COMPANY_CONSULTANT_DETAILS_SUCCESS,
    payload: data,
});

export const companyConsultantDetailsRefreshPage = () => ({
    type: SET_COMPANY_CONSULTANT_DETAILS_REFRESH
});

export const companyConsultantDetailsLoading = (state) => ({
    type: SET_COMPANY_CONSULTANT_DETAILS_LOADING,
    payload: state
});

export const companyConsultantDetailsAction = (index) => {

    console.log(`the index in detailed consultion action is ${index}`)

    return async (dispatch) => {

        dispatch(companyConsultantDetailsLoading(true));

        try {
            const response = await axios.get(`${domainServer}${companyConsulutions}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`the companyConsultantDetailsSuccess data is`,response.data)
                dispatch(companyConsultantDetailsSuccess(response.data.data));
            } else {
                dispatch(companyConsultantDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed Consultants ${JSON.stringify(error.response.data)}`)
                dispatch(companyConsultantDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed consultion:', error.response.data);
                dispatch(companyConsultantDetailsFailure(error.response.data));
            }

        }

        dispatch(companyConsultantDetailsLoading(false));
    }
};
