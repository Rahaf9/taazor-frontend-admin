import { Course } from "../FakeData";
import axios from 'axios';
import { deleteAllNoti, deleteNoti, domainServer, notifications, readAll } from "../General/ServerSideApis/ServerSideApis";

export const NOTIFICATIONS_SUCCESS = "NOTIFICATIONS_SUCCESS";
export const NOTIFICATIONS_DELETE_SUCCESS = "NOTIFICATIONS_DELETE_SUCCESS";
export const NOTIFICATIONS_FAILURE = "NOTIFICATIONS_FAILURE";
export const NOTIFICATIONS_DELETE_FAILURE = "NOTIFICATIONS_DELETE_FAILURE";
export const NOTIFICATIONS_LOADING = "NOTIFICATIONS_LOADING";
export const NOTIFICATIONS_DELETE_LOADING = "NOTIFICATIONS_DELETE_LOADING";
export const NOTIFICATIONS_REFRESH = "NOTIFICATIONS_REFRESH";
export const NOTIFICATIONS_DELETE_REFRESH = "NOTIFICATIONS_DELETE_REFRESH";


export const notificationsSuccess = (details) => ({
  type: NOTIFICATIONS_SUCCESS,
  payload: details
});

export const notificationsDeleteSuccess = (details) => ({
  type: NOTIFICATIONS_DELETE_SUCCESS,
  payload: details
});

export const notificationsFailure = (errors) => ({
  type: NOTIFICATIONS_FAILURE,
  payload: errors,
});

export const notificationsDeleteFailure = (errors) => ({
  type: NOTIFICATIONS_DELETE_FAILURE,
  payload: errors,
});


export const notificationsRefreshPage = () => ({
  type: NOTIFICATIONS_REFRESH
});

export const notificationsDeleteRefreshPage = () => ({
  type: NOTIFICATIONS_DELETE_REFRESH
});


export const notificationsLoading = (state) => ({
  type: NOTIFICATIONS_LOADING,
  payload: state
});

export const notificationsDeleteLoading = (state) => ({
  type: NOTIFICATIONS_DELETE_LOADING,
  payload: state
});

export const notificationsAction = (page) => {

  return async (dispatch) => {

    dispatch(notificationsLoading(true));
    console.log(`the token of me innotifications is ${JSON.stringify(localStorage.getItem('token'))}`)

    try {
      const response = await axios.get(`${domainServer}${notifications}`,
        {
          headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
        });

      if (response.status === 200) {
        console.log(`the data notifications is ${JSON.stringify(response.data.data)}`)
        dispatch(notificationsSuccess(response.data.data));
      } else {
        dispatch(notificationsFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 401) {
        console.log(`i got 401 in notifications ${JSON.stringify(error.response.data)}`)
        dispatch(notificationsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else {
        console.error('Error occurred during your notifications:', error.response.data);
        dispatch(notificationsFailure(error.response.data));
      }

    }

    dispatch(notificationsLoading(false));
  }
};

export const allNotiReadAction = (page) => {

  return async (dispatch) => {

    try {
      const response = await axios.post(`${domainServer}${notifications}${readAll}`,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
        });

      if (response.status === 200) {
        console.log(`the data notifications is ${JSON.stringify(response.data.data)}`)
        // dispatch(notificationsSuccess(response.data.data));
      } else {
        dispatch(notificationsDeleteFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 401) {
        console.log(`i got 401 in notifications ${JSON.stringify(error.response.data)}`)
        dispatch(notificationsDeleteFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else {
        console.error('Error occurred during your notifications:', error.response.data);
        dispatch(notificationsDeleteFailure(error.response.data));
      }

    }

    dispatch(notificationsLoading(false));
  }
};

export const deleteNotiReadAction = (id) => {

  return async (dispatch) => {

    dispatch(notificationsDeleteLoading(true));

    try {
      const response = await axios.delete(`${domainServer}${notifications}${deleteNoti}/${id}`,
        {
          headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
        });

      if (response.status === 200) {
        console.log(`the data delete notifications is ${JSON.stringify(response.data)}`)
        dispatch(notificationsDeleteSuccess(response.data.data));
      } else {
        dispatch(notificationsDeleteFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 401) {
        console.log(`i got 401 in notifications ${JSON.stringify(error.response.data)}`)
        dispatch(notificationsDeleteFailure(error.response.data));
      } else {
        console.error('Error occurred during delete your notifications:', error.response.data);
        dispatch(notificationsDeleteFailure(error.response.data));
      }

    }

    dispatch(notificationsDeleteLoading(false));
  }
};


/////////////////////////////////////////////////////////////////////////////////////


// Add these new action types
export const NOTIFICATIONS_DELETE_ALL_SUCCESS = "NOTIFICATIONS_DELETE_ALL_SUCCESS";
export const NOTIFICATIONS_DELETE_ALL_FAILURE = "NOTIFICATIONS_DELETE_ALL_FAILURE";
export const NOTIFICATIONS_DELETE_ALL_LOADING = "NOTIFICATIONS_DELETE_ALL_LOADING";
export const NOTIFICATIONS_DELETE_ALL_REFRESH = "NOTIFICATIONS_DELETE_ALL_REFRESH";


// Action creator for successful deletion of all notifications
export const notificationsDeleteAllSuccess = (details) => ({
  type: NOTIFICATIONS_DELETE_ALL_SUCCESS,
  payload: details
});

// Action creator for failed deletion of all notifications
export const notificationsDeleteAllFailure = (errors) => ({
  type: NOTIFICATIONS_DELETE_ALL_FAILURE,
  payload: errors
});

// Action creator for loading state of delete all notifications
export const notificationsDeleteAllLoading = (state) => ({
  type: NOTIFICATIONS_DELETE_ALL_LOADING,
  payload: state
});

export const notificationsDeleteAllRefreshPage = (state) => ({
  type: NOTIFICATIONS_DELETE_ALL_REFRESH,
  payload: state
});

// Thunk action to delete all notifications
export const deleteAllNotificationsAction = () => {
  return async (dispatch) => {
    dispatch(notificationsDeleteAllLoading(true));

    try {
      const response = await axios.delete(`${domainServer}${notifications}${deleteAllNoti}`, {
        headers: {
          'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
      });

      if (response.status === 200) {
        dispatch(notificationsDeleteAllSuccess(response.data.data));
      } else {
        dispatch(notificationsDeleteAllFailure(response.data.message));
      }
    } catch (error) {
      if (error.response.status === 401) {
        dispatch(notificationsDeleteAllFailure({ 'message': error.response.data.message }));
      } else {
        dispatch(notificationsDeleteAllFailure(error.response.data));
      }
    }

    dispatch(notificationsDeleteAllLoading(false));
  };
};
