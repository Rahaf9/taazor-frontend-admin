// actionTypes.js

import { addJobApi, domainServer } from "../General/ServerSideApis/ServerSideApis";
import axios from 'axios';

export const SET_ADD_JOB_FAILURE = 'SET_ADD_JOB_FAILURE';
export const SET_ADD_JOB_SUCCESS = 'SET_ADD_JOB_SUCCESS';
export const SET_ADD_JOB_REFRESH = 'SET_ADD_JOB_REFRESH';
export const SET_ADD_JOB_LOADING = 'SET_ADD_JOB_LOADING';

// actionCreators.js

export const addJobFailure = (errors) => ({
  type: SET_ADD_JOB_FAILURE,
  payload: errors,
});

export const addJobSuccess = (data) => ({
  type: SET_ADD_JOB_SUCCESS,
  payload: data,
});

export const addJobRefreshPage = () => ({
  type: SET_ADD_JOB_REFRESH
});

export const addJobLoading = (state) => ({
  type: SET_ADD_JOB_LOADING,
  payload: state
});

export const addJobAction = (formData, questions) => {

  return async (dispatch) => {

    dispatch(addJobLoading(true));
    console.log(`this is the data here in add job ${JSON.stringify(formData)} and this is questions ${JSON.stringify(questions)}`)

    const { jobTitle, topic, salary, eduLevel, reqs, applicants, location, qulaified, languages } = formData.mappedFormData;
    const { startDate, year } = formData.componentDate;
    const { online, offline, all } = formData.envCheckedStatus;
    const { male, female } = formData.genderCheckedStatus;
    const { FullTime, PartTime } = formData.jobTimeCheckedStatus;
    const { غيرمؤجل, مؤجل, متسرح } = formData.militaryCheckedStatus;
    const { driveAgree } = formData.driveCheckedStatus;
    const { imageAgree } = formData.imageCheckedStatus;
    const { userAgree } = formData.userCheckedStatus

    try {
      const response = await axios.post(`${domainServer}${addJobApi}`, {
        'hidden_name': userAgree,
        'number_trainees': applicants,
        'require_qualifications': reqs,
        'job_title': salary,
        'topic': topic,
        'location': location,
        'start_date': startDate,
        'job_environment': online ? 'Online' : offline ? 'Offline' : 'all',
        'salary_fields': year,
        'gender': male ? 'Male' : 'Female',
        'is_required_license': driveAgree,
        'is_required_military': مؤجل ? 'مؤجل' : غيرمؤجل ? 'غيرمؤجل' : 'متسرح',
        'job_time': formData.genderCheckedStatus.male,
        'is_required_image': imageAgree,
        'experiense_years': year,
        'required_languages': languages,
        "require_qualifications": qulaified,
        "forms": {
          "is_required": true,
          "questions": questions
        }
      }, {
        headers: {
          'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
      });

      if (response.status === 200) {
        dispatch(addJobSuccess(response.data.data));
      } else {
        dispatch(addJobFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 422) {
        console.error(`errors in add Courses ${error.response.data.message}`);
        dispatch(addJobFailure(error.response.data));
      } else if (error.response.status === 401) {
        console.log(`i got 401 in add Courses ${JSON.stringify(error.response.data)}`)
        dispatch(addJobFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else {
        console.error('Error occurred during add consultion:', error.response.data);
        dispatch(addJobFailure(error.response.data));
      }

    }

    dispatch(addJobLoading(false));
  }
};
