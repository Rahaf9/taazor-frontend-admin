import axios from "axios";
import { domainServer, userJobDetailsApi } from "../General/ServerSideApis/ServerSideApis";

export const JOB_USER_DETAILS_SUCCESS = "JOB_USER_DETAILS_SUCCESS";
export const JOB_USER_DETAILS_FAILURE = "JOB_USER_DETAILS_FAILURE";
export const JOB_USER_DETAILS_LOADING = "JOB_USER_DETAILS_LOADING";
export const JOB_USER_DETAILS_REFRESH = "JOB_USER_DETAILS_REFRESH";

export const jobUserDetailsSuccess = (users) => ({
    type: JOB_USER_DETAILS_SUCCESS,
    payload: users
});

export const jobUserDetailsFailure = (errors) => ({
    type: JOB_USER_DETAILS_FAILURE,
    payload: errors,
});


export const jobUserDetailsRefreshPage = () => ({
    type: JOB_USER_DETAILS_REFRESH
});

export const jobUserDetailsLoading = (state) => ({
    type: JOB_USER_DETAILS_LOADING,
    payload: state
});


export const jobUserDetailsAction = (index) => {

    return async (dispatch) => {

        console.log(`this is the index we want to get its user details ${JSON.stringify(index)}`)

        dispatch(jobUserDetailsLoading(true));

        try {
            const response = await axios.get(`${domainServer}${userJobDetailsApi}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`this is the job user details data ${JSON.stringify(response.data.data)}`)
                dispatch(jobUserDetailsSuccess(response.data.data));
            } else {
                dispatch(jobUserDetailsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in your course users ${JSON.stringify(error.response.data)}`)
                dispatch(jobUserDetailsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during your course users:', error.response.data);
                dispatch(jobUserDetailsFailure(error.response.data));
            }

        }

        dispatch(jobUserDetailsLoading(false));

        // dispatch(jobUserDetailsSuccess(users));

    }

}