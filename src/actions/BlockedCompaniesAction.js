import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, coursesApi, companiesApi } from "../General/ServerSideApis/ServerSideApis";

export const BLOCKED_COMPANIES_SUCCESS = " BLOCKED_COMPANIES_SUCCESS";
export const  BLOCKED_COMPANIES_FAILURE = " BLOCKED_COMPANIES_FAILURE";
export const  BLOCKED_COMPANIES_LOADING = " BLOCKED_COMPANIES_LOADING";
export const  BLOCKED_COMPANIES_REFRESH = " BLOCKED_COMPANIES_REFRESH";

export const BlockedCompaniesSuccess = (details) => ({
    type: BLOCKED_COMPANIES_SUCCESS,
    payload: details
});

export const BlockedCompaniesFailure = (errors) => ({
    type: BLOCKED_COMPANIES_FAILURE,
    payload: errors,
  });


export const BlockedCompaniesRefreshPage = () => ({
    type: BLOCKED_COMPANIES_REFRESH
  });
  
  export const BlockedCompaniesLoading = (state) => ({
    type: BLOCKED_COMPANIES_LOADING,
    payload: state
  });
  
  export const BlockedCompaniesAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(BlockedCompaniesLoading(true));
      console.log(`the token of me in blocked companies  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${companiesApi}?status=banned&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the bloked companies ${JSON.stringify(response.data.data)}`)
            dispatch(BlockedCompaniesSuccess(response.data.data));
        } else {
          dispatch(BlockedCompaniesFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in ablocked companies ${JSON.stringify(error.response.data)}`)
          dispatch(BlockedCompaniesFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your Blocked companies:', error.response.data);
          dispatch(BlockedCompaniesFailure(error.response.data));
        }
  
      }
  
      dispatch(BlockedCompaniesLoading(false));
    }
  };
  