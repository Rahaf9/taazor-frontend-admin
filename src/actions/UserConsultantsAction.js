import axios from 'axios';
import { domainServer, userConsulting } from '../General/ServerSideApis/ServerSideApis';

export const SET_USER_CONSULTANTS_FAILURE = 'SET_USER_CONSULTANTS_FAILURE';
export const SET_USER_CONSULTANTS_SUCCESS = 'SET_USER_CONSULTANTS_SUCCESS';
export const SET_USER_CONSULTANTS_REFRESH = 'SET_USER_CONSULTANTS_REFRESH';
export const SET_USER_CONSULTANTS_LOADING = 'SET_USER_CONSULTANTS_LOADING';

export const userConsultantsFailure = (errors) => ({
  type: SET_USER_CONSULTANTS_FAILURE,
  payload: errors,
});

export const userConsultantsSuccess = (data) => ({
  type: SET_USER_CONSULTANTS_SUCCESS,
  payload: data,
});

export const userConsultantsRefreshPage = () => ({
  type: SET_USER_CONSULTANTS_REFRESH
});

export const userConsultantsLoading = (state) => ({
  type: SET_USER_CONSULTANTS_LOADING,
  payload: state
});

export const userConsultantsAction = (page) => {
  
  return async (dispatch) => {

    dispatch(userConsultantsLoading(true));
    console.log(`the token of me in user consultions is ${JSON.stringify(localStorage.getItem('token'))} 
    and the page is ${page}`)

    try {
      const response = await axios.get(`${domainServer}${userConsulting}?page=${page}`,
        {
          headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
        });

      if (response.status === 200) {
        console.log(`the user consls success is ${JSON.stringify(response.data.data)}`)
        dispatch(userConsultantsSuccess(response.data.data.consultations));
      } else {
        dispatch(userConsultantsFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 401) {
        console.log(`i got 401 in user consultants ${JSON.stringify(error.response.data)}`)
        dispatch(userConsultantsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else {
        console.error('Error occurred during user consultions:', error.response.data);
        dispatch(userConsultantsFailure(error.response.data));
      }

    }

    dispatch(userConsultantsLoading(false));
  }
};
