import axios from "axios";
import { domainServer, userJobApi } from "../General/ServerSideApis/ServerSideApis";

export const JOB_USERS_SUCCESS = "JOB_USERS_SUCCESS";
export const JOB_USERS_FAILURE = "JOB_USERS_FAILURE";
export const JOB_USERS_LOADING = "JOB_USERS_LOADING";
export const JOB_USER_REFRESH = "JOB_USER_REFRESH";


export const jobUsersSuccess = (users) => ({
    type: JOB_USERS_SUCCESS,
    payload: users
});

export const JobUserFailure = (errors) => ({
    type: JOB_USERS_FAILURE,
    payload: errors,
  });


export const JobUserRefreshPage = () => ({
    type: JOB_USER_REFRESH
  });
  
  export const JobUserLoading = (state) => ({
    type: JOB_USERS_LOADING,
    payload: state
  });

export const jobUsersAction = (index,page) => {


    return async (dispatch) => {

        dispatch(JobUserLoading(true));

        try {
          // here must replace the userCourseApi with the jobUserApi when it be able to access to
          const response = await axios.get(`${domainServer}${userJobApi}/${index}?page=${page}`, {
              headers: {
                  'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                  'Authorization': `Bearer ${localStorage.getItem('token')}`,
              }
          });

          if (response.status === 200) {
              console.log(`this is the course users data ${JSON.stringify(response.data.data)}`)
              dispatch(jobUsersSuccess(response.data.data));
          } else {
              dispatch(JobUserFailure(response.data.message));
          }
      } catch (error) {

          if (error.response.status === 401) {
              console.log(`i got 401 in your course users ${JSON.stringify(error.response.data)}`)
              dispatch(JobUserFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
          } else {
              console.error('Error occurred during your course users:', error.response.data);
              dispatch(JobUserFailure(error.response.data));
          }

      }

        dispatch(JobUserLoading(false));

    }

}