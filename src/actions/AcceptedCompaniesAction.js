import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, coursesApi, companiesApi } from "../General/ServerSideApis/ServerSideApis";

export const ACCEPTED_COMPANIES_SUCCESS = "ACCEPTED_COMPANIES_SUCCESS";
export const ACCEPTED_COMPANIES_FAILURE = "ACCEPTED_COMPANIES_FAILURE";
export const ACCEPTED_COMPANIES_LOADING = "ACCEPTED_COMPANIES_LOADING";
export const ACCEPTED_COMPANIES_REFRESH = "ACCEPTED_COMPANIES_REFRESH";

export const AcceptedCompaniesSuccess = (details) => ({
    type: ACCEPTED_COMPANIES_SUCCESS,
    payload: details
});

export const AcceptedCompaniesFailure = (errors) => ({
    type: ACCEPTED_COMPANIES_FAILURE,
    payload: errors,
  });


export const AcceptedCompaniesRefreshPage = () => ({
    type: ACCEPTED_COMPANIES_REFRESH
  });
  
  export const AcceptedCompaniesLoading = (state) => ({
    type: ACCEPTED_COMPANIES_LOADING,
    payload: state
  });
  
  export const AcceptedCompaniesAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(AcceptedCompaniesLoading(true));
      console.log(`the token of me in accepted companies  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${companiesApi}?status=acceptable&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the accepted companies is ${JSON.stringify(response.data.data)}`)
            dispatch(AcceptedCompaniesSuccess(response.data.data));
        } else {
          dispatch(AcceptedCompaniesFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in accepted companies ${JSON.stringify(error.response.data)}`)
          dispatch(AcceptedCompaniesFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your Accepted companies:', error.response.data);
          dispatch(AcceptedCompaniesFailure(error.response.data));
        }
  
      }
  
      dispatch(AcceptedCompaniesLoading(false));
    }
  };
  