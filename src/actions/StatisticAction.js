import axios from 'axios';
import { addConsulution, advisorDetails, domainServer, getStatisticssApi, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const SET_STATISTIC_FAILURE = 'SET_STATISTIC_FAILURE';
export const SET_STATISTIC_SUCCESS = 'SET_STATISTIC_SUCCESS';
export const SET_STATISTIC_REFRESH = 'SET_STATISTIC_REFRESH';
export const SET_STATISTIC_LOADING = 'SET_STATISTIC_LOADING';

export const setStatisticsFailure = (errors) => ({
    type: SET_STATISTIC_FAILURE,
    payload: errors,
});

export const setStatisticsSuccess = (data) => ({
    type: SET_STATISTIC_SUCCESS,
    payload: data,
});

export const setStatisticsRefreshPage = () => ({
    type: SET_STATISTIC_REFRESH
});

export const setStatisticsLoading = (state) => ({
    type: SET_STATISTIC_LOADING,
    payload: state
});

export const setStatisticsAction = () => {

    return async (dispatch) => {

        dispatch(setStatisticsLoading(true));
        console.log(`the lang of the statics is `,localStorage.getItem('lang'))

        try {
            const response = await axios.get(`${domainServer}${getStatisticssApi}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`the data is ${JSON.stringify(response.data.data)}`)
                dispatch(setStatisticsSuccess(response.data.data));
            } else {
                dispatch(setStatisticsFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in detailed advisor ${JSON.stringify(error.response.data)}`)
                dispatch(setStatisticsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during detailed advisor:', error.response.data);
                dispatch(setStatisticsFailure(error.response.data));
            }

        }

        dispatch(setStatisticsLoading(false));
    }
};
