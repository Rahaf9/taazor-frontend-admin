import axios from 'axios';
import { addConsulution, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const SET_ADD_CONSULTANT_FAILURE = 'SET_ADD_CONSULTANT_FAILURE';
export const SET_ADD_CONSULTANT_SUCCESS = 'SET_ADD_CONSULTANT_SUCCESS';
export const SET_ADD_CONSULTANT_REFRESH = 'SET_ADD_CONSULTANT_REFRESH';
export const SET_ADD_CONSULTANT_LOADING = 'SET_ADD_CONSULTANT_LOADING';

export const addConsultantFailure = (errors) => ({
  type: SET_ADD_CONSULTANT_FAILURE,
  payload: errors,
});

export const addConsultantSuccess = (data) => ({
  type: SET_ADD_CONSULTANT_SUCCESS,
  payload: data,
});

export const addConsultantRefreshPage = () => ({
  type: SET_ADD_CONSULTANT_REFRESH
});

export const addConsultantLoading = (state) => ({
  type: SET_ADD_CONSULTANT_LOADING,
  payload:state
});

export const addConsultantAction = (formData) => {
  
  return async (dispatch) => {

    dispatch(addConsultantLoading(true));
    
    try {
      const response = await axios.post(`${domainServer}${addConsulution}`, {
        'message': formData.inputMessage,
        'type': formData.type
      },{
        headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
      });

      if (response.status === 200) {
        dispatch(addConsultantSuccess(response.data.data));
      } else {
        dispatch(addConsultantFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 400) {
        console.log(`i got 400 in add consultants ${JSON.stringify(error.response.data)}`)
        dispatch(addConsultantFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else if (error.response.status === 422) {
        console.error(`errors in add consultants ${error.response.data.message}`);
        dispatch(addConsultantFailure(error.response.data));
      } else if (error.response.status === 401) {
        console.log(`i got 401 in add consultants ${JSON.stringify(error.response.data)}`)
        dispatch(addConsultantFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else{
        console.error('Error occurred during add consultion:', error.response.data);
        dispatch(addConsultantFailure(error.response.data));
      }

    }

    dispatch(addConsultantLoading(false));
  }
};
