import { Course } from "../FakeData";
import axios from 'axios';
import { domainServer, seekers } from "../General/ServerSideApis/ServerSideApis";

export const USERS_SEEKERS_SUCCESS = "USERS_SEEKERS_SUCCESS";
export const USERS_SEEKERS_LOADING = "USERS_SEEKERS_LOADING";
export const USERS_SEEKERS_REFRESH = "USERS_SEEKERS_REFRESH";
export const USERS_SEEKERS_FAILURE = "USERS_SEEKERS_FAILURE";

export const usersSuccess = (details) => ({
    type: USERS_SEEKERS_SUCCESS,
    payload: details
});

export const usersFailure = (errors) => ({
    type: USERS_SEEKERS_FAILURE,
    payload: errors,
  });


export const usersRefreshPage = () => ({
    type: USERS_SEEKERS_REFRESH
  });
  
  export const usersLoading = (state) => ({
    type: USERS_SEEKERS_LOADING,
    payload: state
  });
  
  export const usersAction = (index,page) => {
  
    return async (dispatch) => {
  
      dispatch(usersLoading(true));
      console.log(`the token of me in advisors  is ${JSON.stringify(localStorage.getItem('token'))}`)
  
      try {
        const response = await axios.get(`${domainServer}${seekers}?page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
          console.log(`the success of seekers is ${JSON.stringify(response.data.data)}`)
            dispatch(usersSuccess(response.data.data));
        } else {
          console.log(`the errors in 20* of seekers is ${JSON.stringify(response.data.message)}`)
          dispatch(usersFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your seekers ${JSON.stringify(error.response.data)}`)
          dispatch(usersFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your seekers:', error.response.data);
          dispatch(usersFailure(error.response.data));
        }
  
      }
  
      dispatch(usersLoading(false));
    }
  };
  