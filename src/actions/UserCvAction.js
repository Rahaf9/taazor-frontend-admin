import axios from "axios";
import { users } from "../FakeData";
import { cv, domainServer, profileApi, seeker, seekers, userCourseApi, userCvApi } from "../General/ServerSideApis/ServerSideApis";

export const USER_CV_SUCCESS = "USER_CV_SUCCESS";
export const USER_CV_FAILURE = "USER_CV_FAILURE";
export const USER_CV_LOADING = "USER_CV_LOADING";
export const USER_CV_REFRESH = "USER_CV_REFRESH";

export const userCvSuccess = (users) => ({
    type: USER_CV_SUCCESS,
    payload: users
});

export const userCvFailure = (errors) => ({
    type: USER_CV_FAILURE,
    payload: errors,
});


export const userCvRefreshPage = () => ({
    type: USER_CV_REFRESH
});

export const userCvLoading = (state) => ({
    type: USER_CV_LOADING,
    payload: state
});


export const userCvAction = (index) => {

    return async (dispatch) => {

        dispatch(userCvLoading(true));

        try {
            const response = await axios.get(`${domainServer}${seeker}${cv}/${index}`, {
                headers: {
                    'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                }
            });

            if (response.status === 200) {
                console.log(`this is the user cv data ${JSON.stringify(response.data.data)}`)
                dispatch(userCvSuccess(response.data.data));
            } else {
                dispatch(userCvFailure(response.data.message));
            }
        } catch (error) {

            if (error.response.status === 401) {
                console.log(`i got 401 in your user cv ${JSON.stringify(error.response.data)}`)
                dispatch(userCvFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
            } else {
                console.error('Error occurred during your user cv:', error.response.data);
                dispatch(userCvFailure(error.response.data));
            }

        }

        dispatch(userCvLoading(false));

        // dispatch(userCvSuccess(users));

    }

}