import axios from 'axios';
import { domainServer, userConsulting, convertApi } from '../General/ServerSideApis/ServerSideApis';

export const SET_USER_REFER_FAILURE = 'SET_USER_REFER_FAILURE';
export const SET_USER_REFER_SUCCESS = 'SET_USER_REFER_SUCCESS';
export const SET_USER_REFER_REFRESH = 'SET_USER_REFER_REFRESH';
export const SET_USER_REFER_LOADING = 'SET_USER_REFER_LOADING';

export const userReferFailure = (errors) => ({
  type: SET_USER_REFER_FAILURE,
  payload: errors,
});

export const userReferSuccess = (data) => ({
  type: SET_USER_REFER_SUCCESS,
  payload: data,
});

export const userReferRefreshPage = () => ({
  type: SET_USER_REFER_REFRESH
});

export const userReferLoading = (state) => ({
  type: SET_USER_REFER_LOADING,
  payload:state
});

export const userReferAction = (advisorId,consultionId) => {
  
  return async (dispatch) => {

    console.log(`the formData in user refer info is ${JSON.stringify(advisorId)} and ${JSON.stringify(consultionId)}`)
    dispatch(userReferLoading(true));
    
    try {
      const response = await axios.post(`${domainServer}${userConsulting}${convertApi}/${consultionId}`, {
        'advisor_id':advisorId
      },{
        headers: {
          'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        }
      });

      if (response.status === 200) {
        console.log(`here in user refer we got 200 code`)
        dispatch(userReferSuccess(response.data.data));
      } else {
        console.log(`here in user refer we got 2** code and its data is ${JSON.stringify(response.data)}`)
        dispatch(userReferFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 409) {
        console.error(`errors userRefer ${error.response.data.message}`);
        dispatch(userReferFailure(error.response.data));
      } else if (error.response.status === 400) {
        console.error(`errors userRefer ${error.response.data.message}`);
        dispatch(userReferFailure(error.response.data)); // the form of the error.response.data is "message": "adkasfhksfh"
      }
      else if (error.response.status === 422) {
        console.error(`errors userRefer ${JSON.stringify(error.response.data.errors)}`);
        dispatch(userReferFailure(error.response.data.errors)); //the form of the error.response.data is  {"password": [ "The password field confirmation does not match." ]}
      } else {
        console.error('Error occurred during user refer:', error.response.data);
        dispatch(userReferFailure(error.response.data));
      }

    }

    dispatch(userReferLoading(false));
  }
};
