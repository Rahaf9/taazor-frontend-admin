import axios from 'axios';
import { addConsulution, coordinators, domainServer, registerApi } from '../General/ServerSideApis/ServerSideApis';

export const SET_ADD_COORDINATOR_FAILURE = 'SET_ADD_COORDINATOR_FAILURE';
export const SET_ADD_COORDINATOR_SUCCESS = 'SET_ADD_COORDINATOR_SUCCESS';
export const SET_ADD_COORDINATOR_REFRESH = 'SET_ADD_COORDINATOR_REFRESH';
export const SET_ADD_COORDINATOR_LOADING = 'SET_ADD_COORDINATOR_LOADING';

export const addCoordinatorFailure = (errors) => ({
  type: SET_ADD_COORDINATOR_FAILURE,
  payload: errors,
});

export const addCoordinatorSuccess = (data) => ({
  type: SET_ADD_COORDINATOR_SUCCESS,
  payload: data,
});

export const addCoordinatorRefreshPage = () => ({
  type: SET_ADD_COORDINATOR_REFRESH
});

export const addCoordinatorLoading = (state) => ({
  type: SET_ADD_COORDINATOR_LOADING,
  payload:state
});

export const addCoordinatorAction = (formData) => {
  
  return async (dispatch) => {

    dispatch(addCoordinatorLoading(true));
    
    try {
      const response = await axios.post(`${domainServer}${coordinators}`, formData ,{
        headers: {
            'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
          }
      });

      if (response.status === 200) {
        console.log(`the coordinator added successfully`)
        dispatch(addCoordinatorSuccess(response.data.data));
      } else {
        dispatch(addCoordinatorFailure(response.data.message));
      }
    } catch (error) {

      if (error.response.status === 422) {
        console.error(`errors in add Coordinators ${error.response.data.message}`);
        dispatch(addCoordinatorFailure(error.response.data));
      } else if (error.response.status === 401) {
        console.log(`i got 401 in add Coordinators ${JSON.stringify(error.response.data)}`)
        dispatch(addCoordinatorFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
      } else{
        console.error('Error occurred during add Coordinators:', error.response.data);
        dispatch(addCoordinatorFailure(error.response.data));
      }

    }

    dispatch(addCoordinatorLoading(false));
  }
};
