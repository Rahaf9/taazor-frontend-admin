import axios from 'axios';
import { domainServer, jobsApi } from "../General/ServerSideApis/ServerSideApis";

export const EXPIRED_JOBS_SUCCESS = "EXPIRED_JOBS_SUCCESS";
export const EXPIRED_JOBS_FAILURE = "EXPIRED_JOBS_FAILURE";
export const EXPIRED_JOBS_LOADING = "EXPIRED_JOBS_LOADING";
export const EXPIRED_JOBS_REFRESH = "EXPIRED_JOBS_REFRESH";

export const expiredJobsSuccess = (details) => ({
    type: EXPIRED_JOBS_SUCCESS,
    payload: details
});

export const expiredJobsFailure = (errors) => ({
    type: EXPIRED_JOBS_FAILURE,
    payload: errors,
  });


export const expiredJobsRefreshPage = () => ({
    type: EXPIRED_JOBS_REFRESH
  });
  
  export const expiredJobsLoading = (state) => ({
    type: EXPIRED_JOBS_LOADING,
    payload: state
  });
  
  export const expiredJobsAction = (page) => {
  
    return async (dispatch) => {
  
      dispatch(expiredJobsLoading(true));
      console.log(`the token of me in Expired Jobss  is ${JSON.stringify(localStorage.getItem('token'))} and 
      page is ${page}`)
  
      try {
        const response = await axios.get(`${domainServer}${jobsApi}?status=finite&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/json','Accept-Language': localStorage.getItem('lang'),
              'Authorization': `Bearer ${localStorage.getItem('token')}`,
            }
          });
  
        if (response.status === 200) {
            console.log(`the expired jobs is ${JSON.stringify(response.data.data)}`)
            dispatch(expiredJobsSuccess(response.data.data));
        } else {
          dispatch(expiredJobsFailure(response.data.message));
        }
      } catch (error) {
  
        if (error.response.status === 401) {
          console.log(`i got 401 in your Expired Jobss ${JSON.stringify(error.response.data)}`)
          dispatch(expiredJobsFailure({ 'message': error.response.data.message })); //the data will be like {message: 'Unauthenticated.'}
        } else {
          console.error('Error occurred during your Expired Jobss:', error.response.data);
          dispatch(expiredJobsFailure(error.response.data));
        }
  
      }
  
      dispatch(expiredJobsLoading(false));
    }
  };
  