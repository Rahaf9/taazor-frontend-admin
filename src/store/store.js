// src/store/index.js

import { createStore, applyMiddleware } from 'redux';
import { thunk } from 'redux-thunk';
import rootReducer from '../reducers'; // Assuming you have defined your reducers

const store = createStore(rootReducer, applyMiddleware(thunk)); // Apply redux-thunk middleware

export default store;
