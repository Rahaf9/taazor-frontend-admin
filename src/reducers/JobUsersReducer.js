import { JOB_USERS_SUCCESS,JOB_USERS_FAILURE, JOB_USERS_LOADING, JOB_USER_REFRESH } from "../actions/JobUsersAction";

const initialState = {
    data: [],
    errors: {},
    done: false,
    isLoading: false
};

const JobUsersReducer = (state = initialState, action) => {

    switch (action.type) {

        case JOB_USERS_FAILURE:
            return { ...state, data: [], errors: action.payload, done: false, isLoading: false };
        case JOB_USERS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case JOB_USER_REFRESH:
            return { ...state, errors: {}, done: false};
        case JOB_USERS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default JobUsersReducer;