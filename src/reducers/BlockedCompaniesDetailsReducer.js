import {BLOCKED_COMPANIES_DETAILS_FAILURE,BLOCKED_COMPANIES_DETAILS_SUCCESS} from '../actions/BlockedCompaniesDeatailsAction'

const initialState = {
    data: [],
    errors: {},
    done: false,
    isLoading: false
};

const BlockedCompaniesDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case BLOCKED_COMPANIES_DETAILS_SUCCESS:
            return {
                ...state,
                data: action.payload,
                error: null,
                isLoading: false
            };
        case BLOCKED_COMPANIES_DETAILS_FAILURE:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        default:
            return state;
    }

}


export default BlockedCompaniesDetailsReducer;