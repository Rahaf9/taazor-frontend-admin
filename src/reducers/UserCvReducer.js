import { USER_CV_FAILURE, USER_CV_LOADING, USER_CV_REFRESH, USER_CV_SUCCESS } from "../actions/UserCvAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const UserCvReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_CV_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case USER_CV_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case USER_CV_REFRESH:
            return { ...state, errors: {}, done: false };
        case USER_CV_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default UserCvReducer;
