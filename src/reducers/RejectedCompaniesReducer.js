import {REJECTED_COMPANIES_FAILURE, REJECTED_COMPANIES_LOADING, REJECTED_COMPANIES_REFRESH, REJECTED_COMPANIES_SUCCESS } from "../actions/RejectedCompaniesAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const RejectedCompaniesReducer = (state = initialState, action) => {
    switch (action.type) {
        case REJECTED_COMPANIES_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case REJECTED_COMPANIES_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case REJECTED_COMPANIES_REFRESH:
            return { ...state, errors: {}, done: false };
        case REJECTED_COMPANIES_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default RejectedCompaniesReducer;
