import { EXPIRED_COURSE_USERS_FAILURE, EXPIRED_COURSE_USERS_LOADING, EXPIRED_COURSE_USERS_SUCCESS, EXPIRED_COURSE_USER_REFRESH } from "../actions/ExpiredCourseUserAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const ExpiredCourseUsersReducer = (state = initialState, action) => {
    switch (action.type) {
        case EXPIRED_COURSE_USERS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case EXPIRED_COURSE_USERS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case EXPIRED_COURSE_USER_REFRESH:
            return { ...state, errors: {}, done: false};
        case EXPIRED_COURSE_USERS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default ExpiredCourseUsersReducer;
