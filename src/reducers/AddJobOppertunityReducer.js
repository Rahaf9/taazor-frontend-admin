import { SET_ADD_JOB_FAILURE, SET_ADD_JOB_LOADING, SET_ADD_JOB_REFRESH, SET_ADD_JOB_SUCCESS } from "../actions/AddJobOpportunityAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AddJobOpportunityReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ADD_JOB_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_ADD_JOB_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_ADD_JOB_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_ADD_JOB_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default AddJobOpportunityReducer;
