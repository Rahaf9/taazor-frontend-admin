import { COURSE_USERS_FAILURE, COURSE_USERS_SUCCESS, COURSE_USERS_LOADING, COURSE_USER_REFRESH } from "../actions/CourseUsersAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CourseUsersReducer = (state = initialState, action) => {

    switch (action.type) {

        case COURSE_USERS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case COURSE_USERS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case COURSE_USER_REFRESH:
            return { ...state, errors: {}, done: false};
        case COURSE_USERS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }


}


export default CourseUsersReducer;