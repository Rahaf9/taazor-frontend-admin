import { JOB_USER_DETAILS_FAILURE, JOB_USER_DETAILS_LOADING, JOB_USER_DETAILS_REFRESH, JOB_USER_DETAILS_SUCCESS } from "../actions/JobUserDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const JobUserDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case JOB_USER_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case JOB_USER_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case JOB_USER_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false };
        case JOB_USER_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }


}


export default JobUserDetailsReducer;