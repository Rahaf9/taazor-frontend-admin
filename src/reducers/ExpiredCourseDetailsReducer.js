import { EXPIRED_COURSE_DETAILS_FAILURE, EXPIRED_COURSE_DETAILS_LOADING, EXPIRED_COURSE_DETAILS_REFRESH, EXPIRED_COURSE_DETAILS_SUCCESS } from "../actions/ExpiredCourseDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const ExpiredCourseDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case EXPIRED_COURSE_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case EXPIRED_COURSE_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case EXPIRED_COURSE_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false, };
        case EXPIRED_COURSE_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default ExpiredCourseDetailsReducer;
