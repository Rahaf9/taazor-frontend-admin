import { SET_COMPANY_REFER_FAILURE, SET_COMPANY_REFER_LOADING, SET_COMPANY_REFER_REFRESH, SET_COMPANY_REFER_SUCCESS } from "../actions/CompanyReferAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CompanyReferReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COMPANY_REFER_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_COMPANY_REFER_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_COMPANY_REFER_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_COMPANY_REFER_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default CompanyReferReducer;
