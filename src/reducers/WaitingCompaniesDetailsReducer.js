import {WAITING_COMPANIES_DETAILS_FAILURE,WAITING_COMPANIES_DETAILS_SUCCESS} from '../actions/WaitingCompaniesDeatailsAction'

const initialState = {
    data: [],
    errors: {},
    done: false,
    isLoading: false
};

const WaitingCompaniesDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case WAITING_COMPANIES_DETAILS_SUCCESS:
            return {
                ...state,
                data: action.payload,
                error: null,
                isLoading: false
            };
        case WAITING_COMPANIES_DETAILS_FAILURE:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        default:
            return state;
    }

}


export default WaitingCompaniesDetailsReducer;