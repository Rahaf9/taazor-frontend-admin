import { SET_OTP_FAILURE, SET_OTP_LOADING, SET_OTP_REFRESH, SET_OTP_SUCCESS } from "../actions/OTPAction";
const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const OtpReducer = (state = initialState, action) => {

    switch (action.type) {

        case SET_OTP_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false }
        case SET_OTP_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_OTP_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_OTP_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;

    }

}

export default OtpReducer;