import { SET_LOGIN_FAILURE,SET_LOGIN_SUCCESS,SET_LOGIN_LOADING,SET_LOGIN_REFRESH } from "../actions/LoginAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const LoginReducer = (state = initialState, action) => {

    switch (action.type) {

        case SET_LOGIN_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false }
        case SET_LOGIN_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_LOGIN_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_LOGIN_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;

    }

}

export default LoginReducer;