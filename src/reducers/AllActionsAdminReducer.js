import { ALL_ADMIN_ACTION_FAILURE, ALL_ADMIN_ACTION_LOADING, ALL_ADMIN_ACTION_REFRESH_PAGE, ALL_ADMIN_ACTION_SUCCESS } from "../actions/AllAdminActions";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AllActionsAdminReducer = (state = initialState, action) => {

    switch (action.type) {

        case ALL_ADMIN_ACTION_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case ALL_ADMIN_ACTION_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case ALL_ADMIN_ACTION_REFRESH_PAGE:
            return { ...state, errors: {}, done: false};
        case ALL_ADMIN_ACTION_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default AllActionsAdminReducer;