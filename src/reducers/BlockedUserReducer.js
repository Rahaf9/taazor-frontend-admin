import { BOLCKED_USERS_FAILURE, BOLCKED_USERS_LOADING, BOLCKED_USERS_REFRESH, BOLCKED_USERS_SUCCESS } from "../actions/BlockedUserAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const BlockedUsersReducer = (state = initialState, action) => {

    switch (action.type) {

        case BOLCKED_USERS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case BOLCKED_USERS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case BOLCKED_USERS_REFRESH:
            return { ...state, errors: {}, done: false};
        case BOLCKED_USERS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default BlockedUsersReducer;