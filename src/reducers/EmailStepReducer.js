import { SET_EMAIL_STEP_FAILURE, SET_EMAIL_STEP_LOADING, SET_EMAIL_STEP_REFRESH, SET_EMAIL_STEP_SUCCESS } from "../actions/EmailStepAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const EmailStepReducer = (state = initialState, action) => {

    switch (action.type) {

        case SET_EMAIL_STEP_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false }
        case SET_EMAIL_STEP_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_EMAIL_STEP_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_EMAIL_STEP_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;

    }

}

export default EmailStepReducer;