import { BLOCKED_USER_DETAILS_FAILURE, BLOCKED_USER_DETAILS_LOADING, BLOCKED_USER_DETAILS_REFRESH, BLOCKED_USER_DETAILS_SUCCESS } from "../actions/BlockedUserDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const BlockedUserDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case BLOCKED_USER_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case BLOCKED_USER_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case BLOCKED_USER_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false};
        case BLOCKED_USER_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default BlockedUserDetailsReducer;