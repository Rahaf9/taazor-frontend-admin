import { CURRENT_JOB_DETAILS_FAILURE, CURRENT_JOB_DETAILS_LOADING, CURRENT_JOB_DETAILS_REFRESH, CURRENT_JOB_DETAILS_SUCCESS } from "../actions/CurrentJobDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CurrentJobDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CURRENT_JOB_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case CURRENT_JOB_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case CURRENT_JOB_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false };
        case CURRENT_JOB_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default CurrentJobDetailsReducer;
