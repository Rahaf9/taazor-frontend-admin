import { ACCEPTED_COMPANIES_FAILURE, ACCEPTED_COMPANIES_LOADING, ACCEPTED_COMPANIES_REFRESH,ACCEPTED_COMPANIES_SUCCESS } from "../actions/AcceptedCompaniesAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AcceptedCompaniesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACCEPTED_COMPANIES_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case ACCEPTED_COMPANIES_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case ACCEPTED_COMPANIES_REFRESH:
            return { ...state, errors: {}, done: false };
        case ACCEPTED_COMPANIES_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default AcceptedCompaniesReducer;
