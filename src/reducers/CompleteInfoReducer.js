import { SET_COMPLETE_INFO_FAILURE, SET_COMPLETE_INFO_LOADING, SET_COMPLETE_INFO_REFRESH, SET_COMPLETE_INFO_SUCCESS } from "../actions/CompleteInfoAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CompleteInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COMPLETE_INFO_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_COMPLETE_INFO_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_COMPLETE_INFO_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_COMPLETE_INFO_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default CompleteInfoReducer;
