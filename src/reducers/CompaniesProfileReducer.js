import { COMPANIES_PROFILE_ACTION_FAILURE, COMPANIES_PROFILE_ACTION_LOADING, COMPANIES_PROFILE_ACTION_REFRESH_PAGE, COMPANIES_PROFILE_ACTION_SUCCESS } from "../actions/CompaniesProfileAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CompaniesProfileAdminReducer = (state = initialState, action) => {

    switch (action.type) {

        case COMPANIES_PROFILE_ACTION_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case COMPANIES_PROFILE_ACTION_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case COMPANIES_PROFILE_ACTION_REFRESH_PAGE:
            return { ...state, errors: {}, done: false};
        case COMPANIES_PROFILE_ACTION_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default CompaniesProfileAdminReducer;