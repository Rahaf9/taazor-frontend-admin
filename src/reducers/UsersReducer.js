import { USERS_SEEKERS_FAILURE, USERS_SEEKERS_LOADING, USERS_SEEKERS_REFRESH, USERS_SEEKERS_SUCCESS } from "../actions/UsersAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const UsersReducer = (state = initialState, action) => {

    switch (action.type) {

        case USERS_SEEKERS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case USERS_SEEKERS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case USERS_SEEKERS_REFRESH:
            return { ...state, errors: {}, done: false};
        case USERS_SEEKERS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default UsersReducer;