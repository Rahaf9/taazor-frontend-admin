import { SET_USER_CONSULTANTS_FAILURE, SET_USER_CONSULTANTS_LOADING, SET_USER_CONSULTANTS_REFRESH, SET_USER_CONSULTANTS_SUCCESS } from "../actions/UserConsultantsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const UserConsultionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_CONSULTANTS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_USER_CONSULTANTS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_USER_CONSULTANTS_REFRESH:
            return { ...state, errors: {}, done: false };
        case SET_USER_CONSULTANTS_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default UserConsultionsReducer;
