import { ADD_QUESTIONS } from "../actions/AddQuestionsAction";

  // reducer.js
  const initialState = {
    data: [],
  };
  
  const AddQuestionsReducer = (state = initialState, action) => {
    switch (action.type) {
      case ADD_QUESTIONS:
        return {
         ...state,
          data: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default AddQuestionsReducer;
  