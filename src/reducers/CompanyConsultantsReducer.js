import { SET_COMPANY_CONSULTANTS_FAILURE, SET_COMPANY_CONSULTANTS_LOADING, SET_COMPANY_CONSULTANTS_REFRESH, SET_COMPANY_CONSULTANTS_SUCCESS } from "../actions/CompanyConsultantsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CompanyConsultionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COMPANY_CONSULTANTS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_COMPANY_CONSULTANTS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_COMPANY_CONSULTANTS_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_COMPANY_CONSULTANTS_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default CompanyConsultionsReducer;
