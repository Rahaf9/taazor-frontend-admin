import { SET_ADD_COURSE_FAILURE, SET_ADD_COURSE_LOADING, SET_ADD_COURSE_REFRESH, SET_ADD_COURSE_SUCCESS } from "../actions/AddCourseOpportunityAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AddCourseOpportunityReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ADD_COURSE_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_ADD_COURSE_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_ADD_COURSE_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_ADD_COURSE_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default AddCourseOpportunityReducer;
