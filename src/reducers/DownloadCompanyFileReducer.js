import { DOWNLOAD_COMPANIE_FILES_FAILURE, DOWNLOAD_COMPANIE_FILES_LOADING, DOWNLOAD_COMPANIE_FILES_REFRESH, DOWNLOAD_COMPANIE_FILES_SUCCESS } from "../actions/DownloadCompanyFileAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const DownloadCompanieFileReducer = (state = initialState, action) => {
    switch (action.type) {
        case DOWNLOAD_COMPANIE_FILES_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case DOWNLOAD_COMPANIE_FILES_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case DOWNLOAD_COMPANIE_FILES_REFRESH:
            return { ...state, errors: {}, done: false };
        case DOWNLOAD_COMPANIE_FILES_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default DownloadCompanieFileReducer;
