import { SET_USER_CONSULTANT_DETAILS_FAILURE, SET_USER_CONSULTANT_DETAILS_LOADING, SET_USER_CONSULTANT_DETAILS_REFRESH, SET_USER_CONSULTANT_DETAILS_SUCCESS } from "../actions/UserConsultantDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const UserConsultionsDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_CONSULTANT_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_USER_CONSULTANT_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_USER_CONSULTANT_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false };
        case SET_USER_CONSULTANT_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default UserConsultionsDetailsReducer;
