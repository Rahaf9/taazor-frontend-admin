import {ADVISOR_DETAILS_FAILURE,ADVISOR_DETAILS_SUCCESS} from '../actions/AdvisorDetailsAction'

const initialState = {
    data: [],
    errors: {},
    done: false,
    isLoading: false
};

const AdvisorDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case ADVISOR_DETAILS_SUCCESS:
            return {
                ...state,
                data: action.payload,
                error: null,
                isLoading: false
            };
        case ADVISOR_DETAILS_FAILURE:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        default:
            return state;
    }

}


export default AdvisorDetailsReducer;