import { EXPIRED_JOBS_FAILURE, EXPIRED_JOBS_LOADING, EXPIRED_JOBS_REFRESH, EXPIRED_JOBS_SUCCESS } from "../actions/ExpiredJobAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const ExpiredJobsReducer = (state = initialState, action) => {
    switch (action.type) {
        case EXPIRED_JOBS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case EXPIRED_JOBS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case EXPIRED_JOBS_REFRESH:
            return { ...state, errors: {}, done: false };
        case EXPIRED_JOBS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default ExpiredJobsReducer;
