import { SET_NEW_PASSWORD_FAILURE, SET_NEW_PASSWORD_LOADING, SET_NEW_PASSWORD_REFRESH, SET_NEW_PASSWORD_SUCCESS } from "../actions/NewPasswordAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const newPasswordReducer = (state = initialState, action) => {

    switch (action.type) {

        case SET_NEW_PASSWORD_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false }
        case SET_NEW_PASSWORD_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_NEW_PASSWORD_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_NEW_PASSWORD_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;

    }

}

export default newPasswordReducer;