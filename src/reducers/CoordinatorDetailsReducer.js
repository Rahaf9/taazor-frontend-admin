import {COORDINATOR_DETAILS_FAILURE,COORDINATOR_DETAILS_SUCCESS} from '../actions/CoodinatorDetailsAction'

const initialState = {
    data: [],
    errors: {},
    done: false,
    isLoading: false
};

const CoordinatorDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case COORDINATOR_DETAILS_SUCCESS:
            return {
                ...state,
                data: action.payload,
                error: null,
                isLoading: false
            };
        case COORDINATOR_DETAILS_FAILURE:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        default:
            return state;
    }

}


export default CoordinatorDetailsReducer;