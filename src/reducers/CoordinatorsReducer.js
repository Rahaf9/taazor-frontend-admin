import { COORDINATORS_FAILURE, COORDINATORS_LOADING, COORDINATORS_REFRESH, COORDINATORS_SUCCESS } from "../actions/CoordinatorsAction";

const initialState = {
    data: [],
    errors: {},
    done: false,
    isLoading: false
};

const CoordinatorsReducer = (state = initialState, action) => {

    switch (action.type) {

        case COORDINATORS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case COORDINATORS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case COORDINATORS_REFRESH:
            return { ...state, errors: {}, done: false};
        case COORDINATORS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default CoordinatorsReducer;