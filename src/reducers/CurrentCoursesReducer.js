import { CURRENT_COURSES_FAILURE, CURRENT_COURSES_LOADING, CURRENT_COURSES_REFRESH, CURRENT_COURSES_SUCCESS } from "../actions/CurrentCoursesAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CurrentCoursesReducer = (state = initialState, action) => {
    switch (action.type) {
        case CURRENT_COURSES_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case CURRENT_COURSES_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case CURRENT_COURSES_REFRESH:
            return { ...state, errors: {}, done: false };
        case CURRENT_COURSES_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default CurrentCoursesReducer;
