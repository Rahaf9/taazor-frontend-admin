import { EXPIRED_JOB_USERS_FAILURE, EXPIRED_JOB_USERS_LOADING, EXPIRED_JOB_USERS_SUCCESS, EXPIRED_JOB_USER_REFRESH } from "../actions/ExpiredJobUserAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const ExpiredJobUsersReducer = (state = initialState, action) => {
    switch (action.type) {
        case EXPIRED_JOB_USERS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case EXPIRED_JOB_USERS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case EXPIRED_JOB_USER_REFRESH:
            return { ...state, errors: {}, done: false };
        case EXPIRED_JOB_USERS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default ExpiredJobUsersReducer;
