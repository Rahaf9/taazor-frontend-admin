import {ACCEPTED_COMPANIES_DETAILS_FAILURE,ACCEPTED_COMPANIES_DETAILS_SUCCESS} from '../actions/AcceptedCompaniesDeatailsAction'

const initialState = {
    data: [],
    errors: {},
    done: false,
    isLoading: false
};

const AcceptedCompaniesDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case ACCEPTED_COMPANIES_DETAILS_SUCCESS:
            return {
                ...state,
                data: action.payload,
                error: null,
                isLoading: false
            };
        case ACCEPTED_COMPANIES_DETAILS_FAILURE:
            return {
                ...state,
                error: action.payload,
                isLoading: false
            };
        default:
            return state;
    }

}


export default AcceptedCompaniesDetailsReducer;