import { SET_ADD_CONSULTANT_FAILURE, SET_ADD_CONSULTANT_LOADING, SET_ADD_CONSULTANT_REFRESH, SET_ADD_CONSULTANT_SUCCESS } from "../actions/AddConsultantAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AddConsultionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ADD_CONSULTANT_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_ADD_CONSULTANT_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_ADD_CONSULTANT_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_ADD_CONSULTANT_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default AddConsultionsReducer;
