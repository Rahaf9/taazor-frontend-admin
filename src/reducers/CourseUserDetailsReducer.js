import { COURSE_USER_DETAILS_FAILURE, COURSE_USER_DETAILS_LOADING, COURSE_USER_DETAILS_REFRESH, COURSE_USER_DETAILS_SUCCESS } from "../actions/CourseUserDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CourseUserDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case COURSE_USER_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case COURSE_USER_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case COURSE_USER_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false };
        case COURSE_USER_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }


}


export default CourseUserDetailsReducer;