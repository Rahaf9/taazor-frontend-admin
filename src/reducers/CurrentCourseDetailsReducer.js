import { CURRENT_COURSE_DETAILS_FAILURE, CURRENT_COURSE_DETAILS_LOADING, CURRENT_COURSE_DETAILS_REFRESH, CURRENT_COURSE_DETAILS_SUCCESS } from "../actions/CurrentCourseDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CurrentCourseDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CURRENT_COURSE_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case CURRENT_COURSE_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case CURRENT_COURSE_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false };
        case CURRENT_COURSE_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default CurrentCourseDetailsReducer;
