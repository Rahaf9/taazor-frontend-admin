import { SET_ADD_ADVISOR_FAILURE, SET_ADD_ADVISOR_LOADING, SET_ADD_ADVISOR_REFRESH, SET_ADD_ADVISOR_SUCCESS } from "../actions/AddAdvisorAdction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AddAdvisorsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ADD_ADVISOR_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_ADD_ADVISOR_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_ADD_ADVISOR_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_ADD_ADVISOR_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default AddAdvisorsReducer;
