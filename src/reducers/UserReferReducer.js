import { SET_USER_REFER_FAILURE, SET_USER_REFER_LOADING, SET_USER_REFER_REFRESH, SET_USER_REFER_SUCCESS } from "../actions/UserReferAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const UserReferReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_REFER_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_USER_REFER_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_USER_REFER_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_USER_REFER_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default UserReferReducer;
