import { SET_STATISTIC_FAILURE, SET_STATISTIC_LOADING, SET_STATISTIC_REFRESH, SET_STATISTIC_SUCCESS } from "../actions/StatisticAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const StatisticReducer = (state = initialState, action) => {

    switch (action.type) {

        case SET_STATISTIC_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false }
        case SET_STATISTIC_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_STATISTIC_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_STATISTIC_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;

    }

}

export default StatisticReducer;