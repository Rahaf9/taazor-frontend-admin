import { SET_ADD_NEW_POLL_FAILURE, SET_ADD_NEW_POLL_LOADING, SET_ADD_NEW_POLL_REFRESH, SET_ADD_NEW_POLL_SUCCESS } from "../actions/AddingNewPollAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AddNewPollOpportunityReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ADD_NEW_POLL_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_ADD_NEW_POLL_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_ADD_NEW_POLL_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_ADD_NEW_POLL_LOADING:
            return { ...state, isLoading: action.payload };

        default:
            return state;
    }
};

export default AddNewPollOpportunityReducer;
