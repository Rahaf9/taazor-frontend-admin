import { USER_DETAILS_FAILURE, USER_DETAILS_LOADING, USER_DETAILS_REFRESH, USER_DETAILS_SUCCESS } from "../actions/UserDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const UsersSeekerReducer = (state = initialState, action) => {

    switch (action.type) {

        case USER_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case USER_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case USER_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false};
        case USER_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default UsersSeekerReducer;