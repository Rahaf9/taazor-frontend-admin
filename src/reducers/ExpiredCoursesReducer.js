import { EXPIRED_COURSES_FAILURE, EXPIRED_COURSES_LOADING, EXPIRED_COURSES_REFRESH, EXPIRED_COURSES_SUCCESS } from "../actions/ExpiredCoursesAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const ExpiredCoursesReducer = (state = initialState, action) => {
    switch (action.type) {
        case EXPIRED_COURSES_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case EXPIRED_COURSES_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case EXPIRED_COURSES_REFRESH:
            return { ...state, errors: {}, done: false };
        case EXPIRED_COURSES_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default ExpiredCoursesReducer;
