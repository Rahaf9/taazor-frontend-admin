import { SET_ADD_COORDINATOR_FAILURE, SET_ADD_COORDINATOR_LOADING, SET_ADD_COORDINATOR_REFRESH, SET_ADD_COORDINATOR_SUCCESS } from "../actions/AddCoordinatorAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AddCoordinatorsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ADD_COORDINATOR_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case SET_ADD_COORDINATOR_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case SET_ADD_COORDINATOR_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case SET_ADD_COORDINATOR_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default AddCoordinatorsReducer;
