import { ADVISORS_FAILURE, ADVISORS_LOADING, ADVISORS_REFRESH, ADVISORS_SUCCESS } from "../actions/AdvisorsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AdivsorsReducer = (state = initialState, action) => {

    switch (action.type) {

        case ADVISORS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case ADVISORS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case ADVISORS_REFRESH:
            return { ...state, errors: {}, done: false};
        case ADVISORS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default AdivsorsReducer;