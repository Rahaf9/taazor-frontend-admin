import { BLOCKED_COMPANIES_FAILURE, BLOCKED_COMPANIES_LOADING, BLOCKED_COMPANIES_REFRESH, BLOCKED_COMPANIES_SUCCESS } from "../actions/BlockedCompaniesAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const BlockedCompaniesReducer = (state = initialState, action) => {
    switch (action.type) {
        case BLOCKED_COMPANIES_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case BLOCKED_COMPANIES_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case BLOCKED_COMPANIES_REFRESH:
            return { ...state, errors: {}, done: false };
        case BLOCKED_COMPANIES_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default BlockedCompaniesReducer;
