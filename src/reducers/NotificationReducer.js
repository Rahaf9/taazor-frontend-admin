import { NOTIFICATIONS_DELETE_ALL_FAILURE, NOTIFICATIONS_DELETE_ALL_LOADING, NOTIFICATIONS_DELETE_ALL_REFRESH, NOTIFICATIONS_DELETE_ALL_SUCCESS, NOTIFICATIONS_DELETE_LOADING, NOTIFICATIONS_DELETE_REFRESH, NOTIFICATIONS_DELETE_SUCCESS, NOTIFICATIONS_FAILURE, NOTIFICATIONS_LOADING, NOTIFICATIONS_REFRESH, NOTIFICATIONS_SUCCESS } from "../actions/NotificationAction";

const initialState = {
    data: [],
    errors: {},
    done: false,
    isLoading: false,
    isDeleteNotiLoading: false,
    deleteDone: false,
    deleteErrors: {},
    isDeleteAllNotiLoading: false,
    deleteAllDone: false,
    deleteAllErrors: {},
    // for delete all noti
};

const NotificationReducer = (state = initialState, action) => {

    switch (action.type) {

        case NOTIFICATIONS_FAILURE:
            return { ...state, data: [], errors: action.payload, done: false, isLoading: false };
        case NOTIFICATIONS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case NOTIFICATIONS_DELETE_SUCCESS:
            return { ...state,errors: {}, deleteDone: true, isLoading: false };
        case NOTIFICATIONS_REFRESH:
            return { ...state, errors: {}, done: false };
        case NOTIFICATIONS_DELETE_REFRESH:
            return { ...state, deleteErrors: {}, deleteDone: false };
        case NOTIFICATIONS_LOADING:
            return { ...state, isLoading: action.payload };
        case NOTIFICATIONS_DELETE_LOADING:
            return { ...state, isDeleteNotiLoading: action.payload };
        // for all noti delete
        case NOTIFICATIONS_DELETE_ALL_LOADING:
            return { ...state, isDeleteAllNotiLoading: action.payload };
        case NOTIFICATIONS_DELETE_ALL_REFRESH:
            return { ...state, deleteAllErrors: {}, deleteAllDone: false };
        case NOTIFICATIONS_DELETE_ALL_SUCCESS:
            return { ...state,deleteAllErrors: {}, deleteAllDone: true, isDeleteAllNotiLoading: false };
        case NOTIFICATIONS_DELETE_ALL_FAILURE:
            return { ...state, deleteAllErrors: action.payload, done: false, isDeleteAllNotiLoading: false };
        default:
            return state;
    }

}


export default NotificationReducer;