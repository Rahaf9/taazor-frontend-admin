import { ADVISOR_CONSOULTIONS_RECEIVED_FAILURE, ADVISOR_CONSOULTIONS_RECEIVED_LOADING, ADVISOR_CONSOULTIONS_RECEIVED_REFRESH, ADVISOR_CONSOULTIONS_RECEIVED_SUCCESS } from "../actions/AdvisorConsoultionsReceivedAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const AdivsorConsoultionsReceivedReducer = (state = initialState, action) => {

    switch (action.type) {

        case ADVISOR_CONSOULTIONS_RECEIVED_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case ADVISOR_CONSOULTIONS_RECEIVED_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case ADVISOR_CONSOULTIONS_RECEIVED_REFRESH:
            return { ...state, errors: {}, done: false};
        case ADVISOR_CONSOULTIONS_RECEIVED_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default AdivsorConsoultionsReceivedReducer;