import { combineReducers } from 'redux';
import CounterReducer from './CounterReducer';
import LoginReducer from './LoginReducer';
import CompleteInfoReducer from './CompleteInfoReducer';
import NewPasswordReducer from './NewPasswordReducer';
import OtpReducer from './OTPReducer';
import CourseUsersReducer from './CourseUsersReducer';
import JobUsersReducer from './JobUsersReducer';
import CurrentJobDetailsReducer from './CurrentJobDetailsReducer';
import ExpiredJobDetailsReducer from './ExpiredJobDetailsReducer';
import CurrentCourseDetailsReducer from './CurrentCourseDetailsReducer';
import EmailStepReducer from './EmailStepReducer';
import AddConsultionsReducer from './AddConsultantReducer';
import CompanyConsultantDetailsReducer from './CompanyConsultantDetailsReducer';
import CurrentJobsReducer from './CurrentJobsReducer';
import CurrentCoursesReducer from './CurrentCoursesReducer';
import ExpiredJobsReducer from './ExpiredJobsReducer';
import ExpiredCoursesReducer from './ExpiredCoursesReducer';
import AddCourseOpportunityReducer from './AddCourseOpportunityReducer';
import AddQuestionsReducer from './AddQuestionsReducer';
import AddJobOpportunityReducer from './AddJobOppertunityReducer';
import AddNewPollOpportunityReducer from './AddingNewPollReducer';
import StatisticReducer from './StatisticReducer';
import ExpiredCourseDetailsReducer from './ExpiredCourseDetailsReducer';
import CourseUserDetailsReducer from './CourseUserDetailsReducer';
import JobUserDetailsReducer from './JobUserDetailsReducer';
import CurrentJobAcceptableUsersReducer from './CurrentJobAcceptableUsersReducer';
import CurrentJobAcceptableUserDetailsReducer from './CurrentJobAcceptableUserDetailsReducer';
import ExpiredJobAcceptableUsersReducer from './ExpiredJobAcceptableUsersReducer';
import ExpiredJobAcceptableUserDetailsReducer from './ExpiredJobAcceptableUserDetailsReducer';
import ExpiredJobUsersReducer from './ExpiredJobUserReducer';
import ExpiredJobUserDetailsReducer from './ExpiredJobUserDetailsReducer';
import ExpiredCourseUsersReducer from './ExpiredCourseUsersReducer';
import ExpiredCourseUserDetailsReducer from './ExpiredCourseUserDetailsReducer copy';
import AcceptedCompaniesReducer from './AcceptedCompaniesReducer';
import WaitingCompaniesReducer from './WaitingCompaniesReducer';
import RejectedCompaniesReducer from './RejectedCompaniesReducer';
import BlockedCompaniesReducer from './BlockedCompaniesReducer';
import UsersReducer from './UsersReducer';
import BlockedUsersReducer from './BlockedUserReducer';
import AdivsorsReducer from './AdvisorReducer';
import CoordinatorsReducer from './CoordinatorsReducer';
import UsersSeekerReducer from './UserDeatailsReducer';
import BlockedUserDetailsReducer from './BlockedUserDetailsReducer';
import AddCoordinatorsReducer from './AddCoordinatorReducer';
import AddAdvisorsReducer from './AddAdvisorReducer';
import AllActionsAdminReducer from './AllActionsAdminReducer';
import AllStoreActionsAdminReducer from './AllStoreActionsAdminReducer';
import CompaniesProfileAdminReducer from './CompaniesProfileReducer';
import CompanyConsultionsReducer from './CompanyConsultantsReducer';
import UserConsultionsReducer from './UserConsultantsReducer';
import UserConsultionsDetailsReducer from './UserConsultantDetailsReducer';
import CompanyReferReducer from './CompanyReferReducer';
import UserReferReducer from './UserReferReducer';
import AdivsorConsoultionsReceivedReducer from './AdvisorConsoultionsReceivedReducer';
import AdvisorConsultionDetailsReducer from './AdvisorConsultantDetailsReducer';
import DownloadCompanieFileReducer from './DownloadCompanyFileReducer';
import UserCvReducer from './UserCvReducer';
import NotificationReducer from './NotificationReducer';

const rootReducer = combineReducers({

    counterReducer: CounterReducer,
    loginReducer: LoginReducer,
    completeInfoReducer:CompleteInfoReducer,
    newPasswordReducer: NewPasswordReducer,
    otpReducer: OtpReducer,
    courseUsersReducer:CourseUsersReducer,
    jobUsersReducer:JobUsersReducer,
    currentJobDetailsReducer:CurrentJobDetailsReducer,
    currentCourseDetailsReducer:CurrentCourseDetailsReducer,
    emailStepReducer:EmailStepReducer,
    companyConsultionsReducer:CompanyConsultionsReducer,
    addConsultionsReducer:AddConsultionsReducer,
    companyConsultantDetailsReducer:CompanyConsultantDetailsReducer,
    currentJobsReducer:CurrentJobsReducer,
    currentCoursesReducer:CurrentCoursesReducer,
    expiredJobsReducer:ExpiredJobsReducer,
    expiredCoursesReducer:ExpiredCoursesReducer,
    expiredJobDetailsReducer:ExpiredJobDetailsReducer,
    expiredCourseDetailsReducer:ExpiredCourseDetailsReducer,
    addCourseOpportunityReducer:AddCourseOpportunityReducer,
    addQuestionsReducer:AddQuestionsReducer,
    addJobOpportunityReducer:AddJobOpportunityReducer,
    addNewPollOpportunityReducer:AddNewPollOpportunityReducer,
    statisticReducer:StatisticReducer,
    courseUserDetailsReducer:CourseUserDetailsReducer,
    jobUserDetailsReducer:JobUserDetailsReducer,
    currentJobAcceptableUsersReducer:CurrentJobAcceptableUsersReducer,
    currentJobAcceptableUserDetailsReducer:CurrentJobAcceptableUserDetailsReducer,
    expiredJobAcceptableUsersReducer:ExpiredJobAcceptableUsersReducer,
    expiredJobAcceptableUserDetailsReducer:ExpiredJobAcceptableUserDetailsReducer,
    expiredJobUsersReducer:ExpiredJobUsersReducer,
    expiredJobUserDetailsReducer:ExpiredJobUserDetailsReducer,
    expiredCourseUsersReducer:ExpiredCourseUsersReducer,
    expiredCourseUserDetailsReducer:ExpiredCourseUserDetailsReducer,
    acceptedCompaniesReducer:AcceptedCompaniesReducer,
    waitingCompaniesReducer:WaitingCompaniesReducer,
    rejectedCompaniesReducer:RejectedCompaniesReducer,
    blockedCompaniesReducer:BlockedCompaniesReducer,
    usersSeekerReducer:UsersReducer,
    blockedUsersReducer:BlockedUsersReducer,
    adivsorsReducer:AdivsorsReducer,
    coordinatorsReducer:CoordinatorsReducer,
    usersSeekerDeailsReducer:UsersSeekerReducer,
    blockedUserDetailsReducer:BlockedUserDetailsReducer,
    addCoordinatorsReducer:AddCoordinatorsReducer,
    addAdvisorsReducer:AddAdvisorsReducer,
    allActionsAdminReducer:AllActionsAdminReducer,
    allStoreActionsAdminReducer:AllStoreActionsAdminReducer,
    companiesProfileAdminReducer:CompaniesProfileAdminReducer,
    userConsultionsReducer:UserConsultionsReducer,
    userConsultionsDetailsReducer:UserConsultionsDetailsReducer,
    companyReferReducer:CompanyReferReducer,
    userReferReducer:UserReferReducer,
    adivsorConsoultionsReceivedReducer:AdivsorConsoultionsReceivedReducer,
    advisorConsultionDetailsReducer:AdvisorConsultionDetailsReducer,
    downloadCompanieFileReducer:DownloadCompanieFileReducer,
    userCvReducer:UserCvReducer,
    notificationReducer:NotificationReducer
});

export default rootReducer;