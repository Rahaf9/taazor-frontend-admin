import {WAITING_COMPANIES_FAILURE, WAITING_COMPANIES_LOADING, WAITING_COMPANIES_REFRESH, WAITING_COMPANIES_SUCCESS } from "../actions/WaitingCompaniesAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const WaitingCompaniesReducer = (state = initialState, action) => {
    switch (action.type) {
        case WAITING_COMPANIES_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case WAITING_COMPANIES_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case WAITING_COMPANIES_REFRESH:
            return { ...state, errors: {}, done: false };
        case WAITING_COMPANIES_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default WaitingCompaniesReducer;
