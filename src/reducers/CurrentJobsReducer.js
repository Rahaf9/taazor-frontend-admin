import { CURRENT_JOB_FAILURE, CURRENT_JOB_LOADING, CURRENT_JOB_REFRESH, CURRENT_JOB_SUCCESS } from "../actions/CurrentJobsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CurrentJobsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CURRENT_JOB_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case CURRENT_JOB_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case CURRENT_JOB_REFRESH:
            return { ...state, errors: {}, done: false };
        case CURRENT_JOB_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default CurrentJobsReducer;
