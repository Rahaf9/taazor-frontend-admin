import { CURRENT_JOB_ACCEPTABLE_USERS_FAILURE, CURRENT_JOB_ACCEPTABLE_USERS_LOADING, CURRENT_JOB_ACCEPTABLE_USERS_SUCCESS, CURRENT_JOB_ACCEPTABLE_USER_REFRESH } from "../actions/CurrentJobAcceptableUsersAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const CurrentJobAcceptableUsersReducer = (state = initialState, action) => {

    switch (action.type) {

        case CURRENT_JOB_ACCEPTABLE_USERS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case CURRENT_JOB_ACCEPTABLE_USERS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case CURRENT_JOB_ACCEPTABLE_USER_REFRESH:
            return { ...state, errors: {}, done: false, isLoading: false };
        case CURRENT_JOB_ACCEPTABLE_USERS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }

}


export default CurrentJobAcceptableUsersReducer;