import { EXPIRED_JOB_DETAILS_FAILURE, EXPIRED_JOB_DETAILS_LOADING, EXPIRED_JOB_DETAILS_REFRESH, EXPIRED_JOB_DETAILS_SUCCESS } from "../actions/ExpiredJobDetailsAction";

const initialState = {
    data: {},
    errors: {},
    done: false,
    isLoading: false
};

const ExpiredJobDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case EXPIRED_JOB_DETAILS_FAILURE:
            return { ...state, data: {}, errors: action.payload, done: false, isLoading: false };
        case EXPIRED_JOB_DETAILS_SUCCESS:
            return { ...state, data: action.payload, errors: {}, done: true, isLoading: false };
        case EXPIRED_JOB_DETAILS_REFRESH:
            return { ...state, errors: {}, done: false };
        case EXPIRED_JOB_DETAILS_LOADING:
            return { ...state, isLoading: action.payload };
        default:
            return state;
    }
};

export default ExpiredJobDetailsReducer;
