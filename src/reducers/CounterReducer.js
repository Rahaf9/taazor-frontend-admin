import { INCREMENT_ACTION } from "../actions/counterActions";

const initialState = {
    number: 0
};

const CounterReducer = ( state = initialState, action) => {

    switch(action.type){
        case  INCREMENT_ACTION: 
            return {...state,number:state.number + 1};
        default :
            return state;

    }

}

export default CounterReducer;


{/*
    import React, { useState, useRef } from 'react';
import { LoadScript, GoogleMap, Autocomplete } from '@react-google-maps/api';

const App = () => {
  const [selectedPlace, setSelectedPlace] = useState(null);
  const autocompleteRef = useRef(null);

  const GoogleMapsApiKey = "AIzaSyBulEKz8sbYBACS0D_7zHR0eReoM70RwGw";

  const handlePlaceSelect = () => {
    if (autocompleteRef.current) {
      const place = autocompleteRef.current.getPlace();
      if (place && place.geometry && place.geometry.location) {
        setSelectedPlace({
          name: place.name,
          address: place.formatted_address,
          location: {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
          }
        });
        console.log('Selected place:', selectedPlace);
      }
    }
  };
  

  return (
    <LoadScript
      googleMapsApiKey={GoogleMapsApiKey}
      libraries={['places']} // Include the libraries prop with the value ['places']
    >
      <GoogleMap
        mapContainerStyle={{ width: '100%', height: '400px' }}
        center={{ lat: 0, lng: 0 }}
        zoom={2}
      >
        <Autocomplete
          onLoad={(autocomplete) => {
            autocompleteRef.current = autocomplete;
          }}
          onPlaceChanged={handlePlaceSelect}
        >
          <input
            type="text"
            placeholder="Search for a place..."
            style={{
              boxSizing: 'border-box',
              border: '1px solid transparent',
              width: '240px',
              height: '32px',
              padding: '0 12px',
              borderRadius: '3px',
              boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)',
              fontSize: '14px',
              outline: 'none',
              textOverflow: 'ellipses',
              position: 'absolute',
              left: '50%',
              marginLeft: '-120px'
            }}
          />
        </Autocomplete>
      </GoogleMap>
    </LoadScript>
  );
};

export default App;

*/ }