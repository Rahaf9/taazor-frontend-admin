import React, { useEffect } from "react";
import IconsCardReusable from "../../General/IconsCardReusable/IconsCardReusable";
import { InfoCircleOutlined, EyeOutlined, CheckCircleOutlined, PlusCircleFilled } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { Col, Row, notification } from "antd";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { useNavigate } from 'react-router-dom';
import { ROUTES } from "../../General/Routes/Route";
import { useDispatch, useSelector } from 'react-redux';
import { CurrentJobAction, CurrentJobRefreshPage, JobNotConvertedAction } from "../../actions/CurrentJobsAction";
import { useTranslation } from 'react-i18next';
import CustomFloatButton from "../../General/CustomFloatButton/CustomFloatButton";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import { horizVerFlex } from "../../General/Styles/general styles";
import Pagination from "../../General/Reusable Componenets/Pagination/Pagination";

const JobsNotConverted = () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const elements = useSelector(state => state.currentJobsReducer.data);
    const isLoading = useSelector(state => state.currentJobsReducer.isLoading);
    const errors = useSelector(state => state.currentJobsReducer.errors);
    const navigate = useNavigate();

    const cardData = [
        {
            icon: () => <InfoCircleOutlined style={{ fontSize: '2.2rem', color: lightGreenPrimary }} />,
            description: t('details') // Translate 'Details' to Arabic if needed
        },
        {
            icon: () => <EyeOutlined style={{ fontSize: '2.2rem', color: lightGreenPrimary }} />,
            description: t('show') // Translate 'Show' to Arabic if needed
        },
        // Add more items as needed
    ];

    useEffect(() => {
        dispatch(JobNotConvertedAction(1))
    }, []);

    useEffect(() => {
        errors && errors.message && notification.error({
            message: 'Error',
            description: errors.message
        })
        dispatch(CurrentJobRefreshPage())
    }, [errors.message]);

    const handleShowIconClick = (index) => {
        console.log("Show Icon clicked at index:", index);
        // Do whatever you need with the index
        navigate(`${ROUTES.JobsNotConvertedUsers.path}/${index}`, { state: { jobID: index } })
    };

    const handleDetailsIconClick = (index) => {
        console.log("Details Current Job Icon clicked at index:", index);
        // Do whatever you need with the index
        navigate(`${ROUTES.CurrentJobDetails.path}/${index}`)
    };


    return (
        <div>
            {
                isLoading ? <Spinner />
                    : <div>
                        <div style={{ width: '100%' }}>
                            <CustomHeader title={t('JobNotConverted')}
                                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
                        </div>
                        <div style={{ maxHeight: '100%', overflowY: 'auto', scrollbarWidth: 'none', msOverflowStyle: 'none' }}>

                            <Row className='d-flex justify-content-around' style={{ width: '80vw' }} gutter={16}>
                                {elements && elements.data && elements.data?.length > 0 ?
                                    elements.data.map((element, index) => (
                                        <Col style={{ margin: '10px' }} key={index} xs={18} sm={16} md={12} lg={8} xl={8}>
                                            <IconsCardReusable title={
                                                <div className="my-2">
                                                    <div>
                                                        {element.company_name}
                                                    </div>
                                                    <div>
                                                        {element.job_title}
                                                    </div>
                                                </div>
                                            } data={cardData}
                                                myIndex={element.id} onDetailsIconClick={handleDetailsIconClick}
                                                onShowIconClick={handleShowIconClick} />
                                        </Col>
                                    )) : <div className='flexible text-danger fw-bold mt-5'>
                                        {t('no_items_found')}
                                    </div>
                                }
                            </Row>
                            <div className={`${horizVerFlex} m-5`}>
                                <Pagination onPageChange={(page)=>{dispatch(JobNotConvertedAction(page))}} currentPage={elements.current_page} totalPages={elements.last_page ?? '0'} />
                            </div>
                        </div>
                    </div>
            }
        </div>
    );
}

export default JobsNotConverted;
