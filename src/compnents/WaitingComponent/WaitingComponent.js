import React, { useEffect } from "react";
import "./WaitingComponent.css";
import { WaitingTitleStyle } from "../../General/Styles/texts";
// import useCheckCompanyStatus from "../../General/CustomHooks/useCheckCompanyStatus ";
import { useTranslation } from "react-i18next";
import { Button } from "antd";
import { ROUTES } from "../../General/Routes/Route";
import { useNavigate } from "react-router-dom";
// import { companyStatusAction } from "../../actions/CompanyStatusAction";

const WaitingComponent = ({ title }) => {

  const { t, i18n } = useTranslation();
  console.log(i18n.language); // Log the current language
  console.log(i18n.dir()); // Log the directionality
  const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl'
  const navigate = useNavigate();

  return (
    <div style={{ direction: direction }} className="container">
      <div className="text-container" style={WaitingTitleStyle}>
        {t(title)}
        <Button title={t('login')} onClick={() => {navigate(ROUTES.Login.path)}}/>
        <div className="circle"></div>
      </div>
      <div className="image-container">
        <img
          className="image"
          src="/images/the light green 2/Welcome-cuate.png"
          alt="Waiting Illustration"
        />
      </div>
    </div>
  );
};

export default WaitingComponent;
