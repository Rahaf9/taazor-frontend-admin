import React, { useEffect, useState } from "react";
import { Col, Row, Select, notification } from "antd";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { WhiteColor } from "../../General/Styles/colors";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { InfoCircleOutlined, UserOutlined } from '@ant-design/icons';
import { advisorCompanyConsolutionsReceivedAction, advisorConsolutionsReceivedRefreshPage, advisorUserConsolutionsReceivedAction } from "../../actions/AdvisorConsoultionsReceivedAction";
import DetailedCard from "../../General/DetailedCard/DetailedCard";
import { useNavigate, useParams } from "react-router-dom";
import { ROUTES } from "../../General/Routes/Route";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";

const { Option } = Select;

const AdvisorConsultionsReceived = () => {
    const [selectedValue, setSelectedValue] = useState('company');
    const { t, i18n } = useTranslation()
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl'
    const { index } = useParams();
    const { advisor, users } = useSelector(state => state.adivsorConsoultionsReceivedReducer.data);
    const isLoading = useSelector(state => state.adivsorConsoultionsReceivedReducer.isLoading);
    const errors = useSelector(state => state.adivsorConsoultionsReceivedReducer.errors);

    const handleChange = (value) => {
        setSelectedValue(value);
        console.log("Selected value:", value);
    };

    useEffect(()=>{
        console.log(`the advisors changed and its value is ${JSON.stringify(advisor)} and the users is ${JSON.stringify(users)}`)
    },[isLoading])

    const onDetailsClick = (index) => {
        if(selectedValue === 'company'){
            // consultants[index].id this represent the id of the card selected and that mean that this is 
            // the user id you want to get its consultants 
            navigate(`${ROUTES.AdvisorCompanyConsultantDetails.path}/${users[index].id}`,
                { state: { advisor_id: advisor.id,advisor_name: advisor.name,advisor_topic: advisor.topics }} )
        }else{
            navigate(`${ROUTES.AdvisorUserConsultantDetails.path}/${users[index].id}`,
                { state: { advisor_id: advisor.id,advisor_name: advisor.name,advisor_topic: advisor.topics }})
        }
    }

    useEffect(() => {
        console.log(`the selected value at first is ${selectedValue}`)
        dispatch(advisorCompanyConsolutionsReceivedAction(index))
    }, [])

    useEffect(() => {
        console.log(`the selected value filter is changed and it is ${selectedValue} 
            so here we must update the comp and dispatch depend on the value selected`)
        if (selectedValue === 'company') {
            dispatch(advisorCompanyConsolutionsReceivedAction(index))
        } else {
            dispatch(advisorUserConsolutionsReceivedAction(index))
        }
    }, [selectedValue])

    useEffect(() => {
        errors && errors.message && notification.error({
            message: t('Error'),
            description: errors.message
        })
        dispatch(advisorConsolutionsReceivedRefreshPage())
    }, [errors.message]);

    return (
        <>
            {
                isLoading ? <Spinner /> : <div style={{ direction: direction }}>
                    <CustomHeader
                        title={t('showConsultantsApplied')}
                        icon={
                            <Select
                                // placeholder={t('select_topic')}
                                style={{ width: 120, fontSize: '1rem', color: WhiteColor }}
                                onChange={handleChange}
                                defaultValue={selectedValue}
                            >
                                <Option value="company">Company</Option>
                                <Option value="user">User</Option>
                            </Select>
                        }
                    />
                    <div style={{ width: '80vw', height: '90vh', overflowY: 'auto', scrollbarWidth: 'none' }} className="mt-3">
                        <Row className='d-flex justify-content-around' style={{ width: '100%', height: '100%' }} gutter={16}>
                            {users && users.length > 0 ?
                                users.map((card, index) => (
                                    <Col style={{ margin: '10px' }} key={index} xs={20} sm={16} md={11} lg={10} xl={9}>
                                        <DetailedCard
                                            description={
                                                <span>
                                                    <InfoCircleOutlined className="mx-2" style={{ fontSize: '1.1rem' }} /> {card.email}
                                                </span>
                                            }
                                            title={
                                                <span>
                                                    <UserOutlined className="mx-2" style={{ fontSize: '1.1rem' }} /> {card.name}
                                                </span>
                                            }
                                            image={'/images/the light green 2/People search-rafiki.png'}
                                            onDetailsClick={() => onDetailsClick(index)}
                                        />
                                    </Col>
                                )) : <div className='flexible text-danger fw-bold mt-5'>
                                    {t('noConsultantsApplied')}
                                </div>
                            }
                        </Row>
                    </div>
                </div>
            }
        </>
    );
}

export default AdvisorConsultionsReceived;
