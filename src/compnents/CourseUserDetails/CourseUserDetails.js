import React, { useEffect, useState } from "react";
import UserDetailsInterface from "../../General/UserDetailsInterface/UserDetailsInterface";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { courseUserDetailsAction } from "../../actions/CourseUserDetailsAction";
import { useNavigate, useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
// import useCheckCompanyStatus from "../../General/CustomHooks/useCheckCompanyStatus ";
import { CheckOutlined } from '@ant-design/icons';
import { ROUTES } from "../../General/Routes/Route";


const CourseUserDetails = () => {

    // useCheckCompanyStatus();
    const { index } = useParams();
    const { t, i18n } = useTranslation();
    const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl';
    const navigate = useNavigate();

    const additionalContent = (is_applied) => (
        <div style={{
            display: 'flex', alignItems: 'space-around', justifyContent: 'center', flexDirection: 'column', marginTop: '20px', fontSize: '.74rem'
            , fontWeight: 'bold'
        }}>
            <div className="w-100 d-flex justify-content-around align-items-center">
                <div className="w-50">{t('user_status')}</div>
                <div className="d-flex flex-column justify-content-around align-items-center">
                    <CheckOutlined className="w-50 m-1" style={{
                        color: 'white', padding: '0px 6px 0px 6px', borderRadius: '10%',
                        marginLeft: '8px', fontSize: '1.7rem', backgroundColor: lightGreenPrimary
                    }} />
                    <div className="m-1">
                        {is_applied === true ? 'Registered' : 'UnRegistered'}
                    </div>
                </div>
            </div>
            <div className="w-100 d-flex justify-content-around align-items-center">
                <div className="w-50">{t('showUserCv')}</div>
                <div className="d-flex flex-column justify-content-around align-items-center">
                    <EyeOutlined onClick={() => { navigate(`${ROUTES.UserCv.path}/${index}`) }} className="m-1 w-50" style={{
                        color: 'white', padding: '0px 6px 0px 6px', borderRadius: '10%',
                        marginLeft: '8px', fontSize: '1.7rem', backgroundColor: lightGreenPrimary
                    }} />
                    <div className="m-1">
                        Show Cv
                    </div>
                </div>
            </div>
        </div>
    );

    // what is needed to make a selector to the CourseUserDetails and make an action to take the users from 
    // the users array stored in the reducer before and save the data in the reducer of the Courseuserdetails 

    return (
        <div style={{ direction: direction }}>
            <CustomHeader title={t('show_applicants_details')}
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
            <UserDetailsInterface detailsAction={courseUserDetailsAction}
                selectorAction={
                    {
                        'detailsSelector': (state) => state.courseUserDetailsReducer.data,
                        'loadingSelector': (state) => state.courseUserDetailsReducer.isLoading,
                        'doneSelector': (state) => state.courseUserDetailsReducer.done,
                    }
                } index={index} additionalContent={additionalContent} />
        </div>
    )

}

export default CourseUserDetails;