import React, { useEffect, useState } from "react";
import { BlackColor, GrayColor, GrayOpacity, OrangeColor, WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { horizAroundAlignCenterColumn, horizVerFlex } from "../../General/Styles/general styles";
import { CheckCircleOutlined } from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';
import './AdvisorCompanyConsultantDetails.css';
import { useLocation, useParams } from 'react-router-dom';
import { notification, Rate } from 'antd';
// import useCheckCompanyStatus from "../../General/CustomHooks/useCheckCompanyStatus ";
import { advisorConsultantDetailsAction, advisorConsultantDetailsRefreshPage } from "../../actions/AdvisorConsultantDetailsAction";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";

const AdvisorCompanyConsultantDetails = () => {
    // useCheckCompanyStatus();
    const { index } = useParams();
    const dispatch = useDispatch();
    const location = useLocation();
    const { advisor_id, advisor_name, advisor_topic } = location.state;
    const errors = useSelector((state) => state.advisorConsultionDetailsReducer.errors);
    const isLoading = useSelector((state) => state.advisorConsultionDetailsReducer.isLoading);
    const messages1 = useSelector((state) => state.advisorConsultionDetailsReducer.data);

    useEffect(() => {
        // the index is the card detected and selected and it its represent the user id
        dispatch(advisorConsultantDetailsAction(advisor_id, index, 'companies'));
    }, [index]);

    useEffect(() => {
        console.log(`the data consultions is ${JSON.stringify(messages1)}`);
    }, [messages1]);

    useEffect(() => {
        if (errors) {
            console.log(`the errors inside detailed consultants ${JSON.stringify(errors.message)}`);
            errors && errors.message && notification.error({
                message: 'Error',
                description: errors.message,
            });
            dispatch(advisorConsultantDetailsRefreshPage());
        }
    }, [errors.message]);

    return (
        <div>
            {
                isLoading ? <Spinner /> :
                    <div style={{ width: '100vw' }} className={`${horizVerFlex}`}>
                        <div style={{ width: '25vw', height: '100vh', backgroundColor: lightGreenPrimary }} className={`${horizAroundAlignCenterColumn}`}>
                            <div className={`h-50 ${horizVerFlex}`}>
                                <img src="/images/the light green 2/People search-amico.png" style={{ width: '80%', height: '65%' }} />
                            </div>
                            <div className={`h-50 ${horizVerFlex}`}>
                                <div className="w-100 h-100 d-flex justify-content-center flex-column">
                                    <div><CheckCircleOutlined style={{ fontSize: '13px', marginRight: '10px', color: WhiteColor }} />
                                        <span className="text-white">{advisor_name}</span>
                                    </div>
                                    <div><CheckCircleOutlined style={{ fontSize: '13px', marginRight: '10px', color: WhiteColor }} />
                                        <span className="text-white">{advisor_topic}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style={{ width: '75vw', height: '100vh', backgroundColor: GrayOpacity }}>
                            <div className={`${horizVerFlex}`} style={{ height: '100%', width: '100%', color: BlackColor }}>
                                <div className="w-100 h-100 d-flex align-items-center mx-2" style={{
                                    overflowY: 'auto', display: 'flex', flexDirection: 'column-reverse', scrollbarWidth: 'none'
                                }}>
                                    {messages1 ? messages1?.map((message, index) => (
                                        <React.Fragment key={message.id}>
                                            <div className="w-100 d-flex justify-content-start m-3">
                                                <div className="w-75 text-center p-3" style={{
                                                    backgroundColor: WhiteColor, borderRadius: '20px',
                                                    border: `2px solid ${lightGreenPrimary}`
                                                }}>
                                                    {message.user_message}
                                                </div>
                                            </div>
                                            {
                                                message.advisor_reply && (
                                                    <div className="w-100 d-flex flex-column align-items-end ">
                                                        <div className="w-75 text-center p-2">
                                                            <span className="w-100 d-flex justify-content-start m-3"
                                                                style={{
                                                                    color: OrangeColor, fontSize: '1rem',
                                                                    fontWeight: 'bold', textAlign: 'start'
                                                                }}>
                                                                Comment
                                                            </span>
                                                            <div className="p-3" style={{background:GrayColor,borderRadius: '20px',}}>
                                                                {message.review}
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                            {
                                                message.advisor_reply && (
                                                    <div className="w-100 d-flex flex-column align-items-start ">
                                                        <div className="w-75 text-center p-2">
                                                            <Rate defaultValue={message.rating} allowHalf disabled />
                                                        </div>
                                                    </div>
                                                )
                                            }
                                            <div className="w-100 d-flex justify-content-end m-3">
                                                <div className="w-75 text-center p-3" style={{ backgroundColor: lightGreenPrimary, borderRadius: '20px' }}>
                                                    {message.advisor_reply ?? 'There is no respond by advisor till now'}
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    )) : <div> there is no data here </div>}
                                </div>
                            </div>
                        </div>
                    </div>
            }
        </div>
    );
}

export default AdvisorCompanyConsultantDetails;

