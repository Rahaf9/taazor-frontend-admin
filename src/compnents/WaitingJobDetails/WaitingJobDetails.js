import React, { useState, useEffect } from "react";
import { Button, Modal, notification } from 'antd';
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import {
  EyeOutlined, FormOutlined, DollarOutlined, UsergroupAddOutlined, InfoCircleOutlined, FileTextOutlined, CheckCircleFilled,
  IdcardOutlined, EnvironmentOutlined, BookOutlined, GlobalOutlined, CloseCircleFilled
} from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { BlackColor, OrangeColor, RedColor, WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { horizAroundAlignCenter, horizAroundAlignCenterColumn, horizCenterAlignCenterColumn, horizVerFlex, horizVerFlexColumn } from "../../General/Styles/general styles";
import { UserDetailsTitleStyle } from "../../General/Styles/texts";
import IconTextEntry from "../../General/IconTextEntry/IconTextEntry";
import StatusEntry from "../../General/StatusEntry/StatusEntry";
import HorizontalScroll from "../../General/HorizontalScroll/HorizontalScroll";
import { CurrentJobDetailsAction } from "../../actions/CurrentJobDetailsAction";
import './WaitingJobDetails.css';
import QuestionDetailsModal from "../../General/QuestionsAnswersModal/QuestionsAnswersModal";
import { useTranslation } from 'react-i18next';
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import { acceptJobAction, deleteCurrentJobAction, rejectJobAction } from "../../General/ServerSideApis/ActionsRequestsCreator";
import { allAdminActionRefreshPage } from "../../actions/AllAdminActions";
import ConfirmationModal from "../../General/Reusable Componenets/ConfirmationModal/ConfirmationModal";
import { ROUTES } from "../../General/Routes/Route";
// import useCheckCompanyStatus from "../../General/CustomHooks/useCheckCompanyStatus ";

const WaitingJobDetails = () => {
  // useCheckCompanyStatus();
  const { t, i18n } = useTranslation();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalData, setModalData] = useState([]);
  const { job,company } = useSelector((state) => state.currentJobDetailsReducer.data);
  const details = job;
  const errors = useSelector((state) => state.currentJobDetailsReducer.errors);
  const isLoading = useSelector((state) => state.currentJobDetailsReducer.isLoading);
  const deleteErrors = useSelector((state) => state.allActionsAdminReducer.errors);
  const deleteDone = useSelector((state) => state.allActionsAdminReducer.done);
  const { index } = useParams();
  const dispatch = useDispatch();
  const direction = i18n.dir() == 'ar' ? 'ltr' : 'rtl';
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false)
  const [isAcceptModalVisible, setIsAcceptModalVisible] = useState(false)
  const [isRejectModalVisible, setIsRejectModalVisible] = useState(false)
  const navigate = useNavigate()

  useEffect(() => {
    console.log(`This is the index of the current job we want to get its details: ${index}`);
    dispatch(CurrentJobDetailsAction(index));
  }, []);

  useEffect(() => {
    if (details) {
      console.log(`Details loaded: ${JSON.stringify(details)}`);
    }
  }, [details]);

  const handleDeleteClick = () => {
    // here you should create the custom action to call delete job api
    dispatch(deleteCurrentJobAction(index))
    setIsDeleteModalVisible(false)
  }

  const handleAcceptClick = () => {
    // here you should create the custom action to call delete job api
    dispatch(acceptJobAction(index))
    setIsAcceptModalVisible(false)
  }

  const handleRejectClick = () => {
    // here you should create the custom action to call delete job api
    dispatch(rejectJobAction(index))
    setIsRejectModalVisible(false)
  }

  useEffect(() => {
    if (errors) {
      console.log(`Error fetching details: ${JSON.stringify(errors.message)}`);
      errors && errors.message && notification.error({
        message: t('error'),
        description: errors.message,
      });
      dispatch(allAdminActionRefreshPage());
    }
  }, [errors.message]);

  useEffect(() => {
    if (deleteErrors) {
      console.log(`Error delete job: ${JSON.stringify(deleteErrors.message)}`);
      deleteErrors && deleteErrors.message && notification.error({
        message: t('error'),
        description: deleteErrors.message,
      });
      dispatch(allAdminActionRefreshPage());
    }
  }, [deleteErrors.message]);

  useEffect(() => {
    if (deleteDone) {
      notification.success({
        message: t('action_success'),
        description: t('action_success'),
      });
      navigate(-1);
      dispatch(allAdminActionRefreshPage());
    }
  }, [deleteDone])

  const transformData = () => {
    return details.forms.flatMap(form =>
      form.questions.map(question => ({
        id: question.id,
        created_at: question.created_at,
        updated_at: question.updated_at,
        form_id: question.form_id,
        question: question.question,
        options: question.options.map(option => ({
          id: option.id,
          created_at: option.created_at,
          updated_at: option.updated_at,
          option_text: option.option_text,
          question_id: option.question_id
        }))
      }))
    );
  };

  const handleIconClick = () => {
    setModalData(transformData());
    setIsModalVisible(true);
  };

  const handleCloseModal = () => {
    setIsModalVisible(false);
  };

  // Define arrays of data for IconTextEntry and StatusEntry
  const iconTextEntries = [
    { icon: <FormOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.topic ?? '' },
    { icon: <IdcardOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.job_title ?? '' },
    { icon: <DollarOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.salary_fields ?? '' },
    { icon: <UsergroupAddOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.number_employees ?? '' },
    { icon: <BookOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.education_level ?? '' },
    { icon: <FileTextOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.require_qualifications ?? '' },
    { icon: <InfoCircleOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: <div style={{cursor:'pointer'}}
    onClick={() => navigate(`${ROUTES.AcceptedCompaniesProfile.path}/${job.company_id}`)}>
      {t('clickDetailsCompany')}  
    </div> },
    { icon: <EnvironmentOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.location ?? '' },
    { icon: <InfoCircleOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.special_qualifications ?? '' },
    { icon: <GlobalOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.required_languages ?? '',
      title:'lang' },
  ];

  const statusEntries = [
    { title: t('languages'), text: details?.required_languages ?? '' },
    { title: t('military_status'), text: details?.is_required_military ?? '' },
    { title: t('drive_license'), text: details?.is_required_license === 1 ? 'Yes' : 'No' },
    { title: t('end_date'), text: details?.end_date ?? '' },
    { title: t('experience'), text: details?.experiense_years ?? '' },
    { title: t('job_environment'), text: details?.job_environment ?? '' },
    { title: t('job_time'), text: details?.job_time ?? '' },
    { title: t('gender'), text: details?.gender ?? '' },
  ];

  return (
    <div>
      {
        isLoading ? <Spinner /> :
          <div style={{ direction: direction }}>
            <CustomHeader title={company?.name} icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
            <div style={{ width: '100vw', minHeight: '76vh' }} className={`horizBetweenJobDetails mt-5`}>
              <div className={`JobDetailsLeftSide ${horizVerFlexColumn}`}>
                <div className="h-100 w-50 px-4 pt-1 pb-3" style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                  <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '100%' }}>
                    {iconTextEntries.slice(0, 7).map((entry, index) => (
                      <IconTextEntry key={index} icon={entry.icon} text={entry.text} />
                    ))}
                  </div>
                  <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenter}`} style={{
                    height: '100%', direction:
                      i18n.dir() === 'rtl' ? 'rtl' : 'ltr'
                  }}>
                    <div style={{ color: WhiteColor }}>
                      {t('questions_details')}
                    </div>
                    <div>
                      <InfoCircleOutlined style={{ color: RedColor, fontSize: '20px', cursor: 'pointer' }}
                        onClick={handleIconClick} />
                    </div>
                  </div>
                </div>
              </div>
              <div className={`JobDetailsRightSide ${horizVerFlexColumn}`}>
                <div className={`w-50 p-4`} style={{ background: lightGreenPrimary, borderRadius: '20px', height: '40%' }}>
                  {statusEntries && statusEntries.title === t('languages') &&
                    <div style={UserDetailsTitleStyle} className="mb-2">
                      {t('languages')}
                    </div>
                  }
                  {statusEntries && (statusEntries.title === t('languages') && statusEntries.text) ? (
                    <HorizontalScroll items={statusEntries.text} />
                  ) : null}
                  <div className={`w-100 d-flex mt-2 ${horizCenterAlignCenterColumn}`} style={{ height: '64%' }}>
                    {statusEntries.slice(1, 3).map((entry, index) => (
                      <StatusEntry key={index} title={entry.title} text={entry.text} />
                    ))}
                  </div>
                </div>
                <div className={`w-50 p-4 mt-3`} style={{ background: lightGreenPrimary, borderRadius: '20px', height: '60%' }}>
                  <div className={`w-100 d-flex mt-2 ${horizCenterAlignCenterColumn}`} style={{ height: '64%' }}>
                    {statusEntries.slice(3).map((entry, index) => (
                      <StatusEntry key={index} title={entry.title} text={entry.text} />
                    ))}
                  </div>
                  <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '36%' }}>
                    {iconTextEntries.slice(7).map((entry, index) => (
                      index === 2 ? <IconTextEntry key={index} icon={entry.icon} text={Array.isArray(entry.text) && entry.text?.join(' ')} /> :
                      <IconTextEntry key={index} icon={entry.icon} text={entry.text} />
                    ))}
                  </div>
                </div>
              </div>
            </div>
            <div className={`${horizVerFlexColumn} mt-2`}>
              <div
                style={{ fontSize: '.9rem', fontWeight: 'bold', color: BlackColor, padding: '10px', width: '100%', marginBottom: '20px' }}>
                <div className={`w-50 ${horizAroundAlignCenter}`}>
                  <span>{t('clickAcceptReject')}</span>
                  <div>
                    <CheckCircleFilled style={{ marginLeft: '10px', fontSize: '1.2rem', color: OrangeColor }}
                    onClick={() => {setIsAcceptModalVisible(true)}} />
                    <CloseCircleFilled style={{ marginLeft: '10px', fontSize: '1.2rem', color: OrangeColor }}
                    onClick={() => {setIsRejectModalVisible(true)}} />
                  </div>
                </div>
              </div>
              {localStorage.getItem('roleName').includes('admin') &&
                <Button style={{ fontSize: '1.2rem', color: WhiteColor, background: OrangeColor }}
                  className={`${horizVerFlex}`} onClick={() => {setIsDeleteModalVisible(true)}}>
                  {t('delete')}
                </Button>
              }
            </div>
            <QuestionDetailsModal
              visible={isModalVisible}
              onClose={handleCloseModal}
              data={modalData}
            />
            <ConfirmationModal modalContent={t('Are you sure you want to perform this action?')}
              visible={isDeleteModalVisible} onOk={handleDeleteClick} onCancel={() => setIsDeleteModalVisible(false)} />
            <ConfirmationModal modalContent={t('Are you sure you want to perform this action?')}
              visible={isAcceptModalVisible} onOk={handleAcceptClick} onCancel={() => setIsAcceptModalVisible(false)} />
              <ConfirmationModal modalContent={t('Are you sure you want to perform this action?')}
              visible={isRejectModalVisible} onOk={handleRejectClick} onCancel={() => setIsRejectModalVisible(false)} />
          </div>
      }
    </div>
  );
};

export default WaitingJobDetails;
