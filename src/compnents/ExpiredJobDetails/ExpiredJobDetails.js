import React, { useState, useEffect } from "react";
import { notification } from 'antd';
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import {
  EyeOutlined, FormOutlined, DollarOutlined, UsergroupAddOutlined, InfoCircleOutlined, FileTextOutlined,
  IdcardOutlined, EnvironmentOutlined, BookOutlined, GlobalOutlined
} from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { RedColor, WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { horizAroundAlignCenter, horizAroundAlignCenterColumn, horizCenterAlignCenterColumn, horizVerFlexColumn } from "../../General/Styles/general styles";
import { UserDetailsTitleStyle } from "../../General/Styles/texts";
import IconTextEntry from "../../General/IconTextEntry/IconTextEntry";
import StatusEntry from "../../General/StatusEntry/StatusEntry";
import HorizontalScroll from "../../General/HorizontalScroll/HorizontalScroll";
import '../CurrentJobDetails/CurrentJobDetails.css';
import QuestionDetailsModal from "../../General/QuestionsAnswersModal/QuestionsAnswersModal";
import { useTranslation } from 'react-i18next';
// import useCheckCompanyStatus from "../../General/CustomHooks/useCheckCompanyStatus ";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import { ExpiredJobDetailsAction } from "../../actions/ExpiredJobDetailsAction";
import { ExpiredJobDetailsRefreshPage } from "../../actions/ExpiredJobDetailsAction";
import { ROUTES } from "../../General/Routes/Route";

const ExpiredJobDetails = () => {
  // useCheckCompanyStatus();
  const { t, i18n } = useTranslation();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalData, setModalData] = useState([]);
  const { job,company } = useSelector((state) => state.expiredJobDetailsReducer.data);
  const details = job;
  const errors = useSelector((state) => state.expiredJobDetailsReducer.errors);
  const isLoading = useSelector((state) => state.expiredJobDetailsReducer.isLoading);
  const { index } = useParams();
  const dispatch = useDispatch();
  const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl';
  const navigate = useNavigate();

  useEffect(() => {
    console.log(`This is the index of the expired job we want to get its details: ${index}`);
    dispatch(ExpiredJobDetailsAction(index));
  }, []);

  useEffect(() => {
    if (details) {
      console.log(`Details loaded: ${JSON.stringify(details)}`);
    }
  }, [details]);

  useEffect(() => {
    if (errors) {
      console.log(`Error fetching details: ${JSON.stringify(errors.message)}`);
      errors && errors.message && notification.error({
        message: t('error'),
        description: errors.message,
      });
      dispatch(ExpiredJobDetailsRefreshPage());
    }
  }, [errors.message]);

  const transformData = () => {
    return details.forms.flatMap(form =>
      form.questions.map(question => ({
        id: question.id,
        created_at: question.created_at,
        updated_at: question.updated_at,
        form_id: question.form_id,
        question: question.question,
        options: question.options.map(option => ({
          id: option.id,
          created_at: option.created_at,
          updated_at: option.updated_at,
          option_text: option.option_text,
          question_id: option.question_id
        }))
      }))
    );
  };

  const handleIconClick = () => {
    setModalData(transformData());
    setIsModalVisible(true);
  };

  const handleCloseModal = () => {
    setIsModalVisible(false);
  };

  // Define arrays of data for IconTextEntry and StatusEntry
  const iconTextEntries = [
    { icon: <FormOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.topic ?? '' },
    { icon: <IdcardOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.job_title ?? '' },
    { icon: <DollarOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.salary_fields ?? '' },
    { icon: <UsergroupAddOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.number_employees ?? '' },
    { icon: <BookOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.education_level ?? '' },
    { icon: <FileTextOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.require_qualifications ?? '' },
    { icon: <InfoCircleOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: <div style={{cursor:'pointer'}}
    onClick={() => navigate(`${ROUTES.AcceptedCompaniesProfile.path}/${job.company_id}`)}>
      {t('clickDetailsCompany')}
    </div> },
    { icon: <EnvironmentOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.location ?? '' },
    { icon: <InfoCircleOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.special_qualifications ?? '' },
    { icon: <GlobalOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.required_languages ?? '' },
  ];

  const statusEntries = [
    { title: t('languages'), text: details?.required_languages ?? '' },
    { title: t('military_status'), text: details?.is_required_military ?? '' },
    { title: t('drive_license'), text: details?.driveLicense ?? '' },
    { title: t('end_date'), text: details?.end_date ?? '' },
    { title: t('experience'), text: details?.experiense_years ?? '' },
    { title: t('job_environment'), text: details?.job_environment ?? '' },
    { title: t('job_time'), text: details?.job_time ?? '' },
    { title: t('gender'), text: details?.gender ?? '' },
  ];

  return (
    <div>
      {
        isLoading ? <Spinner /> :
          <div style={{direction:direction}}>
            <CustomHeader title={company?.name} icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
            <div style={{ width: '100vw', minHeight: '76vh' }} className={`horizBetweenJobDetails mt-5`}>
              <div className={`JobDetailsLeftSide ${horizVerFlexColumn}`}>
                <div className="h-100 w-50 px-4 pt-1 pb-3" style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                  <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '100%' }}>
                    {iconTextEntries.slice(0, 7).map((entry, index) => (
                      <IconTextEntry key={index} icon={entry.icon} text={entry.text} />
                    ))}
                  </div>
                  <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenter}`} style={{
                    height: '100%', direction:
                      i18n.dir() === 'rtl' ? 'rtl' : 'ltr'
                  }}>
                    <div style={{ color: WhiteColor }}>
                      {t('questions_details')}
                    </div>
                    <div>
                      <InfoCircleOutlined style={{ color: RedColor, fontSize: '20px', cursor: 'pointer' }}
                        onClick={handleIconClick} />
                    </div>
                  </div>
                </div>
              </div>
              <div className={`JobDetailsRightSide ${horizVerFlexColumn}`}>
                <div className={`w-50 p-4`} style={{ background: lightGreenPrimary, borderRadius: '20px', height: '40%' }}>
                  {statusEntries && statusEntries.title === t('languages') &&
                    <div style={UserDetailsTitleStyle} className="mb-2">
                      {t('languages')}
                    </div>
                  }
                  {statusEntries && (statusEntries.title === t('languages') && statusEntries.text) ? (
                    <HorizontalScroll items={statusEntries.text} />
                  ) : null}
                  <div className={`w-100 d-flex mt-2 ${horizCenterAlignCenterColumn}`} style={{ height: '64%' }}>
                    {statusEntries.slice(1, 3).map((entry, index) => (
                      <StatusEntry key={index} title={entry.title} text={entry.text} />
                    ))}
                  </div>
                </div>
                <div className={`w-50 p-4 mt-3`} style={{ background: lightGreenPrimary, borderRadius: '20px', height: '60%' }}>
                  <div className={`w-100 d-flex mt-2 ${horizCenterAlignCenterColumn}`} style={{ height: '64%' }}>
                    {statusEntries.slice(3).map((entry, index) => (
                      <StatusEntry key={index} title={entry.title} text={entry.text} />
                    ))}
                  </div>
                  <div className={`w-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '36%' }}>
                    {iconTextEntries.slice(7).map((entry, index) => (
                      index === 2 ? <IconTextEntry key={index} icon={entry.icon} text={Array.isArray(entry.text) && entry.text?.join(' ')} /> :
                      <IconTextEntry key={index} icon={entry.icon} text={entry.text} />
                    ))}
                  </div>
                </div>
              </div>
            </div>
            <QuestionDetailsModal
              visible={isModalVisible}
              onClose={handleCloseModal}
              data={modalData}
            />
          </div>
      }
    </div>
  );
};

export default ExpiredJobDetails;
