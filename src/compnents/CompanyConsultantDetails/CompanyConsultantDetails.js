import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { Button, notification, Rate } from 'antd';
import { CheckCircleOutlined } from '@ant-design/icons';
import { BlackColor, GrayOpacity, OrangeColor, WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { horizAroundAlignCenterColumn, horizVerFlex } from "../../General/Styles/general styles";
import { companyConsultantDetailsAction, companyConsultantDetailsRefreshPage } from "../../actions/CompanyConsultantDetailsAction";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import './CompanyConsultantDetails.css';
import { useTranslation } from "react-i18next";
import AdvisorSelectingModal from "../../General/Reusable Componenets/AdvisorSelectingModal/AdvisorSelectingModal";
import { companyReferAction, companyReferFailure, companyReferRefreshPage } from "../../actions/CompanyReferAction";
import { advisorsAction, allAdvisorsAction } from "../../actions/AdvisorsAction";

const CompanyConsultantDetails = () => {
    const { index } = useParams();
    const dispatch = useDispatch();
    const errors = useSelector((state) => state.companyConsultantDetailsReducer.errors);
    const isLoading = useSelector((state) => state.companyConsultantDetailsReducer.isLoading);
    const messages1 = useSelector((state) => state.companyConsultantDetailsReducer.data);
    const advisors = useSelector((state) => state.adivsorsReducer.data);
    const isReferLoading = useSelector((state) => state.companyReferReducer.isLoading);
    const referErrors = useSelector((state) => state.companyReferReducer.errors);
    const referDone = useSelector((state) => state.companyReferReducer.done);
    const advisorsLoading = useSelector((state) => state.adivsorsReducer.isLoading);
    const { t } = useTranslation();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleOk = (advisorId) => {
        if (advisorId) {
            dispatch(companyReferAction(advisorId, index));
        } else {
            dispatch(companyReferFailure({ message: t('select_advisor') }));
        }
        console.log(`the id of the advisor I got from the modal is ${advisorId}`);
        setIsModalVisible(false);
    };

    useEffect(() => {
        dispatch(companyConsultantDetailsAction(index));
        dispatch(allAdvisorsAction());
    }, []);

    useEffect(() => {
        console.log(`the data consultations is ${JSON.stringify(messages1)}`);
    }, [messages1]);

    useEffect(() => {
        if (errors) {
            console.log(`the errors inside detailed consultants ${JSON.stringify(errors.message)}`);
            errors && errors.message && notification.error({
                message: 'Error',
                description: errors.message,
            });
            dispatch(companyConsultantDetailsRefreshPage());
        }
    }, [errors.message]);

    return (
        <div>
            {isLoading ? <Spinner /> : (
                <div style={{ width: '100vw' }} className={`${horizVerFlex}`}>
                    <div style={{ width: '25vw', height: '100vh', backgroundColor: lightGreenPrimary }} className={`${horizAroundAlignCenterColumn}`}>
                        <div className={`h-50 ${horizVerFlex}`}>
                            <img src="/images/the light green 2/People search-amico.png" style={{ width: '80%', height: '65%' }} alt="Consultant" />
                        </div>
                        <div className={`h-50 ${horizVerFlex}`}>
                            <div className="w-100 h-100 d-flex justify-content-center flex-column">
                                <div>
                                    <CheckCircleOutlined style={{ fontSize: '13px', marginRight: '10px', color: WhiteColor }} />
                                    <span className="text-white">{messages1?.company_name}</span>
                                </div>
                                <div>
                                    <CheckCircleOutlined style={{ fontSize: '13px', marginRight: '10px', color: WhiteColor }} />
                                    <span className="text-white">{messages1?.topic}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style={{ width: '75vw', height: '100vh', backgroundColor: GrayOpacity }}>
                        <div className={`${horizVerFlex}`} style={{ height: '100%', width: '100%', color: BlackColor }}>
                            <div className="w-100 h-100 d-flex align-items-center mx-2" style={{
                                overflowY: 'auto', display: 'flex', flexDirection: 'column-reverse', scrollbarWidth: 'none'
                            }}>
                                {messages1 ? (
                                    <React.Fragment key={messages1.id}>
                                        <div className="w-100 d-flex justify-content-start m-3 flex-column" style={{ marginBottom: '12px' }}>
                                            <div style={{ marginBottom: '15px' }}>
                                                <strong>Request Date:</strong> {messages1.request_date}
                                            </div>
                                            <div style={{ marginBottom: '15px' }}>
                                                <strong>Company Email:</strong> {messages1.company_email}
                                            </div>
                                            <div className="w-75 text-center p-2" style={{
                                                backgroundColor: WhiteColor, borderRadius: '20px',
                                                border: `2px solid ${lightGreenPrimary}`
                                            }}>
                                                {messages1.user_message}
                                            </div>
                                        </div>
                                        {messages1.advisor_reply && (
                                            <div className="w-100 d-flex justify-content-end m-3">
                                                <div className="w-75 text-center p-2" style={{ backgroundColor: lightGreenPrimary, borderRadius: '20px' }}>
                                                    {messages1.advisor_reply}
                                                </div>
                                            </div>
                                        )}
                                        <Button
                                            key="ok"
                                            type="primary"
                                            onClick={showModal}
                                            style={{ background: OrangeColor, color: WhiteColor }}
                                        >
                                            {t('referCompany')}
                                        </Button>
                                    </React.Fragment>
                                ) : <div>There is no data here</div>}
                            </div>
                        </div>
                    </div>
                    <AdvisorSelectingModal
                        visible={isModalVisible}
                        onCancel={handleCancel}
                        onOk={handleOk}
                        advisors={advisors}
                        isLoading={advisorsLoading}
                        errors={referErrors}
                        isReferLoading={isReferLoading}
                        done={referDone}
                        refreshErrors={() => dispatch(companyReferRefreshPage())}
                        topic={messages1.topic}
                    />
                </div>
            )}
        </div>
    );
};

export default CompanyConsultantDetails;
