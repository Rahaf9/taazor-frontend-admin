import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { horizVerFlex } from "../../General/Styles/general styles";
import { addCoordinatorTitleStyle, onErrorText } from "../../General/Styles/texts";
import './AddAdvisor.css'
import { EditOutlined, UserOutlined, LockOutlined } from '@ant-design/icons';
import { Button, Form, Input, Select, notification } from "antd";
import { BlackColor, OrangeColor, WhiteColor } from "../../General/Styles/colors";
import { useDispatch, useSelector } from "react-redux";
import LoadingBar from "../../General/Reusable Componenets/LoadingBar/LoadingBar";
import { jobTitlesData } from "../../General/Styles/DataSets";
import { addAdvisorAction, addAdvisorFailure, addAdvisorRefreshPage } from "../../actions/AddAdvisorAdction";
import { ROUTES } from "../../General/Routes/Route";
import { useNavigate } from "react-router-dom";
import { arabicTopics, englishTopics } from "../../FakeData";

const AddAdvisor = () => {

    const { t, i18n } = useTranslation();
    const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl'
    const { Option } = Select;

    const placeholders = [t('enter_name'), t('enter_email'), t('enter_password')];
    const keys = ["name", "email", "password", "topics"];

    const placeholderIcons = [
        <EditOutlined style={{ fontSize: '20px', color: BlackColor }} />, // For 'Enter Name'
        <UserOutlined style={{ fontSize: '20px', color: BlackColor }} />, // For 'Enter Email'
        <LockOutlined style={{ fontSize: '20px', color: BlackColor }} /> // For 'Enter Password'
    ];

    const [formData, setFormData] = useState(Array(placeholders.length).fill(''));
    const [mappedFormData, setMappedFormData] = useState({});
    const dispatch = useDispatch();
    const done = useSelector(state => state.addAdvisorsReducer.done);
    const isLoading = useSelector(state => state.addAdvisorsReducer.isLoading);
    const errors = useSelector(state => state.addAdvisorsReducer.errors);
    const navigate = useNavigate();

    useEffect(() => {
        console.log(`the errors inside register ${JSON.stringify(errors.message)}`)
        errors && errors.message && notification.error({
            message: t('Error'),
            description: errors.message,
        })
        dispatch(addAdvisorRefreshPage())
    }, [errors.message]);

    useEffect(() => {
        if (done) {
            notification.success({
                message: t('addedSuccess'),
                description: t('addedSuccess'),
            })
            navigate(`${ROUTES.SideBar.path}${ROUTES.Advisors.path}`)
            dispatch(addAdvisorRefreshPage())
        }
    }, [done]);

    const handleChange = (index, value) => {
        const newData = [...formData];
        newData[index] = value;
        setFormData(newData);
        let key = keys[index];

        setMappedFormData(prevData => ({
            ...prevData,
            [key]: value
        }));

        console.log(mappedFormData)
    };

    const handleSubmit = () => {
        // Validate form data
        const { name, email, password, topics } = mappedFormData;

        console.log(`the formData in adding coordinator is ${JSON.stringify(mappedFormData)}`)
        if (!name) {
            console.log('name field is required', JSON.stringify(formData));
            dispatch(addAdvisorFailure({ 'name': t('nameFieldRequired') }))
            return;
        } else if (!email) {
            console.log('email field is required', JSON.stringify(formData));
            dispatch(addAdvisorFailure({ 'email': t('emailFieldRequired') }))
            return;
        } else if (!/\S+@\S+\.\S+/.test(email)) {
            console.log('Invalid email format');
            dispatch(addAdvisorFailure({ 'email': t('invalid_email') }));
            return;
        } else if (!password) {
            console.log('Password field is required', JSON.stringify(formData));
            dispatch(addAdvisorFailure({ 'password': t('password_required') }));
            return;
        } else if (password.length < 8) {
            console.log('Password must be at least 8 characters long');
            dispatch(addAdvisorFailure({ 'password': t('password_length') }));
            return;
        } else if (!topics) {
            console.log('Roles name field is required', JSON.stringify(formData));
            dispatch(addAdvisorFailure({ 'topics': t('topicsFieldRequired') }))
            return;
        }
        // here is the dispatch of success to delete all errors
        // Form data is valid, perform further actions or submit the form
        dispatch(addAdvisorAction(mappedFormData))
        console.log('Form data in add advisor:', JSON.stringify(formData));
        // navigate(ROUTES.AddJobQuestions.path, { state: formData })
        // navigate(`${ROUTES.CompleteInfo.path}`)
    }

    return (
        <div style={{ direction: direction }}>
            <div className={`AddCoordinatorContainer`} >
                <div className={`AddCoordinatorLeft`}>
                    <div className={`innerLeftSide`}>
                        <div className={`${horizVerFlex}`}>
                            <img src='/images/the light green 2/Add User-rafiki.png'>
                            </img>
                        </div>
                        <div style={addCoordinatorTitleStyle} className="text-center">{t('add_advisor')}</div>
                    </div>
                </div>
                <div className={`AddCoordinatorRight`}>
                    <div className={`innerRightSide`}>
                        {placeholders.map((placeholder, index) => (
                            <div className={`w-75 ${horizVerFlex}`}>
                                <Form.Item key={index} className='w-100 d-block'>
                                    <Input style={{ background: `${WhiteColor}`, color: BlackColor }}
                                        value={formData[index]}
                                        onChange={(e) => handleChange(index, e.target.value)}
                                        placeholder={placeholder}
                                        suffix={placeholderIcons[index]}
                                    />
                                    {
                                        errors && JSON.stringify(Object.keys(errors)[0]) == JSON.stringify(keys[index]) &&
                                        <span style={onErrorText}>{errors[Object.keys(errors)[0]]}</span>
                                    }
                                </Form.Item>
                            </div>
                        ))}
                        <Select
                            placeholder={t('select_topic')} // Placeholder text
                            onChange={(values) => handleChange(3, values)}
                            style={{ width: 200 }}
                        >
                            {i18n.language === 'ar' ? arabicTopics.map((option, index) => (
                                <Option key={index} value={option.value}>{option.label}</Option>
                            )) : englishTopics.map((option, index) => (
                                <Option key={index} value={option.value}>{option.label}</Option>
                            ))}
                        </Select>

                        {
                            errors && JSON.stringify(Object.keys(errors)[0]) == JSON.stringify(keys[3]) &&
                            <span style={onErrorText}>{errors[Object.keys(errors)[0]]}</span>
                        }
                        <Button style={{ background: OrangeColor, color: WhiteColor, marginTop: '20px', }} onClick={() => handleSubmit()}>
                            {
                                isLoading ? <span style={{
                                    position: 'absolute', top: '35%', left: '20%',
                                    transform: 'translate(-50%, -50%)' // This centers the element perfectly
                                }}><LoadingBar /></span> : t('Submit')
                            }
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default AddAdvisor;