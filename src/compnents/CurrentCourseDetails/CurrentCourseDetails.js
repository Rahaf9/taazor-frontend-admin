import React, { useEffect, useState } from "react";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { EyeOutlined, FormOutlined, DollarOutlined, UsergroupAddOutlined, ClockCircleOutlined, FieldTimeOutlined, FileTextOutlined, CalendarOutlined } from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { CurrentCourseDetailsAction, CurrentCourseDetailsRefreshPage } from "../../actions/CurrentCourseDetailsAction";
import { OrangeColor, RedColor, WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { horizAroundAlignCenterColumn, horizBetween, horizCenterAlignCenterColumn, horizVerFlex, horizVerFlexColumn } from "../../General/Styles/general styles";
import { jobDetailsTitleStyle } from "../../General/Styles/texts";
import StatusEntry from "../../General/StatusEntry/StatusEntry";
import IconTextEntry from "../../General/IconTextEntry/IconTextEntry";
import './CurrentCourseDetails.css'
import { Button, notification } from 'antd'
import { useTranslation } from 'react-i18next';
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import { allAdminActionRefreshPage } from "../../actions/AllAdminActions";
import { deleteCurrentCourseAction } from "../../General/ServerSideApis/ActionsRequestsCreator";
import ConfirmationModal from "../../General/Reusable Componenets/ConfirmationModal/ConfirmationModal";
// import useCheckCompanyStatus from "../../General/CustomHooks/useCheckCompanyStatus ";

const CurrentCourseDetails = () => {
    // useCheckCompanyStatus();
    const { t, i18n } = useTranslation();
    const { course, company } = useSelector((state) => state.currentCourseDetailsReducer.data);
    const details = course;
    const errors = useSelector((state) => state.currentCourseDetailsReducer.errors);
    const isLoading = useSelector((state) => state.currentCourseDetailsReducer.isLoading);
    const deleteErrors = useSelector((state) => state.allActionsAdminReducer.errors);
    const deleteDone = useSelector((state) => state.allActionsAdminReducer.done);
    const { index } = useParams();
    const dispatch = useDispatch();
    const direction = i18n.dir() == 'ar' ? 'ltr' : 'rtl';
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false)
    const navigate = useNavigate()

    useEffect(() => {
        console.log(`this is the index of the current job we want to get its details ${index} and this is the loading value ${isLoading}`);
        dispatch(CurrentCourseDetailsAction(index));
        console.log(`${JSON.stringify(details)}`);
    }, []);

    useEffect(() => {
        console.log(`this is the loading value again ${isLoading}`);
    }, [isLoading]);

    useEffect(() => {
        console.log(`this is the index of the current job we want to get its details ${index}`);
        console.log(`${JSON.stringify(details)}`);
    }, [details]);

    const handleDeleteClick = () => {
        // here you should create the custom action to call delete job api
        dispatch(deleteCurrentCourseAction(index))
        setIsDeleteModalVisible(false)
    }


    useEffect(() => {
        if (errors) {
            console.log(`the errors inside detailed consultants ${JSON.stringify(errors.message)}`)
            errors && errors.message && notification.error({
                message: t('error'),
                description: errors.message,
            })
            dispatch(CurrentCourseDetailsRefreshPage())
        }
    }, [errors.message]);

    useEffect(() => {
        if (deleteErrors) {
            console.log(`Error delete job: ${JSON.stringify(deleteErrors.message)}`);
            deleteErrors && deleteErrors.message && notification.error({
                message: t('error'),
                description: deleteErrors.message,
            });
            dispatch(allAdminActionRefreshPage());
        }
    }, [deleteErrors.message]);

    useEffect(() => {
        if (deleteDone) {
            notification.success({
                message: t('delete_success'),
                description: t('delete_success'),
            });
            navigate(-1);
            dispatch(allAdminActionRefreshPage());
        }
    }, [deleteDone])

    // Define arrays of data for IconTextEntry and StatusEntry
    const iconTextEntries = [
        { icon: <FormOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.topic ?? '' },
        { icon: <DollarOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.price ?? '' },
        { icon: <UsergroupAddOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.number_trainees ?? '' },
        { icon: <ClockCircleOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.duration ?? '' },
        { icon: <FieldTimeOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.start_date ?? '' },
        { icon: <CalendarOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.end_date ?? '' },
        // { icon: <FileTextOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: details?.requires ?? '' },
        // Add more entries as needed
    ];

    const statusEntries = [
        { title: t('jobEnvironment'), text: details?.type ?? '' },
        // { title: t('gender'), text: details?.gender ?? '' },
        { title: t('courseName'), text: details?.name ?? '' },
        { title: t('location'), text: details?.location ?? '' },
        { title: t('aboutCompany'), text: company?.name ?? '' },
        // { title: t('qualificationRequirements'), text: details?.requires ?? '' },
        // Add more entries as needed   
    ];

    return (
        <div>
            {
                isLoading ? <Spinner /> :
                    <div style={{ direction: direction }}>
                        <CustomHeader title={t('currentCourseDetails')} icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
                        {
                            details ? <div style={{ width: '100vw', minHeight: '76vh' }} className={`horizBetweenCourseDetails mt-5`}>
                                <div className={`CourseDetailsLeftSide ${horizVerFlexColumn}`}>
                                    <div className="h-100 w-50 px-4 pt-1 pb-3" style={{ background: lightGreenPrimary, borderRadius: '20px' }}>
                                        <div className={`w-100 h-100 d-flex mt-2 ${horizAroundAlignCenterColumn}`} style={{ height: '100%' }}>
                                            <div style={jobDetailsTitleStyle} className="d-flex w-100 p-2">
                                                {details?.name ?? ''}
                                            </div>
                                            {/* Map over iconTextEntries array to render IconTextEntry components */}
                                            {/* // Take only the first 7 elements from iconTextEntries */}
                                            {iconTextEntries.slice(0, 7).map((entry, index) => (
                                                <IconTextEntry key={index} icon={entry.icon} text={entry.text} />
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <div className={`CourseDetailsRightSide  ${horizVerFlexColumn}`}>
                                    <div className={`w-50 p-4 ${horizCenterAlignCenterColumn}`} style={{ background: lightGreenPrimary, borderRadius: '20px', height: '100%' }}>
                                        <div className={`w-100 d-flex ${horizCenterAlignCenterColumn}`}>
                                            {/* Map over statusEntries array to render StatusEntry components and take from elements 2 to 3*/}
                                            {statusEntries.slice(0, 3).map((entry, index) => (
                                                <div className="m-3 w-100">
                                                    <StatusEntry key={index} title={entry.title} text={entry.text} />
                                                </div>
                                            ))}
                                        </div>
                                        <div className={`w-100 d-flex ${horizCenterAlignCenterColumn}`}>
                                            {/* Map over statusEntries array to render StatusEntry components from the fourth element to the end*/}
                                            {statusEntries.slice(3).map((entry, index) => (
                                                <div className="m-3 w-100">
                                                    <StatusEntry key={index} title={entry.title} text={entry.text} />
                                                </div>
                                            ))}
                                        </div>
                                        <div className={`w-100 d-flex ${horizAroundAlignCenterColumn}`} >
                                            {iconTextEntries.slice(7).map((entry, index) => (
                                                <div className="w-100 m-3">
                                                    <IconTextEntry key={index} icon={entry.icon} text={entry.text} />
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <ConfirmationModal modalContent={t('Are you sure you want to perform this action?')}
                                    visible={isDeleteModalVisible} onOk={handleDeleteClick} onCancel={() => setIsDeleteModalVisible(false)} />
                            </div> : <span style={{ color: RedColor }}>{t('no_details')}</span>
                        }
                        <div className={`${horizVerFlex} mt-2`} onClick={() => { setIsDeleteModalVisible(true) }}>
                            <Button style={{ fontSize: '1.2rem', color: WhiteColor, background: OrangeColor }}
                                className={`${horizVerFlex}`}>
                                {t('delete')}
                            </Button>
                        </div>
                    </div>
            }
        </div>
    );
}

export default CurrentCourseDetails;
