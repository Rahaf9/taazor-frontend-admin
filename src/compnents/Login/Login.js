import React from 'react';
import FormInterface from '../../General/Reusable Componenets/FormInterface/FormInterface';
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import { loginAction, loginFailure, loginRefreshPage } from '../../actions/LoginAction';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ROUTES } from '../../General/Routes/Route';
import { useTranslation } from 'react-i18next';

const Login = () => {
    const { t, i18n } = useTranslation();
    const direction = i18n.language === 'ar' ? 'rtl' : 'ltr';
    const placeholders = [t('enter_email'), t('enter_password')];
    const keys = ["email", "password"];
    const navigate = useNavigate();
    const done = useSelector((state) => state.loginReducer.done);
    const additionalItems = [];

    const roleName = localStorage.getItem('roleName');
    const routPage = !roleName ? `${ROUTES.SideBar.path}${ROUTES.CurrentJob.path}` :
        roleName.includes('admin') ? `${ROUTES.SideBar.path}${ROUTES.Statistics.path}` :
            roleName.includes('user_consultation_coordinator') ? `${ROUTES.SideBar.path}${ROUTES.UserConsulations.path}` :
                roleName.includes('company_consultation_coordinator') ? `${ROUTES.SideBar.path}${ROUTES.CompanyConsulations.path}` :
                    roleName.includes('job_posting_requests_coordinator') ? `${ROUTES.SideBar.path}${ROUTES.WaitingJobs.path}` :
                        roleName.includes('job_requests_coordinator') ? `${ROUTES.SideBar.path}${ROUTES.JobsNotConverted.path}` : 'not'


    const dispatch = useDispatch();

    const handleSubmit = (formData) => {
        const { email, password } = formData;
        if (!email) {
            console.log('Email field is required', JSON.stringify(formData));
            dispatch(loginFailure({ 'email': t('email_required') }));
            return;
        } else if (!/\S+@\S+\.\S+/.test(email)) {
            console.log('Invalid email format');
            dispatch(loginFailure({ 'email': t('invalid_email') }));
            return;
        } else if (!password) {
            console.log('Password field is required', JSON.stringify(formData));
            dispatch(loginFailure({ 'password': t('password_required') }));
            return;
        } else if (password.length < 8) {
            console.log('Password must be at least 8 characters long');
            dispatch(loginFailure({ 'password': t('password_length') }));
            return;
        }

        console.log('Form data in login:', JSON.stringify(formData));
        dispatch(loginAction(formData));
    };

    const placeholderIcons = [<MailOutlined style={{ fontSize: '20px' }} />, <LockOutlined style={{ fontSize: '20px' }} />];

    return (
        <div dir={direction} style={{ direction }}>
            <FormInterface
                title={t('login_page')}
                placeholders={placeholders}
                buttonName={t('login')}
                placeholderIcons={placeholderIcons}
                photo={'/images/the light green 2/Login.png'}
                additionalItems={additionalItems}
                Selectors={{
                    errorSelector: (state) => state.loginReducer.errors,
                    loadingSelector: (state) => state.loginReducer.isLoading,
                    doneSelector: (state) => state.loginReducer.done
                }}
                onSubmit={handleSubmit}
                keys={keys}
                RefreshPage={loginRefreshPage}
                // here we must handle the rolename to know where we must send the user to any interface
                routePage={routPage}
            />
        </div>
    );
};

export default Login;
