// BlockedUsers.js
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { blockedUsersAction } from '../../actions/BlockedUserAction';
import UserInterface from '../../General/UserInterface/UserInterface';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor } from '../../General/Styles/colors';
import { ROUTES } from '../../General/Routes/Route';
import { useTranslation } from 'react-i18next';
import { Button, notification } from 'antd';
import { UserCardButtonStyle } from '../../General/Styles/texts';
import ConfirmationModal from '../../General/Reusable Componenets/ConfirmationModal/ConfirmationModal';
import { unblockUserAction } from '../../General/ServerSideApis/ActionsRequestsCreator';
import { allAdminActionRefreshPage } from '../../actions/AllAdminActions';

const BlockedUsers = () => {
  const dispatch = useDispatch();
  const blockedUserCards = useSelector(state => state.blockedUsersReducer.data);
  const isLoading = useSelector(state => state.blockedUsersReducer.isLoading);
  const errors = useSelector(state => state.allActionsAdminReducer.errors);
  const done = useSelector(state => state.allActionsAdminReducer.done);
  const routePath = ROUTES.BlockedUserDetails.path;
  const { t, i18n } = useTranslation();
  const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl';

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [currentFunction, setCurrentFunction] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(null);

  const handleOk = () => {
    console.log(`handleOk Implemented`);
    if (currentFunction && currentIndex !== null) {
      currentFunction(currentIndex);
      console.log(`Function executed for index ${currentIndex}`);
    }
    setIsModalVisible(false);
  };

  useEffect(() => {
    errors && errors.message && notification.error({
        message: t('Error'),
        description: errors.message
    })
    dispatch(allAdminActionRefreshPage())
}, [errors.message]);

useEffect(() => {
  if (done) {
    notification.success({
      message: t('Success'),
    })
    dispatch(allAdminActionRefreshPage())
  }
}, [done])

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleUnBlock = (index) => {
    console.log(`The user you want to unblock is at index ${index}`);
    dispatch(unblockUserAction(index))
    dispatch(blockedUsersAction())
  };

  useEffect(() => {
    console.log(`The index of the buttons is ${currentIndex}`);
  }, [currentIndex]);

  const additionalButtons = [
    <Button style={UserCardButtonStyle}>UnBlock</Button>,
  ];

  const additionalFunctions = [
    handleUnBlock,
  ];

  const openModal = (buttonIndex, cardIndex) => {
    console.log(`openModal implemented with buttonIndex ${buttonIndex} and cardIndex ${cardIndex}`);
    setCurrentFunction(() => additionalFunctions[buttonIndex]);
    setCurrentIndex(cardIndex);
    setIsModalVisible(true);
  };

  return (
    <div>
      <CustomHeader title={t('ShowBlockedUsers')}
        icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
      <UserInterface userCards={blockedUserCards} fetchUserCards={blockedUsersAction} routePath={routePath} isLoading={isLoading}
        additionalButtons={additionalButtons} openModal={openModal} />
      <ConfirmationModal
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        modalContent={t('Are you sure you want to perform this action?')}
      />
    </div>
  );
};

export default BlockedUsers;
