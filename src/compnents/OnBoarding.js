import React, { useEffect, useState } from "react";
import OnBoardingInterface from "../General/Reusable Componenets/OnBoard/OnBoardInterface";
import { GrayColor, OrangeColor } from "../General/Styles/colors";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../General/Routes/Route";
import { useTranslation } from 'react-i18next';

const OnBoardingComponent = () => {
    const { i18n } = useTranslation();
    const { t } = useTranslation();
    const [colors, setColors] = useState([OrangeColor, GrayColor, GrayColor, GrayColor]);
    const [titles, setTitle] = useState(["Welcome","Job Hiring","Course Training","Show Applicants"])
    const [images] = useState(["/images/my needs for project/Welcome.png",
                               "/images/my needs for project/Hiring.png",
                               "/images/my needs for project/Training.png",
                               "/images/my needs for project/applicants.png"
                              ]);

    const [currentDivIndex, setCurrentDivIndex] = useState(0);
    const navigate = useNavigate()

    // Function to handle click event and update colors array
    const handleDivClick = (index) => {
        const newColors = colors.map((color, i) => {
            return i === index ? OrangeColor : GrayColor;
        });
        setColors(newColors);
        setCurrentDivIndex(index);
    };

    // Function to handle "Next" button click
    const handleNextClick = () => {
        // Check if the current div index is not the last index
        if (currentDivIndex < colors.length - 1) {
            // Invoke handleDivClick for the next div index
            handleDivClick(currentDivIndex + 1);
            // Update the current div index
            setCurrentDivIndex(currentDivIndex + 1);
        } else {
            // Print message when the last div is reached
            console.log("This is the last div");
            navigate(`${ROUTES.Login.path}`)
        }
    };

    useEffect(()=>{
        // i18n.changeLanguage()
    },[])

    return (
        <div>
            {/* Passing down colors, handleDivClick, and handleNextClick functions as props */}
            <OnBoardingInterface colors={colors} handleDivClick={handleDivClick} handleNextClick={handleNextClick} 
            title={titles[currentDivIndex]} image={images[currentDivIndex]}
            description={'It is the time to get the opportunity to share your jobs and free courses and the employees and the trainers you are looking for so join us to be part of us'}/>
        </div>
    );
}

export default OnBoardingComponent;
