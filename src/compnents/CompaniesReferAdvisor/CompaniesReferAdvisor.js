import React, { useState } from "react";
import { Button } from "antd";
import { useParams } from "react-router-dom";
import AdvisorSelectingModal from "../../General/Reusable Componenets/AdvisorSelectingModal/AdvisorSelectingModal";

const CompaniesReferAdvisor = () => {
    const [isModalVisible, setIsModalVisible] = useState(true);
    const [selectedAdvisor, setSelectedAdvisor] = useState(false);
    const { index } = useParams();

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleOk = (advisorId) => {
        setSelectedAdvisor(advisorId);
        setIsModalVisible(false);
    };

    return (
        <div>
            <Button type="primary" onClick={showModal}>
                Select Advisor
            </Button>
            {selectedAdvisor && <p>Selected Advisor ID: {selectedAdvisor.id}</p>}
            <AdvisorSelectingModal
                visible={isModalVisible}
                onCancel={handleCancel}
                onOk={handleOk}
            />
        </div>
    );
};

export default CompaniesReferAdvisor;
