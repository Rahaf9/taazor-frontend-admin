import React, { useEffect, useState } from "react";
import UserDetailsInterface from "../../General/UserDetailsInterface/UserDetailsInterface";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { jobUserDetailsAction } from "../../actions/JobUserDetailsAction";
import { useNavigate, useParams } from "react-router-dom";
import { ROUTES } from "../../General/Routes/Route";
import { useTranslation } from "react-i18next";



const JobUserDetails = () => {

    const { t, i18n } = useTranslation();
    const direction = i18n.dir() == 'ar' ? "ltr" : 'rtl';

    const navigate = useNavigate();

    const additionalContent = () => (
        <div style={{
            display: 'flex', alignItems: 'space-around', justifyContent: 'center',flexDirection:'column', marginTop: '20px', fontSize: '.74rem'
            , fontWeight: 'bold'
        }}>
            <div className="w-100 d-flex justify-content-around align-items-center">
                <div className="w-50">{t('showUserCv')}</div>
                <div>
                    <EyeOutlined onClick={() => { navigate(`${ROUTES.UserCv.path}/${index}`) }} className="w-50" style={{
                        color: 'white', padding: '0px 6px 0px 6px', borderRadius: '10%',
                        marginLeft: '8px', fontSize: '1.7rem', backgroundColor: lightGreenPrimary
                    }} />
                </div>
            </div>
        </div>
    );

    const { index } = useParams();

    return (
        <div style={{direction:direction, minHeight:'100vh'}}>
            <div className="h-25">
            <CustomHeader title={t('show_applicants_details')}
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
            </div>
            <UserDetailsInterface detailsAction={jobUserDetailsAction}
                selectorAction={
                    {
                        'detailsSelector': (state) => state.jobUserDetailsReducer.data,
                        'loadingSelector': (state) => state.jobUserDetailsReducer.isLoading,
                        'doneSelector': (state) => state.jobUserDetailsReducer.done,
                    }
                } index={index}
                additionalContent={additionalContent}
            />
        </div>
    )

}

export default JobUserDetails;