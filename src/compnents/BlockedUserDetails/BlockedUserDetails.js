import React, { useEffect, useState } from "react";
import UserDetailsInterface from "../../General/UserDetailsInterface/UserDetailsInterface";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { useNavigate, useParams } from "react-router-dom";
import { ROUTES } from "../../General/Routes/Route";
import { useTranslation } from "react-i18next";
import { userDetailsAction } from "../../actions/UserDetailsAction";
import { blockedUserDetailsAction } from "../../actions/BlockedUserDetailsAction";
// import useCheckCompanyStatus from "../../General/CustomHooks/useCheckCompanyStatus ";



const BlockedUserDetails = () => {

    // useCheckCompanyStatus();
    const { t, i18n } = useTranslation();
    const direction = i18n.dir() == 'ar' ? "ltr" : 'rtl';

    const navigate = useNavigate();

    const additionalContent = () => (
        <div style={{
            display: 'flex', alignItems: 'space-around', justifyContent: 'center',flexDirection:'column', marginTop: '20px', fontSize: '.74rem'
            , fontWeight: 'bold'
        }}>
            <div className="w-100 d-flex justify-content-around align-items-center">
                <div className="w-50">{t('showUserCv')}</div>
                <div className="d-flex flex-column justify-content-around align-items-center">
                    <EyeOutlined onClick={() => { navigate(`${ROUTES.UserCv.path}/${index}`) }} className="m-1 w-50" style={{
                        color: 'white', padding: '0px 6px 0px 6px', borderRadius: '10%',
                        marginLeft: '8px', fontSize: '1.7rem', backgroundColor: lightGreenPrimary
                    }} />
                    <div className="m-1">
                        Show Cv
                    </div>
                </div>
            </div>
        </div>
    );

    const { index } = useParams();

    return (
        <div style={{direction:direction, minHeight:'100vh'}}>
            <div className="h-25">
            <CustomHeader title={t('show_applicants_details')}
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
            </div>
            <UserDetailsInterface detailsAction={blockedUserDetailsAction}
                selectorAction={
                    {
                        'detailsSelector': (state) => state.blockedUserDetailsReducer.data,
                        'loadingSelector': (state) => state.blockedUserDetailsReducer.isLoading,
                        'doneSelector': (state) => state.blockedUserDetailsReducer.done,
                    }
                } index={index}
                additionalContent={additionalContent}
            />

        </div>
    )

}

export default BlockedUserDetails;