// CourseUsersComponent.js
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { jobUsersAction } from '../../actions/JobUsersAction';  // Assuming you have an action creator for fetching course user cards
import UserInterface from '../../General/UserInterface/UserInterface';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor } from '../../General/Styles/colors';
import { useParams } from 'react-router-dom';
import { ROUTES } from '../../General/Routes/Route';
import JobUserDetails from '../JobUserDetails/JobUserDetails';
import { useTranslation } from 'react-i18next';
import { expiredJobUsersAction } from '../../actions/ExpiredJobUserAction';
// import useCheckCompanyStatus from '../../General/CustomHooks/useCheckCompanyStatus ';

const ExpiredJobUsersComponent = () => {
  // here must be changed to the expired reducer
  const jobUserCards = useSelector(state => state.expiredJobUsersReducer.data);
  // here must be changed to the expired reducer
  const isLoading = useSelector(state => state.expiredJobUsersReducer.isLoading);
  // here must be changed into the route of the expired job details 
  const routePath = ROUTES.ExpiredJobUserDetails.path;
  const { index } = useParams();
  const { t } = useTranslation();
  // useCheckCompanyStatus();

  useEffect(()=>{
    console.log(`the users in job index ${index}`)
    // here i must load the users data into the reducer to use them and display them
    
  },[]);
  
  // here must be changed to the expired action
  return (
    <div>
      {/* here after making api we must sending the index to inform which jon opp we want to show its users */}
      <CustomHeader title={t('show_job_applicants')} 
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />}/>
      <UserInterface userCards={jobUserCards} fetchUserCards={expiredJobUsersAction} routePath={routePath} index={index} isLoading={isLoading}/>
    </div>
  );
};

export default ExpiredJobUsersComponent;
