// CourseUsersComponent.js
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import UserInterface from '../../General/UserInterface/UserInterface';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { EyeOutlined, PlusCircleFilled } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary } from '../../General/Styles/colors';
import { ROUTES } from '../../General/Routes/Route';
import { useTranslation } from 'react-i18next';
import { coordinatorsAction } from '../../actions/CoordinatorsAction';
import CustomFloatButton from '../../General/CustomFloatButton/CustomFloatButton';
import { useNavigate } from 'react-router-dom';
import { UserCardButtonStyle } from '../../General/Styles/texts';
import { Button, notification } from 'antd';
import ConfirmationModal from '../../General/Reusable Componenets/ConfirmationModal/ConfirmationModal';
import { allAdminActionRefreshPage } from '../../actions/AllAdminActions';
import { deleteCoordinatorAction } from '../../General/ServerSideApis/ActionsRequestsCreator';

const Coordanitors = () => {
  const dispatch = useDispatch();
  const coordinatorCards = useSelector(state => state.coordinatorsReducer.data);
  const isLoading = useSelector(state => state.coordinatorsReducer.isLoading);
  const errors = useSelector(state => state.allActionsAdminReducer.errors);
  const done = useSelector(state => state.allActionsAdminReducer.done);
  const navigate = useNavigate();
  // because there is no need for details
  // const routePath = ROUTES.Advisors.path;

  const { t, i18n } = useTranslation();
  const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl'

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [currentFunction, setCurrentFunction] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(null);
  const [translatedCoordinators, setTranslatedCoordinators] = useState([]);

  useEffect(() => {
    errors && errors.message && notification.error({
      message: t('Error'),
      description: errors.message
    })
    dispatch(allAdminActionRefreshPage())
  }, [errors.message]);

  useEffect(() => {
    if (done) {
      notification.success({
        message: t('Success'),
      })
      dispatch(coordinatorsAction())
    }
  }, [done])

  const handleOk = () => {
    console.log(`handleOk Implemented`);
    if (currentFunction && currentIndex !== null) {
      currentFunction(currentIndex);
      console.log(`Function executed for index ${currentIndex}`);
    }
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleUpdate = (index) => {
    console.log(`The coordinator you want to update is at index ${index}`);
    navigate(`${ROUTES.UpdateCorrdinator.path}/${index}`)
  };

  const handleDelete = (index) => {
    console.log(`the coordinator for delete at index ${index}`);
    dispatch(deleteCoordinatorAction(index));
  };

  const additionalButtons = [
    <Button style={UserCardButtonStyle}>Update</Button>,
    <Button style={UserCardButtonStyle}>Delete</Button>
  ];

  const additionalFunctions = [
    handleUpdate,
    handleDelete
  ];

  const openModal = (buttonIndex, cardIndex) => {
    console.log(`openModal implemented with buttonIndex ${buttonIndex} and cardIndex ${cardIndex}`);
    setCurrentFunction(() => additionalFunctions[buttonIndex]);
    setCurrentIndex(cardIndex);
    setIsModalVisible(true);
  };

  // Function to translate role names
  // const translateRoleNames = (coordinator) => {
  //   console.log(`the coordinator in the function is ${JSON.stringify(coordinator)}`)
  //   // console.log(`the coordinator in the function is ${JSON.stringify(coordinator)}`)
  //   if (Array.isArray(coordinator.role_name)) {
  //     coordinator.role_name = coordinator.role_name.map(role => t(`${role}`));
  //   }
  //   return coordinator;
  // };

  // useEffect(() => {
  //   console.log(`the coordinatorCards did not changed and its value is ${JSON.stringify(coordinatorCards)}`)
  //   if (coordinatorCards && coordinatorCards.data && coordinatorCards.data.length > 0) {
  //     console.log(`the coordinatorCards did changed `)
  //     const translatedData = coordinatorCards.data.map(translateRoleNames);
  //     setTranslatedCoordinators(translatedData);
  //   }
  // }, [coordinatorCards.data]);

  // useEffect(() => {
  //   console.log(`the data in TranslatedCoordinators is changed so its value is ${JSON.stringify(translatedCoordinators)}`)
  // }, [translatedCoordinators])

  // Apply translation to each coordinator in coordinatorCards.data
  // const translatedCoordinatorCards = coordinatorCards.data?  coordinatorCards.data.map(translateRoleNames) : [];


  return (
    <div>
      <CustomHeader title={t('Coordinators')}
        icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
      <UserInterface userCards={coordinatorCards} fetchUserCards={coordinatorsAction}
        additionalButtons={additionalButtons} openModal={openModal}
        // routePath={routePath} 
        isLoading={isLoading} />

      <ConfirmationModal
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        modalContent={t('Are you sure you want to perform this action?')}
      />
      <CustomFloatButton floatButtons={<PlusCircleFilled style={{ color: lightGreenPrimary, fontSize: '1.5rem' }}
        onClick={() => {
          navigate(`${ROUTES.AddCorrdinator.path}`)
        }} />} />
    </div>
  );
};

export default Coordanitors;
