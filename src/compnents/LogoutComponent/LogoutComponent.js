import React from "react";
import { horizAroundAlignCenter, horizAroundAlignCenterColumn, horizVerFlex } from "../../General/Styles/general styles";
import { OrangeColor, WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { useTranslation } from "react-i18next";
import { Button } from "antd";
import { useNavigate } from "react-router-dom";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import {
    LogoutOutlined
} from '@ant-design/icons';

const LogoutComponent = () => {

    const { t, i18n } = useTranslation();
    const navigate = useNavigate();

    const handleLogout = () => {
        localStorage.setItem('token', '');
        localStorage.setItem('status', '');
        localStorage.setItem('data', '');
        navigate('/')
      //  unregisterSpecificServiceWorker();
    }

    const handleNot = () => {
        navigate(-1);
    }

    const unregisterSpecificServiceWorker = async () => {
        if ('serviceWorker' in navigator) {
            try {
                // Get all registered service workers
                const registrations = await navigator.serviceWorker.getRegistrations();

                // Iterate through the registrations and find the one with the specific script URL
                for (let registration of registrations) {
                    if (registration.active && registration.active.scriptURL.includes('firebase-messaging-sw2.js')) {
                        await registration.unregister();
                        console.log('Service Worker with script firebase-messaging-sw2.js unregistered successfully');
                    }
                }
            } catch (error) {
                console.error('Error during service worker unregistration:', error);
            }
        } else {
            console.error("This browser doesn't support service workers.");
        }
    };


    return (
        <>
            <CustomHeader title={t('logout')}
                icon={<LogoutOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
            <div className={`${horizVerFlex}`} style={{ minWidth: '100%', minHeight: '50%' }}>
                <div style={{ width: '70%', height: '50%', background: lightGreenPrimary, borderRadius: '12px' }}
                    className={`${horizAroundAlignCenterColumn} p-5`}>
                    <div className={`${horizVerFlex} p-3`}>
                        {t('Are you sure you want to perform this action?')}
                    </div>
                    <div className={`w-100 h-100 ${horizAroundAlignCenter} p-3`}>
                        <Button key="cancel" onClick={handleNot} style={{ background: OrangeColor, color: WhiteColor }}>
                            {t('no')}
                        </Button>
                        <Button key="ok" type="primary" onClick={handleLogout} style={{ background: OrangeColor, color: WhiteColor }}>
                            {t('ok')}
                        </Button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default LogoutComponent;