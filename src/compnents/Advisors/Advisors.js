// CourseUsersComponent.js
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import UserInterface from '../../General/UserInterface/UserInterface';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { EyeOutlined, PlusCircleFilled } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary } from '../../General/Styles/colors';
import { ROUTES } from '../../General/Routes/Route';
import { t } from 'i18next';
import { advisorsAction } from '../../actions/AdvisorsAction';
import CustomFloatButton from '../../General/CustomFloatButton/CustomFloatButton';
import { useNavigate } from 'react-router-dom';
import { UserCardButtonStyle } from '../../General/Styles/texts';
import { Button, notification } from 'antd';
import ConfirmationModal from '../../General/Reusable Componenets/ConfirmationModal/ConfirmationModal';
import { deleteAdvisorAction } from '../../General/ServerSideApis/ActionsRequestsCreator';
import { allAdminActionRefreshPage } from '../../actions/AllAdminActions';

const Advisors = () => {
  const dispatch = useDispatch();
  const AdvisorsCards = useSelector(state => state.adivsorsReducer.data);
  const isLoading = useSelector(state => state.adivsorsReducer.isLoading);
  const errors = useSelector(state => state.allActionsAdminReducer.errors);
  const done = useSelector(state => state.allActionsAdminReducer.done);
  const navigate = useNavigate();
  
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [currentFunction, setCurrentFunction] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(null);

  // because there is no need for details
  const routePath = `${ROUTES.AdvisorConsultionsReceived.path}`;

  useEffect(() => {
    errors && errors.message && notification.error({
      message: t('Error'),
      description: errors.message
    })
    dispatch(allAdminActionRefreshPage())
  }, [errors.message]);

  useEffect(() => {
    if (done) {
      notification.success({
        message: t('Success'),
      })
      dispatch(advisorsAction())
    }
  }, [done])

  const handleOk = () => {
    console.log(`handleOk Implemented`);
    if (currentFunction && currentIndex !== null) {
      currentFunction(currentIndex);
      console.log(`Function executed for index ${currentIndex}`);
    }
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleUpdate = (index) => {
    console.log(`The advisor you want to update is at index ${index}`);
    navigate(`${ROUTES.UpdateAdvisor.path}/${index}`)
  };

  const handleDelete = (index) => {
    console.log(`the advisor for delete at index ${index}`);
    dispatch(deleteAdvisorAction(index));
  };

  const handleDetails = (index) => {
    console.log(`the advisor for details at index ${index}`);
    navigate(`${ROUTES.AdvisorConsultionsReceived.path}/${index}`)
  };

  useEffect(() => {
    console.log(`the advisors cards changed and its ${JSON.stringify(AdvisorsCards)}`)
  }, [AdvisorsCards])

  const additionalButtons = [
    <Button style={UserCardButtonStyle}>Update</Button>,
    <Button style={UserCardButtonStyle}>Delete</Button>,
    // <Button style={UserCardButtonStyle}>Details</Button>
  ];

  const additionalFunctions = [
    handleUpdate,
    handleDelete,
    // handleDetails
  ];

  const openModal = (buttonIndex, cardIndex) => {
    console.log(`openModal implemented with buttonIndex ${buttonIndex} and cardIndex ${cardIndex}`);
    setCurrentFunction(() => additionalFunctions[buttonIndex]);
    setCurrentIndex(cardIndex);
    setIsModalVisible(true);
  };

  return (
    <div>
      <CustomHeader title={t('Advisors')}
        icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
      <UserInterface userCards={AdvisorsCards} fetchUserCards={advisorsAction} index={1} routePath={routePath}
        additionalButtons={additionalButtons} openModal={openModal}
        // routePath={routePath} 
        isLoading={isLoading} />
      <ConfirmationModal
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        modalContent={t('Are you sure you want to perform this action?')}
      />
      <CustomFloatButton floatButtons={<PlusCircleFilled style={{ color: lightGreenPrimary, fontSize: '1.5rem' }}
        onClick={() => {
          navigate(`${ROUTES.AddAdvisor.path}`)
        }} />} />
    </div>
  );
};

export default Advisors;
