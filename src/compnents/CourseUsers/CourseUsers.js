// CourseUsersComponent.js
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { courseUsersAction } from '../../actions/CourseUsersAction';  // Assuming you have an action creator for fetching course user cards
import UserInterface from '../../General/UserInterface/UserInterface';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor } from '../../General/Styles/colors';
import { ROUTES } from '../../General/Routes/Route';
import { useLocation, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
// import useCheckCompanyStatus from '../../General/CustomHooks/useCheckCompanyStatus ';

const CourseUsersComponent = () => {
  // useCheckCompanyStatus();
  const courseUserCards = useSelector(state => state.courseUsersReducer.data);
  const isLoading = useSelector(state => state.courseUsersReducer.isLoading);
  const routePath = ROUTES.CourseUserDetails.path;
  const { index } = useParams();
  const { t } = useTranslation();

  useEffect(()=>{
    console.log(`the users in course index ${index}`)
  },[]);

  return (
    <div>
      <CustomHeader title={t('show_course_applicants')} 
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />}/>
      <UserInterface userCards={courseUserCards} fetchUserCards={courseUsersAction} routePath={routePath} index = {index} isLoading={isLoading}/>
    </div>
  );
};

export default CourseUsersComponent;
