// CourseUsersComponent.js
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import UserInterface from '../../General/UserInterface/UserInterface';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor } from '../../General/Styles/colors';
import { useParams } from 'react-router-dom';
import { ROUTES } from '../../General/Routes/Route';
import { useTranslation } from 'react-i18next';
import { expiredJobAcceptableUsersUsersAction } from '../../actions/ExpiredJobAcceptableUsersAction';
// import useCheckCompanyStatus from '../../General/CustomHooks/useCheckCompanyStatus ';

const ExpiredJobAcceptableUsers = () => {
  // useCheckCompanyStatus();
  const jobUserCards = useSelector(state => state.expiredJobAcceptableUsersReducer.data);
  const isLoading = useSelector(state => state.expiredJobAcceptableUsersReducer.isLoading);
  const routePath = ROUTES.ExpiredJobAcceptableUserDetails.path;
  const { index } = useParams();
  const { t } = useTranslation();

  useEffect(()=>{
    console.log(`the users in job index ${index}`)
    // here i must load the users data into the reducer to use them and display them
    
  },[]);
  

  return (
    <div>
      {/* here after making api we must sending the index to inform which jon opp we want to show its users */}
      <CustomHeader title={t('show_job_applicants')} 
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />}/>
      <UserInterface userCards={jobUserCards} fetchUserCards={expiredJobAcceptableUsersUsersAction} routePath={routePath} index={index} isLoading={isLoading}/>
    </div>
  );
};

export default ExpiredJobAcceptableUsers;
