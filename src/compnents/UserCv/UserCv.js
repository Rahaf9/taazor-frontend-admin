import React, { useEffect } from "react";
import './UserCv.css';
import {
    CheckCircleOutlined, BookFilled, MailFilled, IdcardFilled, EnvironmentOutlined,
    GlobalOutlined, FlagFilled, CalendarOutlined, InfoCircleFilled, ManOutlined, CarFilled, PhoneFilled
} from '@ant-design/icons';
import IconTextEntry from "../../General/IconTextEntry/IconTextEntry";
import { WhiteColor } from "../../General/Styles/colors";
import UserCvElement from "../../General/UserCvElement/UserCvElement";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { userCvAction, userCvRefreshPage } from "../../actions/UserCvAction";
import { notification } from "antd";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";

const UserCv = () => {
    const dispatch = useDispatch();
    const errors = useSelector(state => state.userCvReducer.errors);
    const data = useSelector(state => state.userCvReducer.data);
    const isLoading = useSelector(state => state.userCvReducer.isLoading);

    const { index } = useParams();

    useEffect(() => {
        dispatch(userCvAction(index));
    }, [dispatch, index]);

    useEffect(() => {
        if (errors && errors.message) {
            notification.error({
                message: 'Error',
                description: errors.message
            });
            dispatch(userCvRefreshPage());
        }
    }, [errors, dispatch]);

    const { t, i18n } = useTranslation();
    const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl';

    if (isLoading || !data) {
        return <Spinner />
    }

    const iconTextEntries = [
        { icon: <PhoneFilled style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.phone ?? '' },
        { icon: <EnvironmentOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.address ?? '' },
        { icon: <FlagFilled style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.nationality ?? '' },
        { icon: <MailFilled style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.email ?? '' },
        { icon: <IdcardFilled style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.education ?? '' },
        { icon: <ManOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.gender ?? '' },
        { icon: <BookFilled style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.education ?? '' },
        { icon: <GlobalOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: Array.isArray(data.user_cv?.languages) ? data.user_cv?.languages?.join(', ') : data.user_cv?.languages ? data.user_cv?.languages : '' },
        { icon: <CheckCircleOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.military_status ?? '' },
        { icon: <CalendarOutlined style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.birthday ?? '' },
        { icon: <CarFilled style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.driving_license ? 'Yes' : 'No' },
        { icon: <InfoCircleFilled style={{ color: WhiteColor, fontSize: '17px' }} />, text: data.user_cv?.job_field ?? '' },
    ];

    const experiences = data.experiences?.map((exp, index) => (
        <UserCvElement key={index} title={`${exp.job_title} at ${exp.company_name}`} body={`${exp.details} (${exp.start_date} - ${exp.end_date})`} />
    )) || [];

    const courses = data.cv_courses?.map((course, index) => (
        <div className="TrainingElement" key={index}>
            <div>{course.name} at {course.source} ({course.duration})</div>
            <div className="TrainingElementImageContainer">
                <img src={course.image} alt={course.name} style={{ width: '100%', height: '100%' }} />
            </div>
        </div>
    )) || [];

    const references = data.references?.map((ref, index) => (
        <UserCvElement key={index} title={ref.name} body={`${ref.employment}, ${ref.email}, ${ref.phone}`} />
    )) || [];

    const links = data.links?.map((link, index) => (
        <UserCvElement key={index} title={link.title} body={<a href={link.link} target="_blank" rel="noopener noreferrer">{link.link}</a>} />
    )) || [];

    return (
        <>
            {
                data.user_cv ? <div style={{ direction: direction }}>
                    <div className="userCvHeader">
                        <div className="userCvLeftHeader">
                            <img className="fixed-size-img" src={data.user_cv?.image} alt={`${data.first_name} ${data.last_name}`} />
                        </div>
                        <div className="userCvRightHeader">
                            <div className="halferWidth">
                                {`${data.first_name} ${data.last_name}`}
                            </div>
                        </div>
                    </div>
                    <div className="userCvBody">
                        <div className="userCvLeftBody">
                            {iconTextEntries.map((entry, index) => (
                                <div className="bodyCvElemnts" key={index}>
                                    <IconTextEntry icon={entry.icon} text={entry.text} />
                                </div>
                            ))}
                        </div>
                        <div className="userCvRightBody">
                            {data.user_cv?.skills && <UserCvElement title={t('skills')} body={data.user_cv?.skills} />}
                            {experiences.length > 0 && <UserCvElement title={t('experinces')} body={experiences} />}
                            {courses.length > 0 && <UserCvElement title={t('traning')} body={courses} />}
                            {references.length > 0 && <UserCvElement title={t('refernces')} body={references} />}
                            {links.length > 0 && <UserCvElement title={t('links')} body={links} />}
                        </div>
                    </div>
                </div> : <div className='d-flex justify-content-center align-items-center text-danger fw-bold mt-5'>
                    {t('cv_empty')}
                </div>
            }
        </>
    );
};

export default UserCv;
