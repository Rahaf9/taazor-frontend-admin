import { EyeOutlined } from "@ant-design/icons";
import React, { useEffect } from "react";
import { WhiteColor } from "../../General/Styles/colors";
import './Statistics.css';
import { useTranslation } from "react-i18next";
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { useDispatch, useSelector } from "react-redux";
import { setStatisticsAction } from "../../actions/StatisticAction";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import PieChartComponent from "../../General/Reusable Componenets/PieChartComponent/PieChartComponent ";
import ProgressBarComponent from "../../General/Reusable Componenets/ProgressBarComponent/ProgressBarComponent";
import BarChartComponent from "../../General/Reusable Componenets/BarChartComponent/BarChartComponent";

const Statistics = () => {
    const data = useSelector(state => state.statisticReducer.data);
    const isLoading = useSelector(state => state.statisticReducer.isLoading);
    const dispatch = useDispatch();
    const { t, i18n } = useTranslation();
    const direction = i18n.language === 'ar' ? 'ltr' : 'ltr'

    useEffect(() => {
        dispatch(setStatisticsAction());
    }, []);

    // Prepare data for charts
    const pieData = data ? [
        { name: t('Users Benefiting'), value: parseFloat(data.percentage_users_benefiting) },
        { name: t('Remaining'), value: 100 - parseFloat(data.percentage_users_benefiting) }
    ] : [];

    const topTopicsData = data?.top_topics?.map(item => ({ name: item.topic, percent: parseFloat(item.percent) ,identifier:'percent' })) || [];
    const topCompaniesData = data?.top_companies?.map(item => ({ name: item.company, percent: parseFloat(item.percent) })) || [];
    const jobsByLocationData = data?.jobsByLocation?.map(item => ({ name: item.location, value: item.total })) || [];
    const coursesByLocationData = data?.coursesByLocation?.map(item => ({ name: item.location, value: item.total })) || [];
    const topJobTopicsData = data?.topJobTopics?.map(item => ({ name: item.topic, value: item.total })) || [];
    const topCourseTopicsData = data?.topCourseTopics?.map(item => ({ name: item.topic, value: item.total })) || [];
    const jobsOnlineOfflineRatioData = Object.entries(data?.jobsOnlineOfflineRatio || {}).map(([name, value]) => ({ name, value: parseFloat(value) })) || [];
    const coursesOnlineOfflineRatioData = Object.entries(data?.coursesOnlineOfflineRatio || {}).map(([name, value]) => ({ name, value: parseFloat(value) })) || [];
    const topAdvisorsData = data?.topAdvisors?.map(item => ({ name: item.name, percent: item.rating, identifier:'rate' })) || [];
    const companyTypeRatiosData = Object.entries(data?.companyTypeRatios || {}).map(([name, value]) => ({ name, value: parseFloat(value) })) || [];
    const topSurveyCompaniesData = data?.topSurveyCompanies?.map(item => ({ name: item.name, percent: item.total })) || [];
    const topRespondedSurveysData = data?.topRespondedSurveys?.map(item => ({ name: item.title, percent: item.responses })) || [];
    const mostFollowedCompaniesData = data?.mostFollowedCompanies?.map(item => ({ name: item.name, percent: item.followers })) || [];

    return (
        <div style={{direction:direction}}>
            <CustomHeader title={t('Statistics')}
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
            {
                // topTopicsData topAdvisorsData topSurveyCompaniesData mostFollowedCompaniesData topRespondedSurveysData
                isLoading ? <Spinner /> :
                    <div className="StatisticsContainer">
                        <PieChartComponent data={pieData} title={t('circle-percentage')} />
                        <ProgressBarComponent data={topTopicsData} title={t('top-5-topics')} valueKey="percent" identifier="percent" />
                        <BarChartComponent data={topCompaniesData} title={t('top-5-companies')} />
                        <PieChartComponent data={jobsByLocationData} title={t('Jobs by Location')} />
                        <PieChartComponent data={coursesByLocationData} title={t('Courses by Location')} />
                        <PieChartComponent data={topJobTopicsData} title={t('Top Job Topics')} />
                        <PieChartComponent data={topCourseTopicsData} title={t('Top Course Topics')} />
                        <PieChartComponent data={jobsOnlineOfflineRatioData} title={t('Jobs Online/Offline Ratio')} />
                        <PieChartComponent data={coursesOnlineOfflineRatioData} title={t('Courses Online/Offline Ratio')} />
                        <ProgressBarComponent data={topAdvisorsData} title={t('Top Advisors')} valueKey="percent" labelKey="name" identifier="rate"/>
                        <PieChartComponent data={companyTypeRatiosData} title={t('Company Type Ratios')} />
                        <BarChartComponent data={topSurveyCompaniesData} title={t('Top Survey Companies')} />
                        <BarChartComponent data={topRespondedSurveysData} title={t('Top Responded Surveys')} />
                        <BarChartComponent data={mostFollowedCompaniesData} title={t('Most Followed Companies')} />
                    </div>
            }
        </div>
    );
}

export default Statistics;
