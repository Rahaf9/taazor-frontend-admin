import React from 'react';
import { useTranslation } from 'react-i18next';
import { GlobalOutlined } from '@ant-design/icons';
import './LanguageSwitcher.css';
import { WhiteColor, lightGreenPrimary } from '../../General/Styles/colors';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';

const LanguageSwitcher = () => {
  const { t, i18n } = useTranslation();

  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
    localStorage.setItem('lang', lng);
    // Example usage: Set the language to 'en' (English)
    setLanguageCookie('en');
  };

  function setLanguageCookie(language) {
    // Set the language cookie with a specified value and an expiration date
    const expirationDays = 30; // The cookie will expire in 30 days
    const date = new Date();
    date.setTime(date.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
    const expires = "expires=" + date.toUTCString();
    document.cookie = "appLang=" + language + ";" + expires + ";path=/";
  }


  return (
    <>
      <CustomHeader title={t('changeLang')}
        icon={<GlobalOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
      <div className="language-switcher">
        <div className="card" style={{ background: lightGreenPrimary }} onClick={() => changeLanguage('en')}>
          <GlobalOutlined style={{ fontSize: '50px', marginBottom: '10px', color: WhiteColor }} />
          <p style={{ color: WhiteColor }}>{t('English')}</p>
        </div>
        <div className="card" style={{ background: lightGreenPrimary }} onClick={() => changeLanguage('ar')}>
          <GlobalOutlined style={{ fontSize: '50px', marginBottom: '10px', color: WhiteColor }} />
          <p style={{ color: WhiteColor }}>{t('Arabic')}</p>
        </div>
      </div>
    </>
  );
};

export default LanguageSwitcher;
