import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { horizVerFlex } from "../../General/Styles/general styles";
import { addCoordinatorTitleStyle, onErrorText } from "../../General/Styles/texts";
import './UpdateCoordinator.css'
import { EditOutlined, UserOutlined, LockOutlined } from '@ant-design/icons';
import { Button, Form, Input, Select, notification } from "antd";
import { BlackColor, OrangeColor, WhiteColor } from "../../General/Styles/colors";
import { addCoordinatorRefreshPage } from "../../actions/AddCoordinatorAction";
import { useDispatch, useSelector } from "react-redux";
import LoadingBar from "../../General/Reusable Componenets/LoadingBar/LoadingBar";
import { allAdminActionFailure, allAdminActionRefreshPage } from "../../actions/AllAdminActions";
import { getCoordinatorDetailsAction, updateCoordinatorsAction } from "../../General/ServerSideApis/ActionsRequestsCreator";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import { useParams } from "react-router-dom";

const UpdateCoordinator = () => {

    const { index } = useParams();
    const { t, i18n } = useTranslation();
    const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl';
    const { Option } = Select;

    const placeholders = [t('enter_name'), t('enter_email'), t('enter_password')];
    const keys = ["name", "email", "password", "role_name"];

    const placeholderIcons = [
        <EditOutlined style={{ fontSize: '20px', color: BlackColor }} />, // For 'Enter Name'
        <UserOutlined style={{ fontSize: '20px', color: BlackColor }} />, // For 'Enter Email'
        <LockOutlined style={{ fontSize: '20px', color: BlackColor }} /> // For 'Enter Password'
    ];

    const [formData, setFormData] = useState(Array(placeholders.length).fill(''));
    const [mappedFormData, setMappedFormData] = useState({});
    const dispatch = useDispatch();
    const done = useSelector(state => state.allActionsAdminReducer.done);
    const isLoading = useSelector(state => state.allActionsAdminReducer.isLoading);
    const errors = useSelector(state => state.allActionsAdminReducer.errors);
    const getIsLoading = useSelector(state => state.allStoreActionsAdminReducer.isLoading);
    const getErrors = useSelector(state => state.allStoreActionsAdminReducer.errors);
    const coordinatorDetails = useSelector(state => state.allStoreActionsAdminReducer.data);

    useEffect(() => {
        console.log(`at the first formData value is ${formData}`)
        dispatch(getCoordinatorDetailsAction(index));
    }, []);

    useEffect(() => {
        if (coordinatorDetails) {
            const { name, email, role_name } = coordinatorDetails;
            setFormData([name, email, '', role_name]); // Password is left empty for security reasons
            setMappedFormData({ name, email, password: '', role_name });
        }
    }, [coordinatorDetails]);

    useEffect(() => {
        if (errors && errors.message) {
            notification.error({
                message: t('Error'),
                description: errors.message,
            });
        }
        dispatch(allAdminActionRefreshPage());
    }, [errors.message]);

    useEffect(() => {
        if (done) {
            notification.success({
                message: t('addedSuccess'),
                description: t('addedSuccess'),
            });
        }
    }, [done]);

    useEffect(() => {
        console.log(`the formedData is ${JSON.stringify(formData)}`)
    }, [formData]);

    const handleChange = (index, value) => {
        const newData = [...formData];
        newData[index] = value;
        setFormData(newData);
        let key = keys[index];

        setMappedFormData(prevData => ({
            ...prevData,
            [key]: value
        }));
    };

    const handleSubmit = () => {
        // Validate form data
        const { name, email, password, role_name } = mappedFormData;

        if (!name) {
            dispatch(allAdminActionFailure({ 'name': t('nameFieldRequired') }));
            return;
        } else if (!email) {
            dispatch(allAdminActionFailure({ 'email': t('emailFieldRequired') }));
            return;
        } else if (!/\S+@\S+\.\S+/.test(email)) {
            dispatch(allAdminActionFailure({ 'email': t('invalid_email') }));
            return;
        } else if (!password) {
            dispatch(allAdminActionFailure({ 'password': t('password_required') }));
            return;
        } else if (password.length < 8) {
            dispatch(allAdminActionFailure({ 'password': t('password_length') }));
            return;
        } else if (!role_name) {
            dispatch(allAdminActionFailure({ 'role_name': t('rolesFieldRequired') }));
            return;
        }

        // Form data is valid, perform further actions or submit the form
        dispatch(updateCoordinatorsAction(index, mappedFormData));
    };

    return (
        <>
            {getIsLoading ? (
                <Spinner />
            ) : (
                <div style={{ direction: direction }}>
                    <div className="AddCoordinatorContainer">
                        <div className="AddCoordinatorLeft">
                            <div className="innerLeftSide">
                                <div className={horizVerFlex}>
                                    <img src='/images/the light green 2/Add User-rafiki.png' alt="Add User" />
                                </div>
                                <div style={addCoordinatorTitleStyle} className="text-center">
                                    {t('update_coordinator')}
                                </div>
                            </div>
                        </div>
                        <div className="AddCoordinatorRight">
                            <div className="innerRightSide">
                                {placeholders.map((placeholder, index) => (
                                    <div className={`w-75 ${horizVerFlex}`} key={index}>
                                        <Form.Item className="w-100 d-block">
                                            <Input
                                                style={{ background: `${WhiteColor}`, color: BlackColor }}
                                                value={formData[index]}
                                                onChange={(e) => handleChange(index, e.target.value)}
                                                placeholder={placeholder}
                                                suffix={placeholderIcons[index]}
                                            />
                                            {errors && JSON.stringify(Object.keys(errors)[0]) === JSON.stringify(keys[index]) && (
                                                <span style={onErrorText}>{errors[Object.keys(errors)[0]]}</span>
                                            )}
                                        </Form.Item>
                                    </div>
                                ))}
                                <Select
                                    mode="multiple"
                                    defaultValue={formData[3] || []}
                                    // placeholder={t('select_roles')}
                                    onChange={(values) => handleChange(3, values)}
                                    style={{ width: 200 }}
                                >
                                    <Option value={'job_requests_coordinator'}>{t('job_requests_coordinator')}</Option>
                                    <Option value='job_posting_requests_coordinator'>{t('job_posting_requests_coordinator')}</Option>
                                    <Option value='user_consultation_coordinator'>{t('user_consultation_coordinator')}</Option>
                                    <Option value='company_consultation_coordinator'>{t('company_consultation_coordinator')}</Option>
                                    {/* Add other roles options */}
                                </Select>
                                {errors && JSON.stringify(Object.keys(errors)[0]) === JSON.stringify(keys[3]) && (
                                    <span style={onErrorText}>{errors[Object.keys(errors)[0]]}</span>
                                )}
                                <Button
                                    style={{ background: OrangeColor, color: WhiteColor, marginTop: '20px' }}
                                    onClick={handleSubmit}
                                >
                                    {isLoading ? (
                                        <span
                                            style={{
                                                position: 'absolute',
                                                top: '35%',
                                                left: '20%',
                                                transform: 'translate(-50%, -50%)'
                                            }}
                                        >
                                            <LoadingBar />
                                        </span>
                                    ) : (
                                        t('Submit')
                                    )}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};

export default UpdateCoordinator;
