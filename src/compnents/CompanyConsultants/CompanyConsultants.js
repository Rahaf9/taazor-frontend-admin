import React, { useEffect } from "react";
import { Col, Row, notification } from "antd"
import DetailedCard from "../../General/DetailedCard/DetailedCard";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor } from "../../General/Styles/colors";
import CustomFloatButton from "../../General/CustomFloatButton/CustomFloatButton";
import { useNavigate } from 'react-router-dom'
import { ROUTES } from "../../General/Routes/Route";
import { useDispatch, useSelector } from 'react-redux'
import { companyConsultantsRefreshPage, companyConsultantsAction } from "../../actions/CompanyConsultantsAction";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import { UserOutlined, InfoCircleOutlined, InfoCircleFilled } from '@ant-design/icons';
import { useTranslation } from "react-i18next";
import { horizVerFlex } from "../../General/Styles/general styles";
import Pagination from "../../General/Reusable Componenets/Pagination/Pagination";

const CompanyConsultants = () => {

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { t,i18n } = useTranslation();
    const consultants = useSelector((state) => state.companyConsultionsReducer.data)
    const errors = useSelector((state) => state.companyConsultionsReducer.errors)
    const isLoading = useSelector((state) => state.companyConsultionsReducer.isLoading)

    useEffect(() => {
        //here to get the consultions 
        dispatch(companyConsultantsAction(1))

    }, []);

    useEffect(() => {
        if (errors) {
            console.log(`the errors inside your consultants ${JSON.stringify(errors.message)}`)
            errors && errors.message && notification.error({
                message: 'Error',
                description: errors.message,
            })
            errors.message && dispatch(companyConsultantsRefreshPage())
        }
    }, [errors.message]);

    const onDetailsClick = (index) => {
        navigate(`${ROUTES.CompanyConsulations.path}/${consultants.data[index].id}`)
    }

    return (
        <>
            {
                isLoading ? <Spinner /> : <div>
                    <CustomHeader title={t('showConsultantsApplied')}
                        icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
                    <div style={{ width: '80vw', height: '100%', overflowY: 'auto', scrollbarWidth: 'none' }} className="mt-3">
                        <Row className='d-flex justify-content-around' style={{ width: '100%', height: '100%' }} gutter={16}>
                            {consultants && consultants.data?.length > 0 ?
                                consultants.data.map((card, index) => (
                                    <Col style={{ margin: '10px' }} key={index} xs={20} sm={16} md={11} lg={10} xl={9}>
                                        <DetailedCard
                                            description={
                                                <span>
                                                    <InfoCircleOutlined className="mx-2" style={{ fontSize: '1.1rem' }} /> {card.topic}
                                                </span>
                                            }
                                            title={
                                                <span>
                                                    <UserOutlined className="mx-2" style={{ fontSize: '1.1rem' }} /> {card.company_name}
                                                </span>
                                            }
                                            request_date={'2024-06-18'}
                                            onDetailsClick={() => onDetailsClick(index)}
                                            action={<InfoCircleFilled key="edit" onClick={
                                                () => navigate(`${ROUTES.AcceptedCompaniesProfile.path}/${card.company_id}`)
                                            } />}
                                        />
                                    </Col>
                                )) : <div className='flexible text-danger fw-bold mt-5'>
                                    {t('noConsultantsApplied')}
                                </div>
                            }
                        </Row>
                        <div className={`${horizVerFlex} m-5`}>
                            <Pagination onPageChange={(page) => {dispatch(companyConsultantsAction(page))}} currentPage={consultants.current_page} totalPages={consultants.last_page ?? '0'} />
                        </div>
                    </div>
                </div>
            }
        </>
    )

}

export default CompanyConsultants;