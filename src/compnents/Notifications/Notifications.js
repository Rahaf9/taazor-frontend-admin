import React, { useEffect, useState } from 'react';
import { Card, notification, Button } from 'antd';
import { NotificationOutlined, DeleteOutlined } from '@ant-design/icons';
import './Notifications.css';
import { useDispatch, useSelector } from 'react-redux';
import { allNotiReadAction, notificationsAction, notificationsRefreshPage, deleteNotiReadAction, deleteAllNotificationsAction, notificationsDeleteRefreshPage, notificationsDeleteAllRefreshPage } from '../../actions/NotificationAction';
import { useTranslation } from 'react-i18next';
import Spinner from '../../General/Reusable Componenets/Spinner/Spinner';
import LoadingBar from '../../General/Reusable Componenets/LoadingBar/LoadingBar';

export const Notifications = () => {
    const { t, i18n } = useTranslation();
    const elements = useSelector(state => state.notificationReducer.data);
    const isLoading = useSelector(state => state.notificationReducer.isLoading);
    const isDeleteNotiLoading = useSelector(state => state.notificationReducer.isDeleteNotiLoading);
    const isDeleteAllNotiLoading = useSelector(state => state.notificationReducer.isDeleteAllNotiLoading);
    const errors = useSelector(state => state.notificationReducer.errors);
    const deleteDone = useSelector(state => state.notificationReducer.deleteDone);
    const deleteAllDone = useSelector(state => state.notificationReducer.deleteAllDone);
    const dispatch = useDispatch();

    const [isReadSent, setIsReadSent] = useState(false);
    const [deletingNotiId, setDeletingNotiId] = useState(null); // Track the ID of the notification being deleted

    useEffect(() => {
        dispatch(notificationsAction());
    }, []);

    useEffect(() => {
        if (!isReadSent && elements) {
            dispatch(allNotiReadAction());
            setIsReadSent(true);
        }
    }, [elements]);

    useEffect(() => {
        if (deleteDone) {
            dispatch(notificationsAction());
            dispatch(notificationsDeleteRefreshPage())
        }
    }, [deleteDone]);

    useEffect(() => {
        if (deleteAllDone) {
            dispatch(notificationsAction());
            dispatch(notificationsDeleteAllRefreshPage())
        }
    }, [deleteAllDone]);

    useEffect(() => {
        if (errors && errors.message) {
            notification.error({
                message: t('Error'),
                description: errors.message,
            });
        }
        dispatch(notificationsRefreshPage());
    }, [errors.message]);


    const handleClick = (url) => {
        window.open(url);
    };

    const handleDelete = (id) => {
        setDeletingNotiId(id); // Set the ID of the notification being deleted
        dispatch(deleteNotiReadAction(id));
    };

    const handleDeleteAll = () => {
        dispatch(deleteAllNotificationsAction());
    };

    return (
        isLoading ? <Spinner /> :
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexWrap: 'wrap',
                    alignItems: 'center',
                    minHeight: '100vh',
                }}
            >
                {/* Left half - Column of Cards */}
                <div
                    style={{
                        flex: '50%',
                        padding: '20px',
                        overflowY: 'auto',
                        maxHeight: 'calc(100vh - 20px)',
                        display: 'flex',
                        flexDirection: 'column',
                        scrollbarWidth: 'none',
                        gap: '20px',
                    }}
                >
                    {/* Button to delete all notifications */}
                    {
                        elements.length > 0 && <Button
                            type="primary"
                            danger
                            onClick={handleDeleteAll}
                            loading={isDeleteAllNotiLoading}
                            style={{ marginBottom: '20px' }}
                        >
                            {t('Delete All Notifications')}
                        </Button>
                    }
                    {/* Loop through the notifications and render a Card for each */}
                    {
                        elements && elements.length > 0 ? elements.map((notification) => (
                            <Card
                                key={notification.id}
                                onClick={() => handleClick(notification.data.url)}
                                style={{
                                    marginBottom: '20px',
                                    border: '1px solid #F3AD44',
                                    margin: '5%',
                                    backgroundColor: notification.seen ? '#f0f0f0' : '#d9d9d9',
                                    position: 'relative'  // Ensure the delete button is positioned correctly
                                }}
                            >
                                <NotificationOutlined style={{ marginRight: '10px', color: '#F3AD44' }} />
                                <h3>{notification.data.messages?.en.title}</h3>
                                <p>{notification.data.messages?.en.description}</p>
                                <Button
                                    type="text"
                                    icon={isDeleteNotiLoading && deletingNotiId === notification.id ? <LoadingBar /> : <DeleteOutlined />}
                                    onClick={(e) => {
                                        e.stopPropagation();  // Prevent card click from firing
                                        handleDelete(notification.id);
                                    }}
                                    style={{
                                        position: 'absolute',
                                        top: '10px',
                                        right: '10px',
                                        color: '#FF4D4F'
                                    }}
                                />
                            </Card>
                        )) : <div className='d-flex justify-content-center text-danger fw-bold mt-5'>
                            {t('no_items_found')}
                        </div>
                    }
                </div>

                {/* Right half - Image */}
                <div className="notification-image-container">
                    <img
                        src={'/images/the light green 2/Push notifications-amico.png'}
                        alt="Notification"
                        style={{ maxWidth: '100%' }}
                    />
                </div>

            </div>
    );
};

export default Notifications;
