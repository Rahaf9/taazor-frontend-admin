// CourseUsersComponent.js
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { courseUsersAction } from '../../actions/CourseUsersAction';  // Assuming you have an action creator for fetching course user cards
import UserInterface from '../../General/UserInterface/UserInterface';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor } from '../../General/Styles/colors';
import { ROUTES } from '../../General/Routes/Route';
import { usersAction } from '../../actions/UsersAction';
import { useTranslation } from 'react-i18next';
import { UserCardButtonStyle } from '../../General/Styles/texts';
import { Button, notification } from 'antd';
import ConfirmationModal from '../../General/Reusable Componenets/ConfirmationModal/ConfirmationModal';
import { blcokUserAction, unblockUserAction } from '../../General/ServerSideApis/ActionsRequestsCreator';
import { allAdminActionRefreshPage } from '../../actions/AllAdminActions';
import { useParams } from 'react-router-dom';

const Users = () => {
  const UserCards = useSelector(state => state.usersSeekerReducer.data);
  const isLoading = useSelector(state => state.usersSeekerReducer.isLoading);
  const errors = useSelector(state => state.allActionsAdminReducer.errors);
  const done = useSelector(state => state.allActionsAdminReducer.done);
  const routePath = ROUTES.UserDetails.path;
  const { t,i18n } = useTranslation()
  const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl'
  const dispatch = useDispatch();

  useEffect(()=>{
    console.log(`the user cards in users seekers is ${JSON.stringify(UserCards)}`)
  },[UserCards])

  useEffect(() => {
    errors && errors.message && notification.error({
        message: t('Error'),
        description: errors.message
    })
    dispatch(allAdminActionRefreshPage())
}, [errors.message]);

useEffect(() => {
  if (done) {
    notification.success({
      message: t('Success'),
    })
    dispatch(allAdminActionRefreshPage())
  }
}, [done])

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [currentFunction, setCurrentFunction] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(null);
  const { index } = useParams();

  const handleOk = () => {
    console.log(`handleOk Implemented`);
    if (currentFunction && currentIndex !== null) {
      currentFunction(currentIndex);
      console.log(`Function executed for index ${currentIndex}`);
    }
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleBlock = (index) => {
    console.log(`The user you want to block is at index ${index}`);
    dispatch(blcokUserAction(index))
    dispatch(usersAction())
  };

  const additionalButtons = [
    <Button style={UserCardButtonStyle}>Block</Button>,
  ];

  const additionalFunctions = [
    handleBlock,
  ];

  const openModal = (buttonIndex, cardIndex) => {
    console.log(`openModal implemented with buttonIndex ${buttonIndex} and cardIndex ${cardIndex}`);
    setCurrentFunction(() => additionalFunctions[buttonIndex]);
    setCurrentIndex(cardIndex);
    setIsModalVisible(true);
  };

  return (
    <div>
      <CustomHeader title={t('show_applicants')} 
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />}/>
      <UserInterface userCards={UserCards} fetchUserCards={usersAction} routePath={routePath} isLoading={isLoading} index={index}
      additionalButtons={additionalButtons} openModal={openModal}/>
      <ConfirmationModal
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        modalContent={t('Are you sure you want to perform this action?')}
      />
    </div>
  );
};

export default Users;
