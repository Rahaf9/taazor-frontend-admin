import React, { useState } from "react";
import OnBoardingInterface from "../General/Reusable Componenets/OnBoard/OnBoardInterface";
import { GrayColor, OrangeColor } from "../General/Styles/colors";

const WelcomeComponent = () => {
    const [colors, setColors] = useState([OrangeColor, GrayColor, GrayColor, GrayColor]);
    const [currentDivIndex, setCurrentDivIndex] = useState(0);

    // Function to handle click event and update colors array
    const handleDivClick = (index) => {
        const newColors = colors.map((color, i) => {
            return i === index ? OrangeColor : GrayColor;
        });
        setColors(newColors);
        setCurrentDivIndex(index);
    };

    // Function to handle "Next" button click
    const handleNextClick = () => {
        // Check if the current div index is not the last index
        if (currentDivIndex < colors.length - 1) {
            // Invoke handleDivClick for the next div index
            handleDivClick(currentDivIndex + 1);
            // Update the current div index
            setCurrentDivIndex(currentDivIndex + 1);
        } else {
            // Print message when the last div is reached
            console.log("This is the last div");
        }
    };

    return (
        <div>
            {/* Passing down colors, handleDivClick, and handleNextClick functions as props */}
            <OnBoardingInterface colors={colors} handleDivClick={handleDivClick} handleNextClick={handleNextClick} />
        </div>
    );
}

export default WelcomeComponent;
