import React, { useEffect } from "react";
import IconsCardReusable from "../../General/IconsCardReusable/IconsCardReusable";
import { InfoCircleOutlined, ContainerOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { Col, Row, notification } from "antd";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { useNavigate } from 'react-router-dom';
import { ROUTES } from "../../General/Routes/Route";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { rejectedCompaniesAction, rejectedCompaniesRefreshPage } from "../../actions/RejectedCompaniesAction";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import { horizVerFlex } from "../../General/Styles/general styles";
import Pagination from "../../General/Reusable Componenets/Pagination/Pagination";

const RejectedCompanies = () => {

    // these elements will be the data for every single job opp and its data to get its title and print it in 
    // the card
    const elements = useSelector(state => state.rejectedCompaniesReducer.data);
    const isLoading = useSelector(state => state.rejectedCompaniesReducer.isLoading);
    const errors = useSelector(state => state.rejectedCompaniesReducer.errors);
    const { t, i18n } = useTranslation()
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const cardData = [
        {
            icon: () => <InfoCircleOutlined style={{ fontSize: '2.2rem', color: lightGreenPrimary }} />,
            description: t('details')
        },
        // Add more items as needed
    ];

    useEffect(() => {
        dispatch(rejectedCompaniesAction(1))
    }, []);

    useEffect(() => {
        errors && errors.message && notification.error({
            message: t('Error'),
            description: errors.message
        })
        dispatch(rejectedCompaniesRefreshPage())
    }, [errors.message]);

    const handleDetailsIconClick = (index) => {
        console.log("Details Current Course Icon clicked at index:", index);
        // Do whatever you need with the index
        navigate(`${ROUTES.RejectedCompaniesProfile.path}/${index}`)
    };

    //   elements will represent the jobs number and must be passed to the Card to display the name of the job

    return (
        <>
            {
                isLoading ? <Spinner /> : <div>
                    <div style={{ height: '10vh', width: '100%' }}>
                        <CustomHeader title={'Rejected Companies'}
                            icon={<CloseCircleOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
                    </div>
                    <div style={{ maxHeight: '90vh', overflowY: 'auto', scrollbarWidth: 'none', msOverflowStyle: 'none' }}>

                        <Row className='d-flex justify-content-around' style={{ width: '80vw' }} gutter={16}>
                            {
                                elements && elements.data?.length > 0 ?
                                    elements.data.map((element, index) => (
                                        <Col style={{ margin: '10px' }} key={index} xs={18} sm={16} md={12} lg={8} xl={8}>
                                            {/* here must the title be element.name */}
                                            <IconsCardReusable title={element.name} myIndex={element.id} data={cardData} onDetailsIconClick={handleDetailsIconClick} />
                                        </Col>
                                    )) : <div className='flexible text-danger fw-bold mt-5'>
                                        No Items Found
                                    </div>
                            }
                        </Row>
                        <div className={`${horizVerFlex} m-5`}>
                            <Pagination onPageChange={(page) => { dispatch(rejectedCompaniesAction(page)) }} currentPage={elements.current_page} totalPages={elements.last_page ?? '0'} />
                        </div>
                    </div>
                </div>
            }
        </>
    );

}

export default RejectedCompanies;