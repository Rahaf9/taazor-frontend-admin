import React, { useEffect } from "react";
import IconsCardReusable from "../../General/IconsCardReusable/IconsCardReusable";
import { InfoCircleOutlined, EyeOutlined, PlusCircleFilled } from '@ant-design/icons';
import { WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { Col, Row, notification } from "antd";
import CustomHeader from "../../General/Reusable Componenets/Headers/Header";
import { useNavigate } from 'react-router-dom';
import { ROUTES } from "../../General/Routes/Route";
import { useDispatch, useSelector } from "react-redux";
import { CurrentCourseAction, CurrentCourseRefreshPage } from "../../actions/CurrentCoursesAction";
import { useTranslation } from "react-i18next";
import CustomFloatButton from "../../General/CustomFloatButton/CustomFloatButton";
import { horizVerFlex } from "../../General/Styles/general styles";
import Pagination from "../../General/Reusable Componenets/Pagination/Pagination";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";

const CurrentCourse = () => {

    // these elements will be the data for every single job opp and its data to get its title and print it in 
    // the card
    const { t } = useTranslation();
    const disptach = useDispatch();
    const elements = useSelector(state => state.currentCoursesReducer.data);
    const isLoading = useSelector(state => state.currentCoursesReducer.isLoading);
    const errors = useSelector(state => state.currentCoursesReducer.errors);
    const navigate = useNavigate();

    const cardData = [
        {
            icon: () => <InfoCircleOutlined style={{ fontSize: '2.2rem', color: lightGreenPrimary }} />,
            description: t('details')
        },
        {
            icon: () => <EyeOutlined style={{ fontSize: '2.2rem', color: lightGreenPrimary }} />,
            description: t('show')
        },
        // Add more items as needed
    ];

    useEffect(() => {
        disptach(CurrentCourseAction())
    }, []);

    useEffect(() => {
        errors && errors.message && notification.error({
            message: 'Error',
            description: errors.message
        })
        disptach(CurrentCourseRefreshPage())
    }, [errors.message]);

    const handleShowIconClick = (index) => {
        console.log("Show Icon clicked at index:", index);
        // Do whatever you need with the index
        navigate(`${ROUTES.CourseUsers.path}/${index}`)
    };

    const handleDetailsIconClick = (index) => {
        console.log("Details Current Course Icon clicked at index:", index);
        // Do whatever you need with the index
        navigate(`${ROUTES.CurrentCourseDetails.path}/${index}`)
    };

    //   elements will represent the jobs number and must be passed to the Card to display the name of the job

    return (
        <div>
            {
                isLoading ? <Spinner /> :
                    <div>
                        <div style={{ width: '100%' }}>
                            <CustomHeader title={t('current_course_opportunities')}
                                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
                        </div>
                        <div style={{ maxHeight: '90vh', overflowY: 'auto', scrollbarWidth: 'none', msOverflowStyle: 'none' }}>

                            <Row className='d-flex justify-content-around' style={{ width: '80vw' }} gutter={16}>
                                {elements && elements.data?.length > 0 ?
                                    elements.data.map((element, index) => (
                                        <Col style={{ margin: '10px' }} key={index} xs={18} sm={16} md={12} lg={8} xl={8}>
                                            {/* here must the title be element.name */}
                                            <IconsCardReusable title={<div className="my-2">
                                                <div>
                                                    {element.company_name}
                                                </div>
                                                <div>
                                                    {element.name}
                                                </div>
                                            </div>} myIndex={element.id} data={cardData} onDetailsIconClick={handleDetailsIconClick} onShowIconClick={handleShowIconClick} />
                                        </Col>
                                    )) : <div className='flexible text-danger fw-bold mt-5'>
                                        {t('no_items_found')}
                                    </div>
                                }
                            </Row>
                            <div className={`${horizVerFlex} m-5`}>
                                <Pagination onPageChange={(page) =>  disptach(CurrentCourseAction(page)) } currentPage={elements.current_page} totalPages={elements.last_page ?? '0'} />
                            </div>
                        </div>
                    </div>
            }
        </div>
    );

}

export default CurrentCourse;