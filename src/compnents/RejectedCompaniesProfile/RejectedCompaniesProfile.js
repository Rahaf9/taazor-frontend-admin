import React, { useEffect, useState } from "react";
import './RejectedCompaniesProfile.css'
import { profileData } from "../../FakeData";
import { EnvironmentOutlined, CheckCircleOutlined, PlusOutlined, IdcardOutlined, MailOutlined, PhoneOutlined, PrinterOutlined, DownloadOutlined, DeleteOutlined, FileDoneOutlined } from '@ant-design/icons';
import { Col, Row, notification } from 'antd';
import { horizAroundAlignCenter, horizEndAlignCenter, horizLeftVerFlex, horizStartAlignCenter, horizVerFlex, horizVerFlexColumn } from "../../General/Styles/general styles";
import { profileMain2Style, profileRedStyle, profileSec2Style, profileSecStyle } from "../../General/Styles/texts";
import ImageSelector from "../../General/Reusable Componenets/ImageSelector/ImageSelector";
import { BlackColor, GrayOpacity, RedColor, WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import IconsCardReusable from "../../General/IconsCardReusable/IconsCardReusable";
import { useDispatch, useSelector } from "react-redux";
// import useCheckCompanyStatus from "../../General/CustomHooks/useCheckCompanyStatus ";
import { CompanyProfileAction } from "../../General/ServerSideApis/CompaniesProfileCreators";
import { companiesProfileActionRefreshPage } from "../../actions/CompaniesProfileAction";
import { useNavigate, useParams } from "react-router-dom";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import { useTranslation } from "react-i18next";
import { downloadCompanieFilesAction, downloadCompanieFilesRefreshPage } from "../../actions/DownloadCompanyFileAction";
import { deleteCompanyAccountAction } from "../../General/ServerSideApis/ActionsRequestsCreator";
import { allAdminActionRefreshPage } from "../../actions/AllAdminActions";
import { ROUTES } from "../../General/Routes/Route";
import LoadingBar from "../../General/Reusable Componenets/LoadingBar/LoadingBar";
import ConfirmationModal from "../../General/Reusable Componenets/ConfirmationModal/ConfirmationModal";
import WaitingComponent from "../WaitingComponent/WaitingComponent";

const RejectCompaniesProfile = () => {

    const truncateString = (str) => {
        return str.length > 10 ? str.substring(0, 12) : str;
    };

    const [currentImage, setCurrentImage] = useState(profileData.data.logo);
    const dispatch = useDispatch();
    const data = useSelector(state => state.companiesProfileAdminReducer.data);
    const errors = useSelector(state => state.companiesProfileAdminReducer.errors);
    const isLoading = useSelector(state => state.companiesProfileAdminReducer.isLoading);
    const done = useSelector(state => state.companiesProfileAdminReducer.done);
    //
    const downloadErrors = useSelector(state => state.downloadCompanieFileReducer.errors);
    const downloadDone = useSelector(state => state.downloadCompanieFileReducer.done);
    const downloadIsLoading = useSelector(state => state.downloadCompanieFileReducer.isLoading);
    const { t, i18n } = useTranslation();
    //
    const deleteAccountErrors = useSelector(state => state.allActionsAdminReducer.errors);
    const deleteAccountDone = useSelector(state => state.allActionsAdminReducer.done);
    const deleteAccountIsLoading = useSelector(state => state.allActionsAdminReducer.isLoading);

    const [isModalVisible, setIsModalVisible] = useState(false);
    const navigate = useNavigate();
    const roleName = localStorage.getItem('roleName');
    const  direction  = i18n.dir(); // الحصول على اتجاه النص

    // useCheckCompanyStatus();
    const { index } = useParams();

    const cardData = [
        {
            icon: () => <FileDoneOutlined style={{ fontSize: '2rem', color: lightGreenPrimary }} />,
            description: t("Download")
        }
    ];

    useEffect(() => {
        dispatch(CompanyProfileAction(index))
    }, []);

    useEffect(() => {
        console.log(`the errors inside register ${JSON.stringify(errors.message)}`);
        errors && errors.message && notification.error({
            message: 'Error',
            description: errors.message,
        });
        dispatch(companiesProfileActionRefreshPage());
    }, [errors.message]);

    //
    useEffect(() => {
        console.log(`the errors inside accepted companies details ${JSON.stringify(errors.message)}`);
        downloadErrors && downloadErrors.message && notification.error({
            message: 'Error',
            description: downloadErrors.message,
        });
        dispatch(downloadCompanieFilesRefreshPage());
    }, [downloadErrors.message]);

    useEffect(() => {
        if (downloadDone) {
            notification.success({
                message: t('download_success'),
                description: t('download_success'),
            });
            dispatch(downloadCompanieFilesRefreshPage());
        }
    }, [downloadDone])

    const handleDownload = () => {
        dispatch(downloadCompanieFilesAction(index))
    }
    //

    useEffect(() => {
        console.log(`the errors inside profilecompanies details ${JSON.stringify(errors.message)}`);
        deleteAccountErrors && deleteAccountErrors.message && notification.error({
            message: 'Error',
            description: deleteAccountErrors.message,
        });
        dispatch(allAdminActionRefreshPage());
    }, [deleteAccountErrors.message]);

    useEffect(() => {
        if (deleteAccountDone) {
            notification.success({
                message: t('delete_success'),
                description: t('delete_success'),
            });
            dispatch(allAdminActionRefreshPage());
            navigate(-1)
        }
    }, [deleteAccountDone])

    const handleDeleteAccount = () => {
        dispatch(deleteCompanyAccountAction(index))
        setIsModalVisible(false);
    }

    const handleCancel = () => {
        setIsModalVisible(false);
    }

    const handleShowModal = () => {
        setIsModalVisible(true);
    }

    const profileMainStyle = {
        fontSize: '1.2rem',
        fontWeight: 'bold',
        textAlign:'left',
        color: `${BlackColor}`,
        width:'60%',
        direction:direction
    
    };

    // Function to map through the types array and render each type with an Ok icon
    const renderTypes = () => {
        return data.type?.map((type, index) => (
            <div className={`${horizLeftVerFlex}`}>
                <CheckCircleOutlined style={{ marginLeft: '10px', fontSize: '1.1rem' }} />
                <div className="mx-2" style={profileSec2Style}>{type}</div>
            </div>
        ));
    };

    // Function to map through the documents array and render each document with an download icon
    const renderDocs = () => {
        return data.documents?.map((type, index) => (
            <Col span={8} key={index} style={{ marginRight: '4px' }}>
                <IconsCardReusable title={truncateString(type)} myIndex={index} data={cardData} />
            </Col>
        ))
    };

    return (
        <>
            {
                isLoading ? <Spinner /> : !done ? <WaitingComponent title={'errorOccuered'} /> :
                    <div className={`profileContainer`}>
                        <div className={`leftProfileSection`}>
                            <ImageSelector initialImage={data.logo ?? currentImage} icon={<PlusOutlined style={{ fontSize: '12px' }} />} />
                            <div className={`${horizAroundAlignCenter} mt-5`} style={profileMainStyle}>
                                <div className={` w-50 ${horizLeftVerFlex}`}>
                                    {t('company_topic')}
                                </div>
                                <div className={`w-50 ${horizLeftVerFlex}`}>
                                    <IdcardOutlined style={{ marginLeft: '20px', fontSize: '1.5rem' }} />
                                    <div className="mx-2" style={profileSec2Style}>{data.topic}</div>
                                </div>
                            </div>
                            <div className={`${horizLeftVerFlex} mt-4`} style={profileMainStyle}>
                                <div className={` w-50 ${horizLeftVerFlex}`}>
                                {t('company_type')}
                                </div>
                                <div className={`w-50 ${horizAroundAlignCenter}`}>
                                    {renderTypes()}
                                </div>
                            </div>
                            <div className={`${horizAroundAlignCenter} mt-4`} style={profileMainStyle}>
                                <div className={` w-50 ${horizLeftVerFlex}`}>
                                {t('company_docs')}
                                </div>
                                <div className={` w-50 ${horizEndAlignCenter}`} style={{ cursor: 'pointer' }}>
                                    {
                                        downloadIsLoading ? <span style={{ fontSize: '1rem', fontWeight: 'normal', color: RedColor }}>downloading...</span> :
                                            <DownloadOutlined onClick={handleDownload} style={{ fontSize: '1.5rem', color: BlackColor }} />
                                    }
                                </div>
                            </div>
                            <div className={`mt-3 ${horizAroundAlignCenter}`} style={{ width: '60%', maxHeight: '300px' }}>
                                <Row style={{ flexWrap: 'nowrap', width: '100%', maxHeight: '300px', overflowX: 'auto', scrollbarWidth: 'none' }}
                                    className={`${horizLeftVerFlex}`}>
                                    {renderDocs()}
                                </Row>
                            </div>
                        </div>
                        <div className="rightProfileSection">
                            <div className="w-100 h-50 flex2CenterColumn" style={{ backgroundColor: lightGreenPrimary }}>
                                <div className={`w-50 flexAlignTopRow`} style={profileMain2Style}>
                                {data.name}
                                </div>
                                <div className={`w-100 mb-4 ${horizVerFlexColumn} `}>
                                    <div className="w-50 mt-4 flexAlignCenterRow ">
                                        <MailOutlined className="d-flex justify-content-center"
                                            style={{ marginLeft: '20px', fontSize: '1.5rem', width: '100%', color: WhiteColor }} />
                                        <div className="mx-2" style={profileSecStyle}>{data.email}</div>
                                    </div>
                                    <div className="w-50 flexAlignCenterRow">
                                        <PhoneOutlined className="d-flex justify-content-center"
                                            style={{ marginLeft: '20px', fontSize: '1.5rem', width: '100%', color: WhiteColor }} />
                                        <div className="mx-2" style={profileSecStyle}>{data.phone}</div>
                                    </div>
                                    <div className="w-50 flexAlignCenterRow">
                                        <PrinterOutlined className="d-flex justify-content-center"
                                            style={{ marginLeft: '20px', fontSize: '1.5rem', width: '100%', color: WhiteColor }} />
                                        <div className="mx-2" style={profileSecStyle}>{data.fax ? data.fax : 'no fax'}</div>
                                    </div>
                                </div>
                            </div>
                            <div className={`bottomRight`}>
                                <div className="h-100 w-100">
                                    <div className={`h-25 w-100 ${horizStartAlignCenter} ${direction === 'rtl' ? 'px-5' : ''}`} style={profileMainStyle}>
                                    {t('location')}
                                    </div>
                                    <div className={`h-25 w-75 my-3 ${horizAroundAlignCenter}`} style={{ background: GrayOpacity, borderRadius: '20px' }}>
                                        <div className={`${horizVerFlex} w-25`}>
                                            <EnvironmentOutlined
                                                style={{ marginLeft: '20px', fontSize: '2em', width: '100%', height: '100%', color: lightGreenPrimary }} />
                                        </div>
                                        <div className={`${horizVerFlex} w-75`}>
                                            {data.location}
                                        </div>
                                    </div>
                                    <div className={`h-25 w-75 my-3 ${horizAroundAlignCenter}`} style={{ background: GrayOpacity, borderRadius: '20px' }}>
                                        <div className={`${horizVerFlex} w-25`}>
                                            <EnvironmentOutlined
                                                style={{ marginLeft: '20px', fontSize: '2em', width: '100%', height: '100%', color: lightGreenPrimary }} />
                                        </div>
                                        <div className={`${horizVerFlex} w-75`}>
                                            {t('show_location_map')}
                                        </div>
                                    </div>
                                </div>
                                {roleName && roleName.includes('admin') &&
                                    <div className={`h-25 w-100 ${horizAroundAlignCenter}`} style={{direction:direction}}>
                                        <div className={`h-100 w-100 ${direction === 'rtl' ? horizEndAlignCenter : horizStartAlignCenter}`} style={profileRedStyle}>
                                            {t('delete_account')}
                                        </div>
                                        <div onClick={handleShowModal} className={`h-100 w-100 ${horizVerFlex}`} style={profileRedStyle}>
                                            {
                                                deleteAccountIsLoading ? <LoadingBar /> : <DeleteOutlined style={{ fontSize: '1.5rem', cursor: 'pointer' }} />
                                            }
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                        <ConfirmationModal
                            visible={isModalVisible}
                            onOk={handleDeleteAccount}
                            onCancel={handleCancel}
                            modalContent={t('Are you sure you want to perform this action?')}
                        />
                    </div>
            }
        </>
    );
}

export default RejectCompaniesProfile;
