import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Button, notification } from 'antd';
import { CheckCircleOutlined } from '@ant-design/icons';
import { BlackColor, GrayOpacity, OrangeColor, WhiteColor, lightGreenPrimary } from "../../General/Styles/colors";
import { horizAroundAlignCenterColumn, horizVerFlex } from "../../General/Styles/general styles";
import Spinner from "../../General/Reusable Componenets/Spinner/Spinner";
import './UserConsultantDetails.css';
import { userConsultantDetailsAction, userConsultantDetailsRefreshPage } from "../../actions/UserConsultantDetailsAction";
import AdvisorSelectingModal from "../../General/Reusable Componenets/AdvisorSelectingModal/AdvisorSelectingModal";
import { useTranslation } from "react-i18next";
import { userReferAction, userReferFailure, userReferRefreshPage } from "../../actions/UserReferAction";
import { advisorsAction, allAdvisorsAction } from "../../actions/AdvisorsAction";
import WaitingComponent from "../WaitingComponent/WaitingComponent";

const UserConsultantDetails = () => {
    const { index } = useParams();
    const dispatch = useDispatch();
    const errors = useSelector((state) => state.userConsultionsDetailsReducer.errors);
    const isLoading = useSelector((state) => state.userConsultionsDetailsReducer.isLoading);
    const done = useSelector((state) => state.userConsultionsDetailsReducer.done);
    const messages1 = useSelector((state) => state.userConsultionsDetailsReducer.data);
    const advisors = useSelector((state) => state.adivsorsReducer.data);
    const advisorsLoading = useSelector((state) => state.adivsorsReducer.isLoading);
    const isReferLoading = useSelector((state) => state.userReferReducer.isLoading);
    const referErrors = useSelector((state) => state.userReferReducer.errors);
    const referDone = useSelector((state) => state.userReferReducer.done);
    const { t } = useTranslation();
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleOk = (advisorId) => {
        if (advisorId) {
            // index that represent the consultion id 
            dispatch(userReferAction(advisorId, index))
        }else{
            dispatch(userReferFailure({'message':t('select_advisor')}))
        }
        console.log(`the id of the advisor i got from the modal is ${advisorId}`)
        setIsModalVisible(false);
    };

    useEffect(() => {
        dispatch(userConsultantDetailsAction(index));
        dispatch(allAdvisorsAction());
    }, []);

    useEffect(() => {
        console.log(`the data consultations is ${JSON.stringify(messages1)}`);
    }, [messages1]);

    useEffect(() => {
        if (errors) {
            console.log(`the errors inside detailed consultants ${JSON.stringify(errors.message)}`);
            errors && errors.message && notification.error({
                message: 'Error',
                description: errors.message,
            });
            dispatch(userConsultantDetailsRefreshPage());
        }
    }, [errors.message]);

    return (
        <div>
            {isLoading ? <Spinner /> : !done ? <WaitingComponent title={t('errorOccuered')}/> : (
                <div style={{ width: '100vw' }} className={`${horizVerFlex}`}>
                    <div style={{ width: '25vw', height: '100vh', backgroundColor: lightGreenPrimary }} className={`${horizAroundAlignCenterColumn}`}>
                        <div className={`h-50 ${horizVerFlex}`}>
                            <img src="/images/the light green 2/People search-amico.png" style={{ width: '80%', height: '65%' }} alt="Consultant" />
                        </div>
                        <div className={`h-50 ${horizVerFlex}`}>
                            <div className="w-100 h-100 d-flex justify-content-center flex-column">
                                <div>
                                    <CheckCircleOutlined style={{ fontSize: '13px', marginRight: '10px', color: WhiteColor }} />
                                    <span className="text-white">{messages1?.user_name}</span>
                                </div>
                                <div>
                                    <CheckCircleOutlined style={{ fontSize: '13px', marginRight: '10px', color: WhiteColor }} />
                                    <span className="text-white">{messages1?.topic}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style={{ width: '75vw', height: '100vh', backgroundColor: GrayOpacity }}>
                        <div className={`${horizVerFlex}`} style={{ height: '100%', width: '100%', color: BlackColor }}>
                            <div className="w-100 h-100 d-flex align-items-center mx-2" style={{
                                overflowY: 'auto', display: 'flex', flexDirection: 'column-reverse', scrollbarWidth: 'none'
                            }}>
                                {messages1 ? (
                                    <React.Fragment key={messages1.id}>
                                        <div className="w-100 d-flex justify-content-start m-3 flex-column" style={{ marginBottom: '10px' }}>
                                            <div style={{ marginBottom: '15px' }}>
                                                <strong>Request Date:</strong> {messages1.request_date}
                                            </div>
                                            <div style={{ marginBottom: '15px' }}>
                                                <strong>User Email:</strong> {messages1.user_email}
                                            </div>
                                            <div className="w-75 text-center p-2" style={{
                                                backgroundColor: WhiteColor, borderRadius: '20px',
                                                border: `2px solid ${lightGreenPrimary}`
                                            }}>
                                                {messages1.user_message}
                                            </div>
                                        </div>
                                        {
                                            messages1.advisor_reply && <div className="w-100 d-flex justify-content-end m-3">
                                                <div className="w-75 text-center p-2" style={{ backgroundColor: lightGreenPrimary, borderRadius: '20px' }}>
                                                    {messages1.advisor_reply }
                                                </div>
                                            </div>
                                        }
                                        <Button
                                            key="ok"
                                            type="primary"
                                            onClick={showModal}
                                            style={{ background: OrangeColor, color: WhiteColor }}
                                        >
                                            {t('referUser')}
                                        </Button>
                                    </React.Fragment>
                                ) : <div>There is no data here</div>}
                            </div>
                        </div>
                    </div>
                    <AdvisorSelectingModal
                        visible={isModalVisible}
                        onCancel={handleCancel}
                        onOk={handleOk}
                        advisors={advisors}
                        isLoading={advisorsLoading}
                        errors={referErrors}
                        isReferLoading={isReferLoading}
                        done={referDone}
                        refreshErrors={() => dispatch(userReferRefreshPage())}
                        topic={messages1.topic}
                    />
                </div>
            )}
        </div>
    );
};

export default UserConsultantDetails;
