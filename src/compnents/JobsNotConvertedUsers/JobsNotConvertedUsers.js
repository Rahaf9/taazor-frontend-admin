import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { courseUsersAction } from '../../actions/CourseUsersAction';  // Assuming you have an action creator for fetching course user cards
import UserInterface from '../../General/UserInterface/UserInterface';
import CustomHeader from '../../General/Reusable Componenets/Headers/Header';
import { EyeOutlined } from '@ant-design/icons';
import { WhiteColor } from '../../General/Styles/colors';
import { ROUTES } from '../../General/Routes/Route';
import { usersAction } from '../../actions/UsersAction';
import { useTranslation } from 'react-i18next';
import { UserCardButtonStyle } from '../../General/Styles/texts';
import { Button, Select, notification } from 'antd';
import ConfirmationModal from '../../General/Reusable Componenets/ConfirmationModal/ConfirmationModal';
import { blcokUserAction, convertAllJobUserAction, convertJobUserAction, unblockUserAction } from '../../General/ServerSideApis/ActionsRequestsCreator';
import { allAdminActionRefreshPage } from '../../actions/AllAdminActions';
import { useLocation, useParams } from 'react-router-dom';
import { JobUserFailure, JobUserLoading, JobUserRefreshPage, jobUsersAction } from '../../actions/JobUsersAction';
import { horizFlex } from '../../General/Styles/general styles';

const JobsNotConvertedUsers = () => {
    const UserCards = useSelector(state => state.jobUsersReducer.data || []);
    const isLoading = useSelector(state => state.jobUsersReducer.isLoading);
    const errors = useSelector(state => state.allActionsAdminReducer.errors);
    const done = useSelector(state => state.allActionsAdminReducer.done);
    const routePath = ROUTES.JobUserDetails.path;
    const { t, i18n } = useTranslation();
    const direction = i18n.dir() === 'ar' ? 'ltr' : 'rtl';
    const dispatch = useDispatch();
    const { Option } = Select;
    const location = useLocation();
    const { jobID } = location?.state ?? '';

    useEffect(()=>{
        if(!jobID){
            dispatch(JobUserFailure("Must Choose Job before"))
        }
    },[])

    useEffect(() => {
        console.log(`the user cards in users seekers is ${JSON.stringify(UserCards)} and the jobId us ${jobID}`);
        dispatch(allAdminActionRefreshPage())
    }, [UserCards]);

    useEffect(() => {
        if (errors && errors.message) {
            notification.error({
                message: t('Error'),
                description: errors.message
            });
            dispatch(allAdminActionRefreshPage());
        }
    }, [errors.message]);

    useEffect(() => {
        if (done) {
            notification.success({
                message: t('Success'),
            });
            dispatch(allAdminActionRefreshPage());
            dispatch(jobUsersAction(index, 1));
        }
    }, [done]);

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [currentFunction, setCurrentFunction] = useState(null);
    const [currentIndex, setCurrentIndex] = useState(null);
    const [priorityValue, setPriorityValue] = useState(null);
    const { index } = useParams();

    const handleOk = () => {
        console.log(`handleOk Implemented and the priorty value is ${priorityValue}`);
        if (currentFunction && currentIndex !== null) {
            currentFunction(currentIndex, priorityValue);
            console.log(`Function executed for index ${currentIndex}`);
        }
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleConverted = (index, priorityValue) => {
        console.log(`The user you want to convert is at index ${index}`);
        dispatch(convertJobUserAction(index, priorityValue))
        // dispatch(usersAction())
    };

    const handleConvertAll = () => {
        console.log(`The user you want to convert is at index ${jobID}`);
        dispatch(convertAllJobUserAction(jobID))
    };

    const additionalButtons = [
        <Button style={UserCardButtonStyle}>Convert</Button>,
    ];

    const additionalFunctions = [
        handleConverted,
    ];

    const openModal = (buttonIndex, cardIndex) => {
        console.log(`openModal implemented with buttonIndex ${buttonIndex} and cardIndex ${cardIndex}`);
        setCurrentFunction(() => additionalFunctions[buttonIndex]);
        setCurrentIndex(cardIndex);
        setIsModalVisible(true);
    };

    return (
        <div style={{ direction: direction, width: '100%' }}>
            <CustomHeader title={t('show_applicants')}
                icon={<EyeOutlined style={{ fontSize: '1.5rem', color: WhiteColor }} />} />
            <UserInterface userCards={UserCards} fetchUserCards={jobUsersAction} routePath={routePath} isLoading={isLoading} index={index}
                additionalButtons={additionalButtons} openModal={openModal} />
            <div className={` ${horizFlex}`}>
                <Button onClick={handleConvertAll} style={UserCardButtonStyle}>ConvertAll</Button>
            </div>
            <ConfirmationModal
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                modalContent={
                    Array.isArray(UserCards.data) && (
                        <Select className={`mb-3 mt-3 select-placeholder`} placeholder={t('priorityTitle')}
                            style={{ width: '100%', textAlign: `${direction === 'ltr' ? 'left' : 'right'} ` }} onChange={(value) => setPriorityValue(value)}>
                            <Option key={'0'} value={'0'}>{'0'}</Option>
                            {UserCards.data.map((option, index) => (
                                <Option key={index + 1} value={index + 1}>{index + 1}</Option>
                            ))}
                        </Select>
                    )
                }
            />
        </div>
    );
};

export default JobsNotConvertedUsers;
