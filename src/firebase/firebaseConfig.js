// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getMessaging } from "firebase/messaging";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export const firebaseConfig = {
  apiKey: "AIzaSyBhl9D0_4MUBL9sZuij5GOyY5Z8onP3sbA",
  authDomain: "tazzur-7cda6.firebaseapp.com",
  projectId: "tazzur-7cda6",
  storageBucket: "tazzur-7cda6.appspot.com",
  messagingSenderId: "614672964979",
  appId: "1:614672964979:web:96d0d961898e5c45ff7906",
  measurementId: "G-M9RFJC4S0G"
};
// export const firebaseConfig = {
//   apiKey: "AIzaSyBbT3VmUuoZ6oXqpFutZ5-jZf9MB1csAOQ",
//   authDomain: "adventur-ads-8a4c7.firebaseapp.com",
//   databaseURL: "https://adventur-ads-8a4c7-default-rtdb.firebaseio.com",
//   projectId: "adventur-ads-8a4c7",
//   storageBucket: "adventur-ads-8a4c7.appspot.com",
//   messagingSenderId: "926822042192",
//   appId: "1:926822042192:web:738a5c4190952b3be09f55",
//   measurementId: "G-6K0J80F6W4"
// };


// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Messaging service
export const messaging = getMessaging(app);
