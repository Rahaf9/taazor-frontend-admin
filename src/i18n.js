// i18n.js
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import translations from './translations';

const resources = translations

i18n
 .use(initReactI18next)
 .init({
    resources,
    lng: localStorage.getItem('lang') ? localStorage.getItem('lang') : 'ar', // Set the initial language here
    fallbackLng:  localStorage.getItem('lang') ? localStorage.getItem('lang') : 'ar',
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
