
// importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
// importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js");

// firebase.initializeApp({
//   apiKey: "AIzaSyBhl9D0_4MUBL9sZuij5GOyY5Z8onP3sbA",
//   authDomain: "tazzur-7cda6.firebaseapp.com",
//   projectId: "tazzur-7cda6",
//   storageBucket: "tazzur-7cda6.appspot.com",
//   messagingSenderId: "614672964979",
//   appId: "1:614672964979:web:96d0d961898e5c45ff7906",
//   measurementId: "G-M9RFJC4S0G"
// });

// // firebase.initializeApp({
// //   apiKey: "AIzaSyBbT3VmUuoZ6oXqpFutZ5-jZf9MB1csAOQ",
// //   authDomain: "adventur-ads-8a4c7.firebaseapp.com",
// //   databaseURL: "https://adventur-ads-8a4c7-default-rtdb.firebaseio.com",
// //   projectId: "adventur-ads-8a4c7",
// //   storageBucket: "adventur-ads-8a4c7.appspot.com",
// //   messagingSenderId: "926822042192",
// //   appId: "1:926822042192:web:738a5c4190952b3be09f55",
// //   measurementId: "G-6K0J80F6W4"
// // });

// // This utility function reads cookies from within the Service Worker
// // function getCookie(name) {
// //   const cookies = self.document.cookie.split(';');
// //   for (const cookie of cookies) {
// //     const [key, value] = cookie.trim().split('=');
// //     if (key === name) {
// //       return value;
// //     }
// //   }
// //   return null;
// // }

// const messaging = firebase.messaging();
// let url = '/'

// messaging.onBackgroundMessage((payload) => {
//   // Log the received payload for debugging purposes
//   console.log('[firebase-messaging-sw.js] Received background message ', payload);

//   // Parse the extra_data JSON string
//   let extraData = JSON.parse(payload.data.extra_data);

//   // Attempt to get the language from a cookie
//   // const appLanguage = getCookie('appLang') || 'ar'; // Default to 'ar' if not set
//   // console.log('the lang', getCookie('appLang'), `the response of the lang`,extraData[appLanguage]);

//   // Select the appropriate title and description based on the app's language
//   const notificationTitle = extraData['en'].title;
//   const notificationDescription = extraData['en'].description;

//   // Prepare notification options
//   const notificationOptions = {
//     body: notificationDescription,
//     data: {
//       url: payload.data.url // Include the URL in the notification's data
//     },
//   };

//   console.log('Attempting to show notification:', notificationTitle, notificationOptions);

//   // Show the notification
//   self.registration.showNotification(notificationTitle, notificationOptions).then(() => {
//     console.log('Notification shown successfully');
//     // url = extraData[i18n.language].url
//   }).catch((error) => {
//     console.error('Error showing notification:', error);
//   });
// });


// // Handle notification click event
// self.addEventListener('notificationclick', (event) => {
//   console.log('Notification click received:', JSON.stringify(event.notification));
//   event.notification.close();

//   // Extract the URL from the notification data
//   const urlToOpen = event.notification.data.url || '/';

//   event.waitUntil(
//     clients.openWindow(urlToOpen)
//   );
// });

// // self.addEventListener('install', (event) => {
// //   console.log('Service Worker installing.');
// // });

// // self.addEventListener('activate', (event) => {
// //   console.log('Service Worker activating.');
// // });

// // self.addEventListener('push', function(event) {
// //   console.log('Push event received.');
// //   const title = 'Test Notification';
// //   const options = {
// //     body: 'This is a test notification.',
// //     // icon: '/default-icon.png'
// //   };

// //   event.waitUntil(
// //     self.registration.showNotification(title, options).then(() => {
// //       console.log('Test Notification shown successfully');
// //     }).catch((error) => {
// //       console.error('Error showing Test Notification:', error);
// //     })
// //   );
// // });
